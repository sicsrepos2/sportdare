//
//  StaticValues.swift
//  Sportdare
//
//  Created by Binoy T on 16/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import UIKit
let kBaseUrl = "http://www.sicsglobal.com/App_projects/sportsdare/api/v1/"
let imageCache = NSCache<AnyObject, AnyObject>()
let sharedSecret = "bbaa575100b04194b527bc7979f70cbe"
enum AppKeys : String
{
    case UserDetail = "UserDetails"
    case IsFirstLogin = "isFirstLogin"
    case DefaultLists = "DefaultLists"
    case Privacy = "privacy"
    
}

enum ProfileLevel:Int{
    case novice1 = 0 ,novice = 1 ,intermediate = 2 ,advanced = 3 ,expert = 4 ,legend = 5
    var name : String {
        switch self {
        case .novice1:
            return "PENDING"
        case .novice:
            return "NOVICE"
        case .intermediate:
            return "INTERMEDIATE"
        case .advanced:
            return "ADVANCED"
        case .expert:
            return "EXPERT"
        case .legend:
            return "LEGEND"

        }
    }
    
}

enum AccountType:String{
    case standard = "Standard"
    case platinum = "Elite Platinum"
    case diamond = "Elite Diamond"
    var image:UIImage{
        switch self {
        case .standard:
            return UIImage(named: "diamond_green")!
        case .platinum:
            return UIImage(named: "diamond_grey")!
        case .diamond:
            return UIImage(named: "diamond_blue")!
            
        }
    }
}

class FeedType
{
    static let athleteAwards = "athlete_awards"
    static let athleteLevel = "athlete_level"
    static let athlete_PB = "athlete_personal_best"
    static let postMedia = "post_media"
    static let post_text = "post_text"
    static let profileUpdate = "user_profile_update"
    static let dareEvent = "dare_event"
    static let dareTraining = "dare_training"
    static let dareChallenge = "dare"
    static let teamCreated = "team_create"
    static let teamMember = "team_member"
    static let root = "root"
}

enum InAppServices:String
{
   case diamond = "com.sportdare.diamond"
   case platinum = "com.sportdare.platinum"
   case gambits =  "com.sportdare.100Gambits"
    var name : String {
        switch self {
        case .diamond:
            return "DIAMOND"
        case .platinum:
            return "PLATINUM"
        case .gambits:
            return "100 Gambits"
        }
    }
    var id : String {
        switch self {
        case .diamond:
            return "3"
        case .platinum:
            return "2"
        case .gambits:
            return "4"
        }
    }
}



class Service
{
    static let kLogin = "auth/login"
    static let kRegister = "auth/register"
    static let kSocialLogin = "auth/social_login"
    static let kProfileImage = "profile_upload"
    static let kEditProfile = "user/profile_update"
    static let kForgotPassword =  "auth/forgot_password"
    static let kUpgradeSubscription = "subscription/new_plan"
    static let kPostFeed =  "posts/add"
    static let kDareChallenge =  "dare/challenge"
    static let kDareTraining = "dare/training"
    static let kDareEvent = "dare/event"
    static let kViewPlayerProfile = "user/view"
    static let kRootUser = "roots/newroot"
    static let kPathProfile = "picture"
    static let kFile = "file"
    static let kAddTeam = "team/add"
    static let kAddBusiness = "business/add"
    static let kViewBusiness = "business/view_all"
    static let kAddChat = "misc/add_chat"
    static let kRemovePost = "posts/remove"
    static let kGetNotifications = "user/notifications"

    //    MARK: Dare
    static let kGetDares = "dare/calender"
    static let kAcceptDare = "dare/challenge_accept"
    static let kDeclineDare = "dare/challenge_decline"
    static let kCompleteDare = "dare/challenge_reply"
    static let kFailedDare = "dare/challenge_failed"
    static let kApproveDareresult = "dare/challenge_complete"
    static let kDareComplete = "dare/challenge_reply"
    static let kViewReply = "dare/view_replys"
    static let kResetChallenge = "dare/reset_challenge"
    static let kBlockCHallenge = "dare/block_advantage "
   
    //    MARK:-Cards And Badges
   static let kGetAvailableCards = "perks/cards"
    static let kGetMyCards = "perks/my_cards"
    static let kGetBadges = "perks/badges_list"
     static let kUserBadges = "perks/user_badges"
    static let kBuyNewCard = "perks/buy_cards"
    static let kBuyGambits = "perks/buy_gambits"
    static let kGetRoots = "roots"
    //    MARK:- Add Roles
    static let kAddSportCategory = "athlete/add_main_sports_category"
    static let kAddSubSport = "athlete/add_sub_sports_category"
    static let kViewSportCategory = "athlete/view_cats"
    static let kAddLevel = "athlete/add_level"
    static let kAddPB = "athlete/add_personal_best"
    static let kAddAward = "athlete/add_athlete_awards"
    static let kAddMedia = "athlete/add_media"
    static let kViewLevels = "athlete/view_level"
    static let kViewPB = "athlete/view_personal_best"
    static let kViewAwards = "athlete/view_athlete_awards"
    static let kViewMedia = "athlete/view_media"
    static let kRemoveMedia = "athlete/remove_media"
    static let kPostComment = "feed/comment"
    static let kHighFiveFeed = "feed/activity_clap"
    static let kHighFive_Wall = "feed/clap"
    static let kVerifyLevel = "athlete/verify_level"
}
struct CollectionViewCellId {
    static let imageCell = "imageCell"
    static let videoCell = "videoCell"
    static let showMoreCell = "moreCell"
}
struct ChattViewCellId {
    static let commentTextCell = "CommentText"
    static let commentImageCell = "CommentImage"
    static let showMoreCell = "moreCell"
     static let postCell = "postCell"
    static let cellYou = "CellYou"
    static let cellMe = "CellMe"

}
struct FeedCellId {
    static let StatusCell = "statusCell"
    static let MediaCell = "mediaCell"
    static let UpdatesCell = "updateCell"
    static let postCell = "postCell"
    static let rootCell = "rootCell"
    
}

struct PostType{
    static let text = "text"
    static let media = "media"
    
}
struct PostMediaType{
    static let image = "image"
    static let video = "video"
    
}
struct Constants{
    
    static let privacyList:[String:UIImage] = ["Public":#imageLiteral(resourceName: "public"),"Fans":UIImage(named: "team")!,"Teams": UIImage(named: "root")!,"Private":#imageLiteral(resourceName: "onlyme")]
    static var sportCategories:[Sports_category]!
    static var challengeTypes : [Challenge_types]!
    static var dareTypes :[Other_dare_types]!
    static var privacyLevels :[Privacy_levels]!
    static var athlete_levels : [Athlete_levels]?
    static var business_Types : [Business_types]?
    static var deviceTokenString = ""
    static var referralId: String = "0"
   
    static let kGoogleApiKey = "AIzaSyBfCc-2bMadlggAkkNiCDa33PJFllID30Y"
    static let sportDareUrl:URL = URL(string:"http://www.sportdare.com/")!
    static func setDefaults(response:Misscellaneous_Base)
    {
        
        privacyLevels = response.privacy_levels
        sportCategories = response.sports_category?.sorted(by: {$0.name!.localizedCaseInsensitiveCompare($1.name!) == ComparisonResult.orderedAscending})
        challengeTypes  = response.challenge_types
        dareTypes = response.other_dare_types!.sorted(by: {$0.name!.localizedCaseInsensitiveCompare($1.name!) == ComparisonResult.orderedAscending})
        athlete_levels = response.athlete_levels
        business_Types = response.business_types
    }
}
struct RGBA32: Equatable {
    private var color: UInt32
    
    var redComponent: UInt8 {
        return UInt8((self.color >> 24) & 255)
    }
    
    var greenComponent: UInt8 {
        return UInt8((self.color >> 16) & 255)
    }
    
    var blueComponent: UInt8 {
        return UInt8((self.color >> 8) & 255)
    }
    
    var alphaComponent: UInt8 {
        return UInt8((self.color >> 0) & 255)
    }
    
    init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) {
        self.color = (UInt32(red) << 24) | (UInt32(green) << 16) | (UInt32(blue) << 8) | (UInt32(alpha) << 0)
    }
    
    init(color: UIColor) {
        let components = color.cgColor.components ?? [0.0, 0.0, 0.0, 1.0]
        let colors = components.map { UInt8($0 * 255) }
        self.init(red: colors[0], green: colors[1], blue: colors[2], alpha: colors[3])
    }
    
    static let bitmapInfo = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
    
    static func ==(lhs: RGBA32, rhs: RGBA32) -> Bool {
        return lhs.color == rhs.color
    }
    
}


