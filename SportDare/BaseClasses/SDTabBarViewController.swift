//
//  SDTabBarViewController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import TransitionableTab
class SDTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
//   self.navigationController?.isNavigationBarHidden = true
        let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: (tabBar.frame.width + 4) / numberOfItems, height: tabBar.frame.height)

        for (index,item) in (self.tabBar.items?.enumerated())!
        {
            if index == 1
            {
                item.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
            }
            else
            {
                item.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -8, right: 0)
            }
        }

        // remove default border
        
    }
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
        self.tabBar.itemSpacing = UIScreen.main.bounds.width / 3
        tabBar.frame.size.width = self.view.frame.width + 4
        tabBar.frame.size.height = UIScreen.main.bounds.height  * 0.09
        tabBar.frame.origin.x = -2
        for (index,item) in (self.tabBar.items?.enumerated())!
        {
            if index == 1
            {
                item.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
            }
            else
            {
                item.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -8, right: 0)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
extension SDTabBarViewController: TransitionableTab {
    
    func transitionDuration() -> CFTimeInterval {
        return 0.4
    }
    
    func transitionTimingFunction() -> CAMediaTimingFunction {
        return CAMediaTimingFunction.easeInOut
    }
    
    func fromTransitionAnimation(layer: CALayer?, direction: Direction) -> CAAnimation {
//        switch type {
//        case .move: return DefineAnimation.move(.from, direction: direction)
//        case .scale: return DefineAnimation.scale(.from)
//        case .fade: return DefineAnimation.fade(.from)
//        case .custom:
//            let animation = CABasicAnimation(keyPath: "transform.translation.y")
//            animation.fromValue = 0
//            animation.toValue = -(layer?.frame.height ?? 0)
//            return animation
//        }
         return DefineAnimation.move(.from, direction: direction)
    }
    
    func toTransitionAnimation(layer: CALayer?, direction: Direction) -> CAAnimation {

         return DefineAnimation.move(.to, direction: direction)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return animateTransition(tabBarController, shouldSelect: viewController)
    }
}

