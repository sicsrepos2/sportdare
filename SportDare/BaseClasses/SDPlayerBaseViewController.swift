//
//  SDPlayerBaseViewController.swift
//  Sportdare
//
//  Created by SICS on 21/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage
class SDPlayerBaseViewController: UIViewController,UITextFieldDelegate {
    @IBInspectable var showBackButton : Bool = false
        {
        didSet{
        }
    }
    @IBInspectable var showProfile : Bool = false
        {
        didSet{
        }
    }
    var playerImageView :CustomImageView!
    var playerNamelabel:UILabel!
    var playerDetail :PlayerDetails!{
        didSet{
            if showProfile
            {
    
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if showBackButton
        {
            let backbutton = UIBarButtonItem(image: #imageLiteral(resourceName: "main_back_btn_white").withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.backAction))
            self.navigationItem.setLeftBarButton(backbutton, animated: true)
        }
        if showProfile
        {
        
                let navView = UIView()
                
            
                // Create the image view
                
                playerImageView =  CustomImageView()
                playerImageView!.frame =  CGRect(x: 0, y: 0, width: 40, height: 40)
                playerImageView!.contentMode = UIView.ContentMode.scaleAspectFill
                playerImageView.cornerRadius = 20
                playerImageView.clipsToBounds = true
                playerNamelabel = UILabel()
                playerNamelabel.frame = CGRect.init(x: 50, y: 8, width: 150, height: 20)
                playerNamelabel.textColor = UIColor.white
                playerNamelabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)

            playerImageView.sd_setImage(with: playerDetail.image?.url, placeholderImage:  UIImage(named: "dumy"), options: .continueInBackground, completed: nil)

            playerNamelabel.text = playerDetail.name! + " " + playerDetail.surName!
            playerNamelabel.frame = CGRect.init(x: 50, y: 8, width: 150, height: 20)
//            let image =  playerImageView.shieldfImageView.image!.changeColor()
//            playerImageView.shieldfImageView.image = image
                navView.addSubview(playerImageView!)
                navView.addSubview(playerNamelabel)
                navView.frame = CGRect(x: 0, y: 0, width: 300, height: 44)
                // Set the navigation bar's navigation item's titleView to the navView
                navView.sizeToFit()
                self.navigationItem.titleView = navView
            }
        
        // Set the navView's frame to fit within the titleView
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        definesPresentationContext = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        definesPresentationContext = false
    }
    @objc func backAction()
    {
        definesPresentationContext = false
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @objc func settingsAction()
    {
        let settingsVC = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SDSettingsTableViewController") as! SDSettingsTableViewController
        definesPresentationContext = false
        self.navigationController?.pushViewController(settingsVC, animated: true)
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
