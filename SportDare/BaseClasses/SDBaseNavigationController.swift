//
//  SDBaseNavigationController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import TLPhotoPicker
class SDBaseNavigationController: UINavigationController {
    @IBInspectable var showBackButton : Bool = false
        {
        didSet{
            
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationBar.barTintColor = UIColor(hexString: "#90C341")
 
   
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func backAction()
     {
        
        
    }

    

}

class BaseViewController: UIViewController,UISearchControllerDelegate,UITextFieldDelegate,SerachResultControllerDelegate {
    @IBInspectable var showBackButton : Bool = false
        {
        didSet{
        }
    }
    
    @IBInspectable var showSearch : Bool = false
        {
        didSet{
            
        }
    }
        var resultsTableViewController: SDSearchResultTableViewController?
        var searchController : UISearchController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = true
        if showBackButton
        {
            let backbutton = UIBarButtonItem(image: #imageLiteral(resourceName: "main_back_btn_white").withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.backAction))
            
            self.navigationItem.setLeftBarButton(backbutton, animated: true)
            if showSearch
            {
                let btn1 = UIButton(type: .custom)
                btn1.setImage(#imageLiteral(resourceName: "settins_icon_white"), for: .normal)
                btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                btn1.addTarget(self, action: #selector(settingsAction), for: .touchUpInside)
                let item1 = UIBarButtonItem(customView: btn1)
                navigationItem.rightBarButtonItem = item1
                resultsTableViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SDSearchResultTableViewController") as? SDSearchResultTableViewController
                resultsTableViewController?.delegate = self
                configureSearchController()
            }
        }
        else
        {
            let btn2 = UIButton(type: .custom)
            btn2.frame = CGRect(x: 0, y: 0, width: 38, height: 55)
            btn2.setImage(#imageLiteral(resourceName: "logo_icon_top_white"), for: .normal)
            btn2.imageEdgeInsets =  UIEdgeInsets(top: -8, left: 0, bottom: 5, right: 0)
            btn2.addTarget(self, action: #selector(goToProfile), for: .touchUpInside)
            let subView = UIView(frame: CGRect(x: 0, y: 0, width: 38, height: 55))
            subView.addSubview(btn2)
            let item2 = UIBarButtonItem(customView: subView)
            //            navigationItem.leftBarButtonItem = item2
            self.navigationItem.setLeftBarButton(item2, animated: true)
            
            let btn1 = UIButton(type: .custom)
            btn1.setImage(#imageLiteral(resourceName: "settins_icon_white"), for: .normal)
            btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            btn1.addTarget(self, action: #selector(settingsAction), for: .touchUpInside)
            let item1 = UIBarButtonItem(customView: btn1)
            navigationItem.rightBarButtonItem = item1
            resultsTableViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SDSearchResultTableViewController") as? SDSearchResultTableViewController
            resultsTableViewController?.delegate = self
            configureSearchController()
            
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
        definesPresentationContext = true
        if searchController != nil
        {
          searchController.hidesNavigationBarDuringPresentation = false
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        definesPresentationContext = false
        navigationController?.view.setNeedsLayout() // force update layout
        navigationController?.view.layoutIfNeeded() // to
    }
   
    func searchResults(_ controller: SDSearchResultTableViewController, didSelectUser userDetail: SearchUserDetail) {
        let playerIB = UIStoryboard(name: "Player", bundle: nil)
        let playerProfile = playerIB.instantiateViewController(withIdentifier: "SDPlayer_ProfileViewController") as? SDPlayer_ProfileViewController
        playerProfile?.playerId = userDetail.user_id
        playerProfile?.basicDetail = userDetail
        self.navigationController?.pushViewController(playerProfile!, animated: true)
    }
    func configureSearchController() {
        searchController = CustomSearchController(searchResultsController: resultsTableViewController)
        searchController.delegate = self
        searchController.searchResultsUpdater = resultsTableViewController
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        searchController.loadViewIfNeeded()
        searchController.searchBar.delegate = resultsTableViewController
        searchController.searchBar.searchBarStyle = .default
        searchController.searchBar.setShowsCancelButton(false, animated: false)
        searchController.searchBar.keyboardAppearance = .default
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.barTintColor = navigationController?.navigationBar.barTintColor
        searchController.searchBar.tintColor = self.view.tintColor
        searchController.isActive = true
        navigationItem.titleView = searchController.searchBar
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
      resultsTableViewController!.searchBarCancelButtonClicked(searchController.searchBar)
        return true
    }
    @objc func backAction()
    {
         definesPresentationContext = false
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func goToProfile()
    {
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "SDMyProfileViewController") as! SDMyProfileViewController
         definesPresentationContext = false
        self.hideTabBar()
        self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    @objc func settingsAction()
    {
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SDSettingsTableViewController") as! SDSettingsTableViewController
         definesPresentationContext = false
        self.hideTabBar()
        self.navigationController?.pushViewController(settingsVC, animated: true)
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func willPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.showsCancelButton = false
    }
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.showsCancelButton = false
    }
}


//MARK:- Base TableViewController
protocol NavigationActions {
    func navigationDidselectBack()
}

class BaseTableViewController: UITableViewController ,UISearchControllerDelegate,UITextFieldDelegate,SerachResultControllerDelegate{
    @IBInspectable var showBackButton : Bool = false
        {
        didSet{

        }
    }
    
    @IBInspectable var showSearch : Bool = false
        {
        didSet{
            
        }
    }
   
    
    var resultsTableViewController: SDSearchResultTableViewController?
    var searchController : UISearchController!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.extendedLayoutIncludesOpaqueBars = true
        if showBackButton
        {
            let backbutton = UIBarButtonItem(image: #imageLiteral(resourceName: "main_back_btn_white").withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.backAction))
            self.navigationItem.setLeftBarButton(backbutton, animated: true)
            if showSearch
            {
                let btn1 = UIButton(type: .custom)
                btn1.setImage(#imageLiteral(resourceName: "settins_icon_white"), for: .normal)
                btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                btn1.addTarget(self, action: #selector(settingsAction), for: .touchUpInside)
                let item1 = UIBarButtonItem(customView: btn1)
                navigationItem.rightBarButtonItem = item1
                resultsTableViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SDSearchResultTableViewController") as? SDSearchResultTableViewController
                resultsTableViewController?.delegate = self
                configureSearchController()
                
            }
        }
        else
        {
            let btn2 = UIButton(type: .custom)
            btn2.setImage(#imageLiteral(resourceName: "logo_icon_top_white"), for: .normal)
            btn2.frame = CGRect(x: 0, y: 0, width: 38, height: 55)
            btn2.imageEdgeInsets =  UIEdgeInsets(top: -8, left: 0, bottom: 5, right: 0)
            btn2.addTarget(self, action: #selector(goToProfile), for: .touchUpInside)
            let subView = UIView(frame: CGRect(x: 0, y: 0, width: 38, height: 55))
            subView.addSubview(btn2)
           
            let item2 = UIBarButtonItem(customView: subView)
            self.navigationItem.setLeftBarButton(item2, animated: true)
            
            let btn1 = UIButton(type: .custom)
            btn1.setImage(#imageLiteral(resourceName: "settins_icon_white"), for: .normal)
            btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            btn1.addTarget(self, action: #selector(settingsAction), for: .touchUpInside)
            let item1 = UIBarButtonItem(customView: btn1)
           navigationItem.rightBarButtonItem = item1
            resultsTableViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SDSearchResultTableViewController") as? SDSearchResultTableViewController
            resultsTableViewController?.delegate = self
            configureSearchController()
           
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        definesPresentationContext = true
        if searchController != nil
        {
            searchController.hidesNavigationBarDuringPresentation = false
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        definesPresentationContext = false
        navigationController?.view.setNeedsLayout() // force update layout
        navigationController?.view.layoutIfNeeded() // to

    }
    
    func searchResults(_ controller: SDSearchResultTableViewController, didSelectUser userDetail: SearchUserDetail) {
        let playerIB = UIStoryboard(name: "Player", bundle: nil)
        let playerProfile = playerIB.instantiateViewController(withIdentifier: "SDPlayer_ProfileViewController") as? SDPlayer_ProfileViewController
      
        playerProfile?.playerId = userDetail.user_id
       
        self.navigationController?.pushViewController(playerProfile!, animated: true)
    }
    func configureSearchController() {
      
        searchController = CustomSearchController(searchResultsController: resultsTableViewController)
        searchController.delegate = self
        searchController.searchResultsUpdater = resultsTableViewController
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        searchController.loadViewIfNeeded()
        searchController.searchBar.delegate = resultsTableViewController
        searchController.searchBar.searchBarStyle = .default
        searchController.searchBar.setShowsCancelButton(false, animated: false)
        searchController.searchBar.keyboardAppearance = .default
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.barTintColor = navigationController?.navigationBar.barTintColor
        searchController.searchBar.tintColor = self.view.tintColor
        searchController.isActive = false
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.titleView = searchController.searchBar
      
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        resultsTableViewController!.searchBarCancelButtonClicked(searchController.searchBar)
        return true
    }
    
    func willPresentSearchController(_ searchController: UISearchController) {
       searchController.searchBar.showsCancelButton = false
    }
    func didPresentSearchController(_ searchController: UISearchController) {
       searchController.searchBar.showsCancelButton = false
    }

    
    
    @objc func backAction()
    {
     definesPresentationContext = false
        self.navigationController?.popViewController(animated: true)
    }
    @objc func goToProfile()
    {
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "SDMyProfileViewController") as! SDMyProfileViewController
         definesPresentationContext = false
        self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    @objc func settingsAction()
    {
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SDSettingsTableViewController") as! SDSettingsTableViewController
         definesPresentationContext = false
        self.navigationController?.pushViewController(settingsVC, animated: true)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
  
}

class CustomPhotoPickerViewController: TLPhotosPickerViewController {
    override func makeUI() {
        super.makeUI()
        self.customNavItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .stop, target: nil, action: #selector(customAction))
    }
    @objc func customAction() {
        self.delegate?.photoPickerDidCancel()
        self.dismiss(animated: true) { [weak self] in
            self?.delegate?.dismissComplete()
            self?.dismissCompletion?()
        }
    }
    /*
     override func maxCheck() -> Bool {
     let imageCount = self.selectedAssets.filter{ $0.phAsset?.mediaType == .image }.count
     let videoCount = self.selectedAssets.filter{ $0.phAsset?.mediaType == .video }.count
     if imageCount > 3 || videoCount > 1 {
     return true
     }
     return false
     }*/
}
