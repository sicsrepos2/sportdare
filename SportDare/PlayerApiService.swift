//
//  PlayerApiService.swift
//  Sportdare
//
//  Created by SICS on 21/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import Foundation
class PlayerApiService{
    static let sharedInstance = PlayerApiService()
    static func verifyLevel(levelId:String,completion:@escaping (_ status:Bool,_ result:Any?,_ error:Error?)->Void)
    {
        
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kVerifyLevel, parameter: ["levelid":levelId]) { (status, response, error) in
            if status
            {
                completion(status,response?.value,nil)
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func getPosts(playerId:String,offset:Int,completion:@escaping (_ status:Bool,_ result:[Posts]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "posts/all/10/\(offset)/DESC/\(playerId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Posts_Base.self, from: data!)
                    if responseData.posts != nil
                    {
                        completion(true,responseData.posts,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func getTeams(playerId:String,completion:@escaping (_ status:Bool,_ result:[Team]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "team/\(playerId)"
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Team_Base.self, from: data!)
                    if responseData.status == true
                    {
                        completion(true,responseData.teams,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    //    GetRoles
    static func getRolesCategories(playerId : String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        
        let url = kBaseUrl + "\(Service.kViewSportCategory)/\(playerId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Roles_Athlete_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    
    
    static func getLevelRatingForRole(playerId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewLevels)/\(playerId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Role_Level_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getPersonalBests(roleId:String,playerId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewPB)/\(roleId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(PB_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    
    static func getRoots(playerId:String,completion:@escaping (_ status:Bool,_ result:[Root]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "roots/\(playerId)"
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Roots_Base.self, from: data!)
                    if responseData.status!
                    {
                        completion(true,responseData.roots,nil)
                    }
                    else
                    {
                        completion(false,nil,error)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    static func getAwards(roleId:String,playerId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewAwards)/\(roleId)/\(playerId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Awards_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getMedia(roleId:String,playerId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewMedia)/\(roleId)/\(playerId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Media_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
}
