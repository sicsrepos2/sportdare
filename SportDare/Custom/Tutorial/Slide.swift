import UIKit
protocol TutorialActionsProtocol{
    func didSelectPepsTalk()
    func didSelectDo_It()
}
class Slide: UIView {
    var delegate : TutorialActionsProtocol?
    @IBOutlet weak var imageView: UIImageView!

    @IBAction func pepsTalkACtion(_ sender: UIButton) {
        delegate?.didSelectPepsTalk()
    }
    
    @IBAction func letsDoitAction(_ sender: UIButton) {
        delegate?.didSelectDo_It()
    }
}
