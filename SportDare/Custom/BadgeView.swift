//
//  BadgeView.swift
//  Sportdare
//
//  Created by SICS on 25/06/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage
import iCarousel
class BadgeView: UIView {
    @IBOutlet weak var imageViewBadge: UIImageView!
    var carousel:iCarousel?{
        didSet{
            self.frame = CGRect(origin: CGPoint.zero, size: (carousel?.frame.size)!)
        }
    }
    var surprise:SurpriseBadge?{
        didSet{
            imageViewBadge.sd_setImage(with: surprise?.badge_image?.url, completed: nil)
            
        }
    }
    var challenge:ChallengeBadge?{
        didSet{
            let image = challenge?.is_earned == "1" ? challenge?.badge_image : challenge?.locked_image
            imageViewBadge.sd_setImage(with:image?.url , completed: nil)
        }
    }
    var gambits:GambitsBadge?{
        didSet{
            let image = gambits?.is_earned == "1" ? gambits?.badge_image : gambits?.locked_image
            imageViewBadge.sd_setImage(with:image?.url , completed: nil)
        }
    }

}
