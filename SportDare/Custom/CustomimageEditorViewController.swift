//
//  CustomimageEditorViewController.swift
//  ExoticCars
//
//  Created by Binoy on 30/08/16.
//  Copyright © 2016 Chitra. All rights reserved.
//

import UIKit
protocol ImageEditingDelegate {
    func imageCroping(controller:CustomimageEditorViewController,didFinishPickingImage image : UIImage)
}
class CustomimageEditorViewController: UIViewController,UIScrollViewDelegate{
    var originImage : UIImage!
    var imageView : UIImageView!
    var cropRectSize : CGSize!
    var crop : CGRect!
    var cropDelegate : ImageEditingDelegate!
    @IBOutlet var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.isNavigationBarHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUi()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setScrollViewOffset()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCropView(Image : UIImage,delegate:ImageEditingDelegate?,cropSize:CGSize)
    {
        originImage = Image
        cropRectSize = cropSize
        cropDelegate = delegate
        imageView = UIImageView(image: originImage)
       
        
    }
    
    func setUi()
    {
        
        scrollView.addSubview(imageView)
        scrollView.contentSize = CGSize(width:originImage.size.width,height: originImage.size.height + 64)
        scrollView.isScrollEnabled = true
        scrollView.minimumZoomScale =  CropRect.minimumScaleFromSize(size: originImage.size, toFitTargetSize: CGSize(width:cropRectSize.width, height:cropRectSize.height))
        scrollView.maximumZoomScale =  2 * CropRect.minimumScaleFromSize(size: originImage.size, toFitTargetSize: UIScreen.main.bounds.size)
        scrollView.zoomScale = CropRect.minimumScaleFromSize(size: originImage.size, toFitTargetSize: CGSize(width:cropRectSize.width, height:cropRectSize.height))
//            CropRect.minimumScaleFromSize(size: originImage.size, toFitTargetSize: UIScreen.main.bounds.size)
        
        let overlayPath = UIBezierPath(rect: CGRect(x:0,y: 0,width:  self.view.bounds.width,height:  self.view.bounds.height))
        
        let transparentPath = UIBezierPath(rect: CGRect(x:cropRect().origin.x,y:cropRect().origin.y,width: cropRectSize.width,height: cropRectSize.height))
        
        
        
        overlayPath.append(transparentPath)
        overlayPath.usesEvenOddFillRule = true
        let fillLayer = CAShapeLayer()
        
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = CAShapeLayerFillRule.evenOdd
        fillLayer.fillColor = UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 0.5).cgColor
        
        let croplayer = CAShapeLayer()
        croplayer.strokeColor = UIColor.white.cgColor
        croplayer.lineWidth = 1.0
        croplayer.path = transparentPath.cgPath
        croplayer.fillRule = CAShapeLayerFillRule.evenOdd
        croplayer.fillColor = UIColor.clear.cgColor
        self.view.layer.addSublayer(croplayer)
        self.view.layer.addSublayer(fillLayer)
    }
    @IBAction func cancelAction(sender: UIBarButtonItem) {
        if let navigationVc = self.navigationController
        {
        navigationVc.popViewController(animated: true)
        }
        else
        {
            self.dismiss(animated: true) {
                
            }
        }
    }
    @IBAction func chooseAction(sender: UIBarButtonItem) {
        let croppedImage = CropRect.cropImage(image: originImage, fromScrollView: self.scrollView, withSize: cropRectSize,imgView:imageView)
        cropDelegate.imageCroping(controller: self, didFinishPickingImage: croppedImage)
        if let navigationVc = self.navigationController
        {
            navigationVc.popViewController(animated: true)
        }
        else
        {
            self.dismiss(animated: true) {
                
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
         return imageView
    }
    
    func cropRect() -> CGRect {
        let newX: CGFloat = UIScreen.main.bounds.size.width / 2 - cropRectSize.width / 2
        let newY: CGFloat = UIScreen.main.bounds.size.height / 2 - cropRectSize.height / 2
        let frameRect = CGRect(x:newX,y: newY,width: cropRectSize.width,height: cropRectSize.height)
        return  frameRect
    }
    
    func setScrollViewOffset() {
        _ = UIScreen.main.bounds
        let bottom: CGFloat = scrollView.frame.size.height - self.cropRect().size.height - self.cropRect().origin.y
        let right: CGFloat = scrollView.frame.size.width - self.cropRect().size.width - self.cropRect().origin.x
        let top: CGFloat = self.cropRect().origin.y
        let left: CGFloat = self.cropRect().origin.x
        self.scrollView.contentInset = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }
}
