//
//  File.swift
//  SportDare
//
//  Created by Binoy T on 20/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import UIKit
enum  PlayersCellType : String {
    
    case Team = "cellTeam"
    case Fans = "cellFans"
    case Heroes = "cellHeroes"
}

 enum ShieldType:String {
   
    case greyZero = "0"
    case greyOne = "1"
    case greyTwo = "2"
    case greyThree = "3"
    case greyFour = "4"
    case bronze = "Bronze"
    case silver = "Silver"
    case gold  = "Gold"
    var image: UIImage {
        switch self {
        case .greyZero: return #imageLiteral(resourceName: "shield_0star")
        case .greyOne: return #imageLiteral(resourceName: "shield_1star")
        case .greyTwo: return #imageLiteral(resourceName: "shield_2star")
        case .greyThree: return #imageLiteral(resourceName: "shield_3star")
        case .greyFour: return #imageLiteral(resourceName: "shield_4star")
        case .bronze: return #imageLiteral(resourceName: "shield_bronze")
        case .silver: return #imageLiteral(resourceName: "shield_silver")
        case .gold: return #imageLiteral(resourceName: "shield_gold")
            
        }
    }
    
}
    
extension Notification.Name {
    static let didUpdateSize = Notification.Name(
        rawValue: "didUpdateSize")
}


