//
//  CropRect.swift
//  ExoticCars
//
//  Created by Binoy on 30/08/16.
//  Copyright © 2016 Chitra. All rights reserved.
//

import UIKit

class CropRect: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
     */

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
        self.alpha = 0.8
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    override func draw(_ rect: CGRect) {

                }
    class func minimumScaleFromSize(size: CGSize, toFitTargetSize targetSize: CGSize) -> CGFloat {
        let widthScale: CGFloat = targetSize.width / size.width
        let heightScale: CGFloat = targetSize.height / size.height
        return (widthScale > heightScale) ? widthScale : heightScale
    }
    
    class func cropImage(image: UIImage, fromScrollView scrollView: UIScrollView, withSize size: CGSize, imgView:UIImageView) -> UIImage {
        
        let scaledSize = CGSize(width: image.size.width * scrollView.zoomScale,height: image.size.height * scrollView.zoomScale)
        let scaledImage = self.imageWithImage(image: image, scaledToSize: scaledSize)
        let x: CGFloat = scrollView.contentOffset.x + scrollView.contentInset.left
        let y: CGFloat = scrollView.contentOffset.y + scrollView.contentInset.top
        let cropRect = CGRect(x:x,y: y,width: size.width,height: size.height)
//        scrollView.convert(cropRect, to: imgView)
//        let scaleSize = CGSize(width:image.size.width * scrollView.zoomScale,height: image.size.height * scrollView.zoomScale)
////            CGSizeMake(1024.0, 720.0)
//        var scale:CGFloat = 1.0
//        if(cropRect.size.width > cropRect.size.height) {
//            scale = scaleSize.width / cropRect.size.width
//        }
//        else {
//            scale = scaleSize.height/cropRect.size.height
//        }
//
//        var drawRect = CGRect(x:0,y: 0,width: image.size.width,height: image.size.height)
//        drawRect = drawRect.applying(CGAffineTransform(scaleX: scale, y: scale))
//
//        var rectTransform = CGAffineTransform()
//        var shift = CGPoint(x:cropRect.origin.x,y: cropRect.origin.y);
//        rectTransform = CGAffineTransform.identity
//        shift = CGPoint(x:shift.x,y:image.size.height - shift.y - cropRect.size.height);
//        drawRect = drawRect.applying(rectTransform);
//        drawRect = drawRect.applying(CGAffineTransform(translationX: -shift.x * scale, y: -shift.y * scale))
//
//        let bitmap = CGContext(data: nil,width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: 8, bytesPerRow: Int(scaleSize.width * 4), space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue )
//        let colorSpace:CGColorSpace = CGColorSpaceCreateDeviceRGB()
//        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
//        let context = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
//
//        context?.interpolationQuality = .high
//        context?.draw(image.cgImage!, in: drawRect)
//
//        let newImageRef = context?.makeImage()
//        let img = UIImage(cgImage: newImageRef!, scale: scale, orientation: image.imageOrientation)
        return  self.cropImage(image: scaledImage, withRect: cropRect)

    }
    
    class func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        image.draw(in: CGRect(x:0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
    class func cropImage(image: UIImage, withRect rect: CGRect) -> UIImage {
        let newRect = CGRect(x:rect.origin.x * image.scale,y: rect.origin.y * image.scale,width:rect.width * image.scale,height: rect.height * image.scale)
        let imageRef = image.cgImage?.cropping(to: newRect)
        let resultImage = UIImage(cgImage: imageRef!, scale: image.scale, orientation:  image.imageOrientation)
      
        return resultImage
    }
}
