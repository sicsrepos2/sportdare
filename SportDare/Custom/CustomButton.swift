//
//  CustomButton.swift
//  ThatsMyHairstylist
//
//  Created by SRISHTI INNOVATIVE on 16/09/1937 Saka.
//  Copyright © 1937 Saka SRISHTI INNOVATIVE. All rights reserved.
//


import Foundation
import UIKit
//@IBDesignable
class CustomButton : UIButton
{
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    @IBInspectable var selectedBackgroundColour: UIColor = UIColor.clear{
        didSet {
            self.setBackgroundColor(color: selectedBackgroundColour
                , forState: .selected)
            self.backgroundColor = self.state == .selected ?  selectedBackgroundColour : super.backgroundColor
        }
    }
    
    @IBInspectable var highlightedBackgroundColour: UIColor = UIColor.clear{
        didSet {
            self.setBackgroundColor(color: highlightedBackgroundColour
                , forState:UIControl.State.highlighted)
//            self.backgroundColor = self.state == .highlighted ?  highlightedBackgroundColour : super.backgroundColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0
        {
        didSet
        {
            
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet
        {
            layer.cornerRadius = cornerRadius
        }
    }
  
   
}


extension UIButton {
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
   
}
