//
//  Utilities.swift
//  SportDare
//
//  Created by Binoy T on 17/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import Photos
import SPGooglePlacesAutocomplete
import FirebaseDynamicLinks
class Utilities
{
   
    
   class func showShareSheet(vc:UIViewController)
    {
        let link = URL(string: "http://www.sportdare.com?invitedby=\(UserBase.currentUser!.details!.account_id!)")
        guard   let shareLink = DynamicLinkComponents.init(link: link!, domainURIPrefix: "https://sportdare.page.link") else{
            return
        }
        if let bundleId = Bundle.main.bundleIdentifier
        {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleId)
            shareLink.iOSParameters?.appStoreID = "123456"
            shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.sportdare")
        }
        shareLink.shorten { (shortUrl, options, error) in
            let promoText = "Hey try SportDare"
            let activityVC = UIActivityViewController(activityItems: [promoText,shortUrl!], applicationActivities: nil)
            vc.present(activityVC, animated: true) {
                
            }
        }
    }
    
    
    class var imageDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths.first!.appendingPathComponent("Sportdare")
        return documentsDirectory
    }
    class func getDataFromAsset(asset:PHAsset,Completion:@escaping (_ data:Data)->Void)
    {
        if asset.mediaType == .image
        {
            PHImageManager.default().requestImageData(for: asset, options: nil, resultHandler: { (data, str, orientation, info) in
                Completion(data!)
            })
        }
        else if asset.mediaType == .video
        {
            
            PHCachingImageManager().requestExportSession(forVideo: asset, options: nil, exportPreset: AVAssetExportPresetLowQuality) { (session, info) in
                
            }
           
        }
        
    }
    class func getNumberOfColoumns(itemCount:Int)->Int
    {
        switch itemCount {
        case 1:
            return 1
        case 2,3,4,5:
            return 2
            
        default:
            return 2
        }
    }
    class func getNumberOfRows(itemCount:Int)->Int
    {
        switch itemCount {
        case 1,2:
            return 1
        case 3,4,5:
            return 2
            
        default:
            return 2
        }
    }
    class func getTimeStamp(date:String?)->String
    {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let postDate = dateFormatter.date(from: date ?? "") else{
            return "0 seconds ago"
        }
        
        if (postDate.isInSameDay(date: now))
        {
            let interval = now.timeIntervalSince(postDate)
            if interval < 60.0
            {
                return "\(Int(interval)) seconds ago"
            }
            else if interval < 3600.0
            {
                let minute = Int(floor(interval/60))
                if minute == 1
                {
                    return "\(minute) minute ago"
                }
                
                return "\(minute) minutes ago"
            }
            else
            {
                let hour = Int(interval / 3600.0)
                if hour == 1
                {
                    return "\(Int(hour)) hour ago"
                }
                return "\(Int(hour)) hours ago"
            }
        }
        else if (postDate.isInSameWeek(date: now))
            
        {
            dateFormatter.dateFormat = "hh:mm a"
            dateFormatter.timeZone = TimeZone.current
            let time = dateFormatter.string(from: postDate)
            if  Calendar.current.isDateInYesterday(postDate)
            {
                return "Yesterday at \(time)"
            }
            else
            {
                let weekday = Calendar.current.component(.weekday, from: postDate)
                
                return "\(dateFormatter.weekdaySymbols[weekday-1]) at \(time)"
            }
        }
        else if (postDate.isInSameYear(date: now))
        {
            dateFormatter.dateFormat = "hh:mm a"
             dateFormatter.timeZone = TimeZone.current
            let time = dateFormatter.string(from: postDate)
            dateFormatter.dateFormat = "LLLL"
            let nameOfMonth = dateFormatter.string(from: postDate)
            let day = Calendar.current.component(.day, from: postDate)
            
            return " \(day) \(nameOfMonth) at \(time)"
        }
        else
        {
            dateFormatter.dateFormat = "hh:mm a"
             dateFormatter.timeZone = TimeZone.current
            let time = dateFormatter.string(from: postDate)
            dateFormatter.dateFormat = "LLLL"
            let nameOfMonth = dateFormatter.string(from: postDate)
            let day = Calendar.current.component(.day, from: postDate)
            let year = Calendar.current.component(.year, from: postDate)
            
            return " \(day) \(nameOfMonth) \(year) at \(time)"
            
        }
        
        
    }
    //    MARK:- Thumnail Generation
    class func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    class func thumbnailImageFor(fileUrl:URL) -> UIImage? {
        
        let video = AVURLAsset(url: fileUrl, options: [:])
        let assetImgGenerate = AVAssetImageGenerator(asset: video)
        assetImgGenerate.appliesPreferredTrackTransform = true
        
        let videoDuration:CMTime = video.duration
        let durationInSeconds:Float64 = CMTimeGetSeconds(videoDuration)
        
        let numerator = Int64(1)
        let denominator = videoDuration.timescale
        let time = CMTimeMake(value: numerator, timescale: denominator)
        if let thumbImageCache = imageCache.object(forKey: fileUrl.absoluteString as AnyObject) as? UIImage {
            return thumbImageCache
        }
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
             imageCache.setObject(thumbnail, forKey: fileUrl.absoluteString as AnyObject)
            return thumbnail
        } catch {
            print(error)
            return nil
        }
    }
    
    class func thumbnailForVideoAtURL(url: URL,completion:@escaping (_ Image:UIImage?,_ error :Error?,_ url:URL)->Void) {
         let urlString = url.absoluteString
        if let thumbImageCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            DispatchQueue.main.async {
                completion(thumbImageCache,nil,url)
            }
            return
        }
        else
        {
            DispatchQueue.global(qos: .background).async {
                let asset = AVAsset(url: url)
                let assetImageGenerator = AVAssetImageGenerator(asset: asset)
                 assetImageGenerator.appliesPreferredTrackTransform = true
                var time = asset.duration
                time.value = min(time.value, 2)
                assetImageGenerator.generateCGImagesAsynchronously(forTimes: [NSValue(time: time)], completionHandler: { (time, cgImage, time2, result, error) in
                    if error == nil
                    {
                        let img =  UIImage(cgImage: cgImage!)
                        imageCache.setObject(img, forKey: urlString as AnyObject)
                        DispatchQueue.main.async {
                            completion(img,nil,url)
                        }
                    }
                    else
                    {
                        completion(nil,error,url)
                    }
                })
   
            }
        }
    }
    
    class func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[0-9])(?=.*[a-z].*[a-z]).{6,}$")
        return passwordTest.evaluate(with: password)
    }
    
    class func getAge(dob:String)->Int
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
      if let dobDate =  dateFormatter.date(from: dob)
      {
        return dobDate.age
        
        }
        else
      {
        return 0
        }
    }
    static func makeFileName() ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let filename = dateFormatter.string(from: Date()) as String
        return filename
    }
    
    class func getLocations_From(key:String,completion:@escaping (_ places:[SPGooglePlacesAutocompletePlace]?,_ placesNameList:[String]?,_ error:Error?)->Void)
    {
        
        let query = SPGooglePlacesAutocompleteQuery(apiKey: Constants.kGoogleApiKey)
        query?.input = key
        query?.fetchPlaces({ (places, error) in
            if places != nil
            {
                 let list = places as? [SPGooglePlacesAutocompletePlace]
               completion(list,(list?.map({$0.name}))!,nil)
               
                
            }
            else
            {
                completion(nil,nil,error)
            }
        })
    }
   class func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    class func getCellFor(tableView:UITableView,feed:Feed,indexPath:IndexPath)->UITableViewCell?
    {
        switch feed.activity_type {
        case FeedType.athleteAwards,FeedType.athleteLevel,FeedType.athlete_PB:
            let cell =  tableView.dequeueReusableCell(withIdentifier: FeedCellId.StatusCell) as? SDFeedTableViewCell
            cell?.feed = feed
            cell?.index = indexPath.row
            return cell
        case FeedType.postMedia:
            let cell = tableView.dequeueReusableCell(withIdentifier: FeedCellId.MediaCell) as? SDFeedTableViewCell
            cell?.feed = feed
            cell?.index = indexPath.row
            return cell
        case FeedType.post_text,FeedType.teamCreated,FeedType.teamMember,FeedType.profileUpdate,FeedType.dareEvent,FeedType.dareChallenge,FeedType.dareEvent:
            let cell = tableView.dequeueReusableCell(withIdentifier: FeedCellId.StatusCell) as? SDFeedTableViewCell
            cell?.feed = feed
            cell?.index = indexPath.row
            return cell
        case FeedType.root:
            let cell = tableView.dequeueReusableCell(withIdentifier: FeedCellId.rootCell) as? SDFeed_Root_TableViewCell
            cell?.feed = feed
            cell?.index = indexPath.row
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: FeedCellId.StatusCell) as? SDFeedTableViewCell
            cell?.feed = feed
            cell?.index = indexPath.row
            return  cell
            
        }
        
        
    }
    
    class func getSizeForItem(at indexPath: IndexPath,totalItemCount:Int,collectionView:UICollectionView)->CGSize
    {
        
        switch totalItemCount{
        case 1:
            return CGSize(width: (collectionView.bounds.width - 10), height: (collectionView.bounds.height - 5))
        case 2:
            
            return CGSize(width: (collectionView.bounds.width - 2) / 2, height: (collectionView.bounds.height - 5))
            
        case 3:
            switch indexPath.item {
            case 0:
                return CGSize(width: ((collectionView.bounds.width) / 2) - 2, height: (collectionView.bounds.height - 3))
            case 1,2:
                return CGSize(width:((collectionView.bounds.width) / 2) - 2 , height: 50 )

                
            default:
                return CGSize(width: (UIScreen.main.bounds.width - 32) / 3, height:  (UIScreen.main.bounds.width) / 3)
            }
        case 4 :
            return CGSize(width: (collectionView.bounds.width-2) / 2, height: (collectionView.bounds.height - 6)/2)
            
            
        default:
            switch indexPath.item {
            case 0,1:
                return CGSize(width: (collectionView.bounds.width-4)/2, height: (collectionView.bounds.height - 4) / 2)
            case 2,3,4:
                 return CGSize(width: (collectionView.bounds.width - 5) / 3, height:  (collectionView.bounds.width - 3) / 3)
                
            default:
                return CGSize(width: (UIScreen.main.bounds.width) / 3, height:  (UIScreen.main.bounds.width) / 3)
            }
        }
    }
    
    enum CellType {
        case normal
        case expanded
    }
    class func getLayoutValuesFor(itemCount:Int)->[CellType]
    {
        switch itemCount{
        case 1:
            return [.expanded]
        case 2:
            return [.expanded,.expanded]
        case 3 :
             return [.expanded,.normal,.normal]
        case 4 :
            return [.normal, .normal, .normal, .normal]
             case 5 :
            return [.expanded,.expanded,.normal, .normal, .normal]
        default:
            return [.normal]
            
        }
    }

    }

