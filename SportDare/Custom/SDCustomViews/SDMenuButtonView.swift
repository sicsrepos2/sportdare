//
//  SDMenuButtonView.swift
//  SportDare
//
//  Created by Binoy T on 17/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import UIKit
//@IBDesignable
class SDMenuButtonView: UIControl {
    
    var contentView : UIView?
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelMySport: UILabel!
    
    @IBOutlet weak var dropImage: UIImageView!
    @IBInspectable var title: String = ""{
        didSet {
            button.setTitle(title, for: .normal)
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet
        {
            layer.cornerRadius = cornerRadius
            button.layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    @IBInspectable var isBold: Bool = false{
        didSet {
            if isBold
            {
                labelMySport.isHidden = false
                button.titleLabel?.font =  UIFont(name: "Arial-BoldMT", size: 17)
                button.titleEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
                
            }
            else
            {
                labelMySport.isHidden = true
                button.titleLabel?.font =  UIFont(name: "Arial", size: 17)
                button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
    }
    
    @IBInspectable override var isSelected: Bool {
        didSet {
            if isSelected
            {
                dropImage.isHidden = false
                button.isSelected = true
                button.setBackgroundColor(color: UIColor.appBase, forState: .selected)
                button.setBackgroundColor(color: UIColor.appBase, forState: .highlighted)
                button.backgroundColor = UIColor.appBase
            }
            else
            {
                dropImage.isHidden = true
                button.isSelected = false
                button.backgroundColor = UIColor.appGrey
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        contentView = loadViewFromNib()
        self.button.setTitle(title, for: .normal)
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        button.titleLabel?.minimumScaleFactor = 0.6
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        
        addSubview(contentView!)
    }

    func loadViewFromNib() -> UIView! {
       
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SDMenuButtonView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func didTapButton(_ sender: UIButton) {
        self.sendActions(for: UIControl.Event.touchUpInside)
    
    }
    
    

}
