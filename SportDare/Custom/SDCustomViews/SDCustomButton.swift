//
//  SDCustomButton.swift
//  Sportdare
//
//  Created by Binoy T on 19/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import UIKit

//@IBDesignable
class SDCustomButton: UIControl {
    
    var contentView : UIView?
    
 
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = imageView.frame.width / 2
    }
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    override func awakeFromNib() {
        self.imageView.contentMode = .scaleAspectFit
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
             imageView.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var selectedBorderColor: UIColor = UIColor.clear{
        didSet {
            if isSelected
            {
                layer.borderColor = selectedBorderColor.cgColor
                imageView.layer.borderColor = selectedBorderColor.cgColor
            }
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0
        {
        didSet
        {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet
        {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var title: String = ""{
        didSet {
            label.text = title
        }
    }
    @IBInspectable var Selectedtitle: String = ""{
        didSet {
            if isSelected
            {
                label.text = Selectedtitle
            }
        }
    }
    
    @IBInspectable var textfontSize: CGFloat = 17{
        didSet {
            
        }
    }
    @IBInspectable var numberOfLines: Int = 2{
        didSet {
            label.numberOfLines = numberOfLines
        }
    }
    @IBInspectable var boldTitle: Bool = false{
        didSet {
            if boldTitle
            {
                label.font =  UIFont.systemFont(ofSize: textfontSize, weight: .bold)
//                    UIFont(name: "Arial-BoldMT", size: textfontSize)
            }
        }
    }
    
    @IBInspectable var titleColor: UIColor!{
        didSet {
            label.textColor =  titleColor
        }
    }
    
    @IBInspectable var selectedTitleColor: UIColor!{
        didSet {
            if isSelected
            {
            label.textColor =  selectedTitleColor
            }
        }
    }
    @IBInspectable var selectedimageColor: UIColor!{
        didSet {
            if isSelected
            {
            imageView.image = buttonImage.tinted(with: selectedimageColor!)
            }
        }
    }
    @IBInspectable var imageColor: UIColor!{
        didSet {
            imageView.image = buttonImage.tinted(with: imageColor!)
        }
    }
   
    @IBInspectable var selectedBGColor: UIColor!{
        didSet {
           self.backgroundColor  = selectedBGColor
        }
    }
    @IBInspectable var BGColor: UIColor!{
        didSet {
            self.backgroundColor  = BGColor

        }
    }
    
    @IBInspectable var buttonImage: UIImage!{
        didSet {
           imageView.image = buttonImage
        }
    }
  
    @IBInspectable var selectedImage: UIImage!{
        didSet {
            if isSelected
            {
            imageView.image = selectedImage
            }
        }
    }
    
    @IBInspectable override var isSelected: Bool {
        didSet {
            if isSelected
            {
                imageView.image = selectedimageColor != nil ?  buttonImage.tinted(with:selectedimageColor!) : selectedImage
                 label.textColor =  selectedTitleColor
                self.backgroundColor = selectedBGColor
                 layer.borderColor = selectedBorderColor.cgColor

            }
            else
            {
                imageView.image = buttonImage.withRenderingMode(.alwaysOriginal)
                label.textColor =  titleColor
                 self.backgroundColor = selectedBGColor
                self.backgroundColor  = BGColor
                 layer.borderColor = borderColor.cgColor
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        contentView = loadViewFromNib()
       
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SDCustomButton", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func didTapButton(_ sender: UIButton) {
        self.sendActions(for: UIControl.Event.touchUpInside)
        
    }
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.sendActions(for: .touchUpInside)
    }

    
    
}
