//
//  SDImageViewShield.swift
//  Sportdare
//
//  Created by Binoy T on 16/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
//@IBDesignable
class SDImageViewShield : UIControl {
    
var contentView : UIView?

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var shieldImageView: UIImageView!
    @IBInspectable override var isSelected: Bool {
    didSet {
       
    }
}
    @IBInspectable var segue :UIStoryboardSegue!{
        didSet {
            
        }
    }
    
    @IBInspectable  var image: UIImage = #imageLiteral(resourceName: "placeholder-1") {
        didSet {
//            if image.size.width < self.profileImageView.frame.size.width
//            {
//                 self.profileImageView.contentMode = .center
//            }
//            else
//            {
                self.profileImageView.contentMode = .scaleAspectFit
//            }
            
            self.profileImageView.image = image
        }
    }
    
//    @IBInspectable  var shieldType: ShieldType = ShieldType.greyZero{
//        didSet {
//            self.shieldImageView.image = shieldType.image
//        }
//    }
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
}

required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    xibSetup()
}
func xibSetup() {
    contentView = loadViewFromNib()
    contentView!.frame = bounds
   
    // Make the view stretch with containing view
    contentView!.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
    addSubview(contentView!)
}
    func setImagewith_Url(url:String,PlaceHolderImage:UIImage?)
    {
        profileImageView.sd_setImage(with: URL(string: url), placeholderImage: PlaceHolderImage, options: .highPriority) { (image, error, cache, url) in
            if image != nil
            {
                self.layoutIfNeeded()
                self.image = image!
//                    (image?.resizeImage(targetSize: self.profileImageView.frame.size))!
                
            }
        }
        
    }
func loadViewFromNib() -> UIView! {
    
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: "SDShieldImageView", bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
}
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.sendActions(for: .touchUpInside)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }

}
