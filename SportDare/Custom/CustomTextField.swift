//
//  CustomeTextField.swift
//  ExtraHourzApp
//
//  Created by Srishti Innovative on 24/01/17.
//  Copyright © 2017 Srishti Innovative. All rights reserved.
//

import UIKit

//@IBDesignable
class CustomTextField: UITextField {
    var padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    @IBInspectable var paddingWidth : CGFloat = 0 {
        didSet {
            padding  = UIEdgeInsets(top: 0, left: paddingWidth, bottom: 0, right: paddingWidth);
            
        }
    }
    
//    @IBInspectable var delegateClass : NSObject! {
//        didSet {
//            self.delegate = delegateClass as! UITextFieldDelegate?
//            
//        }
//    }
    
    @IBInspectable var placeHolderColor : UIColor = UIColor.black {
        didSet {
            self.setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
            
        }
    }
    @IBInspectable var rightImage: UIImage = UIImage(){
        didSet {
            self.rightViewMode = .always
            let imgView = UIImageView(image: rightImage)
            imgView.contentMode = .scaleAspectFit
            //            imgView.frame = CGRect(x: 10, y: self.frame.width - (imgView.frame.width + 5) , width: imgView.frame.width, height: imgView.frame.height)
            self.rightView = imgView
            
        }
    }
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:self.frame.width - ((rightView?.frame.width)! + 5), y:  (self.frame.height -  (rightView?.frame.height)!) / 2 , width: (rightView?.frame.width)!, height:  (rightView?.frame.height)!)
    }

    override func draw(_ rect: CGRect) {
        setBorders()
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0
        {
        didSet
        {
            layer.borderWidth = borderWidth
        }
    }
    
    func setBorders()
    {
        if leftBorder == true
        {
            let leftBorder = CAShapeLayer()
            self.layoutSubviews()
            leftBorder.frame = CGRect(x: 0, y: 0, width: 1.0, height: self.frame.height)
            leftBorder.backgroundColor = self.borderColor.cgColor
            leftBorder.strokeColor =  self.borderColor.cgColor
            self.layer.addSublayer(leftBorder)
            
            
        }
        if rightBorder == true
        {
            let rightBorder = CAShapeLayer()
            self.layoutSubviews()
            rightBorder.frame = CGRect(x: self.frame.width - 1, y: 0, width: 1.0, height: self.frame.height)
            rightBorder.backgroundColor = self.borderColor.cgColor
            rightBorder.strokeColor =  self.borderColor.cgColor
            self.layer.addSublayer(rightBorder)
            
        }
        
        if topBorder == true
        {
            
        }
        
        if bottomBorder == true
        {
            let bottomBorder = CAShapeLayer()
            self.layoutSubviews()
            bottomBorder.frame = CGRect(x: 0, y: self.frame.maxY - 1, width: self.frame.width, height: 1.0)
            bottomBorder.backgroundColor = self.borderColor.cgColor
            bottomBorder.strokeColor =  self.borderColor.cgColor
            self.layer.addSublayer(bottomBorder)
            
        }
        
        if self.setTopCorners == true
        {
            let topPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft,.topRight],
                                       cornerRadii: CGSize(width: 5.0, height: 5.0))
            //            topPath.lineWidth = 5.0
            let layer = CAShapeLayer()
            layer.frame = self.bounds
            layer.path = topPath.cgPath
            self.layer.masksToBounds = true
            self.layer.mask = layer
            self.clipsToBounds = true
            let topBorder = CAShapeLayer()
            topBorder.frame = self.bounds
            topBorder.strokeColor =  self.borderColor.cgColor
            topBorder.fillColor = UIColor.clear.cgColor
            topBorder.path = layer.path
            topBorder.lineWidth = 2.0
            self.layer.addSublayer(topBorder)
            
        }
        if setBottomCorners == true
        {
            let bottomPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft,.bottomRight
                ], cornerRadii: CGSize(width: 5.0, height: 5.0))
            //            bottomPath.lineWidth = 5.0
            let layer = CAShapeLayer()
            layer.frame = self.bounds
            layer.path = bottomPath.cgPath
            self.layer.masksToBounds = true
            self.layer.mask = layer
            self.clipsToBounds = true
            let bottomBorder = CAShapeLayer()
            bottomBorder.frame = self.bounds
            bottomBorder.strokeColor =  self.borderColor.cgColor
            bottomBorder.fillColor = UIColor.clear.cgColor
            bottomBorder.path = layer.path
            bottomBorder.lineWidth = 2.0
            
            self.layer.addSublayer(bottomBorder)
        }
        
    }
    
    @IBInspectable var setTopCorners : Bool = false
        {
        didSet {
        }
        
    }
    @IBInspectable var setBottomCorners : Bool = false
        {
        didSet {
        }
        
    }
    
    @IBInspectable var leftBorder : Bool = false
        {
        didSet{
            
        }
    }
    @IBInspectable var rightBorder : Bool = false
        {
        didSet{
            
            
        }
    }
    @IBInspectable var bottomBorder : Bool = false
        {
        didSet{
            
        }
    }
    @IBInspectable var topBorder : Bool = false
        {
        didSet{
            
        }
    }


}

