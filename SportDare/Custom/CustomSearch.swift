//
//  CustomSearch.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import UIKit
class CustomSearchBar: UISearchBar {
    
   
    
    override func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
         super.setShowsCancelButton(false, animated: false)
    }
    
   
}
class CustomSearchController: UISearchController, UISearchBarDelegate {
    
    lazy var _searchBar: CustomSearchBar = {
        [unowned self] in
        let result = CustomSearchBar(frame: CGRect.zero)
        result.delegate = self
        
        return result
        }()
    
    override var searchBar: UISearchBar {
        get {
            
            return _searchBar
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hidesNavigationBarDuringPresentation = false
    }
}
