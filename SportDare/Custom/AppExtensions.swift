//
//  Entenstions.swift
//  Centavizer
//
//  Created by Srishti Innovative on 07/02/18.
//  Copyright © 2018 Srishti Innovative. All rights reserved.
//

import Foundation
import UIKit
import Photos
//import JHTAlertController

extension PHAsset {
    
    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
    }
    
    func getData(completionHandler : @escaping ((_ data : Data?,_ filename:String) -> Void)){
        if self.mediaType == .image {
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isNetworkAccessAllowed = true
            options.isSynchronous = true

            PHCachingImageManager.default().requestImageData(for: self, options: options) { (data, name, orientation, info) in
                completionHandler(data, Utilities.makeFileName() + ".jpg")
            }
        } else if self.mediaType == .video {
            let options = PHVideoRequestOptions()
            options.deliveryMode = .fastFormat
           options.isNetworkAccessAllowed = true
            
            PHCachingImageManager.default().requestAVAsset(forVideo: self, options: options) { (avasset, audio, info) in
                let asset = avasset as? AVURLAsset
                let data = try?Data(contentsOf: (asset?.url)!)
                completionHandler(data, Utilities.makeFileName() + ".mp4")
            }
        }
    }
}

extension UITabBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = UIScreen.main.bounds.height * 0.08
        return sizeThatFits
    }
}
extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        if size.width > targetSize.width || size.height > targetSize.height
        {
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        
        let rect = CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
            
        UIGraphicsBeginImageContextWithOptions(newSize, true,  UIScreen.main.scale)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
        }
        else
        {
            return self
        }
    }
    
    func changeColor()->UIImage {
      
        let ref1 = createMask(self)
        let colorMasking :[CGFloat] = [254, 255, 254, 255, 254, 255]

        let New = ref1?.copy(maskingColorComponents: colorMasking)
        let resultedimage = UIImage(cgImage: New!)

        return resultedimage
    }
    
    func createMask(_ temp: UIImage?) -> CGImage? {
        let ref = temp?.cgImage
        let mWidth = ref!.width
        let mHeight = ref!.height
        let count: Int = mWidth * mHeight * 4
        var bufferdata = malloc(count)
        
        let colorSpaceRef = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo: CGBitmapInfo = []
        let renderingIntent: CGColorRenderingIntent = .defaultIntent
        
        let cgctx = CGContext(data: bufferdata, width: mWidth, height: mHeight, bitsPerComponent: 8, bytesPerRow: mWidth * 4, space: colorSpaceRef, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
       
        
        let rect: CGRect = CGRect(x: 0, y: 0, width: mWidth, height: mHeight)
        cgctx?.draw(ref!, in: rect)
       
        bufferdata = cgctx!.data
        let releaseMaskImagePixelData: CGDataProviderReleaseDataCallback = { (info: UnsafeMutableRawPointer?, data: UnsafeRawPointer, size: Int) -> () in
            // https://developer.apple.com/reference/coregraphics/cgdataproviderreleasedatacallback
            // N.B. 'CGDataProviderRelease' is unavailable: Core Foundation objects are automatically memory managed
            return
        }
      
        let provider =   CGDataProvider(dataInfo: nil, data: bufferdata!, size:  mWidth * mHeight * 4, releaseData: releaseMaskImagePixelData)
        let savedimageref = CGImage(width: mWidth, height: mHeight, bitsPerComponent: 8, bitsPerPixel: 32, bytesPerRow: mWidth * 4, space: colorSpaceRef, bitmapInfo: bitmapInfo, provider: provider!, decode: nil, shouldInterpolate: false, intent: renderingIntent)
        return savedimageref
    }
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func tinted(with color: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        defer { UIGraphicsEndImageContext() }
        color.setFill()
        withRenderingMode(.alwaysTemplate)
            .draw(in: CGRect(origin: .zero, size: size))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func crop( rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        
        
        rect.size.height*=self.scale
        
        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
    //  Converted to Swift 4 by Swiftify v4.2.20418 - https://objectivec2swift.com/

        
        @discardableResult func replace(color: UIColor, withColor replacingColor: UIColor) -> UIImage? {
            guard let inputCGImage = self.cgImage else {
                return nil
            }
            let colorSpace       = CGColorSpaceCreateDeviceRGB()
            let width            = inputCGImage.width
            let height           = inputCGImage.height
            let bytesPerPixel    = 4
            let bitsPerComponent = 8
            let bytesPerRow      = bytesPerPixel * width
            let bitmapInfo       = RGBA32.bitmapInfo
        
            guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo) else {
                print("unable to create context")
               return nil
            }
            context.draw(inputCGImage, in: CGRect(x: 0, y: 0, width: width, height: height))
            
            guard let buffer = context.data else {
                return nil
            }
            
            let pixelBuffer = buffer.bindMemory(to: RGBA32.self, capacity: width * height)
            
            let inColor = RGBA32(color: color)
            let outColor = RGBA32(color: replacingColor)
            for row in 0 ..< Int(height) {
                for column in 0 ..< Int(width) {
                    let offset = row * width + column
                    if pixelBuffer[offset] == inColor {
                        pixelBuffer[offset] = outColor
                    }
                }
            }
            
            guard let outputCGImage = context.makeImage() else {
                return nil
            }
            let image = UIImage(cgImage: outputCGImage, scale: self.scale, orientation: self.imageOrientation)
            return image
        }
        
    
  

}
extension UITableView{
    func showEmptyMessage(message:String)
    {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        messageLabel.text = message
        messageLabel.textAlignment = .center
         messageLabel.textColor = UIColor.black
        self.backgroundView = messageLabel
    }
    
    func scrollToBottom(animated:Bool,section:Int)
    {
        if self.numberOfRows(inSection: section) != 0
        {
            let indexpath = IndexPath(item: self.numberOfRows(inSection: section) - 1, section: section)
            self.scrollToRow(at: indexpath, at: .bottom, animated: animated)
        }
    }
   
   
    
}
extension UIViewController{
    func addTapGesture(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func hideTabBar() {
        if self.tabBarController?.tabBar.isHidden == false
        {
            var frame = self.tabBarController?.tabBar.frame
            frame!.origin.y = UIScreen.main.bounds.height + (frame?.size.height)!
            UIView.animate(withDuration: 0.5, animations: {
                self.tabBarController?.tabBar.frame = frame!
                self.tabBarController?.tabBar.isHidden = true
            })
        }
    }
    
    func showTabBar() {
        if self.tabBarController?.tabBar.isHidden == true
        {
            var frame = self.tabBarController?.tabBar.frame
            frame!.origin.y = UIScreen.main.bounds.height - (frame?.size.height)!
            UIView.animate(withDuration: 0.5, animations: {
                self.tabBarController?.tabBar.frame = frame!
                self.tabBarController?.tabBar.isHidden = false
            })
        }
    }
    
    //MARK:- EMAIL VALIDATION
    func isValidEmail(_ email: String) -> Bool
    {
        let stricterFilterString:NSString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailRegex:NSString = stricterFilterString
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    func showAlertWith(title:String,message:String,CancelBtnTitle:String?,
                       OtherBtnTitle:String? ,completion:((Int, String) -> Void)?){
        
        AJAlertController.initialization().showAlert_With_Title(aStrTitle: title,aStrMessage: message, aCancelBtnTitle: CancelBtnTitle, aOtherBtnTitle: OtherBtnTitle) { (index, title) in
            print(index,title)
            completion?(index,title)
        }
    }
    func showAlertWith(title:String,message:String,CancelBtnTitle:String?,
                       OtherBtnTitle:String? ,acceptCompletion:@escaping () -> Void?,cancelCompletion:@escaping () -> ()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: CancelBtnTitle, style: .cancel) { (action) in
             cancelCompletion()
        }
        let accept = UIAlertAction(title: OtherBtnTitle, style: .default) { (action) in
            acceptCompletion()
        }
        alert.addAction(cancel)
        alert.addAction(accept)
        self.present(alert, animated: true) {
            
        }
//        AJAlertController.initialization().showAlert_With_Title(aStrTitle: title,aStrMessage: message, aCancelBtnTitle: CancelBtnTitle, aOtherBtnTitle: OtherBtnTitle) { (index, title) in
//            print(index,title)
//            if index == 0
//            {
//                cancelCompletion()
//            }
//            else
//            {
//                acceptCompletion()
//            }
//        }
    }
    func showAlertWithOkButton(message:String){
        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
            
        }
    }
    func show_OK_AlertWith_Title(title:String,message:String,completion:((_ index:Int,_ title:String)->Void)?)
    {
        AJAlertController.initialization().showAlertWithOkButton_Title(aStrTitle: title, aStrMessage: message) { (index, title) in
            completion?(index,title)
        }
    }
}

extension  UITextField{
    func setLeftPaddingView(width: CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }
    func setRightPaddingViewWithImage(image: UIImage){
    
    func setPlaceholderColor(color: UIColor){
        if self.placeholder != nil{
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                        attributes: [NSAttributedString.Key.foregroundColor: color])
        }
    }
    func addBottomBorder(color: UIColor){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
}
extension UIView{
    func customizeWith(cornerRadius: CGFloat, borderColor: UIColor?, borderWidth: CGFloat){
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        if borderColor != nil{
        self.layer.borderColor = borderColor!.cgColor
        self.layer.borderWidth = borderWidth
        }
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    func pushTransition(_ duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromRight
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    func popTransition(_ duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromLeft
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    func hideWithAnimation()
    {
        if !self.isHidden
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0.0
            }) { (status) in
                self.isHidden = true
            }
        }
        
    }
    func showWithAnimation()
    {
        if self.isHidden
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 1
            }) { (status) in
                self.isHidden = false
            }
        }
    }
}

public extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
        
        var appColor:UIColor {
            return UIColor(named: "SDBase")!
            
        }
    }
    

        convenience init(hexString: String, alpha: CGFloat = 1.0) {
            let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let scanner = Scanner(string: hexString)
            if (hexString.hasPrefix("#")) {
                scanner.scanLocation = 1
            }
            var color: UInt32 = 0
            scanner.scanHexInt32(&color)
            let mask = 0x000000FF
            let r = Int(color >> 16) & mask
            let g = Int(color >> 8) & mask
            let b = Int(color) & mask
            let red   = CGFloat(r) / 255.0
            let green = CGFloat(g) / 255.0
            let blue  = CGFloat(b) / 255.0
            self.init(red:red, green:green, blue:blue, alpha:alpha)
        }
        func toHexString() -> String {
            var r:CGFloat = 0
            var g:CGFloat = 0
            var b:CGFloat = 0
            var a:CGFloat = 0
            getRed(&r, green: &g, blue: &b, alpha: &a)
            let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
            return String(format:"#%06x", rgb)
        }
    
    class func bronze()->UIColor
    {
        return UIColor.red
    }
    class var appBase: UIColor {
        return UIColor(named:"SDBase")!
    }
    class var appBaseLight: UIColor {
        return UIColor(named:"SDBaseLight")!
    }
    class var appGrey: UIColor {
        return UIColor(named:"SDGrey")!
    }
    
}
class CustomButtonWithShadow: UIButton {
    
    var shadowLayer: CAShapeLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
        // shadow
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: -2, height: 2)
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 4.0
        
    }
    class CustomTextFieldWithShadow: UITextField {
        
        var shadowLayer: CAShapeLayer!
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            
            
            // shadow
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: -2, height: 2)
            self.layer.shadowOpacity = 0.4
            self.layer.shadowRadius = 4.0
            
        }
}
}
extension Date {
    func isInSameWeek(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .weekOfYear)
    }
    func isInSameMonth(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
    }
    func isInSameYear(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
    }
    func isInSameDay(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .day)
    }
    var isInThisWeek: Bool {
        return isInSameWeek(date: Date())
    }
    var isInToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
    var isInTheFuture: Bool {
        return Date() < self
    }
    var isInThePast: Bool {
        return self < Date()
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    
    var dayOfWeek: Int{
       //Monday as start day , subtracting 1
        let todayDate = Date()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(.weekday, from: todayDate)
        let weekDay = myComponents.weekday
        return weekDay! - 1 == 0 ? 7 : weekDay! - 1
    }
    var startOfWeek: Date {
        
        let date = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        let dslTimeOffset = NSTimeZone.local.daylightSavingTimeOffset(for: date)
        let start = date.addingTimeInterval(dslTimeOffset)
       
        return  Calendar.current.date(byAdding: .day, value: 1, to: start)!
    }
    
    var endOfWeek: Date {
        let end =  Calendar.current.date(byAdding: .day, value: -1, to: self.startOfWeek)!
        return Calendar.current.date(byAdding: .second, value: 604799, to: end)!
    }
    
    var dateOnly : String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: self)
    }
    var dateOnlyYYYY_MM_dd : String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    var isAdult : Bool{
       let age =    Calendar.current.dateComponents([.year], from: self, to: Date())
        return age.year! > 18 ? true : false
    }
    var age : Int{
         let age =    Calendar.current.dateComponents([.year], from: self, to: Date())
         return age.year!
    }
    var timeOnly : String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: self)
    }
    
    var appFormatted_With_Time : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yy hh:mm a"
        return formatter.string(from:self)
        
    }
    
}

//MARK: Data Types
extension String {
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2 - $3", options: .regularExpression, range: nil)
    }
    
   public func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
    
    public var url : URL? {
        return URL(string: self)
    }
    
    public var toAppTime:String{
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "HH:mm:ss"
      let time = dateformatter.date(from: self)
        dateformatter.dateFormat = "hh:mm a"
        return dateformatter.string(from: time ?? Date())
    }
    public var toAppDate:String{
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let date = dateformatter.date(from: self)
        dateformatter.dateFormat = "dd-MM-YYYY"
        return dateformatter.string(from: date ?? Date())
    }
    public var date:Date{
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let date = dateformatter.date(from: self)
        return date ?? Date()
    }
    
}

extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

extension Double
{
   public var stringValue : String {
        
        return String(format: "%.2f", self)
    }
    
}



//MARK: CALayer
extension CATextLayer {
    func getBar() -> CALayer?{
        let name = self.name!
        let superlayer = self.superlayer!
        for sub in superlayer.sublayers!{
            if sub.name == name {
                return sub
            }
        }
        return nil
    }
}


extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x:0,y: 0,width: self.frame.height,height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0,y: self.frame.height - thickness,width: UIScreen.main.bounds.width,height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0,y: 0,width: thickness,height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:(self.frame.width) - thickness,y: 0,width: thickness,height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}





//MARK: TextField

private var maxLengths = [UITextField: Int]()

extension UITextField{
    func showInvalid()
    {
       
        guard let _ =  (self.subviews.filter{$0 is UIImageView}.first) else{
            
            let invalidImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            invalidImage.center = CGPoint(x: self.frame.width - 20, y: self.frame.height/2)
            invalidImage.image = UIImage(named: "errortext")
            invalidImage.contentMode = .scaleAspectFit
            self.addSubview(invalidImage)
            return
        }
        
    }
    func showError()
    {
        let invalidImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        invalidImage.center = CGPoint(x: self.frame.width - 20, y: self.frame.height/2)
        invalidImage.image = UIImage(named: "errortext")
        self.rightViewMode = .always
        self.rightView = invalidImage
        
    }
    func removeError()
    {
        self.rightViewMode = .never
    }
    func removeInvalid()
    {
        guard let oldview =  (self.subviews.filter{$0 is UIImageView}.first) else{
            return
        }
        oldview.removeFromSuperview()
        
        
        
    }
    
    @IBInspectable var cornerRadius: CGFloat{
        set {
            layer.cornerRadius = newValue
        }
        
        get {
            return layer.cornerRadius
        }
    }
    
    
    @IBInspectable var maxLength: Int {
        get {
            // 4
            guard let length = maxLengths[self] else {
                return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            // 5
            addTarget(
                self,
                action: #selector(limitLength),
                for: UIControl.Event.editingChanged
            )
        }
    }
    
    @objc func limitLength(textField: UITextField) {
        // 6
        guard let prospectiveText = textField.text, prospectiveText.count > maxLength else {
            return
        }
        
        let selection = selectedTextRange
        // 7
        
        text = String(prospectiveText[..<prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)])
//            prospectiveText.substring(with:Range<String.Index>(prospectiveText.startIndex ..< prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)))

        selectedTextRange = selection
    }
    
    
    
    
    
    
}
