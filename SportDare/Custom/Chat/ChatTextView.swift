//
//  ChatTextView.swift
//  Chat
//
//  Created by Srishti on 28/12/17.
//  Copyright © 2017 Srishti. All rights reserved.
//

import UIKit

class ChatTextView: UITextView {
    @IBInspectable
    var borderColor :  UIColor = UIColor.white{
        didSet{
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    
    @IBInspectable
    var borderWidth :  CGFloat = 1.0{
        didSet{
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable
    var cornerRadius : CGFloat = 1.0{
        didSet{
            self.layer.cornerRadius = self.cornerRadius
        }
    }
   

}
