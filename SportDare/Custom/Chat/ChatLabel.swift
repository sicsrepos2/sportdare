//
//  ChatLabel.swift
//  Chat
//
//  Created by Srishti on 29/12/17.
//  Copyright © 2017 Srishti. All rights reserved.
//

import UIKit

class ChatLabel: UILabel {

    @IBInspectable
    var borderColor :  UIColor = UIColor.white{
        didSet{
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    
    @IBInspectable
    var borderWidth :  CGFloat = 1.0{
        didSet{
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable
    var cornerRadius : CGFloat = 1.0{
        didSet{
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    @IBInspectable var topInset: CGFloat = 8.0
    @IBInspectable var bottomInset: CGFloat = 8.0
    @IBInspectable var leftInset: CGFloat = 8.0
    @IBInspectable var rightInset: CGFloat = 8.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
