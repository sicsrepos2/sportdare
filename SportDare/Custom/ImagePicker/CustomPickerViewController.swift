//
//  ViewController.swift
//  photosApp2
//
//  Created by Muskan on 10/4/17.
//  Copyright © 2017 akhil. All rights reserved.
//

import UIKit
import Photos

class CustomPickerTypeTableViewController: UITableViewController{
    var imageArray = PHFetchResult<PHAsset>()
    var videoArray = PHFetchResult<PHAsset>()
    var pickerDelegate : SDImagePickerDelegate?
    var selectedCount = 0
    var selectedAsset = [PHAsset]()
    override func viewDidLoad() {
        self.grabPhotos()
    }
    
    func grabPhotos(){
        DispatchQueue.global(qos: .background).async {
            
            let requestOptions=PHImageRequestOptions()
            requestOptions.isSynchronous=true
            requestOptions.deliveryMode = .highQualityFormat
            requestOptions.isNetworkAccessAllowed = false
            
            let fetchOptions=PHFetchOptions()
            fetchOptions.sortDescriptors=[NSSortDescriptor(key:"creationDate", ascending: false)]
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d mediaType = %d", PHAssetMediaType.image.rawValue, PHAssetMediaType.video.rawValue)
            self.imageArray = PHAsset.fetchAssets(with: fetchOptions)
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d",PHAssetMediaType.video.rawValue)
             self.videoArray = PHAsset.fetchAssets(with: fetchOptions)
            
       
            
            print("imageArray count: \(self.imageArray.count)")
            
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
               self.tableView.reloadData()
            }
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count > 0 ? 2 : 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typeCell")
        if indexPath.row == 0
        {
            PHImageManager.default().requestImage(for:  self.imageArray.lastObject!, targetSize: CGSize(width: 80, height: 80), contentMode: .aspectFill, options: nil) { (image, info) in
                cell?.imageView?.image = image
            }
             cell?.textLabel?.text = "Photos"
        }
        else
        {
            PHImageManager.default().requestImage(for:  self.videoArray.lastObject!, targetSize: CGSize(width: 80, height: 80), contentMode: .aspectFill, options: nil) { (image, info) in
                cell?.imageView?.image = image
            }
           cell?.textLabel?.text =  "Videos"
        }
       
        
        return cell!
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pickerView = self.storyboard?.instantiateViewController(withIdentifier: "CustomPickerViewController") as! CustomPickerViewController
        if indexPath.row == 0
        {
            pickerView.imageArray = self.imageArray
        }
        else
        {
              pickerView.imageArray = self.videoArray
        }
        self.navigationController?.pushViewController(pickerView, animated: true)
        
    }
    
}
@objc protocol SDImagePickerDelegate
{
    func customPicker(customPicker:CustomPickerViewController, didSelectMedia media:[PHAsset])
    func customPickerDidCancel(customPicker: CustomPickerViewController)
}
class CustomPickerViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate,MediaSelectProtocol{

    @IBOutlet weak var myCollectionView: UICollectionView!
    var delegate : SDImagePickerDelegate!
//    var imageArray=[UIImage]()
    var imageArray = PHFetchResult<PHAsset>()
    var videoArray = PHFetchResult<PHAsset>()
    var selectedMedia : [PHAsset]!
    var selectedCount  = 0
    var maximumSelectionCount = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Photos"
 
        let layout = UICollectionViewFlowLayout()
        
        myCollectionView.collectionViewLayout = layout
        myCollectionView.delegate=self
        myCollectionView.dataSource=self

        myCollectionView.backgroundColor=UIColor.white
    
        grabPhotos()
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        myCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    //    MARK: Ations
    @IBAction func didCancel(_ sender: UIBarButtonItem) {
     
        self.delegate.customPickerDidCancel(customPicker: self)
       
    }
    
    @IBAction func didSelectAction(_ sender: UIBarButtonItem) {
        if selectedMedia.count > 0
        {
            delegate.customPicker(customPicker: self, didSelectMedia: selectedMedia)
            self.dismiss(animated: true) {
                
            }
        }
    }
    
    
    
    
    //    MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PhotoItemCell
        cell.delegate = self
       let asset = imageArray[indexPath.row]
        
       cell.setDetail_With(asset: asset,index:indexPath.row)
       cell.setSelection(status: selectedMedia.contains(asset))

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
        vc.imgArray = self.imageArray.objects(at: IndexSet(integersIn: ..<imageArray.count))
        vc.passedContentOffset = indexPath
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
       
        if DeviceInfo.Orientation.isPortrait {
            return CGSize(width: width/4 - 1, height: width/4 - 1)
        } else {
            return CGSize(width: width/6 - 1, height: width/6 - 1)
        }
    }
    
 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    
    
    //MARK: grab photos
    func grabPhotos(){
//        imageArray = [PHFetchResult<PHAsset>]()
        
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            
            let requestOptions=PHImageRequestOptions()
            requestOptions.isSynchronous=true
            requestOptions.deliveryMode = .highQualityFormat
            requestOptions.isNetworkAccessAllowed = false
          
            let fetchOptions=PHFetchOptions()
            fetchOptions.sortDescriptors=[NSSortDescriptor(key:"creationDate", ascending: false)]
           fetchOptions.predicate = NSPredicate(format: "mediaType = %d || mediaType = %d", PHAssetMediaType.image.rawValue, PHAssetMediaType.video.rawValue)
            let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: fetchOptions)
            self.imageArray = fetchResult
            var assets = [PHAsset]()
            fetchResult.enumerateObjects({ (asset, index, status) in
                assets.append(asset)
            })
            PHCachingImageManager().startCachingImages(for: assets, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: nil)
            print(fetchResult)
            print(fetchResult.count)

            print("imageArray count: \(self.imageArray.count)")
            
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
                self.myCollectionView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func didSelectItem(index: Int) {
        if selectedMedia.count < maximumSelectionCount - selectedCount
        {
          selectedMedia.append(self.imageArray.object(at: index))
            selectedCount += 1
            self.myCollectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
        }
    }
    func didDeselectItem(index: Int) {
       if let idx = selectedMedia.index(of: self.imageArray.object(at: index))
       {
        selectedMedia.remove(at: idx)
        selectedCount -= 1
        self.myCollectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
        }
        
    }

}
class CustomPickerNavigationController:UINavigationController{
    var pickerDelegate : SDImagePickerDelegate?
    var selectedCount = 0
    var selectedAsset = [PHAsset]()
    override func viewDidLoad() {
       if let topVc =  self.topViewController as? CustomPickerViewController
       {
        topVc.delegate = pickerDelegate
        topVc.selectedCount = selectedCount
        topVc.selectedMedia = selectedAsset
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
}


protocol MediaSelectProtocol {
    func didSelectItem(index:Int)
    func didDeselectItem(index:Int)
}
class PhotoItemCell: UICollectionViewCell {
     var delegate: MediaSelectProtocol!
    @IBOutlet weak var thumbImage: UIImageView!
    
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var imgPlay: UIImageView!
 
    @IBOutlet weak var viewVideoDetail: UIView!
    @IBOutlet weak var btnPick: UIButton!
    
    @IBOutlet weak var viewOverLay: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        img.frame = self.bounds
    }
    func setDetail_With(asset:PHAsset,index:Int)
    {
        PHImageManager.default().requestImage(for: asset, targetSize: self.frame.size, contentMode: .aspectFill, options: nil) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            self.thumbImage.image = image
            if asset.mediaType == .image
            {
                self.viewVideoDetail.isHidden = true
                self.lblDuration.isHidden = true
                self.imgPlay.isHidden = true
            }
            else
            {
                self.viewVideoDetail.isHidden = false
                self.lblDuration.isHidden = false
                self.imgPlay.isHidden = false
                self.lblDuration.text = self.getTime(duration: asset.duration)
            }
        }
        btnPick.tag = index
    }
    @IBAction func didPickAction(_ sender: UIButton) {
        if  btnPick.isSelected
        {
            
            delegate.didDeselectItem(index: sender.tag)
        }
        else
        {
           
            delegate.didSelectItem(index: sender.tag)
        }
        viewOverLay.isHidden = !btnPick.isSelected
    
    }
    func setSelection(status:Bool)
     {
         btnPick.isSelected = status
         viewOverLay.isHidden = !btnPick.isSelected
    }
    func getTime(duration:TimeInterval)->String
    {
        let time: String
        if duration > 3600 {
            time = String(format:"%2d:%02d:%02d",
                          Int(duration/3600),
                          Int((duration/60).truncatingRemainder(dividingBy: 60)),
                          Int(duration.truncatingRemainder(dividingBy: 60)))
        } else {
            time = String(format:"%02d:%02d",
                          Int((duration/60).truncatingRemainder(dividingBy: 60)),
                          Int(duration.truncatingRemainder(dividingBy: 60)))
        }
        
        return time
    }
   
}
class Header: UICollectionViewCell {
    
    var label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        self.addSubview(label)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = self.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct DeviceInfo {
    struct Orientation {
        // indicate current device is in the LandScape orientation
        static var isLandscape: Bool {
            get {
                return UIDevice.current.orientation.isValidInterfaceOrientation
                    ? UIDevice.current.orientation.isLandscape
                    : UIApplication.shared.statusBarOrientation.isLandscape
            }
        }
        // indicate current device is in the Portrait orientation
        static var isPortrait: Bool {
            get {
                return UIDevice.current.orientation.isValidInterfaceOrientation
                    ? UIDevice.current.orientation.isPortrait
                    : UIApplication.shared.statusBarOrientation.isPortrait
            }
        }
    }
}
