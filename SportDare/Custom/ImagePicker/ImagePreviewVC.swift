//
//  ImagePreviewVC.swift
//  photosApp2
//
//  Created by Muskan on 10/4/17.
//  Copyright © 2017 akhil. All rights reserved.
//

import UIKit
import Photos
import AVKit
class ImagePreviewVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,PreviewProtocol  {

    var myCollectionView: UICollectionView!
    var imgArray :[PHAsset]!
    var passedContentOffset = IndexPath()
    var playerViewController: AVPlayerViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isTranslucent = false
        self.view.backgroundColor=UIColor.black
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing=0
        layout.minimumLineSpacing=0
        layout.scrollDirection = .horizontal
        
        myCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        myCollectionView.delegate=self
        myCollectionView.dataSource=self
        myCollectionView.register(ImagePreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        myCollectionView.isPagingEnabled = true
        self.view.addSubview(myCollectionView)
        myCollectionView.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
        view.layoutIfNeeded()
        myCollectionView.scrollToItem(at: passedContentOffset, at: .right, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        view.layoutIfNeeded()
        
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayout = myCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        
        flowLayout.itemSize = myCollectionView.frame.size
        
        flowLayout.invalidateLayout()
        
        myCollectionView.collectionViewLayout.invalidateLayout()
    }
    override func viewDidAppear(_ animated: Bool) {
//         myCollectionView.scrollToItem(at: passedContentOffset, at: .right, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImagePreviewFullViewCell
        cell.imgView.image =  nil
        cell.delegate = self
        
       cell.setPreview_With(asset: (imgArray[indexPath.row]))
//        cell.imgView.image=imgArray[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let offset = myCollectionView.contentOffset
        let width  = myCollectionView.bounds.size.width
        
        let index = round(offset.x / width)
        let newOffset = CGPoint(x: index * size.width, y: offset.y)
        
        myCollectionView.setContentOffset(newOffset, animated: false)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.myCollectionView.reloadData()
            
            self.myCollectionView.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    func didTapPlay(asset: PHAsset) {
//        self.playVideo(view: self, asset: asset)
    }
    func didBeginZooming() {
        
    }
    func didEndZooming() {
        
    }
    func playVideo (view:UIViewController, asset:PHAsset) {
        
        guard (asset.mediaType == PHAssetMediaType.video)
            
            else {
                print("Not a valid video media type")
                return
        }
        PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (asset, audiomix, info) in
            let asset = asset as! AVURLAsset
            DispatchQueue.main.async {
                
                let player = AVPlayer(url: asset.url)
                self.playerViewController = AVPlayerViewController()
                self.playerViewController.player = player
                self.playerViewController.showsPlaybackControls = true
                self.playerViewController.view.frame = self.myCollectionView.bounds
                self.playerViewController.player!.play()
                self.myCollectionView.addSubview(self.playerViewController.view)
                
            }
        }
      
    }
}

@objc protocol PreviewProtocol{
    
  @objc optional  func didTapPlay(asset:PHAsset)
  @objc optional  func didTapPlay(mediaUrl:String)
  @objc optional  func didBeginZooming()
 @objc optional  func didEndZooming()
    
}
class ImagePreviewFullViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    var scrollImg: UIScrollView!
    var imgView: UIImageView!
    var playImage : UIButton!
    var delegate : PreviewProtocol!
    var asset :PHAsset!
    var playerViewController : AVPlayerViewController!
    var textView : UITextView?
    var post : Post_media?{
        didSet{
            guard let imgUrl = URL(string: (post?.media_url)!) else {
                return
            }
            self.textView?.text = post?.media_message
            self.imgView.sd_setImage(with: imgUrl, completed: nil)
            if post?.media_type == PostMediaType.video
            {
                scrollImg.gestureRecognizers?.forEach({ (gesture) in
                    scrollImg.removeGestureRecognizer(gesture)
                })
                scrollImg.maximumZoomScale = 1
                playImage.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "preview_image")
                Utilities.thumbnailForVideoAtURL(url: (post!.media_url?.url)!) { (image, error,url) in
                    if image != nil
                    {
                        DispatchQueue.main.async(execute: {
                             self.imgView.image = image
                            if  self.playerViewController != nil
                            {
                                self.playerViewController.view.removeFromSuperview()
                                self.playerViewController = nil
                            }
                        })
                       
                    }
                }
            }
            else
            {
                let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
                doubleTapGest.numberOfTapsRequired = 2
                scrollImg.addGestureRecognizer(doubleTapGest)
                playImage.isHidden =  true
                scrollImg.minimumZoomScale = 1.0
                scrollImg.maximumZoomScale = 4.0
            }
        }
    }
    
    var media : Media?{
        didSet{
            guard let imgUrl = URL(string: (media?.am_file)!) else {
                return
            }
            textView?.text = media!.am_title
            self.imgView.sd_setImage(with: imgUrl, completed: nil)
            if media?.am_file_type == PostMediaType.video
            {
                self.imgView.image = #imageLiteral(resourceName: "preview_image")
                Utilities.thumbnailForVideoAtURL(url: (media!.am_file?.url)!) { (image, error,url) in
                    if image != nil
                    {
                        self.imgView.image = image
                    }
                }
                scrollImg.gestureRecognizers?.forEach({ (gesture) in
                    scrollImg.removeGestureRecognizer(gesture)
                })
                
                scrollImg.maximumZoomScale = 1
                playImage.isHidden = false
      
            }
            else
            {
                let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
                doubleTapGest.numberOfTapsRequired = 2
                scrollImg.addGestureRecognizer(doubleTapGest)
                playImage.isHidden =  true
                scrollImg.minimumZoomScale = 1.0
                scrollImg.maximumZoomScale = 4.0
            }
        }
    }
    var file : Dare_files!{
        didSet{
            if file.df_file_type == PostMediaType.video
            {
                scrollImg.gestureRecognizers?.forEach({ (gesture) in
                    scrollImg.removeGestureRecognizer(gesture)
                })
                scrollImg.maximumZoomScale = 1
                playImage.isHidden = false
                self.imgView.image = #imageLiteral(resourceName: "preview_image")
                Utilities.thumbnailForVideoAtURL(url: (post!.media_url?.url)!) { (image, error,url) in
                    if image != nil
                    {
                        self.imgView.image = image
                    }
                }
            }
            else
            {
                let imgUrl = URL(string: (file.df_file_name)!)
                   self.imgView.sd_setImage(with: imgUrl, completed: nil)
                let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
                doubleTapGest.numberOfTapsRequired = 2
                scrollImg.addGestureRecognizer(doubleTapGest)
                playImage.isHidden =  true
                scrollImg.minimumZoomScale = 1.0
                scrollImg.maximumZoomScale = 4.0
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        scrollImg = UIScrollView()
        scrollImg.delegate = self
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = false
        scrollImg.flashScrollIndicators()
        
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 4.0
        
      
       
        self.addSubview(scrollImg)
     
        imgView = UIImageView()
        imgView.image = UIImage(named: "user3")
//        imgView.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin,.flexibleTopMargin]
        imgView.isUserInteractionEnabled = true
        playImage = UIButton()
        playImage.setImage(#imageLiteral(resourceName: "Play"), for: .normal)
        playImage.addTarget(self, action: #selector(self.didSelectPlay(_:)), for: .touchUpInside)
        playImage.isUserInteractionEnabled = true
        playImage.contentMode = .scaleAspectFit
        playImage.isHidden = true
        playImage.center = imgView.center
        imgView.addSubview(playImage)
        scrollImg.addSubview(imgView!)
    
        imgView.contentMode = .scaleAspectFit
       
       
        
    }
    @objc func didSelectPlay(_ sender:UIButton)
    {
        if post != nil
        {
            self.playVideo(videoUrl: (post?.media_url)!)
        }
        else if media != nil
        {
             self.playVideo(videoUrl: (media?.am_file)!)
        }
        else
        {
         self.playVideo(asset: self.asset)
        }
    }
    @objc func moreOptions(_ sender:UIButton)
    {
    
    }
    func playVideo ( asset:PHAsset) {
        
        guard (asset.mediaType == PHAssetMediaType.video)
            
            else {
                print("Not a valid video media type")
                return
        }
        PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (asset, audiomix, info) in
            let asset = asset as! AVURLAsset
            DispatchQueue.main.async {
                
                let player = AVPlayer(url: asset.url)
                self.playerViewController = AVPlayerViewController()
                self.playerViewController.player = player
                self.playerViewController.showsPlaybackControls = true
                self.playerViewController.view.frame = self.imgView.bounds
                self.playerViewController.player!.play()
                self.addSubview(self.playerViewController.view)
                
            }
        }
        
    }
    
    func playVideo ( videoUrl:String) {
        
        DispatchQueue.main.async {
            
            let player = AVPlayer(url: URL(string: videoUrl)!)
            self.playerViewController = AVPlayerViewController()
            self.playerViewController.player = player
            self.playerViewController.showsPlaybackControls = true
            self.playerViewController.view.frame = self.imgView.bounds
            self.playerViewController.entersFullScreenWhenPlaybackBegins = true
            self.playerViewController.exitsFullScreenWhenPlaybackEnds = true
            self.playerViewController.player!.play()
           
            self.addSubview(self.playerViewController.view)
            
        }
        
    }
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
      
            if scrollImg.zoomScale == 1 {
                scrollImg.zoom(to: zoomRectForScale(scale: scrollImg.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
            } else {
                scrollImg.setZoomScale(1, animated: true)
            }
        
    }
  
    func setPreview_With(asset:PHAsset)
    {
        self.asset = asset
        if  self.playerViewController != nil
        {
             self.playerViewController.view.removeFromSuperview()
             self.playerViewController = nil
        }
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.resizeMode = .exact
        PHCachingImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: options) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            if let image = image {
                self.imgView.image=image
            }
        }
        if asset.mediaType == .image
        {
        playImage.isHidden =  true
            scrollImg.minimumZoomScale = 1.0
            scrollImg.maximumZoomScale = 4.0
        }
        else
        {
         scrollImg.maximumZoomScale = 1
         playImage.isHidden = false
        }
    }
    func pauseVideo(){
        if self.playerViewController != nil
        {
            self.playerViewController.player?.pause()
        }
    }
    func setPreviewWithUrll(url:String,mediaType:String)
    {
     
        
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
    }
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        delegate?.didBeginZooming!()
      
        
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
       if scrollView.zoomScale == 1
       {
        delegate?.didEndZooming!()
       
        }
    }
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imgView.frame.size.height / 2;
        zoomRect.size.width  = imgView.frame.size.width  / 2;
        let newCenter = imgView.convert(center, from: scrollImg)
        zoomRect.origin.x = center.x - ((zoomRect.size.width / 2.0));
        zoomRect.origin.y = center.y - ((zoomRect.size.height / 2.0));
        return zoomRect
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgView
    }
    
    override func layoutSubviews() {
        scrollImg.frame = CGRect(x: 0, y:0, width: self.bounds.width, height: self.bounds.height)
        imgView.frame = CGRect(x: 0, y:0, width: scrollImg.bounds.width, height: scrollImg.bounds.height)
        playImage.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        playImage.center = imgView.center
         super.layoutSubviews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        scrollImg.setZoomScale(scrollImg.minimumZoomScale, animated: true)
        scrollImg.frame = CGRect(x: 0, y:0, width: self.bounds.width, height: self.bounds.height)
        imgView.frame = CGRect(x: 0, y:0, width: scrollImg.bounds.width, height: scrollImg.bounds.height)
        playImage.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        playImage.center = imgView.center
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

