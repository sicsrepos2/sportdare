//
//  CustomView.swift
//  ThatsMyHairstylist
//
//  Created by SRISHTI INNOVATIVE on 16/09/1937 Saka.
//  Copyright © 1937 Saka SRISHTI INNOVATIVE. All rights reserved.
//

import Foundation
import UIKit
//@IBDesignable

class CustomTable : UITableView {
    
    @IBInspectable var emptyImage: UIImage?
        {
        didSet
        {

        }
    }
    override func reloadData() {
                   if self.numberOfRows(inSection: 0) == 0
                   {
                    setEmpty()
                    }
                    else
                   {
                    self.backgroundView = nil
                    }
    }
    func setEmpty()
    {
        let bgView = UIView()
        let ImageView =  UIImageView(image: emptyImage)
        ImageView.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
        ImageView.center = CGPoint(x: UIScreen.main.bounds.midX, y:120)
        ImageView.contentMode = .scaleAspectFit
        bgView.addSubview(ImageView)
        self.backgroundView = bgView
    }
}
class CustomView : UIView
{
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0
        {
        didSet
        {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet
        {
            layer.cornerRadius = cornerRadius
        }
    }
}

class CustomImageView : UIImageView
{
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0
        {
        didSet
        {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet
        {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var imageColor: UIColor!{
        didSet {
         
            self.image = self.image!.tinted(with: imageColor!)
         
        }
    }
}
