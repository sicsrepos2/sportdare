//
//  BNCustomCalendar.swift
//  testProject
//
//  Created by Binoy T on 02/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import Foundation
//@IBDesignable
 protocol CalendarProtocol {
    func didSelectDate(date:Date)
    func didChangedMonth(calendar:BNCustomCalendar)
    func didSelectDare(dare:[DareData])
}
class BNCustomCalendar: UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var CalendarView: UICollectionView!
    @IBOutlet weak var MonthLabel: UILabel!
    var calendarDelegate : CalendarProtocol?
    var contentView : UIView?
    let date = Date()
    let calendar = Calendar.current

    let day = Calendar.current.component(.day , from: Date())
    var weekday = Calendar.current.component(.weekday, from: Date()) - 1
    var month:Int = Calendar.current.component(.month, from: Date()) - 1
    var year = Calendar.current.component(.year, from: Date())
    let Months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    let DaysOfMonth = ["Monday","Thuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    
    var DaysInMonths = [31,28,31,30,31,30,31,31,30,31,30,31]
    
    var currentMonth = String()
   
    
    var NumberOfEmptyBox = Int()
    
    var NextNumberOfEmptyBox = Int()
    
    var PreviousNumberOfEmptyBox = 0
    
    var Direction = 0
    
    var PositionIndex = 0
    
    var LeapYearCounter = 2
    
    var dayCounter = 0
    var daysInMonth = [CalendarDate]()
    var dares : [UserDare]!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        contentView = loadViewFromNib()
        
        contentView!.frame = bounds
     
                self.CalendarView.register(UINib(nibName: "CalendarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Calendar")
                self.CalendarView.delegate = self
                self.CalendarView.dataSource = self
       
                currentMonth = Months[month]
                MonthLabel.text = "\(currentMonth) \(year)"
                if weekday == 0 {
                    weekday = 7
                }
        let layout = UICollectionViewFlowLayout()
       
        CalendarView.collectionViewLayout = layout
                GetStartDateDayPosition()
                setCalendarDates()
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView!)
    }
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CalendarView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let layout =  CalendarView.collectionViewLayout as? UICollectionViewFlowLayout
        {
           layout.itemSize = CGSize(width: (CalendarView.bounds.width / 7.0) , height: CalendarView.bounds.height/6)
            CalendarView.collectionViewLayout = layout
        }

        print(CalendarView.bounds)
    }
    
    
    

    func GetStartDateDayPosition() {
        switch Direction{
        case 0:
            NumberOfEmptyBox = weekday
            dayCounter = day
            while dayCounter>0 {
                NumberOfEmptyBox = NumberOfEmptyBox - 1
                dayCounter = dayCounter - 1
                if NumberOfEmptyBox == 0 {
                    NumberOfEmptyBox = 7
                }
            }
            if NumberOfEmptyBox == 7 {
                NumberOfEmptyBox = 0
            }
            PositionIndex = NumberOfEmptyBox
        case 1...:
            NextNumberOfEmptyBox = (PositionIndex + DaysInMonths[month])%7
            PositionIndex = NextNumberOfEmptyBox
            
        case -1:
            PreviousNumberOfEmptyBox = (7 - (DaysInMonths[month] - PositionIndex)%7)
            if PreviousNumberOfEmptyBox == 7 {
                PreviousNumberOfEmptyBox = 0
            }
            PositionIndex = PreviousNumberOfEmptyBox
        default:
            fatalError()
        }
    }
    
    //--------------------------------------------------(Next and back buttons)-------------------------------------------------------------
    
    @IBAction func Next(_ sender: Any) {
        switch currentMonth {
        case "December":
            Direction = 1
            
            month = 0
            year += 1
            
            if LeapYearCounter  < 5 {
                LeapYearCounter += 1
            }
            
            if LeapYearCounter == 4 {
                DaysInMonths[1] = 29
            }
            
            if LeapYearCounter == 5{
                LeapYearCounter = 1
                DaysInMonths[1] = 28
            }
            GetStartDateDayPosition()
            
            currentMonth = Months[month]
            MonthLabel.text = "\(currentMonth) \(year)"
            
            setCalendarDates()
        default:
            Direction = 1
            
            GetStartDateDayPosition()
            
            month += 1
            
            currentMonth = Months[month]
            MonthLabel.text = "\(currentMonth) \(year)"
            setCalendarDates()
           
        }
        calendarDelegate?.didChangedMonth(calendar: self)
    }
    
    @IBAction func Back(_ sender: Any) {
        switch currentMonth {
        case "January":
            Direction = -1
            
            month = 11
            year -= 1
            
            if LeapYearCounter > 0{
                LeapYearCounter -= 1
            }
            if LeapYearCounter == 0{
                DaysInMonths[1] = 29
                LeapYearCounter = 4
            }else{
                DaysInMonths[1] = 28
            }
            
            GetStartDateDayPosition()
            
            currentMonth = Months[month]
            MonthLabel.text = "\(currentMonth) \(year)"
             setCalendarDates()
            
        default:
            Direction = -1
            
            month -= 1
            
            GetStartDateDayPosition()
            
            currentMonth = Months[month]
            MonthLabel.text = "\(currentMonth) \(year)"
            setCalendarDates()
        }
        
          calendarDelegate?.didChangedMonth(calendar: self)
    }
    
    func setCalendarDates()
    {
        var totalDays = 0
        var emptyDays = 0
        daysInMonth.removeAll()
        switch Direction{
        case 0:
            totalDays =  DaysInMonths[month] + NumberOfEmptyBox
            emptyDays = NumberOfEmptyBox
        case 1...:
            totalDays =  DaysInMonths[month] + NextNumberOfEmptyBox
             emptyDays = NextNumberOfEmptyBox
        case -1:
            totalDays =  DaysInMonths[month] + PreviousNumberOfEmptyBox
             emptyDays = PreviousNumberOfEmptyBox
        default:
            fatalError()
        }
        for var day in 1...totalDays
        {
          daysInMonth.append(CalendarDate(day: day - emptyDays, month:month+1, year: year))
        }
        CalendarView.reloadData()
    }
    func setDares(dares:[UserDare])
    {
        self.dares = dares
        self.dares.forEach { (dare) in
            let dareDates =   self.daysInMonth.filter({
                if $0.isValid {
                    print($0.date.dateOnlyYYYY_MM_dd == dare.dareDate)
                    print($0.date.dateOnlyYYYY_MM_dd)
                    return $0.date.dateOnlyYYYY_MM_dd == dare.dareDate
                }
                return false
            })
            if dareDates.count  > 0
            {
                
                dareDates.forEach({ (date) in
                    date.dares = dare.dareData
                })
            }
            
        }
        CalendarView.reloadData()
    }
    
    //----------------------------------(CollectionView)------------------------------------------------------------------------------------
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return daysInMonth.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Calendar", for: indexPath) as! CalendarCollectionViewCell
        let cellday = daysInMonth[indexPath.row]
        cell.day = cellday
        cell.DateLabel.textColor = UIColor.black
        if cellday.isValid
        {
           cell.isHidden = false
            cell.DateLabel.text = "\(cellday.day)"
        }
        else
        {
           cell.DateLabel.text = ""
           cell.isHidden = true
        }
        
        
        if currentMonth == Months[calendar.component(.month, from: date) - 1] && year == calendar.component(.year, from: date) && indexPath.row + 1 - NumberOfEmptyBox == day{
//            cell.Circle.isHidden = false
//            cell.DrawCircle()
            
        }
        return cell
    }
    
    func checkAndReturnColor(day:CalendarDate)->UIColor
    {
        if let dare = dares.filter({$0.dareDate == day.date.dateOnlyYYYY_MM_dd}).last
        {
            let lastDare = dare.dareData?.last
            
            return UIColor.red
        }
        else
        {
            return UIColor.white
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        calendarDelegate?.didSelectDate(date: daysInMonth[indexPath.row].date)
        if dares != nil
        {
            if let dare = dares.filter({$0.dareDate == daysInMonth[indexPath.row].date.dateOnlyYYYY_MM_dd}).last
            {
                calendarDelegate?.didSelectDare(dare: dare.dareData!)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
