//
//  Date.swift
//  testProject
//
//  Created by Binoy T on 05/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
class CalendarDate{
    var dareStatus : DareStatus!
    var dareName : String?
    var date : Date!
    var day = 0
    var isValid = true
    var dares : [DareData]?{
        didSet{
            self.dareStatus = (dares!.last?.d_satus).map { DareStatus(rawValue: $0)! }
            self.dareName = dares!.last!.sc_name
        }
    }
    
    
    init(day:Int,month:Int,year:Int) {
        if day > 0
        {
            var dateComponent = DateComponents()
            dateComponent.year = year
            dateComponent.month = month
            dateComponent.day = day
            
            self.date =  Calendar.current.date(from: dateComponent)
            self.day = day
            self.isValid = true
            self.dareName = ""
            self.dareStatus =  DareStatus.noDare
        }
        else
        {
            self.isValid = false
        }
        
    }
    
}
enum  DareStatus : String {
    case pending = "pending"
    case accepted = "accepted"
    case completed = "completed"
    case rejected = "rejected"
    case noDare = "noDare"
    case replied = "replied"
    case failed  = "failed"
    case declined = "declined"
    
    var color: UIColor {
        switch self {
        case .pending: return .red
        case .accepted: return .appBase
        case .completed: return UIColor(hexString: "#E6CD00")
        case .rejected: return .appGrey
        case .noDare: return .white
        case .replied: return UIColor(hexString: "#E6CD00")
        case .failed:
            return UIColor(hexString: "#E6CD00")
        case .declined : return .appGrey
        }
    }
}
