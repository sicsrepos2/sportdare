//
//  CalendarCollectionViewCell.swift
//  testProject
//
//  Created by Binoy T on 05/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var btnMultiples: CustomButton!
    @IBOutlet weak var labelDare: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBInspectable var borderWidth: CGFloat = 0
        {
        didSet
        {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
           
        }
    }
    
    var day : CalendarDate! {
        didSet{
            self.labelDare.text = day.dareName
            if day.dares != nil
            {
            btnMultiples.isHidden = !((day.dares?.count)! > 1)
            self.backgroundColor = day.dareStatus.color
            }
            else
            {
                 btnMultiples.isHidden = true
                self.backgroundColor = UIColor.white 
            }
           
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        print(self.frame.size)

    }

}
