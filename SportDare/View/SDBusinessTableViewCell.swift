//
//  SDBusinessTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 07/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage

class SDBusinessTableViewCell: UITableViewCell {

    @IBOutlet weak var labelBusinessType: UILabel!
    @IBOutlet weak var labelBusinessName: UILabel!
    @IBOutlet weak var businessImageView: CustomImageView!
    var business : Business!{
        didSet{
            labelBusinessName.text = business.name
            labelBusinessType.text = business.cat_name
            businessImageView.sd_setImage(with: business.image?.url, placeholderImage: UIImage(named:"Placeholder"), options: .continueInBackground, completed: nil)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
