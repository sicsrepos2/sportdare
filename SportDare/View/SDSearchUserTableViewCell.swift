//
//  SDSearchUserTableViewCell.swift
//  Sportdare
//
//  Created by Binoy T on 20/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import DLRadioButton
class SDSearchUserTableViewCell: UITableViewCell {
    @IBOutlet weak var selectionBtn: DLRadioButton!
    @IBOutlet weak var userImageView: SDImageViewShield!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelTeamName: UILabel!
    @IBOutlet weak var teamImageView: UIImageView!
    var userDetail : SearchUserDetail? {
        didSet{
            userImageView.setImagewith_Url(url:userDetail!.image!, PlaceHolderImage: #imageLiteral(resourceName: "Placeholder.png"))
            if userDetail?.sheild?.name == "Grey"
            {
                userImageView.shieldImageView.image = ShieldType(rawValue: userDetail!.sheild!.shield_stars!)?.image
                
            }
            else
            {
                userImageView.shieldImageView.image =  ShieldType(rawValue: userDetail!.sheild!.name!)?.image
                
            }
           
            labelUserName.text = "\(userDetail!.first_name!) \(userDetail!.surname!)"
            
        }
    }
    
    var root : Root? {
        didSet{
            userImageView.setImagewith_Url(url:root!.image!, PlaceHolderImage: #imageLiteral(resourceName: "Placeholder.png"))
            labelUserName.text = "\(root!.first_name!) \(root!.surname!)"
            
        }
    }
    
    var team : Team? {
        
        didSet {
            labelTeamName.text = team?.team_name
            teamImageView!.sd_setImage(with: team?.team_image!.url, completed: nil)
            
        }
    }
        var isMultiSelect : Bool? = false {
        didSet{
       
            selectionBtn.isHidden = !isMultiSelect!
        }
    }
    
    var chat : ChatList?{
        didSet{
            userImageView.setImagewith_Url(url:chat!.image!, PlaceHolderImage: #imageLiteral(resourceName: "Placeholder.png"))
            labelUserName.text = "\(chat!.first_name!) \(chat!.surname!)"
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
