//
//  SDChatt_CommentTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 28/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SDChatt_CommentTableViewCell: UITableViewCell {

    @IBOutlet var imageChat:UIImageView!
    @IBOutlet var imageIsSend:UIImageView!
    @IBOutlet var labelChat:ChatLabel!
    @IBOutlet var imageChatUser:SDImageViewShield!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelName: UILabel!
    var comment:Comment?{
        didSet{
            labelChat.text = comment?.comment
            labelTime.text = Utilities.getTimeStamp(date: comment?.comment_time)
        
            labelName.text = (comment?.commented_by?.first_name)! + " " + (comment?.commented_by?.surname)!
            imageChatUser.setImagewith_Url(url: (comment?.commented_by?.image)!, PlaceHolderImage: UIImage(named: "dumy"))
            
        }
    }
    var chat :Chat!{
        didSet{
            labelChat.text = chat.message
            labelTime.text = Utilities.getTimeStamp(date:chat.timestamp!)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
