//
//  SD_PB_AwardsTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 14/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage
class SD_PB_AwardsTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var awardImageView: UIImageView!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var pbImageView: UIImageView!
    var award : Award? {
        didSet{
            labelTitle.text = award?.name
            awardImageView.sd_setImage(with: award?.image?.url, placeholderImage: UIImage(named: "Placeholder"), options: .continueInBackground, completed: nil)
        }
    }
    var personalBest : PersonalBest? {
        didSet{
            labelTitle.text = personalBest?.name
//            pbImageView.sd_setImage(with: personalBest?.image?.url, placeholderImage: UIImage(named: "Placeholder"), options: .continueInBackground, completed: nil)
            labelDescription.text = personalBest?.description
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
