//
//  SDPreviewCollectionViewCell.swift
//  Sportdare
//
//  Created by Binoy T on 09/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import Photos
import TLPhotoPicker
protocol PreviewCellProtocol
{
   func didSelectDelete( at index:Int)
}
class SDPreviewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewVideoInfo: UIView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    var delegate : PreviewCellProtocol!
    var member : Root? {
        didSet{
            self.thumbImage.sd_setImage(with: member?.image?.url, completed: nil)
        }
    }
    var teamMember : Team_members? {
        didSet{
            self.thumbImage.sd_setImage(with: teamMember?.u_profile_image?.url, completed: nil)
        }
    }
    var dareFile : Dare_files?{
        didSet{
            if (dareFile?.df_file_type?.contains("image"))!
            {
                viewVideoInfo.isHidden = true
                
            self.thumbImage.sd_setImage(with: dareFile?.df_file_name?.url, placeholderImage: UIImage(named: "preview_image"), options: .highPriority, completed: nil)
            }
            else
            {
               viewVideoInfo.isHidden = true
                Utilities.thumbnailForVideoAtURL(url: (dareFile?.df_file_name?.url)!) { (image, error,url) in
                    if image != nil
                    {
                        self.thumbImage.image = image
                    }
                    else
                    {
                        
                        self.thumbImage.image = nil
                        self.thumbImage.image = #imageLiteral(resourceName: "preview_image")
                        
                        print(error!)
                    }
                }
            }
        }
    }
    func setPreview(asset:PHAsset)
    {
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.clear.cgColor
        PHImageManager.default().requestImage(for: asset, targetSize: self.frame.size, contentMode: .aspectFill, options: nil) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            self.thumbImage.image = image
            if asset.mediaType == .image
            {
                self.viewVideoInfo.isHidden = true
                
            }
            else
            {
                self.viewVideoInfo.isHidden = false
                 self.lblDuration.text = self.getTime(duration: asset.duration)
            }
        }
        
    }
    @IBAction func didSelectDelete(_ sender: UIButton) {
        delegate.didSelectDelete(at: sender.tag)
    }
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
      
    }
    func setSelection()
    {
        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.appBase.cgColor
    }
    
    func getTime(duration:TimeInterval)->String
    {
        let time: String
        if duration > 3600 {
            time = String(format:"%2d:%02d:%02d",
                          Int(duration/3600),
                          Int((duration/60).truncatingRemainder(dividingBy: 60)),
                          Int(duration.truncatingRemainder(dividingBy: 60)))
        } else {
            time = String(format:"%02d:%02d",
                          Int((duration/60).truncatingRemainder(dividingBy: 60)),
                          Int(duration.truncatingRemainder(dividingBy: 60)))
        }
        
        return time
    }
}
