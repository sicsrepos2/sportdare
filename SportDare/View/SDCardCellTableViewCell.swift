//
//  SDCardCellTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 21/06/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage

class SDCardCellTableViewCell: UITableViewCell {
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var labelCardName: UILabel!
    
    @IBOutlet weak var labelGambits: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
    
    var card: Card_details!{
        didSet{
            cardImageView.sd_setImage(with: card.card_image?.url, completed: nil)
            labelCardName.text = card.card_name?.capitalized
            labelGambits.text = "Gambits : \(card.card_amount!)"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
