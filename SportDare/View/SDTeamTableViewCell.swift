//
//  SDTeamTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 15/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage
class SDTeamTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewTeam: UIImageView!
    
    @IBOutlet weak var labelTeamName: UILabel!
    var team : Team? {
        didSet{
            imageViewTeam.sd_setImage(with: team?.team_image?.url, placeholderImage: #imageLiteral(resourceName: "team_icon"), options: .continueInBackground, completed: nil)
            labelTeamName.text = team?.team_name
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
