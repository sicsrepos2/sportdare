//
//  SDFeedTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 07/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SDFeedTableViewCell: UITableViewCell,UITextViewDelegate {
    @IBOutlet weak var userProfileImage: SDImageViewShield!
    @IBOutlet weak var postNameLbl: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var delegate : TimeLineCellProtocol?
    @IBOutlet weak var lblFeedComment: UILabel!
    @IBOutlet weak var lblHighFive: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var collectionViewMedia: UICollectionView!
    @IBOutlet weak var playerNameBtn: UIButton!
    @IBOutlet weak var btnHighFive: UIButton!
    @IBOutlet weak var btnComments: UIButton!
    @IBOutlet weak var textViewDescription: ReadMoreTextView!
   
    @IBOutlet weak var feedImageView: UIImageView!
    var feedDelegate : FeedCellProtocol?
  
    var index : Int!{
        didSet{
            userProfileImage.tag = index
            btnHighFive?.tag = index
            btnComments?.tag = index
        }
    }
    var feed:Feed!{
        didSet{
            
            btnHighFive.isSelected = feed.is_clapped!
            lblHighFive.text = "\(feed.claps_count!)"
            lblComments.text = "\(feed.comments_count!) Comments"
             textViewDescription.font = UIFont(name: "Arial", size: 14)
             textViewDescription.delegate = nil
             lblTime.text = Utilities.getTimeStamp(date:feed.time)
        
                postNameLbl.text  = "\(feed.user!.userData!.first_name!) \( feed.user!.userData!.surname!)"
                textViewDescription.text = feed.referance_text
                userProfileImage.setImagewith_Url(url: feed.user!.userData!.image!, PlaceHolderImage: UIImage(named: "dumy")!)
            switch feed.activity_type {
            case FeedType.postMedia:
                collectionViewMedia.delegate = self
                collectionViewMedia.dataSource = self
                let flowLayout = SquareFlowLayout()
                flowLayout.numberOfColumns = Utilities.getNumberOfColoumns(itemCount: (feed.post!.post_media?.count)!)
                flowLayout.numberOfRows = Utilities.getNumberOfRows(itemCount:(feed.post!.post_media?.count)! )
                flowLayout.totalItemCount = (feed.post!.post_media?.count)!
                flowLayout.flowDelegate = self
                self.collectionViewMedia.collectionViewLayout = flowLayout
                
                collectionViewMedia.reloadData()
                textViewDescription.font = UIFont(name: "Arial", size: 14)
            case FeedType.athleteAwards,FeedType.athlete_PB,FeedType.athleteLevel:
                textViewDescription.font = UIFont(name:"Arial-BoldMT", size: 15)
            case FeedType.post_text:
                textViewDescription.text = feed.post?.post_message
                textViewDescription.font = UIFont(name: "Arial", size: 14)
            default:
                
                break
            }
            textViewDescription.setNeedsUpdateTrim()
            textViewDescription.layoutIfNeeded()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        guard let collectionView = self.collectionViewMedia else{
            
            return
        }
        guard let flowLayout = collectionView.collectionViewLayout as? SquareFlowLayout else {
            collectionView.collectionViewLayout.invalidateLayout()
            return
        }
        flowLayout.invalidateLayout()
//        textViewDescription.setNeedsUpdateTrim()
//        textViewDescription.layoutIfNeeded()
//        collectionViewMedia.layoutIfNeeded()
        
        
    }
    
    override func layoutSubviews() {
        textViewDescription.layoutSubviews()
        guard let collectionView = self.collectionViewMedia else{
            
            return
        }
        collectionView.layoutSubviews()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textViewDescription.onSizeChange = { _ in }
        textViewDescription.shouldTrim = true
    }
    
   
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return false
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return false
    }
    
}
extension SDFeedTableViewCell: SquareFlowLayoutDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  feed.post?.post_media?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let postData = feed.post!.post_media![indexPath.item]
        var reuseId = ""
        switch postData.media_type {
        case PostMediaType.image:
            reuseId = CollectionViewCellId.imageCell
        case PostMediaType.video:
            reuseId = CollectionViewCellId.videoCell
        default:
            reuseId = CollectionViewCellId.imageCell
        }
        if indexPath.row >= 4
        {
            if (feed.post!.post_media?.count)! > 5
            {
                reuseId = CollectionViewCellId.showMoreCell
            }
            else
            {
                
            }
        }
        
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as? SDTimeLineMediaCollectionViewCell)!
        cell.postmedia = feed.post!.post_media![indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return Utilities.getSizeForItem(at: indexPath, totalItemCount: (feed.post!.post_media?.count)!, collectionView: collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectPost!(post_id:feed.activity_id!,index: indexPath)
    }
    
    func shouldExpandItem(at indexPath: IndexPath) -> Bool {
        return Utilities.getLayoutValuesFor(itemCount: (feed.post!.post_media?.count)!)[indexPath.item] == Utilities.CellType.expanded
    }
    
}
