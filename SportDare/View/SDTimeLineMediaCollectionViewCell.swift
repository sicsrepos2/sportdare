//
//  SDTimeLineMediaCollectionViewCell.swift
//  Sportdare
//
//  Created by Binoy T on 30/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage
class SDTimeLineMediaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var labelMoreCount: UILabel!
    var postmedia : Post_media! {
        didSet{
            if postmedia.media_type == PostMediaType.image
            {
                thumbImageView.sd_setImage(with:  postmedia.media_url?.url, placeholderImage: #imageLiteral(resourceName: "preview_image"), options: .continueInBackground) { (image, error, cacheType, url) in
                    if image != nil
                    {
                        NotificationCenter.default.post(name: NSNotification.Name.didUpdateSize, object: image?.size)
                    }
                }
                
               
            }
            else
            {
                 self.thumbImageView.image = #imageLiteral(resourceName: "preview_image")
//                 self.thumbImageView.image = Utilities.thumbnailImageFor(fileUrl: (postmedia.media_url?.url)!)
                Utilities.thumbnailForVideoAtURL(url: (postmedia.media_url?.url)!) { (image, error,url) in
                    if image != nil
                    {
                        self.thumbImageView.image = image
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.thumbImageView.image = nil
                            self.thumbImageView.image = #imageLiteral(resourceName: "preview_image")
                        }
                     
                        
                        print(error)
                    }
                }
                
            }
            
        }
    }
    
    var media : Media? {
        didSet{
            if (media!.am_file_type?.contains("image"))!
            {
                thumbImageView.sd_setImage(with:  media!.am_file?.url, placeholderImage: #imageLiteral(resourceName: "preview_image"), options: .continueInBackground) { (image, error, cacheType, url) in
                    if image != nil
                    {
                        NotificationCenter.default.post(name: NSNotification.Name.didUpdateSize, object: image?.size)
                    }
                }
                
                
            }
            else
            {
                self.thumbImageView.image = #imageLiteral(resourceName: "preview_image")
                Utilities.thumbnailForVideoAtURL(url: (media!.am_file?.url)!) { (image, error,url) in
                    if image != nil
                    {
                        self.thumbImageView.image = image
                    }
                }
                
            }
        }
    }
    
}



class CustomLayout: UICollectionViewFlowLayout {
    
    // here you can define the height of each cell
    let itemHeight: CGFloat = 120
    var numBerOfItems = 1
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    /**
     Sets up the layout for the collectionView. 1pt distance between each cell and 1pt distance between each row plus use a vertical layout
     */
    func setupLayout() {
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .vertical
    }
    
    /// here we define the width of each cell, creating a 2 column layout. In case you would create 3 columns, change the number 2 to 3
    func itemWidth() -> CGFloat {
        return (collectionView!.frame.width/2)-1
    }
    
    override var itemSize: CGSize {
        set {
            
            self.itemSize = CGSize(width: itemWidth(), height: collectionView!.frame.height)
        }
        get {
            return CGSize(width:itemWidth(),height: itemHeight)
//             Utilities.getSizeForItem(at: indexPath, totalItemCount: (post.post_media?.count)!, collectionView: collectionView)
        }
       
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
