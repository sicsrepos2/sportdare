//
//  SDDareListTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 14/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SDDareListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labeNameWho: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelSportName: UILabel!
    @IBOutlet weak var labelgambits: UILabel!
    var dare : DareData!{
        didSet{
            labelSportName.text = dare.sc_name
            labelTime.text = dare.d_time?.toAppTime
            labelDate.text = dare.d_date?.toAppDate
            labelgambits.text = dare.d_gambits
            labeNameWho.text = (dare.d_created_by?.first_name)! + " " +  (dare.d_created_by?.surname)!
            let dareStatus = (dare.d_satus).map { DareStatus(rawValue: $0)! }
            labelStatus.text = dareStatus?.rawValue.capitalized
            labelStatus.textColor = dareStatus?.color
            
            
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
