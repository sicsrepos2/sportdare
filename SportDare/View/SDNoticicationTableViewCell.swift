//
//  SDNoticicationTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 25/07/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import SDWebImage
class SDNoticicationTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var imageview: SDImageViewShield!
    @IBOutlet weak var labelNotification: UILabel!
    var notification:Notifications!{
        didSet{
            let userDetail = notification.relation_user_data
            labelNotification.text = notification.message
            imageview.profileImageView.sd_setImage(with: userDetail?.image?.url, placeholderImage: UIImage(named: "dumy"), options: SDWebImageOptions.highPriority, completed: nil)
            if userDetail?.sheild?.name == "Grey"
            {
                imageview.shieldImageView.image = ShieldType(rawValue: userDetail!.sheild!.shield_stars!)?.image
                
            }
            else
            {
                imageview.shieldImageView.image =  ShieldType(rawValue: userDetail!.sheild!.name!)?.image
                
            }
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
