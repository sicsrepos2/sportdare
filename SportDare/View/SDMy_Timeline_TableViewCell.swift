//
//  SDMy_Timeline_TableViewCell.swift
//  Sportdare
//
//  Created by Binoy T on 29/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
@objc protocol TimeLineCellProtocol
{
    @objc optional func didSelectPost(post_id:String,index:IndexPath)
    @objc optional func didSelectMore(post_Id:String)
}
class SDMy_Timeline_TableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    
    @IBOutlet weak var userProfileImage: SDImageViewShield!
    @IBOutlet weak var postNameLbl: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var delegate : TimeLineCellProtocol?

    @IBOutlet weak var lblHighFive: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var collectionViewMedia: UICollectionView!
    @IBOutlet weak var playerNameBtn: UIButton!
    @IBOutlet weak var btnHighFive: UIButton!
    @IBOutlet weak var btnComments: UIButton!
    
    @IBOutlet weak var textViewDescription:                                                                                       ReadMoreTextView!

    var post : Posts! {
        didSet {
              textViewDescription.text = post.post_message
             textViewDescription.font = UIFont(name: "Arial", size: 14)
//            textViewDescription.shouldTrim = true
            if post.post_type == PostType.text
            {
              textViewDescription.maximumNumberOfLines = 5
            }
            else if post.post_type == PostType.media
            {
                textViewDescription.maximumNumberOfLines = 4
                collectionViewMedia.delegate = self
                collectionViewMedia.dataSource = self
                let flowLayout = SquareFlowLayout()
                flowLayout.numberOfColumns = getNumberOfColoumns(itemCount: (post.post_media?.count)!)
                flowLayout.numberOfRows = getNumberOfRows(itemCount:(post.post_media?.count)! )
                flowLayout.totalItemCount = (post.post_media?.count)!
                flowLayout.flowDelegate = self
                self.collectionViewMedia.collectionViewLayout = flowLayout

                collectionViewMedia.reloadData()
            }
          
            postNameLbl.text  = "\(UserBase.currentUser!.details!.name!) \(UserBase.currentUser!.details!.surName!)"
            lblTime.text = Utilities.getTimeStamp(date:post.added_on!)
            lblComments.text = "\(post.post_comments_count!) Comments"
            lblHighFive.text = "\(post.post_claps_count!)"
            btnHighFive.isSelected = post.is_clapped ?? false
          
            userProfileImage.setImagewith_Url(url: (UserBase.currentUser?.details?.image)!, PlaceHolderImage: UIImage(named: "dumy")!)
            textViewDescription.setNeedsUpdateTrim()
            textViewDescription.layoutIfNeeded()
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
       
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        textViewDescription.layoutSubviews()
//        
//    }
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        guard let collectionView = self.collectionViewMedia else{
          
            return
        }
            guard let flowLayout = collectionView.collectionViewLayout as? SquareFlowLayout else {
                  collectionView.collectionViewLayout.invalidateLayout()
                return
            }
            flowLayout.invalidateLayout()
        textViewDescription.setNeedsUpdateTrim()
        textViewDescription.layoutIfNeeded()
       
       
    }
    func getNumberOfColoumns(itemCount:Int)->Int
    {
        switch itemCount {
        case 1:
            return 1
        case 2,3,4,5:
            return 2
            
        default:
            return 2
        }
    }
    func getNumberOfRows(itemCount:Int)->Int
    {
        switch itemCount {
        case 1,2:
            return 1
        case 3,4,5:
            return 2
            
        default:
            return 2
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func moreAction(_ sender: UIButton) {
        delegate?.didSelectMore?(post_Id: post.post_id!)
    }
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (post.post_media?.count)! <= 5 ? (post.post_media?.count)! : 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let postData = post.post_media![indexPath.item]
        var reuseId = ""
        switch postData.media_type {
        case PostMediaType.image:
            reuseId = CollectionViewCellId.imageCell
        case PostMediaType.video:
            reuseId = CollectionViewCellId.videoCell
        default:
            reuseId = CollectionViewCellId.imageCell
        }
        if indexPath.row >= 4
        {
            if (post.post_media?.count)! > 5
            {
                reuseId = CollectionViewCellId.showMoreCell
            }
            else
            {
                
            }
        }
        
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as? SDTimeLineMediaCollectionViewCell)!
        cell.postmedia = post.post_media![indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return Utilities.getSizeForItem(at: indexPath, totalItemCount: (post.post_media?.count)!, collectionView: collectionView)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectPost!(post_id:post.post_id!,index: indexPath)
    }
    
}
extension SDMy_Timeline_TableViewCell: WBGridViewLayoutDelegate,SquareFlowLayoutDelegate {
    func colectionView(_ collectionView: UICollectionView, numberOfItemsInRow row: Int) -> CellLayout {
        if row == 3
        {
        return CellLayout.ThreeLeft
        }
        return .Two
    }
    func shouldExpandItem(at indexPath: IndexPath) -> Bool {
       return Utilities.getLayoutValuesFor(itemCount: (post.post_media?.count)!)[indexPath.item] == Utilities.CellType.expanded
    }
  
}
