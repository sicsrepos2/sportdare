//
//  SD_Player_Timeline_TableviewCell.swift
//  Sportdare
//
//  Created by SICS on 23/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import Foundation
import UIKit

class SD_Player_Timeline_TableviewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SquareFlowLayoutDelegate {
    
    
    @IBOutlet weak var userProfileImage: SDImageViewShield!
    @IBOutlet weak var postNameLbl: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var delegate : TimeLineCellProtocol?
    @IBOutlet weak var lblFeedComment: UILabel!
    @IBOutlet weak var lblHighFive: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var collectionViewMedia: UICollectionView!
    @IBOutlet weak var btnHighFive: UIButton!
    
    @IBOutlet weak var btnComments: UIButton!
    var customLayout = WBGridViewLayout()
    var post : Posts! {
        didSet {
            if post.post_type == PostType.text
            {
                lblFeedComment.text = post.post_message
            }
            else if post.post_type == PostType.media
            {
                collectionViewMedia.delegate = self
                collectionViewMedia.dataSource = self
                let flowLayout = SquareFlowLayout()
                flowLayout.numberOfColumns = Utilities.getNumberOfColoumns(itemCount: (post.post_media?.count)!)
                flowLayout.numberOfRows = Utilities.getNumberOfRows(itemCount:(post.post_media?.count)! )
                flowLayout.totalItemCount = (post.post_media?.count)!
                flowLayout.flowDelegate = self
                self.collectionViewMedia.collectionViewLayout = flowLayout
                
                collectionViewMedia.reloadData()
            }
            postNameLbl.text  = "\(UserBase.currentUser!.details!.name!) \(UserBase.currentUser!.details!.surName!)"
            lblTime.text = Utilities.getTimeStamp(date:post.added_on!)
            lblComments.text = "\(post.post_comments_count!) Comments"
            lblHighFive.text = "\(post.post_claps_count!)"
            userProfileImage.setImagewith_Url(url: (UserBase.currentUser?.selectedPlayer?.image)!, PlaceHolderImage: UIImage(named: "dumy")!)
            btnHighFive.isSelected = post.is_clapped!
        }
    }
    
    var index : Int = 0{
        didSet{
            btnHighFive.tag = index
            btnComments.tag = index
        }
    }
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    override func layoutIfNeeded() {
       
        guard let collectionView = self.collectionViewMedia else{
            
            return
        }
        guard let flowLayout = collectionView.collectionViewLayout as? SquareFlowLayout else {
            collectionView.collectionViewLayout.invalidateLayout()
            return
        }
        flowLayout.invalidateLayout()
    }
    override func updateConstraints() {
        super.updateConstraints()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func readMoreAction(_ sender: UIButton) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (post.post_media?.count)! <= 5 ? (post.post_media?.count)! : 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let postData = post.post_media![indexPath.item]
        var reuseId = ""
        switch postData.media_type {
        case PostMediaType.image:
            reuseId = CollectionViewCellId.imageCell
        case PostMediaType.video:
            reuseId = CollectionViewCellId.videoCell
        default:
            reuseId = CollectionViewCellId.imageCell
        }
        if indexPath.row >= 4
        {
            if (post.post_media?.count)! > 5
            {
                reuseId = CollectionViewCellId.showMoreCell
            }
            else
            {
                
            }
        }
        
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as? SDTimeLineMediaCollectionViewCell)!
        cell.postmedia = post.post_media![indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return Utilities.getSizeForItem(at: indexPath, totalItemCount: (post.post_media?.count)!, collectionView: collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectPost!(post_id:post.post_id!,index: indexPath)
    }
    func shouldExpandItem(at indexPath: IndexPath) -> Bool {
        return Utilities.getLayoutValuesFor(itemCount: (post.post_media?.count)!)[indexPath.item] == Utilities.CellType.expanded
    }
    
}
extension SD_Player_Timeline_TableviewCell: WBGridViewLayoutDelegate {
    
    func colectionView(_ collectionView: UICollectionView, numberOfItemsInRow row: Int) -> CellLayout {
        
        return CellLayout.ThreeLeft
    }
    func colectionView(_ collectionView: UICollectionView, sizeOfItemInRow row: Int) -> CGSize? {
        return nil
    }
    
}
