//
//  SDRootsTableViewCell.swift
//  Sportdare
//
//  Created by SICS on 11/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SDRootsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewRoot: SDImageViewShield!
    @IBOutlet weak var labelRootName: UILabel!
    
    @IBOutlet weak var btnUnroot: CustomButton!
    var root : Root! {
        didSet{
            imageViewRoot.setImagewith_Url(url: root.image!, PlaceHolderImage: nil)
            labelRootName.text = root.first_name! + " " + root.surname!
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
