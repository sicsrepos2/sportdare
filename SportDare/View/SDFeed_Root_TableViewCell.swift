//
//  SDFeed_Root_TableViewCell.swift
//  Sportdare
//
//  Created by SICS on 12/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
protocol FeedCellProtocol {
    func didSelectPlayerWith(id:String)

}
extension FeedCellProtocol{
    func didSelectPlayerWith(id:String){
        
    }
}
class SDFeed_Root_TableViewCell: UITableViewCell {
    @IBOutlet weak var userProfileImage: SDImageViewShield!
    @IBOutlet weak var btnHighFive: UIButton!
    @IBOutlet weak var btnComments: UIButton!
    @IBOutlet weak var btnWhoRooted: UIButton!
    @IBOutlet weak var btnWhomRooted: UIButton!
    @IBOutlet weak var lblHighFive: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var feedDelegate : FeedCellProtocol?
    var index : Int!{
        didSet{
            userProfileImage.tag = index
            btnHighFive?.tag = index
            btnComments?.tag = index
        }
    }
    var feed:Feed!{
        didSet{
            btnHighFive.isSelected = feed.is_clapped!
            lblHighFive.text = "\(feed.claps_count!)"
            lblComments.text = "\(feed.comments_count!) Comments"
              lblTime.text = Utilities.getTimeStamp(date:feed.time)
            setRootedStatus()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setRootedStatus()
    {
        let rootedWho = feed.root?.rooted_who
        let rootedWhom = feed.root?.rooted_whom
        userProfileImage.setImagewith_Url(url:rootedWho!.image!, PlaceHolderImage: UIImage(named: "dumy")!)
        btnWhoRooted.setTitle(rootedWho?.first_name, for: .normal)
        btnWhomRooted.setTitle(rootedWhom?.first_name, for: .normal)
    }
    @IBAction func didSelectRootPlayer(_ sender: UIButton) {
        let rootedWho = feed.root?.rooted_who
        feedDelegate?.didSelectPlayerWith(id: (rootedWho?.user_id)!)
    }
    
    @IBAction func didSelectRootedPlayer(_ sender: UIButton) {
         let rootedWhom = feed.root?.rooted_whom
         feedDelegate?.didSelectPlayerWith(id: (rootedWhom?.user_id)!)
    }
}
