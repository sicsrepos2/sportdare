//
//  SDFirstViewController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDFirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToResgisterPage(_ sender: CustomButton) {
    }
    
    
    @IBAction func goToLoginPage(_ sender: CustomButton) {
    }
    
}
