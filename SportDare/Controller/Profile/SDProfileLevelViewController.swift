//
//  SDProfileLevelViewController.swift
//  Sportdare
//
//  Created by Binoy T on 13/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDProfileLevelViewController: UIViewController {
    
    @IBOutlet weak var imageShield: CustomImageView!
    @IBOutlet weak var labelShieldName: UILabel!
    @IBOutlet weak var labelAthleteLevel: UILabel!
    @IBOutlet weak var lblFanLevel: UILabel!
    @IBOutlet weak var lblGambitsLevel: UILabel!
    @IBOutlet weak var ratingAthlete: FloatRatingView!
    @IBOutlet weak var ratingFan: FloatRatingView!
    @IBOutlet weak var ratingGambits: FloatRatingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let shield =  UserBase.currentUser?.details?.sheild
        {
            ratingAthlete.rating = Double(shield.levels!)
            ratingFan.rating  = Double(shield.fans!)
            ratingGambits.rating = Double(shield.gambits!)!
            let athleteLevel = ProfileLevel(rawValue: shield.levels!)
            let fanLevel = ProfileLevel(rawValue: shield.fans!)
            let gambitLevel = ProfileLevel(rawValue: Int(shield.gambits!)!)
            lblFanLevel.text = fanLevel?.name
            lblGambitsLevel.text = gambitLevel?.name
            labelAthleteLevel.text = athleteLevel?.name
        }
        
    }
    

   

}
