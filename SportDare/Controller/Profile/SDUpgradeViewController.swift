//
//  SDUpgradeViewController.swift
//  Sportdare
//
//  Created by Binoy T on 16/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
class SDUpgradeViewController: BaseViewController {
   
    @IBOutlet weak var btnPlatinum: SDCustomButton!
    
    @IBOutlet weak var btnDiamond: SDCustomButton!
    var productIDs: [String] = []
    
    var productsArray: [SKProduct] = []
  
    override func viewDidLoad() {
        super.viewDidLoad()
        verifySubscrtiptions()
//        KRProgressHUD.show()
//        receiptValidation()
        
        // Do any additional setup after loading the view.
    }
    
    func disableUpgrade(status:Bool)
    {
        btnPlatinum.isSelected = status
        btnDiamond.isSelected = status
        btnPlatinum.isUserInteractionEnabled = !status
        btnDiamond.isUserInteractionEnabled = !status
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func upgradeToPlatinum(_ sender: SDCustomButton) {
      showActions(productType: .platinum)
       
    }
    
    @IBAction func upgradeToDiamond(_ sender: SDCustomButton) {
      
        showActions(productType: .diamond)
        
    }
    
    func verifySubscrtiptions()
    {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            
            switch result {
            case .success(let receipt):
                let productIds = Set([ InAppServices.platinum.rawValue,
                                       InAppServices.diamond.rawValue])
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                switch purchaseResult {
                case .purchased(let expiryDate, let items):
                  
                    print("\(productIds) are valid until \(expiryDate)\n\(items)\n")
                    items.last?.productId
//                     self.upgradeSubscription(product: items.last!)
                     self.disableUpgrade(status: true)
                case .expired(let expiryDate, let items):
                    print("\(productIds) are expired since \(expiryDate)\n\(items)\n")
                    KRProgressHUD.showError(withMessage: "Your Subscription expired since \(expiryDate)")
                     self.disableUpgrade(status: false)
                case .notPurchased:
                    print("The user has never purchased \(productIds)")
                    KRProgressHUD.dismiss()
                }
            case .error(let error):
                print("Receipt verification failed: \(error)")
                KRProgressHUD.showError(withMessage: error.localizedDescription)
                self.disableUpgrade(status: true)
            }
        }
    }
    
    
    func upgradeSubscription(product : ReceiptItem)
    {

        let purchase = InAppServices(rawValue: product.productId)
        let param = ["user_id":UserBase.currentUser!.details!.userid!,"plan_id":purchase!.id,"endate":product.subscriptionExpirationDate!.description,"transaction_id":product.transactionId,"subscription_agent":"ios"] as [String : Any]
        ApiService.upgradePlan(parameter: param) { (status, result, error) in
            
            if status{
                KRProgressHUD.showSuccess(withMessage: "Successfully Changed your Subscription.")
                
                ApiService.viewFullProfile( completion: { (status, user, error) in
                    
                })
            }
            else
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            
        }
    }
    func requestProductInfo(productId:String) {
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { result in
            
            if case .success(let purchase) = result {
                // Deliver content from server, then:
                
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                
                let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
                SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
                    
                    if case .success(let receipt) = result {
                        let purchaseResult = SwiftyStoreKit.verifySubscription(
                            ofType: .autoRenewable,
                            productId: productId,
                            inReceipt: receipt)
                        
                        switch purchaseResult {
                        case .purchased(let expiryDate, let receiptItems):
                            print("Product is valid until \(expiryDate)")
                            self.upgradeSubscription(product: receiptItems.first!)
                        case .expired(let expiryDate, let receiptItems):
                            KRProgressHUD.dismiss()
                            print("Product is expired since \(expiryDate)")
                        case .notPurchased:
                            KRProgressHUD.dismiss()
                            print("This product has never been purchased")
                        }
                        
                    } else {
                        // receipt verification error
                    }
                }
            } else {
                // purchase error
                KRProgressHUD.dismiss()
            }
        }

    }
    
    
    
    func showActions(productType:InAppServices) {
      
       
        let actionSheetController = UIAlertController(title: productType.name, message: "Upgrade to \(productType.name)?", preferredStyle: UIAlertController.Style.actionSheet)
        
        let buyAction = UIAlertAction(title: "Buy", style: UIAlertAction.Style.default) { (action) -> Void in
            self.requestProductInfo(productId:productType.rawValue)
            KRProgressHUD.show()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) -> Void in
            
        }
        
        actionSheetController.addAction(buyAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true) {
            
        }
    }
    
  
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
         KRProgressHUD.dismiss()
    }
   
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error for request: \(error.localizedDescription)")
      KRProgressHUD.dismiss()
        
    }
    
    func receiptValidation() {
        KRProgressHUD.show()
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "ee70188badc24b1fa8c78f1ddb4cbb3a" as AnyObject]
        
        do {
            let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            let storeURL = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt")!
            var storeRequest = URLRequest(url: storeURL)
            storeRequest.httpMethod = "POST"
            storeRequest.httpBody = requestData
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                KRProgressHUD.dismiss()
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    print("=======>",jsonResponse)
                    if let date = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                        print(date)
                    }
                } catch let parseError {
                    print(parseError)
                }
            })
            task.resume()
        } catch let parseError {
            print(parseError)
        }
    }
    
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
                return formatter.date(from: expiresDate)
            }
            
            return nil
        }
        else {
            return nil
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
