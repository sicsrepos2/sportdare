//
//  SDEditProfileTableViewController.swift
//  Sportdare
//
//  Created by Binoy T on 26/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDEditProfileTableViewController: BaseTableViewController {
    @IBOutlet weak var txtFieldName:CustomTextField!
    @IBOutlet weak var profileImage: SDImageViewShield!
    @IBOutlet weak var txtFieldEmail: CustomTextField!
    @IBOutlet weak var txtFieldSurName: CustomTextField!
    @IBOutlet weak var txtFieldLocation: CustomTextField!
    @IBOutlet weak var txtFieldGender: CustomTextField!
    @IBOutlet weak var textFieldMotto: CustomTextField!
    @IBOutlet var mandatoryFields: [CustomTextField]!
    
    @IBOutlet weak var textViewGoalDescription: KMPlaceholderTextView!
    @IBOutlet weak var txtFieldGoal: CustomTextField!
    var mediaPicker: UIImagePickerController!
    var imageData : Data?
    var goalList = ["Performance","Health","Aesthetics","Fun"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let detail = UserBase.currentUser?.details
        txtFieldName.text = detail?.name
        txtFieldSurName.text = detail?.surName
        txtFieldEmail.text = detail?.email
        txtFieldLocation.text = detail?.location
        textFieldMotto.text = detail?.moto
        txtFieldGender.text = detail?.gender
        profileImage.setImagewith_Url(url: (detail?.image!)!, PlaceHolderImage: #imageLiteral(resourceName: "placeholder-1"))
        txtFieldGoal.text = detail?.goal
        textViewGoalDescription.text = detail?.goal_description
        
       
    }
    override func viewDidAppear(_ animated: Bool) {
        mediaPicker = UIImagePickerController()
        mediaPicker.delegate = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeProfileImage(_ sender: UIButton) {
        showPicker_Options()
    }
    
    @IBAction func saveDetails(_ sender: CustomButton) {
        
        self.validateFields { (status, message) in
            if status
            {
                updateProfileDetails()
            }
            else
            {
                self.show_OK_AlertWith_Title(title: "Error", message: message, completion: nil)
            }
        }
    }
    
}
extension SDEditProfileTableViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImageEditingDelegate{
//    UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFieldGender
        {
            showDropDown(on: txtFieldGender, dataSource: ["Male","Female","Other"])
            return false
        }
        if textField == txtFieldGoal
        {
            showDropDown(on: txtFieldGoal, dataSource: goalList)
            return false
        }
        
        return true
    }
   
    func showDropDown(on:UITextField,dataSource:[String])
    {
        let dropDown = DropDown(anchorView: on)
        dropDown.bottomOffset = CGPoint(x: 0, y: 30)
        dropDown.dataSource = dataSource
        dropDown.direction = .bottom
        dropDown.show()
        dropDown.selectionAction = { (index,value) in
            on.text = value
        }
    }
    
    func showPicker_Options()
    {
        
        let actionSheet = UIAlertController(title: "Change Profile Image", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.mediaPicker.sourceType = .camera
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            self.mediaPicker.sourceType = .photoLibrary
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        actionSheet.addAction(camera)
        actionSheet.addAction(photos)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
            
        }
        
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
             self.presentCropView(image: img)
            picker.dismiss(animated: false) {

            }
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            
        }
    }
    func imageCroping(controller: CustomimageEditorViewController, didFinishPickingImage image: UIImage) {
        imageData = image.jpegData(compressionQuality: 0.9)
        
        
        self.navigationController?.popViewController(animated: false)
        DispatchQueue.main.async {
            self.profileImage.image = UIImage(data: self.imageData!)!
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    func presentCropView(image:UIImage)
    {
        let cropView = self.storyboard?.instantiateViewController(withIdentifier: "CustomimageEditorViewController") as! CustomimageEditorViewController
        cropView.setCropView(Image: image, delegate: self, cropSize: CGSize(width: profileImage.profileImageView.frame.size.width * 1.5, height: profileImage.profileImageView.frame.size.height * 1.5))
        cropView.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(cropView, animated: false)
        
    }
    func validateFields(completion:(_ status:Bool,_ message:String)->Void)
    {
        var isValid = true
        var message = ""
        for field in mandatoryFields
        {
            if (field.text?.isEmpty)!
            {
                isValid = false
                field.showError()
                message = "Please fill all the required fields"
            }
        }
        
      
        completion(isValid,message)
        
    }
    func getProfiledetails()->[String:Any]
    {
        if imageData == nil
        {
        imageData = profileImage.image.pngData()
        }
        
        return ["name":txtFieldName.text!,"surName":txtFieldSurName.text!,"location":txtFieldLocation.text!,"gender":txtFieldGender.text!,"moto":textFieldMotto.text!,"goal":txtFieldGoal.text!,"goal_description":textViewGoalDescription.text!]
        
    }
    
    func updateProfileDetails()
    {
         KRProgressHUD.show()
        let details = getProfiledetails()
        ApiService.update_Profile(image: imageData, parameter: details) { (status, response, error) in
            if status
            {
            KRProgressHUD.showSuccess(withMessage: "Updated")
            }
            else{
                KRProgressHUD.showError(withMessage: "Failed to update")
            }
            
        }
        
    }
   
}
