//
//  SDMyProfileViewController.swift
//  SportDare
//
//  Created by Binoy T on 20/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import BonsaiController

class SDMyProfileViewController: BaseViewController,BonsaiControllerDelegate {
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblGambits: UILabel!
    @IBOutlet weak var lblShield: UILabel!
    
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblMotto: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var profileImage: SDImageViewShield!
    
    @IBOutlet weak var imageViewAccount: UIImageView!
    @IBOutlet weak var btnViewLevel: UIButton!
    @IBOutlet weak var lblGoal: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getProfile()
         self.setDetails()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
       
//        mediaPicker.allowsEditing = true
    }
    override func viewDidLayoutSubviews() {
        print(profileImage.frame)
    }
    
    internal func setDetails()
    {
        let detail = UserBase.currentUser?.details
        lblName.text = detail?.name
        lblAge.text = "\(Utilities.getAge(dob:(detail?.dOB)!)) Years"
        lblMotto.text = detail?.moto
        lblGender.text = detail?.gender
        lblLocation.text = detail?.location
        lblGambits.text = "\(Int(detail!.gambits!)!)"
        lblAccount.text = detail?.account
        lblGoal.text = detail?.goal
        lblShield.text = detail?.sheild?.name
        profileImage.setImagewith_Url(url: (detail?.image!)!, PlaceHolderImage: UIImage(named: "dumy"))
       if detail?.sheild?.name == "Grey"
       {
        profileImage.shieldImageView.image = ShieldType(rawValue: detail!.sheild!.shield_stars!)?.image
        
        }
        else
       {
        profileImage.shieldImageView.image =  ShieldType(rawValue: detail!.sheild!.name!)?.image
        
        }
        let accoutType = AccountType(rawValue: detail!.account!)
        imageViewAccount.image = accoutType?.image
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getProfile()
    {
        ApiService.viewFullProfile { (status, user, error) in
            if status{
                self.setDetails()
            }
        }
    }
    
    
    //    MARK: Actions
    @IBAction func didTapImage(_ sender: SDImageViewShield) {
       let explainer = self.storyboard?.instantiateViewController(withIdentifier: "ShieldExplainer")
        
        explainer?.modalPresentationStyle = .overFullScreen
        explainer?.modalTransitionStyle = .crossDissolve
        
        self.present(explainer!, animated: true) {
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    @IBAction func editProfileAction(_ sender: UIButton) {
        let editProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "SDEditProfileTableViewController") as! SDEditProfileTableViewController
        
        self.navigationController?.pushViewController(editProfileVc, animated: true)
    }
    @IBAction func getMoreGambitsAction(_ sender: UIButton) {
        let editProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "SDGetMoreGambitsTableViewController") as! SDGetMoreGambitsTableViewController
        
        self.navigationController?.pushViewController(editProfileVc, animated: true)
        
    }
    @IBAction func didSelectViewProfileLevel(_ sender: UIButton) {
        let levelVc = self.storyboard?.instantiateViewController(withIdentifier: "SDProfileLevelViewController") as! SDProfileLevelViewController
        levelVc.transitioningDelegate = self
        levelVc.modalPresentationStyle = .custom
        present(levelVc, animated: true, completion: nil)
        
    }
    
    @IBAction func didSelectDare(_ sender: CustomButton) {
        let activityVC =  self.storyboard?.instantiateViewController(withIdentifier: "SD_My_Funtions_MainViewController") as! SD_My_Funtions_MainViewController
        activityVC.showBackButton = true
        activityVC.showSearch = true
        activityVC.isForActivities = true
        self.navigationController?.pushViewController(activityVC, animated: true)
        
    
    }
    
    @IBAction func didSelectRoles(_ sender: CustomButton) {
        let activityVC =  self.storyboard?.instantiateViewController(withIdentifier: "SD_My_Funtions_MainViewController") as! SD_My_Funtions_MainViewController
        activityVC.showBackButton = true
        activityVC.showSearch = true
        activityVC.isForRoles = true
        self.navigationController?.pushViewController(activityVC, animated: true)
        
    }
    
    @IBAction func didSelectPlayers(_ sender: CustomButton) {
        let activityVC =  self.storyboard?.instantiateViewController(withIdentifier: "SD_My_Funtions_MainViewController") as! SD_My_Funtions_MainViewController
        activityVC.showBackButton = true
        activityVC.showSearch = true
        activityVC.isForPlayers = true
        self.navigationController?.pushViewController(activityVC, animated: true)
    }
    
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        
        return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.height / 4), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height / 2))
    }
    
    // return a Bonsai Controller with SlideIn or Bubble transition animator
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
     
            return  BonsaiController(fromView: btnViewLevel, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
        
        
        
        // or Bubble animation initiated from a view
        //return BonsaiController(fromView: yourOriginView, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
    }

}

