//
//  SDChattViewController.swift
//  Sportdare
//
//  Created by SICS on 28/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SDChattViewController: SDPlayerBaseViewController,UITextViewDelegate {
    @IBOutlet var tableView:UITableView!
    @IBOutlet var ContainerTextView: UIView!
    @IBOutlet var textViewComment: UITextView!
 
    var isKeyBoardHidden:Bool = true
    var keyBoardSize:CGSize!
    let picker = UIImagePickerController()
    var chats = [Chat]()
    var chatDetail : ChatList?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        getMessages(offset: chats.count)
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapOnTableView))
        self.tableView.addGestureRecognizer(tapGesture)
        if chatDetail != nil
        {
        let navView = UIView()
        
        
        // Create the image view
        
       let playerImageView =  CustomImageView()
            playerImageView.frame =  CGRect(x: 0, y: 0, width: 40, height: 40)
            playerImageView.contentMode = UIView.ContentMode.scaleAspectFill
        playerImageView.cornerRadius = 20
        playerImageView.clipsToBounds = true
        playerNamelabel = UILabel()
        playerNamelabel.frame = CGRect.init(x: 50, y: 8, width: 150, height: 20)
        playerNamelabel.textColor = UIColor.white
        playerNamelabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        
            playerImageView.sd_setImage(with: chatDetail!.image?.url, placeholderImage:  UIImage(named: "dumy"), options: .continueInBackground, completed: nil)
        
        playerNamelabel.text = chatDetail!.first_name! + " " + chatDetail!.surname!
        playerNamelabel.frame = CGRect.init(x: 50, y: 8, width: 150, height: 20)
        //            let image =  playerImageView.shieldfImageView.image!.changeColor()
        //            playerImageView.shieldfImageView.image = image
        navView.addSubview(playerImageView)
        navView.addSubview(playerNamelabel)
        navView.frame = CGRect(x: 0, y: 0, width: 300, height: 44)
        // Set the navigation bar's navigation item's titleView to the navView
        navView.sizeToFit()
        self.navigationItem.titleView = navView
        }
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        textViewComment.translatesAutoresizingMaskIntoConstraints = true
        self.tableView.translatesAutoresizingMaskIntoConstraints = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    @objc func tapOnTableView(){
        textViewComment.resignFirstResponder()
        //        self.view.endEditing(true)
    }
    @IBAction func sendAction(){
        if(self.textViewComment.text.count > 0){
            var keyboardHeightValue:CGFloat = 0.0
            if(isKeyBoardHidden){
                keyboardHeightValue = 0.0
            }else{
                keyboardHeightValue = keyBoardSize.height
            }
            self.postMessage(message:  self.textViewComment.text)
            self.textViewComment.text = ""
            
            let wid = ContainerTextView.frame.width
            let widthTextView = textViewComment.frame.width
            ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,y: self.view.bounds.height-(50+keyboardHeightValue+self.view.safeAreaInsets.bottom),width : wid,height : 50 )
            textViewComment.frame = CGRect(x:textViewComment.frame.minX,y: textViewComment.frame.minY,width : widthTextView,height : 34 )
            //            self.tableView.frame = CGRect(x: 0,y: 0,width : UIScreen.main.bounds.width,height :  UIScreen.main.bounds.height-(50+keyboardHeightValue+self.view.safeAreaInsets.bottom))
            
        }
    }
    
    func postMessage(message:String)
    {
        ApiService.post_Chat(message: message, recieverId: self.playerDetail?.userid ?? chatDetail!.user_id!, image: nil) { (status, result, error) in
            if status
            {
                self.getMessages(offset: self.chats.count)
            }
        }
    }
    
    func getMessages(offset:Int)
    {
        ApiService.getChat(recieverId: self.playerDetail?.userid ?? chatDetail!.user_id!,offset:offset) { (status, chats, error) in
            if chats != nil
            {
                if offset == 0
                {
                    self.chats.insert(contentsOf: chats!.reversed(), at: 0)
                  
                }
                else
                {
                    self.chats.append(contentsOf: chats!)
                }

                self.tableView.reloadData()
                 self.tableView.scrollToBottom(animated: false, section: 0)
              
            }
        }
    }
    //MARK: TAP ON ATTACHEMENT BUTTON
    @IBAction func showActionSheet(){
        textViewComment.resignFirstResponder()
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Select Image From", message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Photo Library", style: .default)
        { _ in
            self.photoFromLibrary()
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            self.photoFromCamera()
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    func photoFromLibrary() {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    func photoFromCamera() {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.camera
        picker.cameraCaptureMode = .photo
        picker.modalPresentationStyle = .fullScreen
        present(picker,animated: true,completion: nil)
    }

}

extension SDChattViewController:UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSourcePrefetching{
    //MARK: - IMAGE PICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
           
            picker.dismiss(animated: false) {
                
            }
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            
        }
    }
    //MARK: KEYBOARD NOTIFICATIONS
    @objc func keyboardWillAppear(notification: NSNotification){
        if isKeyBoardHidden
        {
        isKeyBoardHidden = false
        textViewComment.enablesReturnKeyAutomatically = true
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize:CGSize = (userInfo.object(forKey: UIResponder.keyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue.size
        self.keyBoardSize = keyboardSize
        
        var messageFrame:CGRect = self.ContainerTextView.frame
        messageFrame.origin.y -= keyboardSize.height-self.view.safeAreaInsets.bottom
        self.ContainerTextView.frame = messageFrame
        
        self.tableView.frame = CGRect(x: 0,y: self.tableView.frame.origin.y,width : self.view.bounds.width,height : self.view.bounds.height-(self.ContainerTextView.frame.height + keyboardSize.height)-self.tableView.frame.origin.y)
        self.tableView.scrollToBottom(animated: false, section: 0)
        }
        
        
        
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification){
        isKeyBoardHidden = true
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize:CGSize = (userInfo.object(forKey:UIResponder.keyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue.size
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.25)
        self.tableView.contentInset = UIEdgeInsets.zero
        UIView.commitAnimations()
        
        self.tableView.scrollIndicatorInsets = UIEdgeInsets.zero
        
        var messageFrame:CGRect = self.ContainerTextView.frame
        messageFrame.origin.y += keyboardSize.height+self.view.safeAreaInsets.bottom
        self.ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,y:self.view.bounds.height-(messageFrame.height+self.view.safeAreaInsets.bottom),width : messageFrame.width,height : HeightConstants.textViewContainerDefaultHeight )
        self.tableView.frame = CGRect(x: 0,y: self.tableView.frame.origin.y,width : UIScreen.main.bounds.width,height :  self.view.bounds.height-(self.ContainerTextView.frame.height+self.view.safeAreaInsets.bottom+self.tableView.frame.origin.y))
        self.tableView.scrollToBottom(animated: true, section: 0)
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chat = chats[indexPath.row]
        let cellId  = chat.is_iam_sender! ? ChattViewCellId.cellMe :ChattViewCellId.cellYou
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! SDChatt_CommentTableViewCell
        cell.chat = chat
         
            return cell
    
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let lastData = self.chats.count - 1
        if  indexPaths.last!.row == lastData {
            self.getMessages(offset: self.chats.count)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newLineHeight:CGFloat = 0.0
        if (text == "\n"){
            newLineHeight = HeightConstants.newLineHeight
            if(textView.text.isEmpty){
                return false
            }
        }
        
        let wid = ContainerTextView.frame.width
        let widthTextView = textView.frame.width
        textView.sizeToFit()
        let textViewHeight:CGFloat = textView.contentSize.height
        
        if textViewHeight > HeightConstants.textViewDefaultHeight {
            
            textView.frame = CGRect(x:textView.frame.minX,y: textView.frame.minY,width : widthTextView,height :  min(textViewHeight, HeightConstants.textViewMaximumHeight)+newLineHeight )
            ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,
                                             y:  self.view.bounds.height - (keyBoardSize.height + HeightConstants.textViewContainerDefaultHeight + newLineHeight  + (min(textViewHeight, HeightConstants.textViewMaximumHeight) - HeightConstants.textViewDefaultHeight)),
                                             width : wid,
                                             height : HeightConstants.textViewContainerDefaultHeight + ( min(textViewHeight, HeightConstants.textViewMaximumHeight) - HeightConstants.textViewDefaultHeight) + newLineHeight )
            
        }else{
            
            ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,
                                             y:  self.view.bounds.height - (keyBoardSize.height + HeightConstants.textViewContainerDefaultHeight + newLineHeight) ,
                                             width : wid,
                                             height : HeightConstants.textViewContainerDefaultHeight + newLineHeight )
            
            textView.frame = CGRect(x:textView.frame.minX,y: textView.frame.minY,width : widthTextView,height : HeightConstants.textViewDefaultHeight+newLineHeight )
            
        }
        
        
        
        return true
    }
    
}
