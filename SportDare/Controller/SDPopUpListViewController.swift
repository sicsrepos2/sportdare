//
//  SDPopUpListViewController.swift
//  Sportdare
//
//  Created by Binoy T on 03/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
protocol PopupListDelegate
{
    func popController(controller:SDPopUpListViewController, didSelect option: String, at Index : Int)
}
class SDPopUpListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate{
    var SelectionClosure : ((Int, String) -> Void)?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchField: UISearchBar!
    @IBOutlet weak var tableViewList: UITableView!
    var listTitle : String!
    var list :[String]?{
        didSet{
            
        }
    }
    var filtered : [String]?
    var delegate : PopupListDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        filtered = list
        self.tableViewList.delegate = self
        self.tableViewList.dataSource = self
        self.titleLabel.text = listTitle ?? "Select option"
        // Do any additional setup after loading the view.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filtered = list?.filter({$0.contains(searchText)})
        self.tableViewList.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered != nil ? (filtered?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "listCell")
        cell?.textLabel?.text = filtered![indexPath.item]
        cell?.textLabel?.textAlignment = .center
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = filtered![indexPath.item]
        self.SelectionClosure?(indexPath.row,selectedItem)
        
        delegate?.popController(controller: self, didSelect: selectedItem, at: indexPath.row)
        self.dismiss(animated: true) {
            
        }
        
    }
    

}
