//
//  SDDareDetailTableViewController.swift
//  Sportdare
//
//  Created by SICS on 13/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import UserNotifications
import  MobileCoreServices
protocol DareActionProtocol{
    func didChangeDareStatus (dare:DareData)
    func didCompleteActions()
}
extension DareActionProtocol{
    func didChangeDareStatus(dare:DareData){
        
    }
    
    func didCompleteActions()
    {
        
    }
}
class SDDareDetailTableViewController: UITableViewController,UIDocumentPickerDelegate {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubSport: UILabel!
    @IBOutlet weak var lblGambits: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDareStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    var dareProtocol : DareActionProtocol?
    var dare : DareData?
    var dareStatus : DareStatus?
    var isYourPost = false
    
    @IBOutlet  var collectionviewPreview: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
           setDareDetails()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

       
    }
    
    func setDareDetails()
    {
        isYourPost = dare?.d_created_by?.user_id == UserBase.currentUser?.details?.userid
        self.lblSubSport.text = dare!.sc_name
        lblName.text = isYourPost ? "You" : (dare?.d_created_by?.first_name)! + " " + (dare?.d_created_by?.surname)!
        lblGambits.text = dare?.d_gambits
        lblDate.text = dare?.d_date?.toAppDate
        lblTime.text = dare?.d_time?.toAppTime
        lblLocation.text = dare?.d_location
        lblDescription.text = dare?.d_description
        dareStatus = (dare?.d_satus).map { DareStatus(rawValue: $0)! }
        lblDareStatus.text = dareStatus?.rawValue.capitalized
        lblDareStatus.textColor = dareStatus?.color
       
    }
    
    func scheduleNotification() {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            let options: UNAuthorizationOptions = [.alert, .sound]
            center.requestAuthorization(options: options) { (status, error) in
                if status
                {
                    let content = UNMutableNotificationContent()
                    let createdBy = self.dare?.d_created_by
                    content.title = "\(createdBy!.first_name!) dare you for \(self.dare!.sc_name!)"
                    content.body = "\(createdBy!.first_name!) dare you for \(self.dare!.sc_name!) on \(self.dare!.d_date!.toAppDate) \(self.dare!.d_time!.toAppTime)"
                    content.categoryIdentifier = "alarm"
                    content.userInfo = ["id":self.dare?.d_id as Any]
                    content.sound = UNNotificationSound.default
                    let date = Date().addingTimeInterval(120)
                    //            dare?.d_date!.date
                    let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: date)
                    let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate,
                                                                repeats: false)
                    let request = UNNotificationRequest(identifier: (self.dare?.d_id)!, content: content, trigger: trigger)
                    center.add(request)
                    KRProgressHUD.showSuccess(withMessage: "Added Reminder")
                }
            }
            center.getPendingNotificationRequests { (requests) in
                
            }
        } else {
            // Fallback on earlier versions
        }
    }

    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func acceptAction(_ sender: UIButton) {
        KRProgressHUD.show()
        ApiService.acceptDare(dareId: (dare?.d_id)!) { (status, response, error) in
           
            if status
            {
                 KRProgressHUD.dismiss()
                self.dare?.d_satus = "accepted"
                self.dareProtocol?.didChangeDareStatus(dare: self.dare!)
                self.dismiss(animated: true) {
                    
                }
            }
            else if error != nil
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
        }
    }
    
    @IBAction func dareCompletedAction(_ sender: UIButton) {
        let completeVc = self.storyboard?.instantiateViewController(withIdentifier: "SDDareSubmitResultTableViewController") as! SDDareSubmitResultTableViewController
        completeVc.dare = self.dare
        completeVc.resultProtocol = self
        self.present(completeVc, animated: true) {

        }
   
      
    }
    
    @IBAction func confirmResults(_ sender: UIButton)
    {
        self.showAlertWith(title: "Confirm Reply", message: "Are you sure to approve the dare has completed by \(dare!.d_created_for!.first_name!) ?", CancelBtnTitle: "No", OtherBtnTitle: "Yes", acceptCompletion: { () -> Void? in
            ApiService.approveResult(dareId: (self.dare?.d_id)!) { (status, result, error) in
                self.dare?.d_satus = DareStatus.completed.rawValue
                self.dareProtocol?.didChangeDareStatus(dare: self.dare!)
                self.dareProtocol?.didCompleteActions()
                self.dismiss(animated: true, completion: nil)
            }
        }) {
            
        }
        
        
    }
    
    @IBAction func viewResult(_ sender: UIButton) {
      
            let resultsVC = self.storyboard?.instantiateViewController(withIdentifier: "SDViewResultViewController") as! SDViewResultViewController
            resultsVC.dareId = dare?.d_id
            self.present(resultsVC, animated: true) {
                
            }
      
        
    }
    @IBAction func useCards(_ sender: UIButton) {
        let cardsVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_UseCards_TableViewController") as! SD_UseCards_TableViewController
        cardsVC.delegate = self
        cardsVC.isFromDare = true
        self.present(cardsVC, animated: true) {
            
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
    }
    @IBAction func dareFailedAction(_ sender: UIButton) {
        self.showAlertWith(title: "Failed", message: "Are you sure you cannot complete this challenge ?", CancelBtnTitle: "No", OtherBtnTitle: "Yes", acceptCompletion: { () -> Void? in
            ApiService.failedToComplete(dareId: self.dare!.d_id!, completion: { (status, result, error) in
                self.dare?.d_satus = DareStatus.failed.rawValue
                  self.dareProtocol?.didChangeDareStatus(dare: self.dare!)
                self.dareProtocol?.didCompleteActions()
                self.dismiss(animated: true, completion: nil)
            })
        }) {
            
        }
        
    }
    @IBAction func reject(_ sender: UIButton) {
        
        self.showAlertWith(title: "Reject", message: "Are you sure to Reject Dare by \(dare!.d_created_for!.first_name!) ?", CancelBtnTitle: "No", OtherBtnTitle: "Yes", acceptCompletion: { () -> Void? in
            ApiService.declineDare(dareId: self.dare!.d_id!, completion: { (status, result, error) in
                  self.dare?.d_satus = DareStatus.rejected.rawValue
                  self.dareProtocol?.didChangeDareStatus(dare: self.dare!)
                self.dareProtocol?.didCompleteActions()
                self.dismiss(animated: true, completion: nil)
            })
        }) {
            
        }
    }
    
    @IBAction func reminderAction(_ sender: UIButton) {
        scheduleNotification()
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row
        {
             case 3:
            if dare?.dare_files?.count == 0
            {
                return 0
            }
            else
            {
                 return super.tableView(tableView, heightForRowAt: indexPath)
            }
        case 4:
            if dareStatus == .pending && !isYourPost
            {
              return super.tableView(tableView, heightForRowAt: indexPath)
            }
            return 0
        case 5:
            if dareStatus == .accepted && !isYourPost
            {
                return  super.tableView(tableView, heightForRowAt: indexPath)
            }
            return 0
        case 6:
            if dareStatus == .accepted && !isYourPost
            {
                return  super.tableView(tableView, heightForRowAt: indexPath)
            }
            return 0
        case 7:
            if dareStatus == .completed || dareStatus  == .replied
            {
                if isYourPost
                {
                return  super.tableView(tableView, heightForRowAt: indexPath)
                }
                else
                {
                    return 0
                }
            }
            return 0
        default:
            return  super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

}

extension SDDareDetailTableViewController:UICollectionViewDelegate,UICollectionViewDataSource,SubmitResultProtocol,CardsProtocol{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dare?.dare_files?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewCell", for: indexPath) as! SDPreviewCollectionViewCell
        cell.dareFile = dare?.dare_files![indexPath.row]
        cell.layoutIfNeeded()

        return cell
    }
    
    func didCompleteDare() {
        dareProtocol?.didCompleteActions()
        self.dismiss(animated: true) {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let previewVC = self.storyboard?.instantiateViewController(withIdentifier: "SDViewResultViewController") as! SDViewResultViewController
       
        previewVC.files = dare?.dare_files
         previewVC.passedContentOffset = indexPath
        self.present(previewVC, animated: true) {
            
        }
    }
   
    
    func didSelectCard(_ card: Card_details) {
        if card.card_name == "yellow"
        {
            self.applyCard(card, type: Service.kResetChallenge)
        }
        else if card.card_name == "red"
        {
            self.applyCard(card, type: Service.kBlockCHallenge)
        }
        else{
            
        }
    }
    
    func applyCard(_ card:Card_details,type:String)
    {
        let param = ["dare_id":dare!.d_id,"card_id":card.card_id]
        ApiService.useCard(param: param) { (status, result, error) in
            
        }
    }
}
