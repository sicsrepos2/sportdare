//
//  SDDareListTableViewController.swift
//  Sportdare
//
//  Created by SICS on 13/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SDDareListTableViewController: BaseTableViewController,PopupViewControllerDelegate,DareActionProtocol {
   
    

    var darelist : [DareData]!
    override func viewDidLoad() {
        super.viewDidLoad()


    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return darelist.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dareCell") as! SDDareListTableViewCell
        cell.dare = darelist[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dareDetailVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDDareDetailTableViewController") as? SDDareDetailTableViewController
        dareDetailVc?.dare = darelist[indexPath.row]
        dareDetailVc!.dareProtocol = self
        let popupVC = PopupViewController(contentController: dareDetailVc!, position: PopupViewController.PopupPosition.top(100), popupWidth: self.view.frame.width * 0.85, popupHeight: self.view.frame.height)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        popupVC.delegate = self
        self.present(popupVC, animated: true) {
            
        }
    }
    func didChangeDareStatus(dare: DareData) {
      if var daredata =   self.darelist.filter({$0.d_id == dare.d_id}).last
      {
        daredata.d_satus = dare.d_satus
        self.tableView.reloadData()
        }
    }
}
