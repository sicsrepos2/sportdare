//
//  SDFeedPreviewViewController.swift
//  Sportdare
//
//  Created by SICS on 12/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import AVKit
import BonsaiController
class SDFeedPreviewViewController: BaseViewController {
    @IBOutlet weak var buttonComments: UIButton!
    @IBOutlet weak var accessoryView: UIView!
    @IBOutlet weak var labelHighFive: UILabel!
    @IBOutlet weak var labelComments: UILabel!
    //    var accessoryView: UIView!
    @IBOutlet weak var btnHighFive: UIButton!
    
    var mediaArray :[Any]?
    var previewDelegate : PreviewVCProtocol?
    var passedContentOffset = IndexPath()
    var playerViewController: AVPlayerViewController!
    var btnMore : UIButton?
    var myCollectionView: UICollectionView!
    
    var feed : Feed?
    
    override func viewDidLoad() {
        self.showSearch = false
        self.showBackButton = true
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.view.backgroundColor=UIColor.black
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0 , left: 0, bottom:  accessoryView.frame.height, right: 0)
        layout.minimumInteritemSpacing=3
        layout.minimumLineSpacing=0
        layout.scrollDirection = .horizontal
        myCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        myCollectionView.delegate=self
        myCollectionView.dataSource=self
        
        myCollectionView.register(ImagePreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        myCollectionView.isPagingEnabled = true
        self.view.addSubview(myCollectionView)
        myCollectionView.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
        
       
     self.navigationController?.navigationBar.isTranslucent = false
        
       btnHighFive.isSelected = (feed?.is_clapped)!
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.view.setNeedsLayout() // force update layout
        navigationController?.view.layoutIfNeeded() // to
         view.layoutIfNeeded()
        var frame = myCollectionView.frame
        frame.size.height = frame.height - 50
        myCollectionView.frame = frame
          myCollectionView.scrollToItem(at: passedContentOffset, at: .right, animated: false)
        self.view.bringSubviewToFront(accessoryView)
        
        if feed != nil
        {
            self.labelHighFive.text = "\(feed!.claps_count!)"
            self.labelComments.text = "\(feed!.comments_count!) comments"
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
      
        
        guard let flowLayout = myCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
 
            flowLayout.sectionInset = UIEdgeInsets(top: 0 , left: 0, bottom:  accessoryView.frame.height, right: 0)
            flowLayout.itemSize = CGSize(width: myCollectionView.frame.size.width, height: myCollectionView.frame.size.height - accessoryView.frame.height)
       
        myCollectionView.collectionViewLayout.invalidateLayout()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
//          myCollectionView.frame = self.view.frame

        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewWillLayoutSubviews()
        self.viewDidLayoutSubviews()
      
        self.extendedLayoutIncludesOpaqueBars = true
    }
    @objc func moreOptions(_ sender:UIButton)
    {
        let alertOptions = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete Photo", style: .destructive) { (action) in
//            self.removeMedia()
            
        }
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertOptions.addAction(deleteAction)
        alertOptions.addAction(cancelAction)
        self.present(alertOptions, animated: true) {
            
        }
        
    }
    
    @IBAction func commentAction(_ sender: UIButton) {
        let commentsVc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDCommentsViewController") as! SDCommentsViewController
        commentsVc.commentProtocol = self
        commentsVc.feed = self.feed
        commentsVc.modalTransitionStyle = .coverVertical
        commentsVc.modalPresentationStyle = .custom
        commentsVc.transitioningDelegate = self
        self.present(commentsVc, animated: true) {
            
        }
    }
    
    @IBAction func highFiveAction(_ sender: UIButton) {
        feed!.is_clapped = !feed!.is_clapped!
        highFive()
        if  feed!.is_clapped!
        {
            feed!.claps_count = (feed!.claps_count)! + 1
        }
        else
        {
            
            
            feed!.claps_count = (feed!.claps_count)! - 1
        }
        self.viewWillAppear(true)
        btnHighFive.isSelected = feed!.is_clapped!
        
    }
    
    func highFive()
    {
        ApiService.postHighFiveFeed(param: ["type":feed!.activity_type!,"id":feed!.referance_id!]) { (status, result, error) in
            if result != nil
            {
                
                self.previewDelegate?.didUpdateFeed(feed: self.feed!)
            }
        }
    }
    
   
//    func removeMedia()
//    {
//        let mediaIndex = self.passedContentOffset.row
//        if let media = self.mediaArray![mediaIndex] as? Media
//        {
//            ApiService.removeMedia(mediaId: (media.am_id)!, completion: { (status, result) in
//                if status
//                {
//                    self.mediaArray?.remove(at: mediaIndex)
//                    self.post?.post_media?.remove(at: mediaIndex)
//                    self.previewDelegate?.didUpdatePost(post: self.post!)
//                    self.myCollectionView.reloadData()
//                    self.previewDelegate?.didDeletedMedia_at(index: mediaIndex)
//                }
//            })
//        }
//    }
}
extension SDFeedPreviewViewController:BonsaiControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,CommentsProtocol,PreviewProtocol{
    
    func didAddedComment() {
        self.feed?.comments_count =  self.feed!.comments_count! + 1
        self.viewWillAppear(true)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (mediaArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImagePreviewFullViewCell
        cell.imgView.image =  nil
        cell.delegate = self
        if let post = mediaArray?[indexPath.row] as? Post_media
        {
            cell.post = post
        }
        else if let media = mediaArray?[indexPath.row] as? Media
        {
            cell.media = media
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        passedContentOffset =  self.myCollectionView.indexPath(for:self.myCollectionView.visibleCells.first!)!
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let offset = myCollectionView.contentOffset
        let width  = myCollectionView.bounds.size.width
        
        let index = round(offset.x / width)
        let newOffset = CGPoint(x: index * size.width, y: offset.y)
        
        myCollectionView.setContentOffset(newOffset, animated: false)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.myCollectionView.reloadData()
            
            self.myCollectionView.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    func didBeginZooming() {
        
        accessoryView.hideWithAnimation()
        self.viewDidLayoutSubviews()
    }
    func didEndZooming() {
        accessoryView.showWithAnimation()
        self.viewDidLayoutSubviews()
        
    }
    
    //    MARK:- Popup List Controller
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: 0, y: 70), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height-70))
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
    }
}
