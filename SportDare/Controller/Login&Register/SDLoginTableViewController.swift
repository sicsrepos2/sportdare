//
//  SDLoginTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Alamofire
import MessageUI
class SDLoginTableViewController: BaseTableViewController,MFMailComposeViewControllerDelegate {
  @IBOutlet weak var txtFieldEmail: CustomTextField!
  @IBOutlet weak var txtFieldPassword: CustomTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
  
    
    @IBAction func loginAction(_ sender: CustomButton) {
    
        self.validateFields { (isValid, message) in
            if isValid
            {
                self.view.endEditing(true)
                self.login(param: ["email":txtFieldEmail.text!,"password":txtFieldPassword.text!,"device_id":Constants.deviceTokenString,"device_type":"ios","social_agent":"","social_id":""])
            }
            else
            {
                self.showAlertWithOkButton(message: message)
            }
        }
    }
    
    @IBAction func loginFBAction(_ sender: Any) {
        
        if let _ = AccessToken.current
        {
            GraphRequest(graphPath: "me", parameters:["fields":"first_name,email,last_name,birthday,gender,picture.width(250).height(250)"]).start(completionHandler: { (requst, result, error) in
                if let user = result as? NSDictionary{
                      let id =  user["id"]!
                    let imgUrl = "http://graph.facebook.com/\(id)/picture?type=large"
                    let detail = ["email":user["email"]!,"password":self.txtFieldPassword.text!,"device_id":Constants.deviceTokenString,"device_type":"ios","social_agent":"facebook","social_id":user["id"]!,"name":user["first_name"]!,"surName":user["last_name"]!,"social_image_url":imgUrl]
                    self.socialLogin(param: detail)
                }
               
            })
        }
        else
        {
            let loginManager = LoginManager()
            loginManager.logIn(permissions: ["user_age_range","email","user_birthday,public_profile,user_gender"], from: self) { (result, error) in
                if result != nil
                {
                    
                    GraphRequest(graphPath: "me", parameters: ["fields":"first_name,email,last_name"]).start(completionHandler: { (requst, result, error) in
                        if let user = result as? NSDictionary{
                            let id =  user["id"]!
                            let imgUrl = "http://graph.facebook.com/\(id)/picture?type=large"
                            let detail = ["email":user["email"]!,"password":self.txtFieldPassword.text!,"device_id":Constants.deviceTokenString,"device_type":"ios","social_agent":"facebook","social_id":user["id"]!,"name":user["first_name"]!,"surName":user["last_name"]!,"social_image_url":imgUrl]
                            self.socialLogin(param: detail)
                        }
                    })
                }
                
            }
            
            
        }
       
    }
    @IBAction func getHelpAction(_ sender: UIButton) {
    }
    
    @IBAction func viewSportDareAction(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(Constants.sportDareUrl)
        {
            UIApplication.shared.open(Constants.sportDareUrl, options: [:], completionHandler: nil)
        }
    }
    @IBAction func getHelp(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@sportdare.com"])
//            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return super.tableView(tableView, heightForRowAt: indexPath)
    }


}

extension  SDLoginTableViewController{
    //    MARK:- Texfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         textField.removeError()
        return true
    }
    func socialLogin(param:[String:Any])
    {
        KRProgressHUD.show()
        ApiService.socialLogin_withDetails(param: param, completion: { (status, result, error) in
            KRProgressHUD.dismiss()
            if error == nil
            {
                if let isFirstlogin = UserDefaults.standard.value(forKey: AppKeys.IsFirstLogin.rawValue) as? Bool
                {
                    if isFirstlogin == true
                    {
                        let tutVC = self.storyboard?.instantiateViewController(withIdentifier: "SDTutorialViewController") as! SDTutorialViewController
                        self.present(tutVC, animated: true, completion: {
                            
                        })
                    }
                    else
                    {
                        let delegate = UIApplication.shared.delegate as! AppDelegate
                        delegate.setRootView()
                    }
                }
                else
                {
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.setRootView()
                }
                
            }
            else
            {
                self.show_OK_AlertWith_Title(title: "Error", message: (error?.localizedDescription)!, completion: nil)
            }
            
        })
    }
    func login(param:[String:Any])
    {
        KRProgressHUD.show()
        ApiService.login_User_With(param: param, completion: { (status, result, error) in
            KRProgressHUD.dismiss()
            if error == nil
            {
                if let isFirstlogin = UserDefaults.standard.value(forKey: AppKeys.IsFirstLogin.rawValue) as? Bool
                {
                    if isFirstlogin == true
                    {
                        let tutVC = self.storyboard?.instantiateViewController(withIdentifier: "SDTutorialViewController") as! SDTutorialViewController
                        self.present(tutVC, animated: true, completion: {
                            
                        })
                    }
                    else
                    {
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.setRootView()
                    }
                }
                else
                {
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.setRootView()
                }
                
            }
            else
            {
                self.show_OK_AlertWith_Title(title: "Error", message: (error?.localizedDescription)!, completion: nil)
            }
            
        })
    }
    
    func validateFields(completion:(_ status:Bool,_ message:String)->Void)
    {
        var isValid = true
        var message = ""
        
        if (txtFieldEmail.text?.isEmpty)!
        {
            isValid = false
            txtFieldEmail.showError()
            message = "Please fill all the required fields"
        }
        else if (txtFieldPassword.text?.isEmpty)!
        {
            isValid = false
            txtFieldPassword.showError()
            message = "Please fill all the required fields"
        }
            
        else if !isValidEmail(txtFieldEmail.text!)
        {
            isValid = false
            txtFieldEmail.showError()
            message = "Please enter a valid email"
        }
        else if !Utilities.isPasswordValid(txtFieldPassword.text!)
        {
            isValid = false
            txtFieldPassword.showError()
            message = "Please enter valid password"
            
        }
        completion(isValid,message)
    }
    
}
