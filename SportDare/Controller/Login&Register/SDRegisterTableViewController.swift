//
//  SDRegisterTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class SDRegisterTableViewController: BaseTableViewController {
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var mandatoryFields: [CustomTextField]!
    @IBOutlet weak var txtFieldName: CustomTextField!
    @IBOutlet weak var txtFieldSurname: CustomTextField!
    @IBOutlet weak var txtFieldEmail: CustomTextField!
    @IBOutlet weak var txtFieldDOB: CustomTextField!
    
    @IBOutlet weak var txtFieldGuardian: CustomTextField!
    @IBOutlet weak var txtFieldConfirmPwd: CustomTextField!
    @IBOutlet weak var buttonAccept: CustomButton!
    @IBOutlet weak var txtFieldPassword: CustomTextField!
    var isAdult = true
    var guardianDropDown :DropDown!
    var guardianId : String!
    override func viewDidLoad() {
        super.viewDidLoad()
          setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
   internal func setupUI()
   {
     
    txtFieldDOB.inputView = datePicker
    txtFieldDOB.inputAccessoryView = toolBar
    self.datePicker.maximumDate = Date()
    self.tableView.delegate = self
    
    guardianDropDown = DropDown(anchorView: txtFieldGuardian)
    
    guardianDropDown.bottomOffset = CGPoint(x: 0, y: 30)
   
   
    }
    
  
   
//    MARK: Actions
    
    @IBAction func didPickDOB(_ sender: UIDatePicker) {
        if txtFieldDOB.isFirstResponder
        {
           txtFieldDOB.text =  sender.date.dateOnly
            isAdult = sender.date.isAdult
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
    
    @IBAction func acceptTerms(_ sender: CustomButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func viewSportDareAction(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(Constants.sportDareUrl)
        {
            UIApplication.shared.open(Constants.sportDareUrl, options: [:], completionHandler: nil)
        }
    }
    @IBAction func signUp_Action(_ sender: Any) {
       
        self.validateFields { (status, message) in
            if !status
            {
                self.showAlertWithOkButton(message: message)
            }
            else
            {
                self.SignUp(param: getRegDetails())
            }
        }
        
    }
    
    @IBAction func doneAction(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    @IBAction func FBLogin_Action(_ sender: CustomButton) {

        if let _ = AccessToken.current
        {
            GraphRequest(graphPath: "me", parameters:["fields":"first_name,email,last_name,birthday,gender,picture.width(250).height(250)"]).start(completionHandler: { (requst, result , error) in
                if let detail = result as? NSDictionary{
                    self.socialLogin(param: detail as! [String : Any])
                }
            })
        }
        else
        {
            let loginManager = LoginManager()
            loginManager.logIn(permissions: ["user_age_range","email","user_birthday,public_profile,user_gender"], from: self) { (result, error) in
                if result != nil
                {
                    GraphRequest(graphPath: "me", parameters: ["fields":"first_name,email,last_name,birthday,gender,picture.width(250).height(250)"]).start(completionHandler: { (requst, result, error) in
                        if let detail = result as? NSDictionary{
                            self.socialLogin(param: detail  as! [String : Any])
                        }
                        
                    })
                }
                
            }
        }
    }
    
    
    func fbSignup(detail:NSDictionary)
    {
        let email = detail["email"]
        let firstName = detail["first_name"]
        let lastName = detail["last_name"]
        let id =  detail["id"]
        var regDetails = self.getRegDetails()
        regDetails["email"] = email
        regDetails["name"] = firstName
        regDetails["surName"] = lastName
        regDetails["social_agent"] = "facebook"
        regDetails["social_id"] = id
        regDetails["social_image_url"] = "http://graph.facebook.com/\(id!)/picture?type=large"
        self.socialLogin(param: regDetails)
    }
    
    
    func socialLogin(param:[String:Any])
    {
        KRProgressHUD.show()
        ApiService.socialLogin_withDetails(param: param, completion: { (status, result, error) in
            KRProgressHUD.dismiss()
            if error == nil
            {
                
                if self.isAdult
                {
                    do
                    {
                        let encoder = JSONEncoder()
                        let jsonData = try encoder.encode( UserBase.currentUser)
                        UserDefaults.standard.set(jsonData, forKey: AppKeys.UserDetail.rawValue)
                    }catch let error {
                        print(error)
                        
                    }
                    
                    let tutVC = self.storyboard?.instantiateViewController(withIdentifier: "SDTutorialViewController") as! SDTutorialViewController
                    self.present(tutVC, animated: true, completion: {
                        UserDefaults.standard.set(true, forKey: AppKeys.IsFirstLogin.rawValue)
                    })
                    
                    
                }
                else
                {
                    self.show_OK_AlertWith_Title(title: "Success", message: "Your Guardian needs to verify your Sportdare account.Please SignIn after verification", completion: { (idx, str) in
                        DispatchQueue.main.asyncAfter(deadline:.now() + 0.5 , execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                    })
                }
                
            }
            else
            {
                
                self.show_OK_AlertWith_Title(title: "Error", message: (error?.localizedDescription)!, completion: nil)
            }
            
        })
    }
    func SignUp(param:[String:Any])
    {
        
        KRProgressHUD.show()
        ApiService.registerUser_withDetails(param: param, completion: { (status, result, error) in
            KRProgressHUD.dismiss()
            if error == nil
            {
                
                if self.isAdult
                {
                    do
                    {
                        let encoder = JSONEncoder()
                        let jsonData = try encoder.encode( UserBase.currentUser)
                        UserDefaults.standard.set(jsonData, forKey: AppKeys.UserDetail.rawValue)
                    }catch let error {
                        print(error)
                        
                    }
                    
                    let tutVC = self.storyboard?.instantiateViewController(withIdentifier: "SDTutorialViewController") as! SDTutorialViewController
                    self.present(tutVC, animated: true, completion: {
                        UserDefaults.standard.set(true, forKey: AppKeys.IsFirstLogin.rawValue)
                    })
                    
                    
                }
                else
                {
                    self.show_OK_AlertWith_Title(title: "Success", message: "Your Guardian needs to verify your Sportdare account.Please SignIn after verification", completion: { (idx, str) in
                        DispatchQueue.main.asyncAfter(deadline:.now() + 0.5 , execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                    })
                }
                
            }
            else
            {
                
                self.show_OK_AlertWith_Title(title: "Error", message: (error?.localizedDescription)!, completion: nil)
            }
            
        })
    }
}

extension SDRegisterTableViewController{
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 5 && isAdult
        {
            return 0.0
        }
        else
        {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    
   
    //    MARK:- Texfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        return true
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == txtFieldGuardian
//        {
//            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//            getGuardianList_For(key: str)
//
//        }
        return true
    }
    
    
    
    //    MARK:- Field Validation
    func validateFields(completion:(_ status:Bool,_ message:String)->Void)
    {
        var isValid = true
        var message = ""
        for field in mandatoryFields
        {
            if (field.text?.isEmpty)!
            {
                isValid = false
                field.showError()
                message = "Please fill all the required fields"
            }
        }
        
        if isValid
        {
            if !isAdult
            {
                if (txtFieldGuardian.text?.isEmpty)!
                {
                    isValid = false
                    txtFieldGuardian.showError()
                     message = "Please give a guardian email"
                }
                else if !isValidEmail(txtFieldGuardian.text!)
                {
                    isValid = false
                    txtFieldGuardian.showError()
                    message = "Please enter a valid email"
                }
            }
            else if !isValidEmail(txtFieldEmail.text!)
            {
                isValid = false
                txtFieldEmail.showError()
                message = "Please enter a valid email"
            }
            else if !Utilities.isPasswordValid(txtFieldPassword.text!)
            {
                isValid = false
                txtFieldPassword.showError()
                message = "Password should contain minimum 6 characters including alphabets and numbers."
                
            }
            else if txtFieldPassword.text != txtFieldConfirmPwd.text
            {
                isValid = false
                txtFieldConfirmPwd.showError()
                message = "Password do not match"
            }
            else if !buttonAccept.isSelected
            {
                 isValid = false
                 message = "Please accept our terms and conditions before you proceed"
            }
            
        }
        completion(isValid,message)
       
    }
    
    func getRegDetails()->[String:Any]
    {
        return ["name":txtFieldName.text!,"surName":txtFieldSurname.text!,"DOB":txtFieldDOB.text!,"email":txtFieldEmail.text!,"password":txtFieldPassword.text!,"location":"","guardian_email":txtFieldGuardian.text!,"gender":"","device_type":"ios","device_id":Constants.deviceTokenString,"time_zone":TimeZone.current.identifier,"referal_id":Constants.referralId]
        
    }
    
    //    MARK:- Get Guardian List
    func getGuardianList_For(key:String)
    {
        ApiService.searchGuardian(key: key) {[weak self] (status, guardianList) in
            if status
            {
                self?.guardianDropDown.dataSource = (guardianList?.map({$0.name! + " " + $0.surName!}))!
                 self?.guardianDropDown.direction = .bottom
                self?.guardianDropDown.show()
                self?.guardianDropDown.selectionAction = { (index,value) in
                    self?.txtFieldGuardian.text = value
                    self?.guardianId = guardianList![index].userid
                }
            }
            
        }
    }
}
