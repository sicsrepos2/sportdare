//
//  SDForgotPasswordViewController.swift
//  Sportdare
//
//  Created by Binoy T on 08/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDForgotPasswordViewController: BaseViewController {

    @IBOutlet weak var txtFieldEmail: CustomTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func submitAction(_ sender: CustomButton) {
        if !isValidEmail(txtFieldEmail.text!)
        {
            txtFieldEmail.showError()
             self.show_OK_AlertWith_Title(title: "Invalid Email", message: "Please enter a valid email", completion: nil)
           
        }
        else
        {
            self.submitEmail()
        }
    }
    func submitEmail()
    {
        KRProgressHUD.show()
        ApiService.forgot_Password(param: ["email":txtFieldEmail.text!]) { (status, result, error) in
            if error == nil
            {
                KRProgressHUD.showSuccess(withMessage: "Password reset link has been sent to your email.")
                self.dismiss(animated: true, completion: {
                    
                })
                
            }
            else
            {
                KRProgressHUD.dismiss()
                self.show_OK_AlertWith_Title(title: "Error", message: (error?.localizedDescription)!, completion: nil)
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.removeError()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
