//
//  SD_MyRoots_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 14/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_MyRoots_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
     var roots : [Root]?
    @IBOutlet weak var tableViewMyRoots: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewMyRoots.estimatedRowHeight = 44.0
        tableViewMyRoots.rowHeight = UITableView.automaticDimension
        roots = [Root]()
        getRoots()
        // Do any additional setup after loading the view.
    }
    func getRoots()
    {
        ApiService.getRoots { (status, myRoots, error) in
            if status == true
            {
                self.roots = myRoots
                self.tableViewMyRoots.reloadData()
            }
        }
    }
    @IBAction func unrootAction(_ sender: CustomButton) {
        let root = roots![sender.tag]
        self.showAlertWith(title: "UNROOT?", message: "Are you sure to Unroot \(root.first_name!)", CancelBtnTitle: "No", OtherBtnTitle: "Yes") { (index, str) in
            if index == 1
            {
                self.roots?.remove(at: sender.tag)
                self.tableViewMyRoots.deleteRows(at: [IndexPath(item: sender.tag, section: 0)], with: .left)
                self.tableViewMyRoots.reloadData()
                
            }
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roots?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewMyRoots.dequeueReusableCell(withIdentifier: "cellFans") as! SDRootsTableViewCell
        cell.btnUnroot.tag = indexPath.row
        cell.root = roots![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
