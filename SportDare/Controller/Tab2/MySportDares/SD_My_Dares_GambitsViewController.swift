//
//  SD_My_Dares_GambitsViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_My_Dares_GambitsViewController: BaseViewController {

    @IBOutlet weak var labelGambit: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
       

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        labelGambit.text = UserBase.currentUser?.details?.gambits
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getIt(_ sender: SDCustomButton) {
        self.performSegue(withIdentifier: "get", sender: self)
        
    }
    @IBAction func UseIt(_ sender: SDCustomButton) {
        self.performSegue(withIdentifier: "use", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
