//
//  SD_My_Dares_ActivitiesViewController.swift
//  Sportdare
//
//  Created by Binoy T on 25/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import BonsaiController
class SD_My_Dares_ActivitiesViewController: BaseViewController,CalendarProtocol,PopupViewControllerDelegate,DareActionProtocol{
   
    
    @IBOutlet weak var tabHeight: NSLayoutConstraint!
    @IBOutlet weak var btnAddDare: SDCustomButton!
    @IBOutlet weak var btnCalendar: SDCustomButton!
    @IBOutlet weak var btnTimeline: SDCustomButton!
    @IBOutlet weak var calendarView: BNCustomCalendar!
    @IBOutlet weak var tableViewTimeline: UITableView!
       var isFromProfile = Bool()
       var isLoading = false
       var posts : [Posts]?
    var viewHeight : CGFloat!
    var refreshControl = UIRefreshControl()
    
      var expandedCells = Set<Int>()
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarView.calendarDelegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.tintColor = UIColor.appBase
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableViewTimeline.addSubview(refreshControl)
        tableViewTimeline.estimatedRowHeight = 300.0
        tableViewTimeline.rowHeight = UITableView.automaticDimension

        posts = [Posts]()
       loadData()
        if isFromProfile
        {
            btnTimeline.isSelected = true
            btnCalendar.isSelected = false
            calendarView.isHidden = true
            tableViewTimeline.isHidden = false
            btnAddDare.title = "Add anything sport"
        }
        else
        {
            btnCalendar.isSelected = true
            btnTimeline.isSelected = false
            calendarView.isHidden = false
            tableViewTimeline.isHidden = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.didDidSetHeight(sender:)), name: Notification.Name.didUpdateSize, object: nil)
        if isFromProfile
       {
        tabHeight.constant = 50
        }
        else
       {
        tabHeight.constant = 0
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//       self.showTabBar()
        if !isFromProfile
        {
              self.showTabBar()
        }
        let rect = self.view.frame
        if rect.origin.y < 0
        {
             self.btnAddDare.isHidden = true
        }
        else
        {
        self.btnAddDare.isHidden = false
        }
        getDares()
    }
    @objc func didDidSetHeight(sender:NSNotification)
    {
        
    }
    
    @objc func refresh(sender:UIRefreshControl)
    {
      loadDataWithoutOffset()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//         self.btnAddDare.isHidden = false
        if !isFromProfile
        {
            self.btnAddDare.isHidden = false
        }
       self.viewHeight =  self.view.frame.height
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        calendarView.layoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
         calendarView.layoutSubviews()
       
         tableViewTimeline.layoutIfNeeded()
       
    }
  
    
    @IBAction func addSportDare(_ sender: SDCustomButton) {
        let addSportVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAddSportDareViewController") as! SDAddSportDareViewController
        self.navigationController?.pushViewController(addSportVC, animated: true)
        
    }
    @IBAction func show_timelineView(_ sender: SDCustomButton) {
        sender.isSelected = true
        btnCalendar.isSelected = false
        calendarView.isHidden = true
        tableViewTimeline.isHidden = false
        tableViewTimeline.reloadData()
    }
    @IBAction func show_CalendarView(_ sender: SDCustomButton) {
        sender.isSelected = true
        btnTimeline.isSelected = false
        calendarView.isHidden = false
        tableViewTimeline.isHidden = true
       changeTopBar(hidden: false, animated: true)
    }
    
    
    @IBAction func highFiveAction(_ sender: UIButton) {
        let post = self.posts![sender.tag]
        post.is_clapped = !post.is_clapped!
        highFive(post: post)
        if  post.is_clapped!
        {
            post.post_claps_count = (post.post_claps_count)! + 1
        }
        else
        {
            
            
            post.post_claps_count = (post.post_claps_count)! - 1
        }
        
        self.tableViewTimeline.reloadRows(at: [IndexPath(item: sender.tag, section: 0)], with: .fade)
    }
   
   
    @IBAction func commentAction(_ sender: UIButton) {
        let post = self.posts![sender.tag]
        let commentsVc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDCommentsViewController") as! SDCommentsViewController
        commentsVc.post = post
        commentsVc.commentProtocol = self
        commentsVc.postId = post.post_id
        self.hideTabBar()
       self.navigationController?.pushViewController(commentsVc, animated: true)
    }
    
    //    MARK:- API Calls
    
    func loadData()
    {
        isLoading = true
        ApiService.getMyPosts(offset: posts!.count) { (status, result, error) in
            if status
            {
                self.isLoading = false
                self.posts!.append(contentsOf: result!)
                self.tableViewTimeline.reloadData()
                
            }
            else if error != nil
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            else
            {
                 self.tableViewTimeline.reloadData()
            }
           
            self.refreshControl.endRefreshing()
        }
        
    }
    func loadDataWithoutOffset()
    {
        isLoading = true
        ApiService.getMyPosts(offset: 0) { (status, result, error) in
            if status
            {
                self.posts?.removeAll()
                self.isLoading = false
                self.posts!.append(contentsOf: result!)
                self.tableViewTimeline.reloadData()
                
            }
            else if error != nil
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            self.refreshControl.endRefreshing()
        }
        
    }
    
    func highFive(post:Posts)
    {
        ApiService.postHighFive_Wall(param: ["type":post.post_type!,"id":post.post_id! as Any]) { (status, result, error) in
            if result != nil
            {
                
                
            }
        }
    }
    
    func getDares()
    {
        
        ApiService.getDares(month: "\( self.calendarView.month + 1)", year: "\( self.calendarView.year)") { (status, result, error) in
            if let dares = result
            {
                self.calendarView.setDares(dares: dares)
            }
        }
    }
    
}

extension SD_My_Dares_ActivitiesViewController:UITableViewDataSource,UITableViewDelegate,TimeLineCellProtocol,UITableViewDataSourcePrefetching,PreviewVCProtocol,BonsaiControllerDelegate,CommentsProtocol{
    //    MARK:- Calendar Delegate
    func didSelectDate(date: Date) {
        
    }
    
    func didChangedMonth(calendar: BNCustomCalendar) {
        getDares()
    }
    //    MARK:- Select Dare
    func didSelectDare(dare: [DareData]) {
        if dare.count > 1
        {
            let dareListVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDDareListTableViewController") as? SDDareListTableViewController
            self.hideTabBar()
            dareListVc?.darelist = dare
            self.navigationController?.pushViewController(dareListVc!, animated: true)
        }
        else
        {
            let dareDetailVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDDareDetailTableViewController") as? SDDareDetailTableViewController
            dareDetailVc?.dareProtocol = self
            dareDetailVc?.dare = dare.last
            let popupVC = PopupViewController(contentController: dareDetailVc!, position: PopupViewController.PopupPosition.top(100), popupWidth: self.view.frame.width * 0.85, popupHeight: self.view.frame.height)
            popupVC.backgroundAlpha = 0.3
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 10
            popupVC.shadowEnabled = true
            popupVC.delegate = self
            self.present(popupVC, animated: true) {
               
              
            }
        }
        
    }
    func didAddedComment() {
        
    }
    func didUpdatePost(post: Posts) {
        let changedIndex =  self.posts!.firstIndex(where: {$0.post_id == post.post_id})
        self.posts?.remove(at: changedIndex!)
        if (self.posts?.count)! > changedIndex!
        {
            self.posts?.insert(post, at: changedIndex!)
        }
        else
        {
            self.posts?.insert(post, at: changedIndex!-1)
            
        }
        self.tableViewTimeline.reloadData()
    }
    
    
    
    func didDeletedMedia_at(index: Int) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return (posts?.count)!
        
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let post = posts![indexPath.row]
        if post.post_type == PostType.text
        {
            let cell = tableViewTimeline.dequeueReusableCell(withIdentifier: "commentCell") as! SDMy_Timeline_TableViewCell
            cell.textViewDescription.shouldTrim = !expandedCells.contains(indexPath.row)
            cell.post = post
             cell.delegate = self
            cell.btnComments.tag = indexPath.row
            cell.btnHighFive.tag = indexPath.row
            return cell
        }
        else
        {
            let cell = tableViewTimeline.dequeueReusableCell(withIdentifier: "mediaCell") as! SDMy_Timeline_TableViewCell
            cell.textViewDescription.shouldTrim = !expandedCells.contains(indexPath.row)
            cell.post = post
            cell.delegate = self
            cell.btnComments.tag = indexPath.row
            cell.btnHighFive.tag = indexPath.row
          
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let post = posts![indexPath.row]
        if post.post_type == PostType.text
        {
        return UITableView.automaticDimension
        }
        else
        {

              return  UITableView.automaticDimension

        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastData = self.posts!.count - 1
        if !isLoading && indexPath.row == lastData {
            self.loadData()
        }
        
        if let textView = (cell as? SDMy_Timeline_TableViewCell)?.textViewDescription
        {
            textView.onSizeChange = { [unowned tableView, unowned self] r in
            let point = tableView.convert(r.bounds.origin, from: r)
        
            guard let indexPath = tableView.indexPathForRow(at: point) else { return }
                if r.shouldTrim {
                    self.expandedCells.remove(indexPath.row)
                } else {
                    self.expandedCells.insert(indexPath.row)
                }
//                self.tableViewTimeline.reloadData()
                self.tableViewTimeline.beginUpdates()
                self.tableViewTimeline.endUpdates()
            
            
            }
            
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       

    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0{
            changeTopBar(hidden: true, animated: true)
        }
        else{
            changeTopBar(hidden: false, animated: true)
        }
    }
    
    func changeTopBar(hidden:Bool, animated: Bool){
        if  btnAddDare.isHidden  == hidden{
            return
        }
        print(hidden)
        
        let duration:TimeInterval = (animated ? 0.5 : 0.0)
        btnAddDare.isHidden = false
        let originOffset:CGFloat = hidden ? -100 : 100
        
        var rect = self.view.frame
      
        //        rect.origin.y = hidden ? (-originOffset) : 0
    
         rect.size.height = hidden ? self.viewHeight + 100 :  self.viewHeight
        
        UIView.animate(withDuration: duration, animations: {
           
            self.view.frame = rect.offsetBy(dx: 0, dy: originOffset)
        }, completion: { (true) in
            
            self.btnAddDare.isHidden = hidden
        })
    }
    
    //    MARK:- Popup List Controller
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: 0, y: 70), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height-70))
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
    }
    
    //    MARK:- Post Cell Delegates
    func didSelectPost(post_id: String, index: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SDPostPreviewVCViewController") as! SDPostPreviewVCViewController
        vc.previewDelegate = self
        if let post = (posts?.filter({$0.post_id == post_id}).last)
        {
        vc.mediaArray = post.post_media
        vc.post = post
        }
        vc.passedContentOffset = index
         self.hideTabBar()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func didSelectMore(post_Id: String) {
        let alertOptions = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Remove Post", style: .destructive) { (action) in
            
            self.removePost(id: post_Id)
        }
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertOptions.addAction(deleteAction)
        alertOptions.addAction(cancelAction)
        self.present(alertOptions, animated: true) {
            
        }
    }
    func removePost(id:String)
      {
        KRProgressHUD.show()
        ApiService.removePost(postId: id) { (status, result, error) in
            KRProgressHUD.dismiss()
            if status
            {
                self.posts?.removeAll(where:{ $0.post_id == id})
                self.tableViewTimeline.reloadData()
                self.loadData()
            }
        }
    }
    func didAcceptOrReject() {
        self.getDares()
    }
    
    func didCompleteActions() {
        self.getDares()
    }
    func didChangeDareStatus(dare: DareData) {
        self.getDares()
    }
}
