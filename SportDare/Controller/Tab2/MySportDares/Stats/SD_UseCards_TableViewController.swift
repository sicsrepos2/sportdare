//
//  SD_UseCards_TableViewController.swift
//  Sportdare
//
//  Created by SICS on 26/07/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
protocol CardsProtocol {
    func didSelectCard(_ card:Card_details)
}
class SD_UseCards_TableViewController: BaseTableViewController {

    @IBOutlet weak var labelGreenCardsCount: UILabel!
    @IBOutlet weak var labelYellowCount: UILabel!
    
    @IBOutlet weak var labelRedCount: UILabel!
    @IBOutlet weak var closeView : UIView!
    @IBOutlet weak var tableViewCards: UITableView!
    var cards : [Card_details]?
    var delegate:CardsProtocol?
    var isFromDare = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cards"
        // Do any additional setup after loading the view.
        closeView.isHidden = !isFromDare
        getAvailableCards()
    }
    
    func getAvailableCards()
    {
        ApiService.getMyCards { (status, result) in
            if status{
                self.cards = result?.card_details
                self.setUi()
                
            }
            
        }
    }
    func setUi()
    {
        let greenCount =  cards!.filter{$0.card_id == "3"}.first?.cards_count
        let redCount =  cards!.filter{$0.card_id == "2"}.first?.cards_count
        let yellowCount =  cards!.filter{$0.card_id == "1"}.first?.cards_count
        labelRedCount.text = "X\(redCount!)"
        labelYellowCount.text = "X\(yellowCount!)"
        labelGreenCardsCount.text = "X\(greenCount!)"
        
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let card = cards![indexPath.row]
        didSelectCard(card: card)
    }
    
    func didSelectCard(card:Card_details)
    {
        if card.cards_count != "0"
        {
            delegate?.didSelectCard(card)
        }
        else
        {
            self.show_OK_AlertWith_Title(title: "No Cards", message: "You don't have enough cards ?", completion: nil)
        }
    }

}
