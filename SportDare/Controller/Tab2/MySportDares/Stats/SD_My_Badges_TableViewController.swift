//
//  SD_My_Badges_TableViewController.swift
//  Sportdare
//
//  Created by SICS on 24/06/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import iCarousel
class SD_My_Badges_TableViewController: BaseTableViewController {
    @IBOutlet weak var gambitsCarousel: iCarousel!
    
    @IBOutlet weak var dareCarousel: iCarousel!
    @IBOutlet weak var surpriseCarousel: iCarousel!
    var badges : Badges_Base?{
        didSet{
         
            gambitsCarousel.reloadData()
            dareCarousel.reloadData()
            surpriseCarousel.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Badges"
        getBadges()
       
    }

    
    func getBadges()
    {
        ApiService.getMyBadges { (status, badges,error) in
            if status
            {
                self.badges = badges
            }
            else{
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
        }
    }
    // MARK: - Table view data source

    @IBAction func actionGambits(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            if gambitsCarousel.currentItemIndex > 0
            {
            gambitsCarousel.scrollToItem(at: gambitsCarousel.currentItemIndex - 1, animated: true)
            }
            case 2:
                if gambitsCarousel.currentItemIndex < gambitsCarousel.numberOfItems - 1
                {
                    gambitsCarousel.scrollToItem(at: gambitsCarousel.currentItemIndex + 1, animated: true)
            }
        default:
            break
        }
    }
    @IBAction func actionDare(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            if dareCarousel.currentItemIndex > 0
            {
                dareCarousel.scrollToItem(at: dareCarousel.currentItemIndex - 1, animated: true)
            }
        case 2:
            if dareCarousel.currentItemIndex < dareCarousel.numberOfItems - 1
            {
                dareCarousel.scrollToItem(at: dareCarousel.currentItemIndex + 1, animated: true)
            }
        default:
            break
        }
    }
    
    @IBAction func actionSurprise(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            if surpriseCarousel.currentItemIndex > 0
            {
                surpriseCarousel.scrollToItem(at: surpriseCarousel.currentItemIndex - 1, animated: true)
            }
        case 2:
            if surpriseCarousel.currentItemIndex < surpriseCarousel.numberOfItems - 1
            {
                surpriseCarousel.scrollToItem(at: surpriseCarousel.currentItemIndex + 1, animated: true)
            }
        default:
            break
        }
    }
}

extension  SD_My_Badges_TableViewController:iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        switch carousel {
        case gambitsCarousel:
            return badges?.gambits?.count ?? 0
        case dareCarousel:
            return badges?.challenge?.count ?? 0
        case surpriseCarousel:
            return badges?.surprise?.count == 0 ? 1: badges!.surprise!.count
        default:
             return 0
        }
       
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let badgeView = Bundle.main.loadNibNamed("TeamView", owner: self, options: nil)?[1] as! BadgeView
       badgeView.carousel = carousel
        switch carousel {
        case gambitsCarousel:
            let badge = badges?.gambits![index]
            badgeView.gambits = badge
        
        case dareCarousel:
            let badge = badges?.challenge![index]
           badgeView.challenge = badge
        case surpriseCarousel:
            if (badges?.surprise!.count)! > 0
           {
             badgeView.surprise = badges?.surprise![index]
            }
            else
           {
            badgeView.imageViewBadge.image = UIImage(named: "random")
            }
        default:
            break
        }
        return badgeView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        else if option == .visibleItems
        {
            return 1
        }
        return value
    }
}
