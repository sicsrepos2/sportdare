//
//  SD_MyRanking_ViewController.swift
//  Sportdare
//
//  Created by SICS on 20/06/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import iCarousel
class SD_MyRanking_ViewController: BaseViewController,iCarouselDataSource,iCarouselDelegate {
    @IBOutlet weak var labelRanking: UILabel!
     @IBOutlet weak var playerRankingLabel: UILabel!
    @IBOutlet weak var teamViewCarousel: iCarousel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.title = "Ranking"
        labelRanking.text = "\(UserBase.currentUser!.details!.rank!)"
        // Do any additional setup after loading the view.
    }
    


    func numberOfItems(in carousel: iCarousel) -> Int {
        return 1
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let  teamView = Bundle.main.loadNibNamed("TeamView", owner: self, options: nil)?[0] as! SDCustomButton
        
        return teamView
    }
    
}
