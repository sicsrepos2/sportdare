//
//  SD_My_Cards_ViewController.swift
//  Sportdare
//
//  Created by SICS on 20/06/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SD_My_Cards_ViewController: BaseTableViewController {
    @IBOutlet weak var labelGreenCardsCount: UILabel!
    @IBOutlet weak var labelYellowCount: UILabel!
    
    @IBOutlet weak var labelRedCount: UILabel!
    @IBOutlet weak var closeView : UIView!
    @IBOutlet weak var tableViewCards: UITableView!
    var cards : [Card_details]?
    var isFromDare = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
         self.title = "Cards"
        // Do any additional setup after loading the view.
        closeView.isHidden = true
        getAvailableCards()
    }
    
    func getAvailableCards()
    {
        ApiService.getMyCards { (status, result) in
            if status{
                self.cards = result?.card_details
                self.setUi()
               
            }
            
        }
    }
    func setUi()
    {
        let greenCount =  cards!.filter{$0.card_id == "3"}.first?.cards_count
         let redCount =  cards!.filter{$0.card_id == "2"}.first?.cards_count
         let yellowCount =  cards!.filter{$0.card_id == "1"}.first?.cards_count
        labelRedCount.text = "X\(redCount!)"
        labelYellowCount.text = "X\(yellowCount!)"
        labelGreenCardsCount.text = "X\(greenCount!)"
        
    }
    @IBAction func buyCardsAction(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            let addSportVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAddSportDareViewController") as! SDAddSportDareViewController
            self.navigationController?.pushViewController(addSportVC, animated: true)
        }
        else
        {
        let card = cards![sender.tag]
        self.showAlertWith(title: "Buy \(card.card_name!) ?", message: "200 Gambits will be needed", CancelBtnTitle: "No", OtherBtnTitle: "Yes") { (index, title) in
            if index == 1
            {
                self.buyCards(card: card)
            }
        }
        }
        
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    func buyCards(card:Card_details)
    {
        KRProgressHUD.show()
        ApiService.buyCard(cardId: card.card_id!) { (status, result, error) in
            if status
            {
                KRProgressHUD.showSuccess(withMessage: "You bought a \(card.card_name!)")
            }
            else
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func didSelectCard(card:Card_details)
    {

    }
}

    
    

