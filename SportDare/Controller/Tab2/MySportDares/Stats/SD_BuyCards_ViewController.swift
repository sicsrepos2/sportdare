//
//  SD_BuyCards_ViewController.swift
//  Sportdare
//
//  Created by SICS on 21/06/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SD_BuyCards_ViewController: BaseViewController {

    @IBOutlet weak var tableViewCards: UITableView!
    var cards : [Card_details]?
    override func viewDidLoad() {
        super.viewDidLoad()
        getAvailableCards()
        self.title = "BUY CARDS"
        // Do any additional setup after loading the view.
    }
    
    func getAvailableCards()
    {
        ApiService.getAvailableCards { (status, result) in
            if status{
                self.cards = result?.card_details
                self.tableViewCards.reloadData()
            }
            
        }
    }
    
    @IBAction func buyCardsAction(_ sender: UIButton) {
        let card = cards![sender.tag]
        KRProgressHUD.show()
        ApiService.buyCard(cardId: card.card_id!) { (status, result, error) in
            if status
            {
                KRProgressHUD.showSuccess(withMessage: "You bought a \(card.card_name!)")
            }
            else
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            
        }
        
        
    }
}
extension SD_BuyCards_ViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell") as! SDCardCellTableViewCell
        cell.card = cards![indexPath.row]
        cell.btnBuy.tag = indexPath.row
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
}
