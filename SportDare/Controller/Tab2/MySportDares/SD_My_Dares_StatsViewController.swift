//
//  SD_My_Dares_StatsViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_My_Dares_StatsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func viewGambitsRanking(_ sender: SDCustomButton) {
        let rankingVc = self.storyboard?.instantiateViewController(withIdentifier: "SD_MyRanking_ViewController") as! SD_MyRanking_ViewController
        self.navigationController?.pushViewController(rankingVc, animated: true)
    }
    @IBAction func viewBadges(_ sender: SDCustomButton) {
        let badgesVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_My_Badges_TableViewController") as! SD_My_Badges_TableViewController
        self.navigationController?.pushViewController(badgesVC, animated: true)
    }
    
    @IBAction func viewCards(_ sender: SDCustomButton) {
//        SD_My_Cards_ViewController
        let cardsVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_My_Cards_ViewController") as! SD_My_Cards_ViewController
        self.navigationController?.pushViewController(cardsVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
