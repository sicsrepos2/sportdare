//
//  SD_MyTeams_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 14/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_MyTeams_ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PopupViewControllerDelegate,AddTeamProtocol {

    @IBOutlet weak var tableViewTeams: UITableView!
      var teams :[Team]?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewTeams.estimatedRowHeight = 44.0
        tableViewTeams.rowHeight = UITableView.automaticDimension
        teams = [Team]()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getTeams()
    }
    
    @IBAction func addTeamAction(_ sender: SDCustomButton) {
        let addTeamVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAdd_Team_TableViewController") as? SDAdd_Team_TableViewController
        addTeamVC?.addTeamDelegate = self
        let popupVC = PopupViewController(contentController: addTeamVC!, position: PopupViewController.PopupPosition.top(40), popupWidth: self.view.frame.width * 0.85, popupHeight: UIScreen.main.bounds.height * 0.80)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        popupVC.delegate = self
        self.present(popupVC, animated: true) {
            
        }
    }
    
    func getTeams()
    {
        ApiService.getTeams { (status, teamList, error) in
            if status == true && error == nil
            {
                self.teams = teamList
                self.tableViewTeams.reloadData()
            }
        }
    }
    
    func popupViewControllerDidDismiss(sender: PopupViewController) {
        self.getTeams()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewTeams.dequeueReusableCell(withIdentifier: "cellTeam") as! SDTeamTableViewCell
        cell.team = teams![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
    func didAddedTeam() {
        self.getTeams()
    }

}
