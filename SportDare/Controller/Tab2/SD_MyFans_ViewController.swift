//
//  SD_MyFans_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 14/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_MyFans_ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableViewFans: UITableView!
//    var fans:
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewFans.estimatedRowHeight = 44.0
        tableViewFans.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        setEmpty()
    }
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewFans.dequeueReusableCell(withIdentifier: "cellFans")
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }

}
