//
//  SD_MyHeroes_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 14/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_MyHeroes_ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableViewHeroes: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewHeroes.estimatedRowHeight = 44.0
        tableViewHeroes.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewHeroes.dequeueReusableCell(withIdentifier: "cellHeroes")
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
        
    }
    

}
