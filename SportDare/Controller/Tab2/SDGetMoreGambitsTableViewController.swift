//
//  SDGetMoreGambitsTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 18/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import SwiftyStoreKit
class SDGetMoreGambitsTableViewController: BaseTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func inviteFriends(_ sender: UIButton) {
        Utilities.showShareSheet(vc: self)
    }
    
    @IBAction func buyGambits(_ sender: UIButton) {
        showActions(productType: .gambits)
    }
    
    @IBAction func upgradeAccount(_ sender: UIButton) {
        let upgradeVC = self.storyboard?.instantiateViewController(withIdentifier: "SDUpgradeViewController") as! SDUpgradeViewController
        
        self.navigationController?.pushViewController(upgradeVC, animated: true)
    }
    @IBAction func dare(_ sender: UIButton) {
        let addSportVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAddSportDareViewController") as! SDAddSportDareViewController
        self.navigationController?.pushViewController(addSportVC, animated: true)
    }
    
    func showActions(productType:InAppServices) {
        
        
        let actionSheetController = UIAlertController(title: productType.name, message: "Buy \(productType.name)?", preferredStyle: UIAlertController.Style.actionSheet)
        
        let buyAction = UIAlertAction(title: "Buy", style: UIAlertAction.Style.default) { (action) -> Void in
            self.requestProductInfo(productId:productType.rawValue)
            KRProgressHUD.show()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) -> Void in
            
        }
        
        actionSheetController.addAction(buyAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true) {
            
        }
    }
    func requestProductInfo(productId:String) {
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { result in
            KRProgressHUD.dismiss()
            if case .success(let purchase) = result {
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                self.getGambits()
              
            } else {
                // purchase error
            }
        }
        
    }
    
    func getGambits()
    {
        let param = ["user_id":UserBase.currentUser!.details!.userid!,"gambits":"100","message":"bought via inapp purchase","privacy":"4"]
        
        ApiService.buyGambits(param: param as [String : Any]) { (status, result, error) in
            if status
            {
                ApiService.viewFullProfile(completion: { (status, user, error) in
                    
                })
            }
        }
    }
}
