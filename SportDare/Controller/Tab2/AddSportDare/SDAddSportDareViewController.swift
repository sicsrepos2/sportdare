//
//  SDAddSportDareTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 18/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDAddSportDareViewController: BaseViewController {
    @IBOutlet weak var btnChallenge: SDCustomButton!
    @IBOutlet weak var btnEvent: SDCustomButton!
    @IBOutlet weak var btnTraining: SDCustomButton!
    @IBOutlet weak var containerMain: UIView!
    var darePlayer :PlayerDetails?
    var addChallengeVC : SDAdd_ChallengeTableViewController!
    var addTrainingVC : SDAdd_TrainingTableViewController!
    var addEventVC : SDAdd_EventTableViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnChallenge.isSelected = true
        loadVC()
        btnChallenge.isSelected = true
        btnTraining.isSelected = false
        btnEvent.isSelected = false
//        self.removeChildVC()
        self.displayContentController(content: addChallengeVC)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {

    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    // MARK: - Table view data source

    @IBAction func didSelectChallenge(_ sender: SDCustomButton) {
        btnChallenge.isSelected = true
        btnTraining.isSelected = false
        btnEvent.isSelected = false
        self.removeChildVC()
        self.displayContentController(content: addChallengeVC)
       
    }
    
    @IBAction func didSelectTraining(_ sender: SDCustomButton) {
        btnTraining.isSelected = true
        btnEvent.isSelected = false
        btnChallenge.isSelected = false
        self.removeChildVC()
       self.displayContentController(content: addTrainingVC)
       
    }
    
    @IBAction func didSelectEvent(_ sender: SDCustomButton) {
         btnEvent.isSelected = true
        btnChallenge.isSelected = false
        btnTraining.isSelected = false
        self.removeChildVC()
        self.displayContentController(content: addEventVC)
  
        
    }
    
   


}
extension SDAddSportDareViewController{
    
    func displayContentController(content: UIViewController) {
        addChild(content)
        content.view.frame = CGRect(x: 0, y: 0, width: containerMain.frame.width, height: containerMain.frame.height)
        self.containerMain.addSubview(content.view)
        content.didMove(toParent: self)
    }
    
    func removeChildVC() {
        if let content = self.children.first
        {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
    func loadVC()
    {
  
        addChallengeVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAdd_ChallengeTableViewController") as? SDAdd_ChallengeTableViewController
        
        addTrainingVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAdd_TrainingTableViewController") as? SDAdd_TrainingTableViewController
        addEventVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAdd_EventTableViewController") as? SDAdd_EventTableViewController
        if darePlayer != nil
        {
            addChallengeVC.darePlayer = self.darePlayer
            addTrainingVC.darePlayer = self.darePlayer
            btnEvent.isEnabled = false
            btnEvent.isHighlighted = false
            btnEvent.alpha = 0.5
            
        }
    }
}
