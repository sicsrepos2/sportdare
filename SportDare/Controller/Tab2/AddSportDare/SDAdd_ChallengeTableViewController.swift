//
//  SDAdd_ChallengeTableViewController.swift
//  Sportdare
//
//  Created by Binoy T on 30/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import BonsaiController
import Photos
import MobileCoreServices
import SPGooglePlacesAutocomplete

class SDAdd_ChallengeTableViewController: UITableViewController,UITextFieldDelegate{
    
     //    MARK:- Variables
    var dropDown : DropDown!
    var challengetype : Challenge_types!
    var selectedMedia : [SDMedia]!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var timePicker: UIDatePicker!
    @IBOutlet weak var textFieldPlayerName: CustomTextField!
    @IBOutlet weak var textFieldSubSport: CustomTextField!
    @IBOutlet weak var textFieldSport: CustomTextField!
    @IBOutlet weak var txtFieldChallengeType: CustomTextField!
    @IBOutlet weak var textFieldGambits: CustomTextField!
    @IBOutlet weak var textFieldTime: CustomTextField!
    @IBOutlet weak var textFieldDate: CustomTextField!
    @IBOutlet weak var textFieldLocation: CustomTextField!
    @IBOutlet weak var collectionViewMedia: UICollectionView!
    @IBOutlet weak var textViewDescription: KMPlaceholderTextView!
    var selectedSportCategory : Sports_category!
    var selectedSubSport : Sub_category!
    var places_DropDown : DropDown!
    var challengeLocation : CLLocation!
    var selectedPrivacyLevel : Privacy_levels!
    var selectedPlayer : Root?
    var selectedTeam : Team!
    var darePlayer :PlayerDetails?
    var selectedDate : String?
    
    //    MARK: ----------------Internal Methods------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldDate.inputView = datePicker
        textFieldDate.inputAccessoryView = toolBar
        textFieldTime.inputView = timePicker
        textFieldTime.inputAccessoryView = toolBar
        textFieldGambits.inputAccessoryView = toolBar
        textViewDescription.inputAccessoryView = toolBar
        selectedMedia = [SDMedia]()
        selectedPrivacyLevel = Constants.privacyLevels.first
        datePicker.minimumDate = Date()
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if darePlayer != nil
        {
            textFieldPlayerName.isUserInteractionEnabled = false
            self.textFieldPlayerName.text =  "\(self.darePlayer!.name!) \(self.darePlayer!.surName!)"
        }
       txtFieldChallengeType.delegate =  self 
    }
    
    @IBAction func didSelectDate(_ sender: UIDatePicker) {
        textFieldDate.text = sender.date.dateOnly
        selectedDate = sender.date.dateOnlyYYYY_MM_dd
    }
    
    @IBAction func didSelectTime(_ sender: UIDatePicker) {
        textFieldTime.text = sender.date.timeOnly
    }
    @IBAction func doneAction(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    @IBAction func addImageAction(_ sender: UIButton) {
        self.showOptions()
    }
    
    @IBAction func dareAction(_ sender: CustomButton) {
        self.validateFields { (status, message) in
            if status
            {
                KRProgressHUD.show()
                ApiService.sharedInstance.make_challenge(parameters: self.getAllFieldDatas(), files: selectedMedia, completion: { (status, result, error) in
                    
                }, startedBlock: { (started) in
                    KRProgressHUD.showSuccess(withMessage: "You Challenged \(self.selectedPlayer!.first_name!) successfully")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })

                }, progressBlock: { (progress) in
                    
                })
            }
            else
            {
             self.showAlertWithOkButton(message: message)
            }
        }
    }
    
    
    // MARK:----------------UITextField Delegate-----------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
  
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFieldChallengeType 
        {
            showDropDown(on: textField)
            return false
        }
        if textField == textFieldSport
        {
            self.showSportList(dataSource: Constants.sportCategories.map({$0.name!}), title: "Sport Category") { (option,index) in
                textField.text = option
                self.selectedSportCategory = Constants.sportCategories.filter({$0.name == option}).last
                self.selectedSubSport = nil
                self.textFieldSubSport.text = ""
            }
             return false
        }
        if textField == textFieldSubSport
        {
            if let category = selectedSportCategory
            {
                self.showSportList(dataSource: (category.sub_category?.map({$0.name}))! as! [String], title: "Sub Sport Category") { (option,index) in
                    textField.text = option
                    self.selectedSubSport = category.sub_category!.filter({$0.name == option}).last
                }
            }
             return false
        }
        if textField == textFieldPlayerName
        {
            let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SDSearchPlayers_ViewController") as? SDSearchPlayers_ViewController
            searchPage?.delegate = self
            if challengetype != nil
            {
                if self.challengetype.name == Constants.challengeTypes.last?.name
                {
                    searchPage?.isTeamSearch = true
                }
            }
            searchPage?.modalTransitionStyle = .coverVertical
            searchPage!.modalPresentationStyle = .overFullScreen
            self.present(searchPage!, animated: true) {
                
            }
            return false
        }
        if textField == textFieldLocation
        {
           places_DropDown = DropDown(anchorView: textField)
           places_DropDown.bottomOffset = CGPoint(x: 0, y: 30)
           
            return true
        }
        
        return true
    }
    
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldLocation
        {
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            self.showLocationDropDown(key: str)
            
        }
        return true
    }
    
    

    
    //    MARK:- ---------Dropdowns----------
    func showDropDown(on field:UITextField)
    {
        dropDown = DropDown(anchorView: field)
        dropDown.bottomOffset = CGPoint(x: 0, y: 30)
        dropDown.dataSource =  Constants.challengeTypes.map({$0.name!})
        dropDown.show()
        dropDown.selectionAction = { (index,value) in
            self.challengetype = Constants.challengeTypes[index]
            field.text = value
            self.textFieldPlayerName.text = ""
            if index == 2
            {
                self.textFieldPlayerName.placeholder = "Add Team"
                
            }
            else
            {
                self.textFieldPlayerName.placeholder = "Player Name"
            }
        }
    }
    
    func showLocationDropDown(key:String)
    {
        Utilities.getLocations_From(key: key) {[weak self] (places, nameList, error) in
           if error == nil
            {
                DispatchQueue.main.async {
                    self?.places_DropDown.dataSource = nameList!
                    self?.places_DropDown.show()
                    self?.places_DropDown.selectionAction = { (index,value) in
                        self?.textFieldLocation.text = value
                        places![index].resolve(toPlacemark: { (placemark, name, error) in
                            self?.challengeLocation = placemark?.location
                        })
                    }
                }
            }
        }
       
    }
    @IBAction func setPrivacyAction(_ sender: CustomButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.dataSource = Constants.privacyLevels.map({$0.name!})
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            cell.logoImageView.image = Constants.privacyList[item]
            cell.optionLabel.text = item
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = {(index,value) in
            sender.setTitle(value, for: .normal)
            sender.setImage(Constants.privacyList[value], for: .normal)
            self.selectedPrivacyLevel = Constants.privacyLevels[index]
        }
        dropDown.show()
    }
    
    
    
    func showSportList(dataSource:[String],title:String,completion:@escaping (_ option:String,_ index:Int)->Void)
    {
        let listVc =
            self.storyboard?.instantiateViewController(withIdentifier: "SDPopUpListViewController") as! SDPopUpListViewController
        listVc.list = dataSource
        listVc.delegate = self
        listVc.listTitle = title
        listVc.SelectionClosure = {(index,option) in
            completion(option,index)
            
        }
        listVc.transitioningDelegate = self
        listVc.modalPresentationStyle = .custom
        present(listVc, animated: true, completion: nil)
        
    }
    
}
extension SDAdd_ChallengeTableViewController: BonsaiControllerDelegate,PopupListDelegate,SearchPlayersVCProtocol {
    func serchPlayerVC(_ viewController: SDSearchPlayers_ViewController, didSelect roots: [Root]) {
        
    }
    
    //  MARK:- Saerch Protocol, SportCategory Protocol
    func searchPlayerVc(_ viewController: SDSearchPlayers_ViewController, didSelect root: Root) {
        self.selectedPlayer = root
        self.textFieldPlayerName.text =  "\(self.selectedPlayer!.first_name!) \(self.selectedPlayer!.surname!)"
    }
    func searchPlayerVc(_ viewController: SDSearchPlayers_ViewController, didSelect team: Team) {
        self.selectedTeam = team
        self.textFieldPlayerName.text =  "\(self.selectedTeam!.team_name!)"
    }
    func popController(controller: SDPopUpListViewController, didSelect option: String, at Index: Int) {
        
    }
    
    
    // return the frame of your Bonsai View Controller
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        
        return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.height / 4), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height / (4/3)))
    }
    
    // return a Bonsai Controller with SlideIn or Bubble transition animator
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        // Slide animation from .left, .right, .top, .bottom
        if  let vc = presented as? ImagePreviewVC
        {
            return  BonsaiController(fromView: collectionViewMedia.cellForItem(at: collectionViewMedia.indexPathsForSelectedItems!.first!)!, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
        }
        else
        {
        return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
        }
        

        
        // or Bubble animation initiated from a view
        //return BonsaiController(fromView: yourOriginView, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
    }
}
extension SDAdd_ChallengeTableViewController:UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,PreviewCellProtocol,UITextViewDelegate,SDImagePickerDelegate
{
    
    
    func didSelectDelete(at index: Int) {
        self.selectedMedia.remove(at: index)
        self.collectionViewMedia.reloadData()
    }
    
   
    
    //    MARK: CustomPicker Delegate
   
    
    
    
    //    MARK: UIImagePicker Delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if info[UIImagePickerController.InfoKey.mediaType] as? String == kUTTypeImage as String
        {
            if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            {
                
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
            else
            {
                
                
            }
            
        }
        else
        {
            let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum((videoUrl?.path)!){
                UISaveVideoAtPathToSavedPhotosAlbum("\(videoUrl!.path)", self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //    MARK:Saving Delegate
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            fetchLast()
        }
    }
    
    func fetchLast()
    {
        let fetchOptions=PHFetchOptions()
        fetchOptions.sortDescriptors=[NSSortDescriptor(key:"creationDate", ascending: false)]
        fetchOptions.fetchLimit = 1
        let fetchResult =  PHAsset.fetchAssets(with: fetchOptions)
        selectedMedia.append(SDMedia(asset: fetchResult.lastObject!))
        self.collectionViewMedia.reloadData()
        self.tableView.reloadData()
        self.viewWillAppear(true)
      
        
        
    }
    
    
    
    //    MARK:---------------UICollectionView Delegate---------------
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedMedia.count > 0 ? selectedMedia.count < 10 ? selectedMedia.count+1 : selectedMedia.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if selectedMedia.count > 0
        {
            if selectedMedia.count < 10 && indexPath.row == collectionView.numberOfItems(inSection: indexPath.section) - 1
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nilCell", for: indexPath)
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewCell", for: indexPath) as! SDPreviewCollectionViewCell
            cell.delegate = self
            cell.setPreview(asset: selectedMedia[indexPath.row].asset)
            cell.layoutIfNeeded()
            cell.closeButton.tag = indexPath.row
            return cell
            
            
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nilCell", for: indexPath)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedMedia.count > 0
        {
           
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewVC") as? ImagePreviewVC
            vc!.imgArray = self.selectedMedia.map({$0.asset})
            vc!.passedContentOffset = indexPath
            vc!.transitioningDelegate = self
            vc!.modalPresentationStyle = .custom
            self.present(vc!, animated: true) {
                
            }

            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        return CGSize(width:height, height: height)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionViewMedia.collectionViewLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
}
    
    
    func customPicker(customPicker: CustomPickerViewController, didSelectMedia media: [PHAsset]) {
        
        for asset in media
        {
            if !((self.selectedMedia.map({$0.asset}) as? [PHAsset])?.contains(asset))!
           {
            self.selectedMedia.append(SDMedia(asset: asset))
            }
            
        }
        customPicker.navigationController?.dismiss(animated: true, completion: {
            
        })
        
        self.collectionViewMedia.reloadData()
       
    }
    
    func customPickerDidCancel(customPicker: CustomPickerViewController) {
        customPicker.navigationController?.dismiss(animated: true, completion: {
           
        })
    }
    //    MARK:- Gallery
    func showOptions()
    {
        
        let alertController = UIAlertController(title: "Select option", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Gallery", style: .default) { (action) in
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .notDetermined {
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized{
                        self.openGallery()
                    } else {
                        let alert = UIAlertController(title: "Photos Access Denied", message: "Sportdare needs access to photos library.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                })
            } else if photos == .authorized {
                self.openGallery()
            }
            
            
        }
        let action2 = UIAlertAction(title: "Camera", style: .default) { (action) in
            if PHPhotoLibrary.authorizationStatus() == .authorized
            {
                self.openCamera()
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(cancel)
        self.present(alertController, animated: true) {
            
        }
    }
    
    
    //    MARK:- Open Gallery
    func openGallery()
    {
        
        let picker = self.storyboard?.instantiateViewController(withIdentifier: "CustomPickerNavigationController") as! CustomPickerNavigationController
        picker.pickerDelegate = self
        picker.selectedCount = selectedMedia.count
        picker.selectedAsset = selectedMedia.map({$0.asset})
        present(picker, animated: true, completion: nil)
        
        
    }
    
    //    MARK:- Open Camera
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let picker =  UIImagePickerController()
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.delegate = self
            picker.allowsEditing = true
            picker.videoMaximumDuration = 30.0
            picker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
            self.present(picker, animated: true) {
                
            }
        }
        
    }
    
    //    MARK:-------------Validate Feilds----------------
    func validateFields(completion:(_ isValid:Bool,_ message:String)->Void)
    {   var isValid = true
        var message = ""
        func checkRemaining()
        {
            if selectedSportCategory == nil
            {
                isValid = false
                message = "Please select a Sport category of challenge"
                
            }
            else if selectedSubSport == nil
            {
                isValid = false
                message = "Please select a Sub Sport category of challenge"
            }
            else if challengeLocation == nil
            {
                isValid = false
                message = "Please give Challenge location"
            }
            else if textFieldGambits.text?.count == 0
            {
                isValid = false
                message = "Please enter how much gambits you Challenge"
            }
            else if selectedDate == nil
            {
                isValid = false
                message = "Please enter a valid date"
            }
            else if let gambits = Int(textFieldGambits.text!)
            {
                if gambits > Int(UserBase.currentUser!.details!.gambits!)!
                {
                    isValid = false
                    message = "You dont have enogh gambits with you to make this Challenge"
                }
                
            }
            else if (textFieldDate.text?.isEmpty)!
            {
                isValid = false
                message = "Please enter a date for Challenge"
            }
            else if (textFieldTime.text?.isEmpty)!
            {
                isValid = false
                message = "Please enter a time for Challenge"
            }
        }
        if challengetype == nil
        {
            isValid = false
            message = "Please select a Challenge Type"
        }
        else if challengetype.name == Constants.dareTypes.last?.name
        {
             if darePlayer != nil
            {
               isValid = true
            }
            else if selectedTeam == nil
            {
                isValid = false
                message = "Please select a Team for Challenge"
            }
        }
        else if challengetype.name == Constants.dareTypes[1].name || challengetype.name == Constants.dareTypes[0].name
        {
            if darePlayer != nil
            {
                isValid = true
            }
            else if selectedPlayer == nil
            {
                isValid = false
                message = "Please select a Player for Challenge"
            }
            else
            {
                checkRemaining()
            }
        }
       
       
        completion(isValid,message)
    }
    
    func getAllFieldDatas()->[String:Any]
    {
//        created_for:14
//        type:1
//        sub_category:55
//        other:bla bla
//        location:kollam
//        description:sample description
//        gambits:10
//        date:2018-12-11
//        time:17:11:11
//        privacy:1
        //lat:
        //long:if darePlayer != nil
        let id = darePlayer != nil ? darePlayer?.userid : challengetype.name == Constants.challengeTypes.last?.name ? selectedTeam.id : selectedPlayer?.user_id
        return ["created_for":id!,"type":challengetype!.id!,"sub_category":selectedSubSport!.id!,"location":textFieldLocation.text!,"description":textViewDescription.text!,"gambits":textFieldGambits.text!,"date":selectedDate!,"time":textFieldTime.text!,"privacy":selectedPrivacyLevel.id!,"lat":String(challengeLocation.coordinate.latitude),"long":String(challengeLocation.coordinate.latitude),"other":""]
    }
    
}
