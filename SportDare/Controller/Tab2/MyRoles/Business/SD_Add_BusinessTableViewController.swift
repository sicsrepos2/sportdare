//
//  SD_Add_BusinessTableViewController.swift
//  Sportdare
//
//  Created by SICS on 06/02/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import BonsaiController
class SD_Add_BusinessTableViewController: BaseTableViewController,ImageEditingDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PopupListDelegate,BonsaiControllerDelegate{
    @IBOutlet weak var businessImage: CustomImageView!
    @IBOutlet weak var txtFieldBusinessType: CustomTextField!
    @IBOutlet weak var txtFieldBusinessName: CustomTextField!
    var mediaPicker: UIImagePickerController!
    var imageData : Data?
    var selectedBusinessType : Business_types?
    var business : Business?
    @IBOutlet weak var txtViewDetails: KMPlaceholderTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
       mediaPicker = UIImagePickerController()
        mediaPicker.delegate = self
        
       
    }
    @IBAction func addBusinessAction(_ sender: CustomButton) {
        addBusiness()
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func addImageAction(_ sender: UIButton) {
        showPicker_Options()
    }
    //    MARK: UITextField
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFieldBusinessType
        {
            self.showSportList(dataSource: (Constants.business_Types?.map({$0.name?.capitalized}))! as! [String], title: "Business Type") { (option,index) in
                textField.text = option
                self.selectedBusinessType = Constants.business_Types![index]
                
                
            }
            
            return false
        }
        return true
    }
    
    func showPicker_Options()
    {
        
        let actionSheet = UIAlertController(title: "Change Profile Image", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.mediaPicker.sourceType = .camera
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            self.mediaPicker.sourceType = .photoLibrary
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        actionSheet.addAction(camera)
        actionSheet.addAction(photos)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
            
        }
    }
    
    //    MARK:- UIImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            //            picker.dismiss(animated: true) {
            //
            //            }
            self.presentCropView(image: img)
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            
        }
    }
    func imageCroping(controller: CustomimageEditorViewController, didFinishPickingImage image: UIImage) {
        imageData = image.jpegData(compressionQuality: 0.9)
        
        self.navigationController?.popViewController(animated: false)
        DispatchQueue.main.async {
            self.businessImage.image = UIImage(data: self.imageData!)!
            self.mediaPicker.dismiss(animated: false) {
                
            }
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    
    //    MARK: Bonsai Controller Delegate
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.height / 4), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height / (4/3)))
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
    }
    func popController(controller: SDPopUpListViewController, didSelect option: String, at Index: Int) {
        
    }
    func presentCropView(image:UIImage)
    {
        let cropView = self.storyboard?.instantiateViewController(withIdentifier: "CustomimageEditorViewController") as! CustomimageEditorViewController
        cropView.setCropView(Image: image, delegate: self, cropSize: CGSize(width: businessImage.frame.size.width * 1.5, height: businessImage.frame.size.height * 1.5))
        cropView.hidesBottomBarWhenPushed = true
        //    UINavigationController(rootViewController: cropView)
        mediaPicker.present(cropView, animated: false) {
            
        }
        
    }
    
    func showSportList(dataSource:[String],title:String,completion:@escaping (_ option:String,_ index:Int)->Void)
    {
        let listVc =
            self.storyboard?.instantiateViewController(withIdentifier: "SDPopUpListViewController") as! SDPopUpListViewController
        listVc.list = dataSource
        listVc.delegate = self
        listVc.listTitle = title
        listVc.SelectionClosure = {(index,option) in
            completion(option,index)
            
        }
        listVc.transitioningDelegate = self
        listVc.modalPresentationStyle = .custom
        present(listVc, animated: true, completion: nil)
        
    }
    
    func addBusiness()
    {
        if (self.txtFieldBusinessName.text?.isEmpty)!
        {
            self.showAlertWithOkButton(message: "Please Give a Business Name")
        }
        else if (txtFieldBusinessType.text?.isEmpty)!
        {
            self.showAlertWithOkButton(message: "Please Give a Business Type")
        }
       
        else
        {
           

            let param = ["name":txtFieldBusinessName.text!,"details":txtViewDetails.text,"cat_id":selectedBusinessType!.id!] as [String : Any]
            KRProgressHUD.show()
            ApiService.add_Business(image: self.businessImage.image?.jpegData(compressionQuality: 0.6), parameter: param) { (status, response, error) in
                if status
                {
                    KRProgressHUD.showSuccess(withMessage: "Successfully added new team")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        
    }
}
