//
//  SD_My_Roles_BusinessViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_My_Roles_BusinessViewController: BaseViewController {

 @IBOutlet weak var tableViewBusiness: UITableView!
    var businesses : [Business]?
    override func viewDidLoad() {
        super.viewDidLoad()
        businesses = [Business]()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBusinesses()
        definesPresentationContext = false
        
    }
    
    func getBusinesses()
    {
        ApiService.getBusiness(id: nil) { (status, result, errro) in
            
            
            if let businessList = result as? [Business]
            {
                self.businesses?.removeAll(keepingCapacity: true)
                self.businesses?.append(contentsOf: businessList)
                self.tableViewBusiness.reloadData()
                
            }
            
            
        }
    }
    @IBAction func addBusinessAction(_ sender: SDCustomButton) {
        let addBusinessVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Add_BusinessTableViewController") as? SD_Add_BusinessTableViewController
        let popupVC = PopupViewController(contentController: addBusinessVC!, position: PopupViewController.PopupPosition.top(40), popupWidth: self.view.frame.width * 0.85, popupHeight: UIScreen.main.bounds.height * 0.80)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        popupVC.delegate = self
        self.present(popupVC, animated: true) {
            
        }
    }
    

}
extension SD_My_Roles_BusinessViewController :UITableViewDataSource, UITableViewDelegate,PopupViewControllerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businesses!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let businessCell = tableView.dequeueReusableCell(withIdentifier: "cellBusiness") as! SDBusinessTableViewCell
        businessCell.business = businesses![indexPath.row]
        return businessCell
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
}
