//
//  SD_My_Roles_FanViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_My_Roles_FanViewController: BaseViewController {
    var roots : [Root]?
 @IBOutlet weak var tableViewRoots: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        roots = [Root]()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getRoots()
        definesPresentationContext = false
        
    }

 
    func getRoots()
    {
        ApiService.getRoots { (status, myRoots, error) in
            if status == true
            {
                self.roots = myRoots
                self.tableViewRoots.reloadData()
            }
        }
    }
    @IBAction func unrootAction(_ sender: CustomButton) {
        let root = roots![sender.tag]
        self.showAlertWith(title: "UNROOT?", message: "Are you sure to Unroot \(root.first_name!)", CancelBtnTitle: "No", OtherBtnTitle: "Yes") { (index, str) in
            if index == 1
            {
                ApiService.unRoot_User(id: root.user_id!, completion: { (status, result, error) in
                    if status
                    {
                        self.roots?.remove(at: sender.tag)
                        DispatchQueue.main.async(execute: {
                            self.tableViewRoots.deleteRows(at: [IndexPath(item: sender.tag, section: 0)], with: .left)
                            self.tableViewRoots.reloadData()
                        })
                        
                    }
                })
            }
            
        }
        
    }

}

extension SD_My_Roles_FanViewController :UITableViewDataSource, UITableViewDelegate {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  roots!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableViewRoots.dequeueReusableCell(withIdentifier: "cellRoots") as! SDRootsTableViewCell
            cell.btnUnroot.tag = indexPath.row
            cell.root = roots![indexPath.row]
            
        return cell
       
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
}
