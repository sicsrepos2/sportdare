//
//  SD_Add_Best_AwardsTableViewController.swift
//  Sportdare
//
//  Created by SICS on 10/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
protocol AddAwards_PB_Protocol{
    func didCompleteAdding()
}
class SD_Add_Best_AwardsTableViewController: BaseTableViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImageEditingDelegate {
    @IBOutlet weak var awardImageView: CustomImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var txtFieldTitle: CustomTextField!
    @IBOutlet weak var txtViewDescription: KMPlaceholderTextView!
    var isForAwards = false
    var subSport : SubSport_category?
    var delegate : AddAwards_PB_Protocol?
        var mediaPicker: UIImagePickerController!
    override func viewDidLoad() {
        super.viewDidLoad()
        mediaPicker = UIImagePickerController()
        mediaPicker.delegate = self
    
        if isForAwards
        {
            labelTitle.text = "Add Awards"
        }
        else
        {
             labelTitle.text = "Add Personal Best"
        }
           
    }

    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func addAction(_ sender: CustomButton) {
        if !(txtFieldTitle.text?.isEmpty)!{
            addAwardOrPB()
            
        }
        else
        {
            self.showAlertWithOkButton(message: "Please give a title")
        }
        
    }
    @IBAction func addImageAction(_ sender: UIButton) {
        showPicker_Options()
    }
    
    
    func addAwardOrPB()
    {
        if isForAwards
        {
          KRProgressHUD.show()
            let imageData = awardImageView.image != UIImage(named: "Placeholder") ? awardImageView.image?.jpegData(compressionQuality: 0.7) : nil
            ApiService.addAward(param: ["role_id":subSport!.id!,"title":txtFieldTitle.text!,"description":txtViewDescription.text!],image:imageData) { (status, result, error) in
                if status == true
                {
                    KRProgressHUD.showSuccess(withMessage: "Updated Your Award")
                    self.delegate?.didCompleteAdding()
                    self.dismiss(animated: true) {
                        
                    }
                }
            }
        }
        else
        {
             KRProgressHUD.show()
            ApiService.addPB(param: ["role_id":subSport!.id!,"title":txtFieldTitle.text!,"description":txtViewDescription.text!]) { (status, result, error) in
                if status == true
                {
                      KRProgressHUD.showSuccess(withMessage: "Updated Your Personal Best")
                    self.delegate?.didCompleteAdding()
                    self.dismiss(animated: true) {
                        
                    }
                }
                
            }
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !isForAwards
        {
            if indexPath.row == 0
            {
                return 0
            }
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func showPicker_Options()
    {
        
        let actionSheet = UIAlertController(title: "Change Profile Image", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.mediaPicker.sourceType = .camera
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            self.mediaPicker.sourceType = .photoLibrary
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        actionSheet.addAction(camera)
        actionSheet.addAction(photos)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
            
        }
        
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            self.presentCropView(image: img)
            
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            
        }
    }
    func imageCroping(controller: CustomimageEditorViewController, didFinishPickingImage image: UIImage) {
       
        
        
        self.navigationController?.popViewController(animated: false)
        DispatchQueue.main.async {
            self.awardImageView.image = image
            self.mediaPicker.dismiss(animated: false) {
                
            }
            
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    func presentCropView(image:UIImage)
    {
        let cropView = self.storyboard?.instantiateViewController(withIdentifier: "CustomimageEditorViewController") as! CustomimageEditorViewController
        cropView.setCropView(Image: image, delegate: self, cropSize: CGSize(width: awardImageView.frame.size.width * 1.5, height: awardImageView.frame.size.height * 1.5))
        cropView.hidesBottomBarWhenPushed = true
        //    UINavigationController(rootViewController: cropView)
        mediaPicker.present(cropView, animated: false) {
            
        }
        
    }
}
