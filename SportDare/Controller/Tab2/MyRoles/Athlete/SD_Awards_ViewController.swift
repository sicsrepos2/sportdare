//
//  SD_Awards_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 08/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SD_Awards_ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PopupViewControllerDelegate,AddAwards_PB_Protocol {

    @IBOutlet weak var tableViewAwards: UITableView!
      var subSport : SubSport_category?
    var awards : [Award]?
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if subSport != nil
        {
        getAwards()
        }
    }
    @IBAction func addAwardAction(_ sender: SDCustomButton) {
        let addAwardVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Add_Best_AwardsTableViewController") as? SD_Add_Best_AwardsTableViewController
        addAwardVC!.isForAwards = true
        addAwardVC?.subSport = self.subSport
        addAwardVC?.delegate = self
        let popupVC = PopupViewController(contentController: addAwardVC!, position: PopupViewController.PopupPosition.top(50), popupWidth: UIScreen.main.bounds.width * 0.85, popupHeight: UIScreen.main.bounds.height * 0.80)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        popupVC.delegate = self
        self.present(popupVC, animated: true) {
            
        }
    }
    
    @IBAction func deleteAward(_ sender: UIButton) {
        let award = awards![sender.tag]
    }
    func getAwards()
    {
        ApiService.getAwards(roleId: (subSport?.id)!) { (status, result) in
            if status
            {
                if let awardBase = result as? Awards_Base , let awards = awardBase.awards
                {
                    self.awards = awards
                   
                }
                else
                {
                    self.awards?.removeAll()
                }
                 self.tableViewAwards.reloadData()
            }
        }
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.awards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewAwards.dequeueReusableCell(withIdentifier: "regular") as! SD_PB_AwardsTableViewCell
        cell.award = awards![indexPath.row]
        cell.btnDelete.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func popupViewControllerDidDismiss(sender: PopupViewController) {
        self.getAwards()
        
    }
    func didCompleteAdding() {
        self.getAwards()
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return tableViewAwards.dequeueReusableCell(withIdentifier: "header")
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//        
//    }
}
