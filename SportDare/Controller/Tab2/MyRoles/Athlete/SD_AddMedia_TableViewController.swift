//
//  SD_AddMedia_TableViewController.swift
//  Sportdare
//
//  Created by SICS on 17/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import TLPhotoPicker
import AVKit
import Photos
class SD_AddMedia_TableViewController: UITableViewController,UICollectionViewDelegate,UICollectionViewDataSource,PreviewCellProtocol {
  
    
    @IBOutlet weak var collectionViewPreview: UICollectionView!
    @IBOutlet weak var previewImageView: UIImageView!
    var isUploadingWithProgress :((Progress)->Void)?
    var didUploadedMedia : ((_ status:Bool?,_ result:Any?,_ error:Error?)->Void)?
    var didStartedUploading :((_ status:Bool?)->Void)?
    var selectedMedia : [SDMedia]!
    var previewMedia : SDMedia!
     var playerController : AVPlayerViewController!
    var subSport : SubSport_category?
    override func viewDidLoad() {
        super.viewDidLoad()
        if selectedMedia.count > 0
        {
            previewMedia = selectedMedia.first
            self.showPreview()
        }

    }

    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    func showPreview()
    {
        if previewMedia != nil
        {
          
            if previewMedia.type == .Image
            {
                if self.playerController != nil
                {
                    self.playerController.player?.pause()
                    self.playerController.player = nil
                    self.playerController.view.removeFromSuperview()
                }
                let options = PHImageRequestOptions()
                options.deliveryMode = .fastFormat
                options.resizeMode = .fast
                PHImageManager.default().requestImage(for: previewMedia.asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: options) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
                    if let image = image {
                        
                        self.previewImageView.image = image
                    }
                }
            }
            else
            {
                self.playVideo()
            }
        }
        else
        {
            self.previewImageView.image = #imageLiteral(resourceName: "preview_image")
            if self.playerController != nil
            {
                self.playerController.player?.pause()
                self.playerController.player = nil
                self.playerController.view.removeFromSuperview()
            }
        }
        
    }
    
    func playVideo() {
        guard (previewMedia.asset.mediaType == PHAssetMediaType.video)
            
            else {
                print("Not a valid video media type")
                return
        }
        PHCachingImageManager().requestAVAsset(forVideo: previewMedia.asset, options: nil) { [unowned self](asset, audiomix, info) in
            let asset = asset as! AVURLAsset
            DispatchQueue.main.async {
                let player = AVPlayer(url: asset.url)
                if self.playerController == nil
                {
                    self.playerController = AVPlayerViewController()
                }
                else
                {
                    self.playerController.player?.pause()
                    self.playerController.player = nil
                    self.playerController.view.removeFromSuperview()
                    
                }
                self.playerController.player = player
                self.playerController.showsPlaybackControls = true
                self.playerController.view.frame = self.previewImageView.bounds
                self.previewImageView.addSubview(self.playerController.view)
                
                
            }
        }
        
    }
    
    func didSelectDelete(at index: Int) {
        selectedMedia.remove(at: index)
        self.collectionViewPreview.reloadData()
        self.showPreview()
        
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        if selectedMedia.count > 0
        {
            KRProgressHUD.show()
            ApiService.sharedInstance.add_Media(files: selectedMedia, role_id:(subSport?.id)! , completion: { (status, result, error) in
                self.didUploadedMedia?(status,result,error)
            }, startedBlock: { (started) in
                self.didStartedUploading?(started)
                self.dismiss(animated: true, completion: {
                    
                })
            }) { (progress) in
                self.isUploadingWithProgress?(progress)
            }
        }
    }
    
    //    MARK: UICollectionView Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedMedia.count > 0 ?selectedMedia.count  : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if selectedMedia.count > 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewCell", for: indexPath) as! SDPreviewCollectionViewCell
            cell.delegate = self
            cell.setPreview(asset: selectedMedia[indexPath.row].asset)
            if selectedMedia[indexPath.row] === previewMedia
            {
                cell.setSelection()
            }
            cell.closeButton.tag = indexPath.row
            return cell
            
            
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nilCell", for: indexPath)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedMedia.count > 0
        {
            previewMedia = selectedMedia[indexPath.row]
            self.collectionViewPreview.reloadData()
            self.showPreview()
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        
        return CGSize(width:height, height: height)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionViewPreview.collectionViewLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}
