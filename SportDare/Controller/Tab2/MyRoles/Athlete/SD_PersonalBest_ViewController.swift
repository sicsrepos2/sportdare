//
//  SD_PersonalBest_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 08/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SD_PersonalBest_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PopupViewControllerDelegate,AddAwards_PB_Protocol {
 
    @IBOutlet weak var tableViewPB: UITableView!
      var subSport : SubSport_category?
    var personalBests : [PersonalBest]?
    override func viewDidLoad() {
        super.viewDidLoad()
        personalBests = [PersonalBest]()
      
    }
    override func viewWillAppear(_ animated: Bool) {
         getPb()
    }
    @IBAction func addPbAction(_ sender: SDCustomButton) {
        let addAwardVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Add_Best_AwardsTableViewController") as? SD_Add_Best_AwardsTableViewController
        addAwardVC?.subSport = self.subSport
        addAwardVC?.delegate = self
        let popupVC = PopupViewController(contentController: addAwardVC!, position: PopupViewController.PopupPosition.top(50), popupWidth: UIScreen.main.bounds.width * 0.85, popupHeight: UIScreen.main.bounds.height * 0.70)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        popupVC.delegate = self
        self.present(popupVC, animated: true) {
            
        }
    }

  
    func getPb()
    {
        ApiService.getPersonalBests(roleId: (subSport?.id)!) { (status, result) in
            if status
            {
                if let pb = result as? PB_Base , let bests = pb.personalBest
                {
                    self.personalBests = bests
                   
                }
                else
                {
                    self.personalBests?.removeAll()
                }
                 self.tableViewPB.reloadData()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personalBests!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewPB.dequeueReusableCell(withIdentifier: "regular") as! SD_PB_AwardsTableViewCell
        cell.personalBest = personalBests![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func popupViewControllerDidDismiss(sender: PopupViewController) {
        self.getPb()
    }
    func didCompleteAdding() {
        self.getPb()
    }
    
}
