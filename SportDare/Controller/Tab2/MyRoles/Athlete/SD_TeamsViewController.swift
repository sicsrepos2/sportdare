//
//  SD_TeamsViewController.swift
//  Sportdare
//
//  Created by SICS on 10/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SD_TeamsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var tableViewTeams: UITableView!
    var teams :[Team]?
    var addingPlayer : SearchUserDetail?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewTeams.estimatedRowHeight = 44.0
        tableViewTeams.rowHeight = UITableView.automaticDimension
        teams = [Team]()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getTeams()
    }
    func getTeams()
    {
        ApiService.getTeams { (status, teamList, error) in
            if status == true && error == nil
            {
                self.teams = teamList
                self.tableViewTeams.reloadData()
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewTeams.dequeueReusableCell(withIdentifier: "cellTeam") as! SDTeamTableViewCell
        cell.team = teams![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if addingPlayer != nil
        {
            addToTeam(team: teams![indexPath.row], player: addingPlayer!)
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableViewTeams.dequeueReusableCell(withIdentifier: "cellHeader") 
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return addingPlayer == nil ? 0 : 40
    }
    func addToTeam(team:Team,player:SearchUserDetail)
    {
        let playerName = "\(player.first_name!) \(player.surname!)"
        self.showAlertWith(title: "", message: "Are you sure you want to recruit \(playerName) to \(team.team_name!)", CancelBtnTitle: "Cancel", OtherBtnTitle: "Add") { (index, title) in
            
        }
    }
   

}
