//
//  My_Roles_AthleteViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import BonsaiController
class SD_My_Roles_AthleteViewController: BaseTableViewController,PopupListDelegate,BonsaiControllerDelegate,FloatRatingViewDelegate{
    @IBOutlet weak var btnLevel: SDCustomButton!
    @IBOutlet weak var btnPB: SDCustomButton!
    @IBOutlet weak var btnAwards: SDCustomButton!
    @IBOutlet weak var btnTeams: SDCustomButton!
    @IBOutlet weak var btnMedia: SDCustomButton!
    @IBOutlet var btnCollection: [SDCustomButton]!
    @IBOutlet weak var viewDetails: UIView!
    
    @IBOutlet weak var labelSubSportCategory: UILabel!
    
    @IBOutlet weak var labelSportCategory: UILabel!
    
    @IBOutlet weak var athleteDetailView: UIView!
    var sportCategoryId : String?
    var subSportId : String?
    var personalBestVC : SD_PersonalBest_ViewController?
    var awardsVC : SD_Awards_ViewController?
    var mediaVC : SD_Media_ViewController?
    var teamVC : SD_TeamsViewController?
    var roleAthlete : Roles_Athlete_Base?
    var athleteLevel :Role_Level_Base?
    var currentMainSport : SportCategory!
    var currentSubSport : SubSport_category!
    var indexMainSport = 0
    var indexSubsport = 0
   
    @IBOutlet weak var viewLevel: UIView!
    @IBOutlet weak var labelStar: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var labelLevel: UILabel!
    @IBOutlet weak var btnVerified: CustomButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        viewRating.delegate = self
        viewRating.rating = 0
        viewLevel.isHidden = true
        btnCollection.forEach { (button) in
            button.isUserInteractionEnabled = false
        }
        loadViewControllers()
        getUserRoles()
        self.tableView.canCancelContentTouches = false
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        definesPresentationContext = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(athleteDetailView.frame)
    }
    //MARK:- Actions
    
    @IBAction func sportCategoryOptions(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Sport Category", message: nil, preferredStyle: .actionSheet)
//        let edit = UIAlertAction(title: "Edit", style: .default) { (action) in
//        }
        let remove = UIAlertAction(title: "Remove", style: .default) { (action) in
           
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
//        actionSheet.addAction(edit)
        actionSheet.addAction(remove)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
            
        }
        
    }
    
    @IBAction func subSportOptions(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Sub Sport Category", message: nil, preferredStyle: .actionSheet)
//        let edit = UIAlertAction(title: "Edit", style: .default) { (action) in
//        }
        let remove = UIAlertAction(title: "Remove", style: .default) { (action) in
            
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
//        actionSheet.addAction(edit)
        actionSheet.addAction(remove)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
            
        }
    }
    @IBAction func switchSportCategory(_ sender: UIButton) {
        if sender.tag == 1
        {
         
            if indexMainSport-1 >= 0 && indexMainSport-1 < (roleAthlete?.sportCategories?.count)!
            {
                indexSubsport = 0
                let sport = roleAthlete?.sportCategories![indexMainSport-1]
                indexMainSport-=1
                currentMainSport = sport
                 self.sportCategoryId = currentMainSport.id
                labelSportCategory.popTransition(0.2)
                labelSportCategory.text = sport!.name
                if let subSport = sport?.sub_category?.first
                {
                    currentSubSport  = subSport
                    labelSubSportCategory.text = subSport.name
                     showLevels()
                }
                else
                {
                    currentSubSport = nil
                    labelSubSportCategory.text = ""
                    self.viewLevel.isHidden = true
                    self.viewRating.rating = 0
                    self.labelStar.text = ""
                    self.labelLevel.text = ""
                }
                
            }
        }
        else
        {
            if indexMainSport+1 >= 0 && indexMainSport+1 < (roleAthlete?.sportCategories?.count)!
                
            {
                 indexSubsport = 0
                let sport = roleAthlete?.sportCategories![indexMainSport+1]
                indexMainSport+=1
                 currentMainSport = sport
                 self.sportCategoryId = currentMainSport.id
                  labelSportCategory.pushTransition(0.2)
                labelSportCategory.text = sport!.name
                if let subSport = sport?.sub_category?.first
                {
                    currentSubSport  = subSport
                    labelSubSportCategory.text = subSport.name
                     showLevels()
                }
                else
                {
                    currentSubSport = nil
                    labelSubSportCategory.text = ""
                    self.viewLevel.isHidden = true
                    self.viewRating.rating = 0
                    self.labelStar.text = ""
                    self.labelLevel.text = ""
                }
               
            }
        }
    }
    
    @IBAction func switchSubSport(_ sender: UIButton) {
        if currentMainSport != nil
        {
            if sender.tag == 3
            {
                if indexSubsport-1 >= 0 &&  indexSubsport-1 < (currentMainSport.sub_category?.count)!
                {
                    showLevelAction(btnLevel)
                    indexSubsport -= 1
                    let subSport = currentMainSport.sub_category![indexSubsport]
                    labelSubSportCategory.popTransition(0.2)
                    labelSubSportCategory.text = subSport.name
                    currentSubSport = subSport
                    if self.athleteLevel != nil
                    {
                        if let level = athleteLevel?.atheletLevel?.filter({$0.sub_cat_id == subSport.id}).first
                        {
                            self.viewRating.rating = Double(level.stars!)!
                            let rating = Constants.athlete_levels?.filter({$0.als_id == level.level_id!}).first
                            labelStar.text = "\(level.stars!) Star \(rating!.als_name!)"
                            labelLevel.text = rating?.als_subname
                        }
                        else
                        {
                            self.viewRating.rating = 0
                            labelStar.text = ""
                            labelLevel.text = ""
                            
                        }
                    }
                    
                }
            }
            else
            {
                if indexSubsport+1 >= 0 &&  indexSubsport+1 < (currentMainSport.sub_category?.count)!
                {
                    showLevelAction(btnLevel)
                    indexSubsport += 1
                    let subSport = currentMainSport.sub_category![indexSubsport]
                    labelSubSportCategory.pushTransition(0.2)
                    labelSubSportCategory.text = subSport.name
                    currentSubSport = subSport
                    if self.athleteLevel != nil
                    {
                        if let level = athleteLevel?.atheletLevel?.filter({$0.sub_cat_id == subSport.id}).first
                        {
                            self.viewRating.rating = Double(level.stars!)!
                            let rating = Constants.athlete_levels?.filter({$0.als_id == level.level_id!}).first
                            labelStar.text = "\(level.stars!) Star \(rating!.als_name!)"
                            labelLevel.text = rating?.als_subname
                        }
                        else
                        {
                            self.viewRating.rating = 0
                            labelStar.text = ""
                            labelLevel.text = ""
                            
                        }
                    }
                    
                }
            }
        }
    }
    
    @IBAction func addSportCategory(_ sender: SDCustomButton) {
        
        self.showSportList(dataSource: Constants.sportCategories.map({$0.name!}), title: "Sport Category") {[unowned self] (option,index) in
            DispatchQueue.main.async {
                self.showAlertWithOkButton(message: "")
           
            self.showAlertWith(title: "Sport Category", message: "Are you sure you want to add \(option) as Sport Category ?", CancelBtnTitle: "No", OtherBtnTitle: "Yes", acceptCompletion: { () -> Void? in
                 self.addSportCategory(option: option)
            }, cancelCompletion: {
                
            })
            }
        }
    }
    
    @IBAction func addSubSport(_ sender: SDCustomButton) {
        if self.sportCategoryId != nil
        {
           let mainSportCategory = Constants.sportCategories.filter({$0.id == self.sportCategoryId}).last
            self.showSportList(dataSource: (mainSportCategory!.sub_category?.map({$0.name}))! as! [String], title: "Sub Sport Category") { (option,index) in
               
                self.addSubSportCategory(option: option)
        }
        }
    }
    
    @IBAction func showLevelAction(_ sender: SDCustomButton) {
        btnCollection.forEach { (button) in
            button.isSelected = false
        }
        sender.isSelected = true
        self.viewLevel.isHidden = false
        self.removeChildVC()
       
    }
    
    @IBAction func showPersonalBest(_ sender: SDCustomButton) {
        btnCollection.forEach { (button) in
            button.isSelected = false
        }
        sender.isSelected = true
        self.removeChildVC()
        personalBestVC?.subSport = currentSubSport
        displayContentController(content: personalBestVC!)
    }
    @IBAction func showAwards(_ sender: SDCustomButton) {
        btnCollection.forEach { (button) in
            button.isSelected = false
        }
        sender.isSelected = true
        self.removeChildVC()
         awardsVC?.subSport = currentSubSport
        displayContentController(content: awardsVC!)
    }
    
    @IBAction func showTeams(_ sender: SDCustomButton) {
        btnCollection.forEach { (button) in
            button.isSelected = false
        }
        sender.isSelected = true
        self.removeChildVC()
        displayContentController(content: teamVC!)
    }
    @IBAction func showMedia(_ sender: SDCustomButton) {
        btnCollection.forEach { (button) in
            button.isSelected = false
        }
        sender.isSelected = true
        self.removeChildVC()
        mediaVC?.subSport = currentSubSport
        displayContentController(content: mediaVC!)
    }
    
    @IBAction func submitRatingsAction(_ sender: UIButton) {
        if let athletelevel = Constants.athlete_levels?.filter({$0.als_id == "\(Int(viewRating.rating))"}).first
        {
            KRProgressHUD.show()
            ApiService.addLevels(level: athletelevel, categoryId: currentSubSport.id!) { (status, result, error) in
                KRProgressHUD.showSuccess()
                self.showLevels()
                
            }
        }
    }
    
    func setDetails()
    {
        if roleAthlete?.sportCategories != nil
        {
            if (roleAthlete?.sportCategories?.count)! > 0
            {
                if let firstMain = roleAthlete?.sportCategories![indexMainSport]
                {
                    currentMainSport = firstMain
                    self.labelSportCategory.text = firstMain.name
                    self.sportCategoryId = currentMainSport.id
                    
                    if  indexSubsport < (firstMain.sub_category?.count)!
                        
                    {
                        let firstSub = firstMain.sub_category?[indexSubsport]
                        currentSubSport = firstSub
                        self.labelSubSportCategory.text = firstSub?.name
                        viewLevel.isHidden = false
                        btnCollection.forEach { (button) in
                            button.isUserInteractionEnabled = true
                        }
                        showLevels()
                    }
                    else
                    {
                        self.viewRating.rating = 0
                        self.labelStar.text = ""
                        self.labelLevel.text = ""
                    }
                }
                
            }
            else
            {
                viewLevel.isHidden = true
                btnCollection.forEach { (button) in
                    button.isUserInteractionEnabled = false
                }
            }
        }
    }
    
    func addSportCategory(option:String)
    {
        KRProgressHUD.show()
         self.sportCategoryId =  Constants.sportCategories.filter({$0.name == option}).last?.id!
        ApiService.addSportCategory(categoryId: self.sportCategoryId!) { (status, result, error) in
            if result != nil
            {
              KRProgressHUD.showSuccess(withMessage: "Added Sport Category")
                self.labelSportCategory.text = option
                self.labelSubSportCategory.text = ""
                self.indexMainSport = (self.roleAthlete?.sportCategories?.count)!
                self.getUserRoles()
               
            }
            else{
                  KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            
        }
    }
    
    func addSubSportCategory(option:String)
    {
         KRProgressHUD.show()
        let mainSportCategory = Constants.sportCategories.filter({$0.id == self.sportCategoryId}).last
        self.subSportId = mainSportCategory?.sub_category!.filter({$0.name == option}).last?.id!
        ApiService.addSubSportCategory(subCategoryId: self.subSportId!, categoryId: self.sportCategoryId!) { (status, result, error) in
            if result != nil
            {
             KRProgressHUD.showSuccess(withMessage: "Added Sub Sport Category")
                self.labelSubSportCategory.text = option
                self.indexSubsport =  (self.currentMainSport.sub_category?.count)!
                self.getUserRoles()
               
            }else
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            
        }
    }
    
    func getUserRoles()
    {
        ApiService.getRolesCategories { (status, result) in
            if result != nil
            {
                self.roleAthlete = result as? Roles_Athlete_Base
                self.setDetails()
               
            
            }
        }
    }
   
    func showLevels()
    {
        ApiService.getLevelRatingForRole(roleId: currentSubSport.id!) { (status, result) in
            if status
            {
                self.athleteLevel = result as? Role_Level_Base
                if self.currentSubSport != nil
                {
                    if let level = self.athleteLevel?.atheletLevel?.filter({$0.sub_cat_id == self.currentSubSport.id}).first
                    {
                        self.viewRating.rating = Double(level.stars!)!
                        let rating = Constants.athlete_levels?.filter({$0.als_id == level.level_id!}).first
                        self.labelStar.text = "\(level.stars!) Star \(rating!.als_name!)"
                        self.labelLevel.text = rating?.als_subname
                        self.btnVerified.setTitle("\(level.verified_count!) Verified", for: .normal)
                    }
                    else
                    {
                        self.viewRating.rating = 0
                        self.labelStar.text = ""
                        self.labelLevel.text = ""
                        
                    }
                }
                else
                {
                    self.viewRating.rating = 0
                    self.labelStar.text = ""
                    self.labelLevel.text = ""
                }
                
            }
        }
        
    }
    
    func showSportList(dataSource:[String],title:String,completion:@escaping (_ option:String,_ index:Int)->Void)
    {
        let listVc =
            self.storyboard?.instantiateViewController(withIdentifier: "SDPopUpListViewController") as! SDPopUpListViewController
        listVc.list = dataSource
        listVc.delegate = self
        listVc.listTitle = title
        listVc.SelectionClosure = {(index,option) in
            completion(option,index)
            
        }
        listVc.transitioningDelegate = self
        listVc.modalPresentationStyle = .custom
        present(listVc, animated: true, completion: nil)
        
    }
    
    
        //    MARK:- ChildView COntrollers
    func displayContentController(content: UIViewController) {
         self.viewLevel.isHidden = true
        addChild(content)
        content.view.frame = CGRect(x: 0, y: 0, width: viewDetails.frame.width, height: viewDetails.frame.height)
        self.viewDetails.addSubview(content.view)
        viewDetails.layoutIfNeeded()
        content.didMove(toParent: self)
    }
    
    func removeChildVC() {
        if let content = self.children.first
        {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
    func loadViewControllers()
    {
        personalBestVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_PersonalBest_ViewController") as? SD_PersonalBest_ViewController
        awardsVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Awards_ViewController") as? SD_Awards_ViewController
        mediaVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Media_ViewController") as? SD_Media_ViewController
        teamVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_TeamsViewController") as? SD_TeamsViewController
        
    }
    
    //    MARK:- Popup List Controller
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.height / 4), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height / (4/3)))
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
    }
    func popController(controller: SDPopUpListViewController, didSelect option: String, at Index: Int) {
        
    }
    
    //    MARK:- Rating Delegate
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
      let athletelevel = Constants.athlete_levels?.filter({$0.als_id == "\(Int(rating))"}).first
        labelStar.text = "\(Int(rating)) Star \(athletelevel!.als_name!)"
        labelLevel.text = athletelevel?.als_subname
      
    }
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        
    }
}
