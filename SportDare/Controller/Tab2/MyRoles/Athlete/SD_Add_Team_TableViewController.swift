//
//  SDAdd_Team_TableViewController.swift
//  Sportdare
//
//  Created by Binoy T on 04/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import BonsaiController
protocol AddTeamProtocol {
    func didAddedTeam()
}
class SDAdd_Team_TableViewController: BaseTableViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ImageEditingDelegate,BonsaiControllerDelegate,PopupListDelegate,SearchPlayersVCProtocol{
   
    
    @IBOutlet weak var txtFieldSportCategory: CustomTextField!
    @IBOutlet weak var txtFieldTeamName: CustomTextField!
    @IBOutlet weak var collectionViewMembers: UICollectionView!
    @IBOutlet weak var txtFieldTeamLeader: CustomTextField!
   
    var mediaPicker: UIImagePickerController!
    var imageData : Data?
    var selectedSportCategory : Sports_category!
    var teamLeader : Root?
    var teamMembers : [Root]?
    var addTeamDelegate :AddTeamProtocol?
    @IBOutlet weak var teamImageView: CustomImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        teamMembers = [Root]()

        
    }
    override func viewDidAppear(_ animated: Bool) {
        mediaPicker = UIImagePickerController()
        mediaPicker.delegate = self
    }
    @IBAction func addImageAction(_ sender: UIButton) {
        showPicker_Options()
    }
    @IBAction func addMembersAction(_ sender: CustomButton) {
        let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SDSearchPlayers_ViewController") as? SDSearchPlayers_ViewController
        searchPage?.delegate = self
        searchPage?.isMultiSelection = true
        searchPage?.modalTransitionStyle = .coverVertical
        searchPage!.modalPresentationStyle = .overFullScreen
        self.present(searchPage!, animated: true) {
            
        }
        
    }
    
    @IBAction func cancelAction(_ sender: CustomButton) {
         dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createTeamAction(_ sender: CustomButton) {
        addTeam()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
  
        if textField == txtFieldSportCategory
        {
            self.showSportList(dataSource: Constants.sportCategories.map({$0.name!}), title: "Sport Category") { (option,index) in
                textField.text = option
                self.selectedSportCategory = Constants.sportCategories![index]
            

            }
            
            return false
        }
        if textField ==  txtFieldTeamLeader
        {
            let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SDSearchPlayers_ViewController") as? SDSearchPlayers_ViewController
            searchPage?.delegate = self
            searchPage?.isMultiSelection = false
            searchPage?.modalTransitionStyle = .coverVertical
            searchPage!.modalPresentationStyle = .overFullScreen
            self.present(searchPage!, animated: true) {
                
            }
            return false
        }
       
        return true
    }
    func showPicker_Options()
    {
        
        let actionSheet = UIAlertController(title: "Change Profile Image", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.mediaPicker.sourceType = .camera
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            self.mediaPicker.sourceType = .photoLibrary
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        actionSheet.addAction(camera)
        actionSheet.addAction(photos)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
            
        }
    }
    
    func addTeam()
    {
        if (self.txtFieldTeamName.text?.isEmpty)!
        {
              self.showAlertWithOkButton(message: "Please Give a Team Name")
        }
        else if (txtFieldSportCategory.text?.isEmpty)!
        {
             self.showAlertWithOkButton(message: "Please Give a Team Sport Category")
        }
        else if teamMembers?.count == 0
        {
           self.showAlertWithOkButton(message: "Please add some Team members")
        }
        else
        {
    
        let teamMembersid = (teamMembers!.map({$0.user_id!})).joined(separator: ",")
        let param = ["team_name":txtFieldTeamName.text!,"team_members":teamMembersid,"cat_id":selectedSportCategory.id!] as [String : Any]
            KRProgressHUD.show()
            ApiService.add_Team(image: self.teamImageView.image?.jpegData(compressionQuality: 0.6), parameter: param) { (status, response, error) in
                if status
                {
                    KRProgressHUD.showSuccess(withMessage: "Successfully added new team")
                     self.addTeamDelegate?.didAddedTeam()
                     self.dismiss(animated: true, completion: nil)
                }
            }
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
           
//            picker.dismiss(animated: true) {
//
//            }
             self.presentCropView(image: img)
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            
        }
    }
    func imageCroping(controller: CustomimageEditorViewController, didFinishPickingImage image: UIImage) {
        imageData = image.jpegData(compressionQuality: 0.9)
       
        self.navigationController?.popViewController(animated: false)
        DispatchQueue.main.async {
            self.teamImageView.image = UIImage(data: self.imageData!)!
            self.mediaPicker.dismiss(animated: false) {
                
            }
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    func presentCropView(image:UIImage)
    {
        let cropView = self.storyboard?.instantiateViewController(withIdentifier: "CustomimageEditorViewController") as! CustomimageEditorViewController
        cropView.setCropView(Image: image, delegate: self, cropSize: CGSize(width: teamImageView.frame.size.width * 1.5, height: teamImageView.frame.size.height * 1.5))
        cropView.hidesBottomBarWhenPushed = true
//    UINavigationController(rootViewController: cropView)
        mediaPicker.present(cropView, animated: false) {
            
        }
        
    }
    
    func showSportList(dataSource:[String],title:String,completion:@escaping (_ option:String,_ index:Int)->Void)
    {
        let listVc =
            self.storyboard?.instantiateViewController(withIdentifier: "SDPopUpListViewController") as! SDPopUpListViewController
        listVc.list = dataSource
        listVc.delegate = self
        listVc.listTitle = title
        listVc.SelectionClosure = {(index,option) in
            completion(option,index)
            
        }
        listVc.transitioningDelegate = self
        listVc.modalPresentationStyle = .custom
        present(listVc, animated: true, completion: nil)
        
    }
    
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
         return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.height / 4), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height / (4/3)))
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
         return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
    }
    func popController(controller: SDPopUpListViewController, didSelect option: String, at Index: Int) {
        
    }
    func searchPlayerVc(_ viewController: SDSearchPlayers_ViewController, didSelect root: Root) {
        self.teamLeader = root
        self.txtFieldTeamLeader.text =  "\(self.teamLeader!.first_name!) \(self.teamLeader!.surname!)"
    }
    
    func searchPlayerVc(_ viewController: SDSearchPlayers_ViewController, didSelect team: Team) {
        
    }
    func serchPlayerVC(_ viewController: SDSearchPlayers_ViewController, didSelect roots: [Root]) {
        for member in roots.enumerated()
        {
            if !(self.teamMembers?.contains(where: {$0.user_id == member.element.user_id}))!
            {
                self.teamMembers?.append(member.element)
            }
        }
         self.collectionViewMembers.reloadData()
    }
}

extension SDAdd_Team_TableViewController : UICollectionViewDataSource,PreviewCellProtocol
{
    func didSelectDelete(at index: Int) {
        self.teamMembers?.remove(at: index)
        self.collectionViewMembers.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return teamMembers!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewCell", for: indexPath) as!  SDPreviewCollectionViewCell
        cell.delegate = self
        cell.member = teamMembers![indexPath.row]
        return cell
    }
    
    
}
