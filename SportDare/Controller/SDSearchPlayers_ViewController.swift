//
//  SDSearchPlayers_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 04/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
protocol SearchPlayersVCProtocol {
    func searchPlayerVc(_ viewController:SDSearchPlayers_ViewController, didSelect root:Root)
    func serchPlayerVC(_ viewController:SDSearchPlayers_ViewController, didSelect roots:[Root])
    func searchPlayerVc(_ viewController:SDSearchPlayers_ViewController, didSelect team:Team)
}
class SDSearchPlayers_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PopupViewControllerDelegate,UISearchBarDelegate {
   
    var delegate : SearchPlayersVCProtocol?
    @IBOutlet weak var tableviewPlayers: UITableView!
    @IBOutlet weak var addBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var addBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    var isTeamSearch = Bool()
    var roots : [Root]?
    var teams :[Team]?
    var selectedRoots : [Root]?
    var isMultiSelection = false
    var filteredRoots : [Root]?
    var isSeraching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableviewPlayers.estimatedRowHeight = 44.0
        tableviewPlayers.rowHeight = UITableView.automaticDimension
        
      
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isTeamSearch
        {
            addBtnHeight.constant = 0
            addBtnWidth.constant = 0
            if isMultiSelection
            {
                btnSelect.isHidden = false
                selectedRoots = [Root]()
            }
            roots = [Root]()
            filteredRoots = [Root]()
            self.getRoots()
            searchBar.placeholder = " Search Players"
        }
        else
        {
            teams = [Team]()
            searchBar.placeholder = "Search Teams"
            getTeams()
        }
    }
func getRoots()
{
    ApiService.getRoots { (status, rootsList, error) in
        if status == true && error == nil
        {
            self.roots = rootsList
            self.tableviewPlayers.reloadData()
        }
    }
    }
    func getTeams()
    {
        ApiService.getTeams { (status, teamList, error) in
            if status == true && error == nil
            {
                self.teams = teamList
                self.tableviewPlayers.reloadData()
            }
        }
    }
    @IBAction func selectAction(_ sender: UIButton) {
        selectedRoots = roots?.filter({
            $0.isSelected == true
        })
        if (selectedRoots?.count)! > 0
        {
            delegate?.serchPlayerVC(self, didSelect: selectedRoots!)
        }
    }
    
    @IBAction func addTeamAction(_ sender: CustomButton) {
        let addTeamVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAdd_Team_TableViewController") as? SDAdd_Team_TableViewController
        let popupVC = PopupViewController(contentController: addTeamVC!, position: PopupViewController.PopupPosition.top(40), popupWidth: self.view.frame.width * 0.85, popupHeight: self.view.frame.height * 0.80)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        popupVC.delegate = self
        self.present(popupVC, animated: true) {
            
        }
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
        
    }
    
    //    MARK: UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isSeraching = searchText == "" ? false : true
        if isTeamSearch
        {
            
        }
        else{
            filteredRoots = roots?.filter({
                "\($0.first_name!) \($0.surname!)".localizedCaseInsensitiveContains(searchText)
            })
            tableviewPlayers.reloadData()
        }
         tableviewPlayers.reloadData()
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSeraching = false
        filteredRoots?.removeAll()
        tableviewPlayers.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isTeamSearch == true ? (teams?.count)! : (isSeraching == true ? filteredRoots?.count : (roots?.count)!)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if !isTeamSearch
        {
            let cell = tableviewPlayers.dequeueReusableCell(withIdentifier: "playerCell") as! SDSearchUserTableViewCell
            cell.root = isSeraching == true ? filteredRoots![indexPath.row] : roots![indexPath.row]
            cell.isMultiSelect = self.isMultiSelection
            cell.selectionBtn.isSelected = roots![indexPath.row].isSelected
             return cell
        }
        else
        {
             let cell = tableviewPlayers.dequeueReusableCell(withIdentifier: "teamCell") as! SDSearchUserTableViewCell
            cell.team = teams![indexPath.row]
            return cell
        }
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isTeamSearch
        {
            if !isMultiSelection
            {
                delegate?.searchPlayerVc(self, didSelect: roots![indexPath.row])
                self.dismiss(animated: true) {
                    
                }
            }
            else
            {
                if let root =  roots?[indexPath.row]
                {
                    let cell = tableviewPlayers.cellForRow(at: indexPath) as! SDSearchUserTableViewCell
                        root.isSelected = !root.isSelected
                        cell.selectionBtn.isSelected = root.isSelected

                }
            }
        }
        else
        {
            delegate?.searchPlayerVc(self, didSelect: teams![indexPath.row])
            self.dismiss(animated: true) {
                
            }
        }
    }
    
    func popupViewControllerDidDismiss(sender: PopupViewController) {
        self.getTeams()
    }
}
