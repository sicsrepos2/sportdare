//
//  SDViewTeamTableViewController.swift
//  Sportdare
//
//  Created by SICS on 30/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import BonsaiController

class SDViewTeamTableViewController: BaseTableViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ImageEditingDelegate,BonsaiControllerDelegate,PopupListDelegate,SearchPlayersVCProtocol {
    
    @IBOutlet weak var txtFieldSportCategory: CustomTextField!
    @IBOutlet weak var txtFieldTeamName: CustomTextField!
    @IBOutlet weak var collectionViewMembers: UICollectionView!
    @IBOutlet weak var txtFieldTeamLeader: CustomTextField!
    
    var mediaPicker: UIImagePickerController!
    var imageData : Data?
    var selectedSportCategory : Sports_category!
    var teamLeader : Root?
    var teamMembers : [Root]?
    var team : Team?
    @IBOutlet weak var teamImageView: CustomImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        mediaPicker = UIImagePickerController()
        mediaPicker.delegate = self
         setDetails()
    }
    
    func setDetails()
    {
        txtFieldTeamName.text = team?.team_name
        teamImageView.sd_setImage(with: team?.team_image?.url, completed: nil)
        
    }
    @IBAction func addImageAction(_ sender: UIButton) {
        showPicker_Options()
    }
    @IBAction func addMembersAction(_ sender: CustomButton) {
        let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SDSearchPlayers_ViewController") as? SDSearchPlayers_ViewController
        searchPage?.delegate = self
        searchPage?.isMultiSelection = true
        searchPage?.modalTransitionStyle = .coverVertical
        searchPage!.modalPresentationStyle = .overFullScreen
        self.present(searchPage!, animated: true) {
            
        }
        
    }
    
    func showPicker_Options()
    {
        
        let actionSheet = UIAlertController(title: "Change Profile Image", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.mediaPicker.sourceType = .camera
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            self.mediaPicker.sourceType = .photoLibrary
            self.present(self.mediaPicker, animated: true, completion: {
                
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        actionSheet.addAction(camera)
        actionSheet.addAction(photos)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            
        }
    }
    func imageCroping(controller: CustomimageEditorViewController, didFinishPickingImage image: UIImage) {
        imageData = image.jpegData(compressionQuality: 0.9)
        
        self.navigationController?.popViewController(animated: false)
        DispatchQueue.main.async {
            self.teamImageView.image = UIImage(data: self.imageData!)!
            self.mediaPicker.dismiss(animated: false) {
                
            }
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    func presentCropView(image:UIImage)
    {
        let cropView = self.storyboard?.instantiateViewController(withIdentifier: "CustomimageEditorViewController") as! CustomimageEditorViewController
        cropView.setCropView(Image: image, delegate: self, cropSize: CGSize(width: teamImageView.frame.size.width * 1.5, height: teamImageView.frame.size.height * 1.5))
        cropView.hidesBottomBarWhenPushed = true
        //    UINavigationController(rootViewController: cropView)
        mediaPicker.present(cropView, animated: false) {
            
        }
        
    }
    
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.height / 4), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height / (4/3)))
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
    }
    func popController(controller: SDPopUpListViewController, didSelect option: String, at Index: Int) {
        
    }
    func searchPlayerVc(_ viewController: SDSearchPlayers_ViewController, didSelect root: Root) {
        self.teamLeader = root
        self.txtFieldTeamLeader.text =  "\(self.teamLeader!.first_name!) \(self.teamLeader!.surname!)"
    }
    
    func searchPlayerVc(_ viewController: SDSearchPlayers_ViewController, didSelect team: Team) {
        
    }
    func serchPlayerVC(_ viewController: SDSearchPlayers_ViewController, didSelect roots: [Root]) {
        for member in roots.enumerated()
        {
            if !(self.teamMembers?.contains(where: {$0.user_id == member.element.user_id}))!
            {
                self.teamMembers?.append(member.element)
            }
        }
        self.collectionViewMembers.reloadData()
    }
}
extension SDViewTeamTableViewController : UICollectionViewDataSource,PreviewCellProtocol
{
    func didSelectDelete(at index: Int) {
       team?.team_members?.remove(at: index)
        self.collectionViewMembers.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (team?.team_members?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewCell", for: indexPath) as!  SDPreviewCollectionViewCell
        cell.delegate = self
        cell.teamMember = team?.team_members![indexPath.row]
        return cell
    }
    
    
}
