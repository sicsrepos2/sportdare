//
//  TestTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
protocol SerachResultControllerDelegate {
    func searchResults(_ controller : SDSearchResultTableViewController, didSelectUser userDetail:SearchUserDetail)
}
class SDSearchResultTableViewController: BaseTableViewController,UISearchResultsUpdating, UISearchBarDelegate {

    var searchList : [SearchUserDetail]?
     var delegate : SerachResultControllerDelegate?
     var searchTypes = ["Players","Events","Business"]
    override func viewDidLoad() {
        definesPresentationContext = false
        extendedLayoutIncludesOpaqueBars = true
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SDSearchUserTableViewCell
        let obj = searchList![indexPath.row]
        cell.userDetail =  obj
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.searchResults(self, didSelectUser: searchList![indexPath.row])
       
    }
    func updateSearchResults(for searchController: UISearchController) {
        
        
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
          let str = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        self.searchWithKey(key: str)
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
      
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchList?.removeAll()
        tableView.reloadData()
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//       searchBar.showsCancelButton = false
        tableView.reloadData()
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
//         searchBar.showsCancelButton = false
       
        
        return true
    }
   
    func searchWithKey(key:String)
    {
        ApiService.searchAll(key: key) { (status, list) in
            if status
            {
                 self.searchList?.removeAll()
                self.searchList = nil
                self.searchList  = list
                self.tableView.reloadData()
            }
        }
    }
}

extension SDSearchResultTableViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "filterCell", for: indexPath)
        let button = cell.viewWithTag(100) as? UIButton
        button?.setTitle(searchTypes[indexPath.row], for: .normal)
        
        return cell
        
    }
    
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        let width = collectionView.frame.width / 3
        return CGSize(width:width - 10, height: height)
    }
}
