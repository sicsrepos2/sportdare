//
//  SD_My_DaresViewController.swift
//  Sportdare
//
//  Created by Binoy T on 25/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_DaresViewController: BaseViewController {
    @IBOutlet weak var btnActivities: SDMenuButtonView!
    @IBOutlet weak var btnGambits: SDMenuButtonView!
    @IBOutlet weak var btnStats: SDMenuButtonView!
    @IBOutlet weak var ContainerActivities: UIView!
    var activitiesVC : SD_Player_Dares_ActivitiesViewController!
    var gambitsVC : SD_Player_Dares_GambitsViewController!
    var statsVC : SD_Player_Dares_StatsViewController!
 
    override func viewDidLoad() {
        super.viewDidLoad()
     
        btnActivities.isSelected = true
       loadViewControllers()
        activitiesVC.isFromProfile = true
        self.displayContentController(content: activitiesVC)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }

    @IBAction func didSelectSportDares(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnGambits.isSelected = false
        btnStats.isSelected = false
        removeChildVC()
        activitiesVC.isFromProfile = true
        self.displayContentController(content: activitiesVC)
       
    }
    
    @IBAction func didSelectGambits(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnActivities.isSelected = false
        btnStats.isSelected = false
        removeChildVC()
         self.displayContentController(content: gambitsVC)
       
    }
    
    @IBAction func didSelectStats(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnActivities.isSelected = false
        btnGambits.isSelected = false
        removeChildVC()
         self.displayContentController(content: statsVC)
       
    }
}

extension SD_Player_DaresViewController{
    
    func displayContentController(content: UIViewController) {
        addChild(content)
         content.view.frame = CGRect(x: 0, y: 0, width: ContainerActivities.frame.width, height: ContainerActivities.frame.height)
        self.ContainerActivities.addSubview(content.view)
        content.didMove(toParent: self)
    }
    
    func removeChildVC() {
        if let content = self.children.first
        {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
    
    func loadViewControllers()
    {
        if let child = self.children.first as? SD_Player_Dares_ActivitiesViewController
        {
            activitiesVC = child
             activitiesVC.isFromProfile = true
         
    
        }
        else
        {
        activitiesVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Dares_ActivitiesViewController") as? SD_Player_Dares_ActivitiesViewController
             activitiesVC.isFromProfile = true
        }
        
        gambitsVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Dares_GambitsViewController") as? SD_Player_Dares_GambitsViewController
        statsVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Dares_StatsViewController") as? SD_Player_Dares_StatsViewController
    }
    
}
