//
//  SD_My_Dares_ActivitiesViewController.swift
//  Sportdare
//
//  Created by Binoy T on 25/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_Dares_ActivitiesViewController: SDPlayerBaseViewController,CalendarProtocol{
   

    @IBOutlet weak var tableViewTimeline: UITableView!
       var isFromProfile = Bool()
       var isLoading = false
       var posts : [Posts]?
     var viewHeight : CGFloat!
     var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewTimeline.estimatedRowHeight = 300.0
        tableViewTimeline.rowHeight = UITableView.automaticDimension
        posts = [Posts]()
        loadData(offset: posts?.count)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.tintColor = UIColor.appBase
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableViewTimeline.addSubview(refreshControl)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didDidSetHeight(sender:)), name: Notification.Name.didUpdateSize, object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
      
       
    }
    @objc func didDidSetHeight(sender:NSNotification)
    {
        
    }
    @objc func refresh(sender:UIRefreshControl)
    {
       loadData(offset: 0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
         tableViewTimeline.layoutIfNeeded()
       
    }
    func loadData(offset:Int?)
    {
        if offset == 0
        {
            self.posts?.removeAll()
        }
        isLoading = true
        PlayerApiService.getPosts(playerId:(UserBase.currentUser?.selectedPlayer?.userid!)!,offset: offset!) { (status, result, error) in
            if status
            {
                self.isLoading = false
                self.posts!.append(contentsOf: result!)
                self.tableViewTimeline.reloadData()
            }
            self.refreshControl.endRefreshing()
        }
        
    }
    @IBAction func addSportDare(_ sender: SDCustomButton) {
        let addSportVC = self.storyboard?.instantiateViewController(withIdentifier: "SDAddSportDareViewController") as! SDAddSportDareViewController
        self.navigationController?.pushViewController(addSportVC, animated: true)
        
    }
  
    func didSelectDate(date: Date) {
        
    }
    
    @IBAction func commentAction(_ sender: UIButton) {
        let post = self.posts![sender.tag]
        let commentsVc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDCommentsViewController") as! SDCommentsViewController
        commentsVc.post = post
        commentsVc.commentProtocol = self
        commentsVc.postId = post.post_id
        self.hideTabBar()
        self.navigationController?.pushViewController(commentsVc, animated: true)
    
    }
    
}

extension SD_Player_Dares_ActivitiesViewController:UITableViewDataSource,UITableViewDelegate,TimeLineCellProtocol,PreviewVCProtocol,CommentsProtocol{
    func didChangedMonth(calendar: BNCustomCalendar) {
        
    }
    func didSelectDare(dare: [DareData]) {
        
    }
    
    func didAddedComment() {
        self.loadData(offset:0)
    }
    func didUpdatePost(post: Posts) {
        let changedIndex =  self.posts!.firstIndex(where: {$0.post_id == post.post_id})
        self.posts?.remove(at: changedIndex!)
        if (self.posts?.count)! > changedIndex!
        {
            self.posts?.insert(post, at: changedIndex!)
        }
        else
        {
            self.posts?.insert(post, at: changedIndex!-1)
            
        }
         self.tableViewTimeline.reloadData()
    }
    
        
    
    func didDeletedMedia_at(index: Int) {
        
    }
    @IBAction func highFiveAction(_ sender: UIButton) {

        let post = self.posts![sender.tag]
        post.is_clapped = !post.is_clapped!
         highFive(post: post)
        if  post.is_clapped!
        {
            post.post_claps_count = (post.post_claps_count)! + 1
        }
        else
        {
        
    
            post.post_claps_count = (post.post_claps_count)! - 1
        }
        
        self.tableViewTimeline.reloadRows(at: [IndexPath(item: sender.tag, section: 0)], with: .automatic)
       
    }
    
    func highFive(post:Posts)
    {
        ApiService.postHighFive_Wall(param: ["type":post.post_type!,"id":post.post_id! as Any]) { (status, result, error) in
            if result != nil
            {
                
                
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return (posts?.count)!
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let post = posts![indexPath.row]
        if post.post_type == PostType.text
        {
            let cell = tableViewTimeline.dequeueReusableCell(withIdentifier: "commentCell") as! SD_Player_Timeline_TableviewCell
            cell.post = post
            cell.index = indexPath.row
            return cell
        }
        else
        {
            let cell = tableViewTimeline.dequeueReusableCell(withIdentifier: "mediaCell") as! SD_Player_Timeline_TableviewCell
            cell.post = post
            cell.index = indexPath.row
            cell.delegate = self
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let post = posts![indexPath.row]
        if post.post_type == PostType.text
        {
        return UITableView.automaticDimension
        }
        else
        {

              return 400

        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastData = self.posts!.count - 1
        if !isLoading && indexPath.row == lastData {
            self.loadData(offset: posts?.count)
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       

    }
    
 
    func didSelectPost(post_id: String, index: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDPostPreviewVCViewController") as! SDPostPreviewVCViewController
        vc.isPlayerFeed = true
        if let post = (posts?.filter({$0.post_id == post_id}).last)
        {
            vc.mediaArray = post.post_media
            vc.post = post
        }
        vc.passedContentOffset = index
        vc.previewDelegate = self
         self.hideTabBar()
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
}
