//
//  SD_PersonalBest_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 08/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_PersonalBest_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PopupViewControllerDelegate {
 
    @IBOutlet weak var tableViewPB: UITableView!
      var subSport : SubSport_category?
    var personalBests : [PersonalBest]?
    override func viewDidLoad() {
        super.viewDidLoad()
        personalBests = [PersonalBest]()
      
    }
    override func viewWillAppear(_ animated: Bool) {
         getPb()
    }
    
  
    func getPb()
    {
        PlayerApiService.getPersonalBests(roleId: (subSport?.id)!, playerId:(UserBase.currentUser?.selectedPlayer?.userid)!) { (status, result) in
            if status
            {
                if let pb = result as? PB_Base , let bests = pb.personalBest
                {
                    self.personalBests = bests
                   
                }
                else
                {
                    self.personalBests?.removeAll()
                }
                 self.tableViewPB.reloadData()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personalBests!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewPB.dequeueReusableCell(withIdentifier: "regular") as! SD_PB_AwardsTableViewCell
        cell.personalBest = personalBests![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func popupViewControllerDidDismiss(sender: PopupViewController) {
        self.getPb()
    }

    
}
