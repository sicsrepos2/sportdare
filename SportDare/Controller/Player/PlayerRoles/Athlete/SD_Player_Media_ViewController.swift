//
//  SD_Media_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 08/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import TLPhotoPicker
import Photos
class SD_Player_Media_ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,PopupViewControllerDelegate,UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var collectionViewMedia: UICollectionView!
    var medias : [Media]?
    var subSport : SubSport_category?
     var selectedAssets = [TLPHAsset]()
    override func viewDidLoad() {
        super.viewDidLoad()
      medias = [Media]()
        // Do any additional setup after loading the view
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getMedia()
    }
    func getMedia()
    {
        PlayerApiService.getMedia(roleId: (subSport?.id)!,playerId:(UserBase.currentUser?.selectedPlayer?.userid)!) { (status, result) in
            if status
            {
                if let mediaBase = result as? Media_Base , let medias = mediaBase.medias
                {
                    self.medias = medias
                    
                }
                else
                {
                    self.medias?.removeAll()
                }
                self.collectionViewMedia.reloadData()
            }
        }
    }
    @IBAction func addMedia(_ sender: Any) {
        let viewController = CustomPhotoPickerViewController()
        viewController.delegate = self
        
        viewController.didExceedMaximumNumberOfSelection = { [weak self] (picker) in
         
        }
        self.selectedAssets.removeAll()
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        configure.selectedColor = UIColor.appBaseLight
        configure.maxSelectedAssets = 5
        configure.maxVideoDuration = 60.0
        configure.allowedAlbumCloudShared = true
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        viewController.logDelegate = self
        
        self.present(viewController, animated: true, completion: nil)
    }
    
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return (medias?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let reuseId = CollectionViewCellId.imageCell
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as? SDTimeLineMediaCollectionViewCell)!
        cell.media = medias![indexPath.row]
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/3)-2, height:  (collectionView.frame.height/3)-1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = SDPostPreviewVCViewController()
        vc.mediaArray =  medias!
        vc.isPlayerFeed = true
        vc.previewDelegate = self
        vc.passedContentOffset = IndexPath(row: (indexPath.row), section: 0)
        self.hideTabBar()

        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
extension SD_Player_Media_ViewController:TLPhotosPickerViewControllerDelegate,TLPhotosPickerLogDelegate,PreviewVCProtocol{
    func didDeletedMedia_at(index: Int) {
        self.medias?.remove(at: index)
        self.collectionViewMedia.reloadData()
    }
    
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        self.selectedAssets = withTLPHAssets
     
    }
    func dismissComplete() {
        if  self.selectedAssets.count > 0
        {
            var selectedMedia = [SDMedia]()
            self.selectedAssets.forEach { (tlpAsset) in
                selectedMedia.append(SDMedia(asset: tlpAsset.phAsset!))
            }
            let addMediaVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_AddMedia_TableViewController") as? SD_AddMedia_TableViewController
            addMediaVC?.selectedMedia = selectedMedia
            addMediaVC?.subSport = self.subSport
            addMediaVC?.didStartedUploading = { [weak self] (started) in
                KRProgressHUD.showOn(self!)
            }
            addMediaVC?.didUploadedMedia = {[weak self] (status,result,error) in
                if status!
                {
                KRProgressHUD.showSuccess(withMessage: "Added Media Successfully")
                }
                else
                {
                    KRProgressHUD.showError(withMessage: "Failed To Add")
                }
                self!.getMedia()
            }
            let popupVC = PopupViewController(contentController: addMediaVC!, position: PopupViewController.PopupPosition.top(40), popupWidth:  UIScreen.main.bounds.width * 0.85, popupHeight: UIScreen.main.bounds.height * 0.50)
            popupVC.backgroundAlpha = 0.3
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = false
            popupVC.cornerRadius = 10
            popupVC.shadowEnabled = true
            popupVC.delegate = self
            self.present(popupVC, animated: false) {
                
            }
        }
        
    }
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
//        PHAssetSourceType
    }
    func selectedCameraCell(picker: TLPhotosPickerViewController) {
        
    }
    
    func deselectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        
    }
    
    func selectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        
    }
    
    func selectedAlbum(picker: TLPhotosPickerViewController, title: String, at: Int) {
        
    }
    
    
}
