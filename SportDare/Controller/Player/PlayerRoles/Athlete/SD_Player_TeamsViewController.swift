//
//  SD_TeamsViewController.swift
//  Sportdare
//
//  Created by SICS on 10/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_TeamsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var tableViewTeams: UITableView!
    var teams :[Team]?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewTeams.estimatedRowHeight = 44.0
        tableViewTeams.rowHeight = UITableView.automaticDimension
        teams = [Team]()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getTeams()
    }
    func getTeams()
    {
        PlayerApiService.getTeams(playerId: (UserBase.currentUser?.selectedPlayer?.userid)!) { (status, teamList, error) in
            if status == true && error == nil
            {
                self.teams = teamList
                self.tableViewTeams.reloadData()
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewTeams.dequeueReusableCell(withIdentifier: "cellTeam") as! SDTeamTableViewCell
        cell.team = teams![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
   

}
