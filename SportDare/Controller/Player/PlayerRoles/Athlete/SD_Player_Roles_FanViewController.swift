//
//  SD_My_Roles_FanViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_Roles_FanViewController: BaseViewController {
    var roots : [Root]?
 @IBOutlet weak var tableViewRoots: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        roots = [Root]()
        getRoots()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getRoots()
    {
        PlayerApiService.getRoots(playerId: (UserBase.currentUser?.selectedPlayer?.userid)!) { (status, myRoots, error) in
            if status == true
            {
                self.roots = myRoots
                self.tableViewRoots.reloadData()
            }
        }
        
    }

}

extension SD_Player_Roles_FanViewController :UITableViewDataSource, UITableViewDelegate {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  roots!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellRoots") as! SDRootsTableViewCell
        cell.root = roots![indexPath.row]
            
        return cell
       
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  70
    }
}
