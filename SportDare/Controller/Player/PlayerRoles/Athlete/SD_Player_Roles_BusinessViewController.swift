//
//  SD_My_Roles_BusinessViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_Roles_BusinessViewController: BaseViewController {

 @IBOutlet weak var tableViewBusiness: UITableView!
     var businesses : [Business]?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         getBusinesses()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getBusinesses()
    {
        ApiService.getBusiness(id: (UserBase.currentUser?.selectedPlayer?.userid)!) { (status, result, errro) in
            
            
            if let businessList = result as? [Business]
            {
                self.businesses?.removeAll(keepingCapacity: true)
                self.businesses?.append(contentsOf: businessList)
                self.tableViewBusiness.reloadData()
                
            }
            
            
        }
    }
    @IBAction func addBusinessAction(_ sender: SDCustomButton) {
        
    }
    

}
extension SD_Player_Roles_BusinessViewController :UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businesses?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let businessCell = tableView.dequeueReusableCell(withIdentifier: "cellBusiness") as! SDBusinessTableViewCell
              businessCell.business = businesses![indexPath.row]
        return businessCell
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  70
    }
}
