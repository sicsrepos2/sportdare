//
//  SD_MyRoles_BaseViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_Roles_BaseViewController: SDPlayerBaseViewController {
    @IBOutlet weak var btnFan: SDMenuButtonView!
    
    @IBOutlet weak var btnAthlete: SDMenuButtonView!
    
    @IBOutlet weak var btnBusiness: SDMenuButtonView!
    @IBOutlet var labelsFan: [UILabel]!
    @IBOutlet var labelsAthlete: [UILabel]!
    @IBOutlet weak var labelBusiness: UILabel!
    @IBOutlet weak var containerRoles: UIView!
    var athleteVC : SD_Player_Roles_AthleteViewController!
    var fanVC : SD_Player_Roles_FanViewController!
    var businessVC : SD_Player_Roles_BusinessViewController!
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
      edgesForExtendedLayout = []
        btnAthlete.isSelected = true
           _ = labelsAthlete.map({$0.isHidden = false})
        loadViewControllers()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
          containerRoles.layoutIfNeeded()
    }
    
    // MARK:    ******************ROLES**********************
    
    @IBAction func didSelectFan(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnAthlete.isSelected = false
        btnBusiness.isSelected = false
//        btnAddMore.setTitle("Add more ROOTS", for: .normal)
        _ = labelsFan.map({$0.isHidden = false})
        _ = labelsAthlete.map({$0.isHidden = true})
        labelBusiness.isHidden = true
        self.removeChildVC()
        displayContentController(content: fanVC)
       
    }
    
    @IBAction func didSelectAthlete(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnFan.isSelected = false
        btnBusiness.isSelected = false
        _ = labelsAthlete.map({$0.isHidden = false})
        _ = labelsFan.map({$0.isHidden = true})
        labelBusiness.isHidden = true
        self.removeChildVC()
        displayContentController(content: athleteVC)
        
    }
    
    @IBAction func didSelectBusiness(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnAthlete.isSelected = false
        btnFan.isSelected = false
//        btnAddMore.setTitle("Add a BUSINESS", for: .normal)
        _ = labelsFan.map({$0.isHidden = true})
        labelBusiness.isHidden = false
        _ = labelsAthlete.map({$0.isHidden = true})
        self.removeChildVC()
        displayContentController(content: businessVC)
    }
   

}



extension SD_Player_Roles_BaseViewController{
    
    func displayContentController(content: UIViewController) {
        addChild(content)
         content.view.frame = CGRect(x: 0, y: 0, width: containerRoles.frame.width, height: containerRoles.frame.height)
        self.containerRoles.addSubview(content.view)
        containerRoles.layoutIfNeeded()
        content.didMove(toParent: self)
    }
    
    func removeChildVC() {
        if let content = self.children.first
        {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
    func loadViewControllers()
    {
        athleteVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Roles_AthleteViewController") as? SD_Player_Roles_AthleteViewController
        fanVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Roles_FanViewController") as? SD_Player_Roles_FanViewController
        businessVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Roles_BusinessViewController") as? SD_Player_Roles_BusinessViewController
    }
}
