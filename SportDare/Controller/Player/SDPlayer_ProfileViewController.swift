//
//  SDPlayer_ProfileViewController.swift
//  Sportdare
//
//  Created by Binoy T on 20/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import SkeletonView

class SDPlayer_ProfileViewController: SDPlayerBaseViewController,PopupViewControllerDelegate {

    @IBOutlet weak var profileImageView: SDImageViewShield!
    
    @IBOutlet weak var btnRoot: CustomButton!
    @IBOutlet weak var btnsStack: UIStackView!
    
    @IBOutlet var btnHero: CustomButton!
    @IBOutlet weak var accountTypeImage: UIImageView!
    @IBOutlet weak var accountType: UILabel!
    @IBOutlet weak var gambits: UILabel!
    @IBOutlet weak var shieldType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblMotto: UILabel!
    var basicDetail : SearchUserDetail?
    var playerId :String?
    override func viewDidLoad() {
        super.viewDidLoad()
      
        getPalayerDetails()
//        btnsStack.addArrangedSubview(btnHero)
        
    }
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        navigationController?.view.setNeedsLayout() // force update layout
        navigationController?.view.layoutIfNeeded() // to
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getPalayerDetails()
    {
        
       self.view.showAnimatedGradientSkeleton()
        ApiService.viewProfile(id: playerId!) { (status, result,error) in
            DispatchQueue.main.async {
                 self.view.hideSkeleton()
            }
           
            if status && result != nil
            {
              self.setPlayerDetail(player: result)
            }
            else if error != nil
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
        }
    }
    func setPlayerDetail(player:PlayerDetails?)
    {
        self.playerDetail = player
        UserBase.currentUser?.selectedPlayer = player
        self.profileImageView.setImagewith_Url(url: (player?.image)!, PlaceHolderImage:UIImage(named: "placeholder-1")!)
        self.lblName.text = (player?.name)! + " " + (player?.surName)!
        if self.basicDetail?.rooting != "NO"
        {
            self.btnRoot.isSelected = true
        }
        self.lblAge.text =  "\(Utilities.getAge(dob:(player?.dOB)!)) Years"
        self.accountType.text = player?.account
        self.gambits.text = player?.gambits
        self.lblGender.text = player?.gender
        self.lblLocation.text = player?.location
        self.lblMotto.text = player?.moto
        let accoutType = AccountType(rawValue: player!.account!)
        accountTypeImage.image = accoutType?.image
    }
    //    MARK:- Actions
    @IBAction func dareUser(_ sender: CustomButton) {
        let addSportVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDAddSportDareViewController") as! SDAddSportDareViewController
        addSportVC.darePlayer = self.playerDetail
        self.navigationController?.pushViewController(addSportVC, animated: true)
    }
    @IBAction func root_Unroot_Action(_ sender: CustomButton) {
        if sender.isSelected
        {
            unRootUser()
        }
        else
        {
            rootUser()
        }
    }
    @IBAction func recruitAction(_ sender: CustomButton) {
        let addTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SD_TeamsViewController") as? SD_TeamsViewController
        addTeamVC?.addingPlayer = self.basicDetail
        let popupVC = PopupViewController(contentController: addTeamVC!, position: PopupViewController.PopupPosition.top(40), popupWidth: self.view.frame.width * 0.85, popupHeight: UIScreen.main.bounds.height * 0.80)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        popupVC.delegate = self
        
        self.present(popupVC, animated: true) {
            
        }
    }
    
    @IBAction func messageUser(_ sender: CustomButton) {
       let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDChattViewController") as! SDChattViewController
        self.hideTabBar()
        chatVC.playerDetail = self.playerDetail
        chatVC.showBackButton = true
        self.navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
    
    func rootUser()
    {
        ApiService.root_User(param: ["root_id":basicDetail!.user_id!]) { (status, result, error) in
            if status
            {
                self.btnRoot.isSelected = true
                self.basicDetail?.rooting = "YES"
            }
        }
    }
    func unRootUser()
    {
        ApiService.unRoot_User(id: basicDetail!.user_id!) { (status, result, error) in
            if status
            {
                self.btnRoot.isSelected = false
                self.basicDetail?.rooting = "NO"
            }
        }
    }
    
    @IBAction func showPlayerDares(_ sender: CustomButton) {
        let activityVC =  self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Funtions_MainViewController") as! SD_Player_Funtions_MainViewController
        activityVC.showBackButton = true
        activityVC.isForActivities = true
        activityVC.playerDetail = self.playerDetail
        self.navigationController?.pushViewController(activityVC, animated: true)
        
    }
    @IBAction func showPlayerRoles(_ sender: CustomButton) {
        let activityVC =  self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Funtions_MainViewController") as! SD_Player_Funtions_MainViewController
        activityVC.showBackButton = true
        activityVC.isForRoles = true
        activityVC.playerDetail = self.playerDetail
        self.navigationController?.pushViewController(activityVC, animated: true)
    }
    @IBAction func showPlayers(_ sender: CustomButton) {
        let activityVC =  self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Funtions_MainViewController") as! SD_Player_Funtions_MainViewController
        activityVC.showBackButton = true
        activityVC.isForPlayers = true
        activityVC.playerDetail = self.playerDetail
        self.navigationController?.pushViewController(activityVC, animated: true)
    }
    
    
}
