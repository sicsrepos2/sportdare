//
//  SD_My_PlayersViewController.swift
//  Sportdare
//
//  Created by Binoy T on 31/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_PlayersViewController: BaseViewController {
    @IBOutlet weak var btnRoots: SDMenuButtonView!
    @IBOutlet weak var btnTeams: SDMenuButtonView!
    @IBOutlet weak var btnFans: SDMenuButtonView!
    @IBOutlet weak var btnHeroes: SDMenuButtonView!
     @IBOutlet weak var tableViewPlayers: UITableView!
    @IBOutlet weak var containerView: UIView!
    var teamsVC : SD_Player_TeamsViewController!
    var fansVC : SD_Player_Fans_ViewController!
    var rootsVC : SD_Player_Roots_ViewController!
    var heroesVC : SD_Player_Heroes_ViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadViewControllers()
          btnTeams.isSelected = true
         self.displayContentController(content: teamsVC)
      
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func didSelectRoot(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnTeams.isSelected = false
        btnFans.isSelected = false
        btnHeroes.isSelected = false
        self.removeChildVC()
        displayContentController(content: rootsVC)
    }
    @IBAction func didSelectTeam(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnRoots.isSelected = false
        btnFans.isSelected = false
        btnHeroes.isSelected = false
        self.removeChildVC()
        displayContentController(content: teamsVC)
    }
    @IBAction func didSelectFans(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnRoots.isSelected = false
        btnTeams.isSelected = false
        btnHeroes.isSelected = false
        self.removeChildVC()
        displayContentController(content: fansVC)
    }
    @IBAction func didSelectHeros(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnRoots.isSelected = false
        btnTeams.isSelected = false
        btnFans.isSelected = false
        self.removeChildVC()
        displayContentController(content: heroesVC)
    }
    func loadViewControllers()
    {
        teamsVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_TeamsViewController") as? SD_Player_TeamsViewController
        fansVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Fans_ViewController") as? SD_Player_Fans_ViewController
        rootsVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Roots_ViewController") as? SD_Player_Roots_ViewController
         heroesVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_Player_Heroes_ViewController") as? SD_Player_Heroes_ViewController
    }

}
extension SD_Player_PlayersViewController{
    
    func displayContentController(content: UIViewController) {
        addChild(content)
        content.view.frame = CGRect(x: 0, y: 0, width: containerView.frame.width, height: containerView.frame.height)
        self.containerView.addSubview(content.view)
        containerView.layoutIfNeeded()
        content.didMove(toParent: self)
    }
    
    func removeChildVC() {
        if let content = self.children.first
        {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
}
