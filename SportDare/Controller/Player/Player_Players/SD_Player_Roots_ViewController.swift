//
//  SD_MyRoots_ViewController.swift
//  Sportdare
//
//  Created by Binoy T on 14/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_Player_Roots_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    
    @IBOutlet weak var tableViewMyRoots: UITableView!
    var roots : [Root]?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewMyRoots.estimatedRowHeight = 44.0
        tableViewMyRoots.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
        roots = [Root]()
        getRoots()
    }
    func getRoots()
    {
        ApiService.getRoots { (status, myRoots, error) in
            if status == true
            {
                self.roots = myRoots
                self.tableViewMyRoots.reloadData()
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (roots?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewMyRoots.dequeueReusableCell(withIdentifier: "cellRoots") as! SDRootsTableViewCell
        cell.root = roots![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
}
