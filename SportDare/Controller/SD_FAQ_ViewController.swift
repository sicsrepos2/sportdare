//
//  SD_FAQ_ViewController.swift
//  Sportdare
//
//  Created by SICS on 25/06/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
import WebKit

class SD_FAQ_ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var fileUrl : URL!
    override func viewDidLoad() {
        super.viewDidLoad()
     webView.loadFileURL(fileUrl, allowingReadAccessTo: fileUrl)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
   

}
