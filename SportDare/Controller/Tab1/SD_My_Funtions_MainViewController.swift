//
//  SD_My_Funtions_MainViewController.swift
//  Sportdare
//
//  Created by Binoy T on 25/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SD_My_Funtions_MainViewController: BaseViewController {
    @IBOutlet weak var btnSportDares: SDMenuButtonView!
    @IBOutlet weak var btnRoles: SDMenuButtonView!
    @IBOutlet weak var btnPlayers: SDMenuButtonView!
    
    @IBOutlet weak var containerMain: UIView!
    var isForActivities = Bool()
    var isForRoles = Bool()
    var isForPlayers = Bool()
    var myDaresVC : SD_My_DaresViewController!
    var myRolesVC : SD_MyRoles_BaseViewController!
    var myPlayersVC : SD_My_PlayersViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadVC()
        if isForPlayers
        {
            btnPlayers.isSelected =  true
            self.removeChildVC()
            self.displayContentController(content: myPlayersVC)
        }
        else if isForRoles
        {
            btnRoles.isSelected =  true
            self.removeChildVC()
            self.displayContentController(content: myRolesVC)
        }
        else
        {
            btnSportDares.isSelected =  true
            self.removeChildVC()
           
            self.displayContentController(content: myDaresVC)
        }
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      
    }
    @IBAction func didSelectActivities(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnRoles.isSelected = false
        btnPlayers.isSelected = false
        self.removeChildVC()
    
        self.displayContentController(content: myDaresVC)
    }
    
    @IBAction func didSelectRoles(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        
        btnSportDares.isSelected = false
        btnPlayers.isSelected = false
        self.removeChildVC()
        self.displayContentController(content: myRolesVC)
    }
    @IBAction func didSelectPlayers(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnRoles.isSelected = false
        btnSportDares.isSelected = false
        self.removeChildVC()
        self.displayContentController(content: myPlayersVC)
       
    }
   

}
extension SD_My_Funtions_MainViewController{
    
    func displayContentController(content: UIViewController) {
        addChild(content)
        content.view.frame = CGRect(x: 0, y: 0, width: containerMain.frame.width, height: containerMain.frame.height)
        self.containerMain.addSubview(content.view)
        content.didMove(toParent: self)
    }
    
    func removeChildVC() {
        if let content = self.children.first
        {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
    func loadVC()
    {
        myDaresVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_My_DaresViewController") as? SD_My_DaresViewController
        myRolesVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_MyRoles_BaseViewController") as? SD_MyRoles_BaseViewController
        myPlayersVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_My_PlayersViewController") as? SD_My_PlayersViewController
    }
}
