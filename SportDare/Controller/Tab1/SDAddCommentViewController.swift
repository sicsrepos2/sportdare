//
//  SDAddCommentViewController.swift
//  SportDare
//
//  Created by Binoy T on 17/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDAddCommentViewController: BaseViewController {
    @IBOutlet weak var textViewComment: KMPlaceholderTextView!
    @IBOutlet var toolBar: UIToolbar!
    
    @IBOutlet weak var btnPrivacy: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewComment.inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneAction(_ sender: UIBarButtonItem) {
        self.textViewComment.resignFirstResponder()
    }
    @IBAction func postAction(_ sender: CustomButton) {
        if !textViewComment.text.isEmpty
        {
             let privacy = btnPrivacy.currentTitle
            KRProgressHUD.show()
            ApiService.post_Comment(comment: textViewComment.text, privacy: privacy!) { (status, result, error) in
              
                if status
                {
                    KRProgressHUD.showSuccess(withMessage: "Successfully posted your comment.")
                    self.textViewComment.text = ""
                }
                else
                {
                      KRProgressHUD.dismiss()
                }
                
            }
        }
        
    }
    @IBAction func setPrivacyAction(_ sender: CustomButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.dataSource = Constants.privacyLevels.map({$0.name!})
        
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            cell.logoImageView.image = Constants.privacyList[item]
            cell.optionLabel.text = item
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = {(index,value) in
            sender.setTitle(value, for: .normal)
            sender.setImage(Constants.privacyList.values.map({$0})[index], for: .normal)
        }
        dropDown.show()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
