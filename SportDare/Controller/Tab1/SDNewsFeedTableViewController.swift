//
//  SDNewsFeedTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import Foundation
//import ListPlaceholder
class SDNewsFeedTableViewController: BaseViewController,AddFeedProtocol,UITextViewDelegate{
    @IBOutlet var viewGettingStarted: UIView!
    
    @IBOutlet var imageGettingStarted: UIImageView!
    @IBOutlet weak var tableViewFeeds: UITableView!
    @IBOutlet weak var progressStatus: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var viewProgress: UIView!
    var isLoading = false
    var feeds : [Feed]?
      var refreshControl = UIRefreshControl()
     var expandedCells = Set<Int>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.feeds = [Feed]()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.tintColor = UIColor.appBase
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableViewFeeds.addSubview(refreshControl)
        self.tableViewFeeds.contentInsetAdjustmentBehavior = .never
       
        
        
       
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.showTabBar()
     
        if let isFirstlogin = UserDefaults.standard.value(forKey: AppKeys.IsFirstLogin.rawValue) as? Bool
        {
            if isFirstlogin
            {
                UserDefaults.standard.set(false, forKey: AppKeys.IsFirstLogin.rawValue)
                
                self.tableViewFeeds.backgroundView = imageGettingStarted
                DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
                    self.loadData(with: (self.feeds?.count)!)
                }
                
            }
            else
            {
                self.tableViewFeeds.backgroundView = nil
                 self.loadData(with: (feeds?.count)!)
            }
            
        }
        
        self.checkPostProgress()
        
    }
    
    override func viewDidLayoutSubviews() {
        viewGettingStarted.frame =  self.tableViewFeeds.bounds
    }
    override func viewWillDisappear(_ animated: Bool) {
        //        view.stopSkeletonAnimation()
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let addPopupVC = segue.destination as? SDAddAnythingPopupViewController
        {
            addPopupVC.addAnythingProtocol = self
        }
    }
    @objc func refresh(sender:UIRefreshControl)
    {
        self.loadData(with: 0)
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }


   
    func addPopupDidSelectAddDare() {
        let addDareVC =  self.storyboard?.instantiateViewController(withIdentifier: "SDAddSportDareViewController") as! SDAddSportDareViewController
        addDareVC.showBackButton = true
        addDareVC.showSearch = true
        self.navigationController?.pushViewController(addDareVC, animated: true)
//        self.tabBarController?.selectedIndex = 1
        
    }
    func addPopupDidSelectAddPhotos() {
        let addPhotosVc = self.storyboard?.instantiateViewController(withIdentifier: "SDAddPhotos_VideosViewController") as! SDAddPhotos_VideosViewController
        self.navigationController?.pushViewController(addPhotosVc, animated: true)
    }
    
    func addPopupDidSelectAddComent() {
        let addCommentVc = self.storyboard?.instantiateViewController(withIdentifier: "SDAddCommentViewController") as! SDAddCommentViewController
        self.navigationController?.pushViewController(addCommentVc, animated: true)
    }
    
    private func checkPostProgress()
    {
        progressBar.transform = CGAffineTransform(scaleX: 1, y: 3)
        self.tableViewFeeds.beginUpdates()
        viewProgress.frame = CGRect(x: 0, y: 0, width: viewProgress.frame.width, height: 0)
        
        self.tableViewFeeds.endUpdates()
        if ApiService.sharedInstance.isTaskRunning
        {
           
            ApiService.sharedInstance.progressHandler = {(isRunning,progress,error) in
                if isRunning
                {
                    self.viewProgress.frame = CGRect(x: 0, y: 0, width: self.viewProgress.frame.width, height: 60)
                    self.progressBar.setProgress(Float(progress!.fractionCompleted), animated: true)
                    self.progressStatus.text = progress!.fractionCompleted > 0.80 ? "Almost Finished" : "Uploading Your Post"
                    if error != nil
                    {
                        self.progressBar.progressTintColor = UIColor.red
                        self.progressStatus.text = "Error Occured While Uploading!"
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                            self.viewProgress.frame = CGRect(x: 0, y: 0, width: self.viewProgress.frame.width, height: 0)
                            
                            self.view.layoutSubviews()
                        })
                    }
                    self.tableViewFeeds.beginUpdates()
                    self.tableViewFeeds.endUpdates()
                    self.view.layoutSubviews()
                    
                }
                else
                {
                     self.loadData(with: 0)
                    self.viewProgress.frame = CGRect(x: 0, y: 0, width: self.viewProgress.frame.width, height: 0)
                    
                    self.view.layoutSubviews()
                   
                    //                self.tableViewFeeds.reloadInputViews()
                }
                
            }
        }
    }

    @IBAction func didSelectUserAction(_ sender: SDImageViewShield) {
        let feed = feeds![sender.tag]
       
           guard let userId = feed.user?.userData?.user_id
           else{return}
        if userId != UserBase.currentUser?.details?.userid
        {
            let playerProfilVC = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(withIdentifier: "SDPlayer_ProfileViewController") as! SDPlayer_ProfileViewController
            playerProfilVC.playerId = userId
            self.navigationController?.pushViewController(playerProfilVC, animated: true)
        }
        
    }
    
    @IBAction func commentAction(_ sender: UIButton) {
        let feed = self.feeds![sender.tag]
        let commentsVc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDCommentsViewController") as! SDCommentsViewController
        commentsVc.feed = feed
        commentsVc.isFeed = true
        commentsVc.commentProtocol = self
        commentsVc.postId = feed.activity_id
        self.hideTabBar()
        self.navigationController?.pushViewController(commentsVc, animated: true)
    }
    
    @IBAction func highFiveAction(_ sender: UIButton) {
        let feed = self.feeds![sender.tag]
        feed.is_clapped = !feed.is_clapped!
        highFive(feed: feed)
        if  feed.is_clapped!
        {
            feed.claps_count = (feed.claps_count)! + 1
        }
        else
        {
            feed.claps_count = (feed.claps_count)! - 1
        }
        
        self.tableViewFeeds.reloadRows(at: [IndexPath(item: sender.tag, section: 0)], with: .fade)
    }
    
    func highFive(feed:Feed)
    {
        ApiService.postHighFiveFeed(param: ["type":feed.activity_type!,"id":feed.referance_id!]) { (status, result, error) in
            if result != nil
            {
                
                
            }
        }
    }
}

extension SDNewsFeedTableViewController :UITableViewDelegate,UITableViewDataSource,UITableViewDataSourcePrefetching,CommentsProtocol,TimeLineCellProtocol,PreviewVCProtocol,FeedCellProtocol
{
    func didSelectPlayerWith(id: String) {
        
    }
    func didDeletedMedia_at(index: Int) {
        
    }
    
    
    func didUpdateFeed(feed: Feed) {
        let changedIndex =  self.feeds!.firstIndex(where: {$0.activity_id == feed.activity_id})
        self.feeds?.remove(at: changedIndex!)
        if (self.feeds?.count)! > changedIndex!
        {
            self.feeds?.insert(feed, at: changedIndex!)
        }
        else
        {
            self.feeds?.insert(feed, at: changedIndex!-1)
            
        }
        self.tableViewFeeds.reloadData()
    }
    // MARK: - Table view data source
    func loadData(with offset:Int)
    {
        isLoading = true
       
        ApiService.getNewsFeed(offset: offset) { (status, result, error) in
             self.isLoading = false
            if status
            {
               
                if offset == 0
                {
                    self.feeds?.removeAll()
                }
                self.feeds!.append(contentsOf: result!)
                self.tableViewFeeds.reloadData()
                self.tableViewFeeds.showEmptyMessage(message: "")
                
            }
            else if error != nil
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
                self.tableViewFeeds.showEmptyMessage(message: "Nothing Found")
            }
            self.refreshControl.endRefreshing()
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return feeds!.count
    }
    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feed =   feeds![indexPath.row]
        let cell = Utilities.getCellFor(tableView: tableViewFeeds,feed:feed, indexPath: indexPath)!
        if let feedCell =  cell as? SDFeedTableViewCell
        {
         feedCell.textViewDescription.shouldTrim = !expandedCells.contains(indexPath.row)
         feedCell.textViewDescription.setNeedsUpdateTrim()
         feedCell.textViewDescription.layoutIfNeeded()
         feedCell.delegate = self
        }
        else if let rootCell = cell as? SDFeed_Root_TableViewCell
        {
            rootCell.feedDelegate = self
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastData = self.posts!.count - 1
//        if !isLoading && indexPath.row == lastData {
//            self.loadData()
//        }
        if let textView = (cell as? SDFeedTableViewCell)?.textViewDescription
        {
            textView.onSizeChange = { [unowned tableView, unowned self] r in
                let point = tableView.convert(r.bounds.origin, from: r)
                
                guard let indexPath = tableView.indexPathForRow(at: point) else { return }
                if r.shouldTrim {
                    self.expandedCells.remove(indexPath.row)
                } else {
                    self.expandedCells.insert(indexPath.row)
                }
                self.tableViewFeeds.reloadRows(at: [indexPath], with: .fade)
//                self.tableViewFeeds.beginUpdates()
//                self.tableViewFeeds.endUpdates()
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let lastData = self.feeds!.count - 1
        if !isLoading && indexPaths.last!.row == lastData {
            self.loadData(with: self.feeds!.count)
        }
    }
    
    func didSelectPost(post_id: String, index: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SDFeedPreviewViewController") as! SDFeedPreviewViewController
        self.hideTabBar()
        vc.previewDelegate = self
        if let feed = (feeds?.filter({$0.activity_id == post_id}).last)
        {
            vc.mediaArray = feed.post!.post_media
            vc.feed = feed
        }
        vc.passedContentOffset = index
      
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
