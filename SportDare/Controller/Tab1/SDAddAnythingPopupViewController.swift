//
//  SDAddAnythingPopupViewController.swift
//  SportDare
//
//  Created by Binoy T on 17/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
protocol AddFeedProtocol {
    func addPopupDidSelectAddPhotos()
    func addPopupDidSelectAddComent()
    func addPopupDidSelectAddDare()
}

class SDAddAnythingPopupViewController: UIViewController {
    public var addAnythingProtocol : AddFeedProtocol!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismissView(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func invitePeople(_ sender: CustomButton) {
      Utilities.showShareSheet(vc: self)
//        self.dismiss(animated: true) {
//            
//        }
    }
    @IBAction func addComment(_ sender: CustomButton) {
        addAnythingProtocol.addPopupDidSelectAddComent()
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func addPhotos(_ sender: CustomButton) {
        addAnythingProtocol.addPopupDidSelectAddPhotos()
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func addDare(_ sender: CustomButton) {
        addAnythingProtocol.addPopupDidSelectAddDare()
        self.dismiss(animated: true) {
            
        }
    }
    
   

}
