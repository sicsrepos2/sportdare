//
//  SDAddPhotos&VideosViewController.swift
//  SportDare
//
//  Created by Binoy T on 17/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.


import UIKit
import MobileCoreServices
import Photos
import AVKit
import TLPhotoPicker

class SDAddPhotos_VideosViewController: BaseTableViewController {
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var txtViewDescription: KMPlaceholderTextView!
    @IBOutlet weak var collectionViewPreview: UICollectionView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var btnPrivacy: CustomButton!
    var selectedMedia : [SDMedia]!
    var previewMedia : SDMedia!
    var playerController : AVPlayerViewController!
    var selectedAssets = [TLPHAsset]()
    @IBOutlet weak var progressStatus: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var viewProgress: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtViewDescription.inputAccessoryView = toolBar
        selectedMedia = [SDMedia]()
        progressBar.transform = CGAffineTransform(scaleX: 1, y: 3)
        viewProgress.frame = CGRect(x: 0, y: 0, width: viewProgress.frame.width, height: 0)
       self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if ApiService.sharedInstance.isTaskRunning
        {
            checkPostProgress()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addPhotosAction(_ sender: CustomButton) {
        
        showOptions()
        
    }
    @IBAction func setPrivacyAction(_ sender: CustomButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.dataSource = Constants.privacyList.keys.map({$0})
        
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            cell.logoImageView.image = Constants.privacyList.values.map({$0})[index]
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = {(index,value) in
            sender.setTitle(value, for: .normal)
            sender.setImage(Constants.privacyList.values.map({$0})[index], for: .normal)
        }
        dropDown.show()
    }
    
    @IBAction func postAction(_ sender: CustomButton) {
        if selectedMedia.count > 0
        {
             KRProgressHUD.showMessage("Uploading Your Post")
            let privacy = btnPrivacy.currentTitle
            ApiService.sharedInstance.post_Media(files: selectedMedia,privacy:privacy! ,completion: { (status, result, error) in
                if status
                {
                    KRProgressHUD.showSuccess(withMessage: "Posted Successfully")
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    KRProgressHUD.showError(withMessage: error?.localizedDescription)
                    
                }
            }, startedBlock: { (started) in
                if started
                {
                    self.selectedMedia.removeAll()
                    self.previewMedia = nil
                    self.showPreview()
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()
                }
            }) { (progress) in
                
            }
           
            
        }
    }
    
    @IBAction func textViewDone(_ sender: UIBarButtonItem) {
        self.txtViewDescription.resignFirstResponder()
    }
    //    MARK:- Options
    
func showOptions()
{
    
    let alertController = UIAlertController(title: "Select option", message: "", preferredStyle: .actionSheet)
    let action1 = UIAlertAction(title: "Gallery", style: .default) { (action) in
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    self.openGallery()
                } else {
                    let alert = UIAlertController(title: "Photos Access Denied", message: "Sportdare needs access to photos library.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        } else if photos == .authorized {
            self.openGallery()
        }
        
        
    }
    let action2 = UIAlertAction(title: "Camera", style: .default) { (action) in
        if PHPhotoLibrary.authorizationStatus() == .authorized
        {
            self.openCamera()
        }
    }
    let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        
    }
    alertController.addAction(action1)
    alertController.addAction(action2)
    alertController.addAction(cancel)
    self.present(alertController, animated: true) {
        
    }
    }
    
    
     //    MARK:- Open Gallery
//    func openGallery()
//    {
//
//        let picker = self.storyboard?.instantiateViewController(withIdentifier: "CustomPickerNavigationController") as! CustomPickerNavigationController
//        picker.pickerDelegate = self
//        picker.selectedCount = selectedMedia.count
//        picker.selectedAsset = selectedMedia.map({$0.asset})
//        present(picker, animated: true, completion: nil)
//
//
//    }
    func openGallery()
    {
        let viewController = CustomPhotoPickerViewController()
        viewController.delegate = self
        
        viewController.didExceedMaximumNumberOfSelection = { [weak self] (picker) in
            
        }
        self.selectedAssets.removeAll()
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        configure.selectedColor = UIColor.appBaseLight
        configure.maxSelectedAssets = 5
        configure.maxVideoDuration = 60.0
        configure.allowedAlbumCloudShared = true
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        viewController.logDelegate = self
        DispatchQueue.main.async {
            self.present(viewController, animated: true, completion: nil)
        }
        
        
        
    }
     //    MARK:- Open Camera
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let picker =  UIImagePickerController()
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.delegate = self
            picker.allowsEditing = true
            picker.videoMaximumDuration = 30.0
            picker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
            self.present(picker, animated: true) {
                
            }
        }
        
    }
    
    
     //    MARK:- Show FullPreview
    func showPreview()
    {
        if previewMedia != nil
        {
            txtViewDescription.text = previewMedia.description
            if previewMedia.type == .Image
            {
                if self.playerController != nil
                {
                    self.playerController.player?.pause()
                    self.playerController.player = nil
                    self.playerController.view.removeFromSuperview()
                }
                self.previewImageView.image = previewMedia.tlAsset.fullResolutionImage
//                let options = PHImageRequestOptions()
//                options.deliveryMode = .fastFormat
//                options.resizeMode = .fast
//                PHImageManager.default().requestImage(for: previewMedia.asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: options) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
//                    if let image = image {
//                        
//                        self.previewImageView.image = image
//                    }
//                }
            }
            else
            {
                self.playVideo()
            }
        }
        else
        {
            self.previewImageView.image = #imageLiteral(resourceName: "preview_image")
            if self.playerController != nil
            {
                self.playerController.player?.pause()
                self.playerController.player = nil
                self.playerController.view.removeFromSuperview()
            }
        }
        
    }
    
    func playVideo() {
        guard (previewMedia.asset.mediaType == PHAssetMediaType.video)
            
            else {
                print("Not a valid video media type")
                return
        }
        PHCachingImageManager().requestAVAsset(forVideo: previewMedia.asset, options: nil) { [unowned self](asset, audiomix, info) in
            let asset = asset as! AVURLAsset
            DispatchQueue.main.async {
                let player = AVPlayer(url: asset.url)
                if self.playerController == nil
                {
                    self.playerController = AVPlayerViewController()
                }
                else
                {
                       self.playerController.player?.pause()
                       self.playerController.player = nil
                        self.playerController.view.removeFromSuperview()
    
                }
                self.playerController.player = player
                self.playerController.showsPlaybackControls = true
                self.playerController.view.frame = self.previewImageView.bounds
                self.previewImageView.addSubview(self.playerController.view)

               
            }
        }

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
        else
        {
            if selectedMedia.count > 0
            {
                return  super.tableView(tableView, heightForRowAt: indexPath)
            }
            else
            {
                return 0
            }
        }
    }
}

extension SDAddPhotos_VideosViewController:UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SDImagePickerDelegate,PreviewCellProtocol,UITextViewDelegate,TLPhotosPickerViewControllerDelegate,TLPhotosPickerLogDelegate
{
    func selectedCameraCell(picker: TLPhotosPickerViewController) {
        
    }
    
    func deselectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        
    }
    
    func selectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        
    }
    
    func selectedAlbum(picker: TLPhotosPickerViewController, title: String, at: Int) {
        
    }
    
    func didSelectDelete(at index: Int) {
        self.selectedMedia.remove(at: index)
        if selectedMedia.count>0
        {
            self.previewMedia = self.selectedMedia.first
            self.showPreview()
        }
        self.collectionViewPreview.reloadData()
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if self.previewMedia != nil
        {
        self.previewMedia.description = textView.text
        }
        return true
    }
    
     //    MARK: CustomPicker Delegate
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        self.selectedAssets = withTLPHAssets
        self.selectedAssets.forEach { (asset) in
           
             self.selectedMedia.append(SDMedia(asset: asset))
        }
        if let asset =  self.selectedAssets.first
        {
//            self.previewAsset = asset
//            self.setPreview()
            self.previewMedia = self.selectedMedia.first
            self.collectionViewPreview.reloadData()
            self.tableView.reloadData()
            self.showPreview()
            self.viewWillAppear(true)
        }
        self.collectionViewPreview.reloadData()
    }
    func customPicker(customPicker: CustomPickerViewController, didSelectMedia media: [PHAsset]) {

        for asset in media
        {
             self.selectedMedia.append(SDMedia(asset: asset))
        }
        customPicker.navigationController?.dismiss(animated: true, completion: {
          
        })
        self.previewMedia = self.selectedMedia.first
        self.collectionViewPreview.reloadData()
        self.tableView.reloadData()
        self.showPreview()
        self.viewWillAppear(true)
    }
    
    func customPickerDidCancel(customPicker: CustomPickerViewController) {
        customPicker.navigationController?.dismiss(animated: true, completion: {
            self.viewWillAppear(true)
        })
    }
    
   
    
    //    MARK: UIImagePicker Delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if info[UIImagePickerController.InfoKey.mediaType] as? String == kUTTypeImage as String
        {
            if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            {
                
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
            else
            {
                
                
            }
            
        }
        else
        {
            let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum((videoUrl?.path)!){
                UISaveVideoAtPathToSavedPhotosAlbum("\(videoUrl!.path)", self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
     //    MARK:Saving Delegate
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            fetchLast()
        }
    }
    
    func fetchLast()
    {
        let fetchOptions=PHFetchOptions()
        fetchOptions.sortDescriptors=[NSSortDescriptor(key:"creationDate", ascending: false)]
        fetchOptions.fetchLimit = 1
       let fetchResult =  PHAsset.fetchAssets(with: fetchOptions)
        selectedMedia.append(SDMedia(asset: fetchResult.lastObject!))
        self.previewMedia = self.selectedMedia.first
        self.collectionViewPreview.reloadData()
        self.tableView.reloadData()
        self.viewWillAppear(true)
        self.showPreview()
    
       
    }
       
    
    
     //    MARK: UICollectionView Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedMedia.count > 0 ?selectedMedia.count  : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if selectedMedia.count > 0
        {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewCell", for: indexPath) as! SDPreviewCollectionViewCell
            cell.delegate = self
            cell.setPreview(asset: selectedMedia[indexPath.row].asset)
            if selectedMedia[indexPath.row] === previewMedia
            {
                cell.setSelection()
            }
            cell.closeButton.tag = indexPath.row
            return cell
            
            
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nilCell", for: indexPath)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedMedia.count > 0
        {
        previewMedia = selectedMedia[indexPath.row]
        self.collectionViewPreview.reloadData()
        self.showPreview()
        }
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        
         return CGSize(width:height, height: height)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionViewPreview.collectionViewLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    private func checkPostProgress()
    {
        ApiService.sharedInstance.progressHandler = {(isRunning,progress,error) in
            if isRunning
            {
                self.viewProgress.frame = CGRect(x: 0, y: 0, width: self.viewProgress.frame.width, height: 60)
                self.progressBar.setProgress(Float(progress!.fractionCompleted), animated: true)
                self.progressStatus.text = progress!.fractionCompleted > 0.80 ? "Almost Finished" : "Previous Upload in progress..."
                if error != nil
                {
                    self.progressBar.progressTintColor = UIColor.red
                    self.progressStatus.text = "Error Occured While Uploading!"
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        self.viewProgress.frame = CGRect(x: 0, y: 0, width: self.viewProgress.frame.width, height: 0)
                        
                        self.view.layoutSubviews()
                    })
                }
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                self.view.layoutSubviews()
                
            }
            else
            {
                self.viewProgress.frame = CGRect(x: 0, y: 0, width: self.viewProgress.frame.width, height: 0)
                
                self.view.layoutSubviews()
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                //                self.tableViewFeeds.reloadInputViews()
            }
            
        }
    }
}





