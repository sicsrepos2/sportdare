//
//  SDCommentsViewController.swift
//  Sportdare
//
//  Created by SICS on 28/01/19.
//  Copyright © 2019 Binoy T. All rights reserved.
//

import UIKit
struct HeightConstants {
    static let textViewDefaultHeight:CGFloat = 34.0
    static let newLineHeight:CGFloat = 17.0
    static let textViewContainerDefaultHeight:CGFloat = 50.0
    static let textViewMaximumHeight:CGFloat = 140.0
}
 protocol CommentsProtocol
{
      func didAddedComment()
}
extension CommentsProtocol{
    func didAddedComment(){
    
    }
}
class SDCommentsViewController: BaseViewController {
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet var tableView:UITableView!
    @IBOutlet var ContainerTextView: UIView!
    @IBOutlet var textViewComment: UITextView!
     var isKeyBoardHidden:Bool = true
     var keyBoardSize:CGSize!
    var postId :String?
    var comments : [Comment]?
    var commentProtocol :CommentsProtocol?
    @IBOutlet weak var labelComments: UILabel!
    var post : Posts?
    var feed : Feed?
    var postView = UIView()
    var isFeed = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapOnTableView))
        self.tableView.addGestureRecognizer(tapGesture)
        tableView.estimatedRowHeight = 137.0
        tableView.rowHeight = UITableView.automaticDimension
        comments = [Comment]()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         getComments()
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
         textViewComment.translatesAutoresizingMaskIntoConstraints = true
         self.tableView.translatesAutoresizingMaskIntoConstraints = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    private func getComments()
    {
        ApiService.getCommentsFor(Id: post?.post_id! ?? feed!.referance_id!) { (status, comments, error) in
            if status && comments != nil
            {
                self.comments = comments
                self.tableView.reloadData()
                self.tableView.scrollToBottom(animated: false, section: 1)
                
            }
            else if error != nil
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
            
        }
    }
    
    private func postComment(comment:String)
    {
        let param = ["type":"post","id":post?.post_id! ?? feed!.referance_id!,"comment":comment]
        ApiService.postComment(param: param as [String : Any]) { (status, result, error) in
            if status
            {
                self.getComments()
                self.commentProtocol?.didAddedComment()
            }
            
        }
    }
    @objc func tapOnTableView(){
        textViewComment.resignFirstResponder()
//        self.view.endEditing(true)
    }
    @IBAction func dismissView(_ sender: UIButton) {
        self.textViewComment.resignFirstResponder()
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func highFiveAction(_ sender: UIButton) {
        if post != nil
        {
            post!.is_clapped = !post!.is_clapped!
            highFive()
            if  post!.is_clapped!
            {
                post!.post_claps_count = (post!.post_claps_count)! + 1
            }
            else
            {
                
                
                post!.post_claps_count = (post!.post_claps_count)! - 1
            }
        }
        else
        {
            feed?.is_clapped = !(feed?.is_clapped)!
            highFive()
        }
        
    }
    func highFive()
    {
        if isFeed{
            ApiService.postHighFiveFeed(param: ["type":post?.post_type! ?? feed!.activity_type!,"id":post?.post_id! ?? feed!.referance_id!]) { (status, result, error) in
                if result != nil
                {
                    
                    
                }
            }
        }
        else
        {
            ApiService.postHighFive_Wall(param: ["type":post?.post_type! ?? feed!.activity_type!,"id":post?.post_id! ?? feed!.referance_id!]) { (status, result, error) in
                if result != nil
                {
                    
                    
                }
            }
        }
    }
    //MARK: KEYBOARD NOTIFICATIONS
    @objc func keyboardWillAppear(notification: NSNotification){
        
        isKeyBoardHidden = false
        textViewComment.enablesReturnKeyAutomatically = true
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize:CGSize = (userInfo.object(forKey: UIResponder.keyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue.size
        self.keyBoardSize = keyboardSize
   
        var messageFrame:CGRect = self.ContainerTextView.frame
        messageFrame.origin.y -= keyboardSize.height-self.view.safeAreaInsets.bottom
        self.ContainerTextView.frame = messageFrame

        self.tableView.frame = CGRect(x: 0,y: self.tableView.frame.origin.y,width : self.view.bounds.width,height : self.view.bounds.height-(self.ContainerTextView.frame.height + keyboardSize.height)-self.tableView.frame.origin.y)
        self.tableView.scrollToBottom(animated: false, section: 1)
     
       

    }
    
    @objc func keyboardWillDisappear(notification: NSNotification){
        isKeyBoardHidden = true
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize:CGSize = (userInfo.object(forKey:UIResponder.keyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue.size
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.25)
        self.tableView.contentInset = UIEdgeInsets.zero
        UIView.commitAnimations()
        
        self.tableView.scrollIndicatorInsets = UIEdgeInsets.zero
        
        var messageFrame:CGRect = self.ContainerTextView.frame
        messageFrame.origin.y += keyboardSize.height+self.view.safeAreaInsets.bottom
        self.ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,y:self.view.bounds.height-(messageFrame.height+self.view.safeAreaInsets.bottom),width : messageFrame.width,height : HeightConstants.textViewContainerDefaultHeight )
        self.tableView.frame = CGRect(x: 0,y: self.tableView.frame.origin.y,width : UIScreen.main.bounds.width,height :  self.view.bounds.height-(self.ContainerTextView.frame.height+self.view.safeAreaInsets.bottom+self.tableView.frame.origin.y))
        self.tableView.scrollToBottom(animated: true, section: 1)
        
       
    }
    @IBAction func sendAction(){
        if(self.textViewComment.text.count > 0){
            var keyboardHeightValue:CGFloat = 0.0
            if(isKeyBoardHidden){
                keyboardHeightValue = 0.0
            }else{
                keyboardHeightValue = keyBoardSize.height
            }
            self.postComment(comment: self.textViewComment.text)
            self.textViewComment.text = ""
            
            let wid = ContainerTextView.frame.width
            let widthTextView = textViewComment.frame.width
            ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,y: self.view.bounds.height-(50+keyboardHeightValue+self.view.safeAreaInsets.bottom),width : wid,height : 50 )
            textViewComment.frame = CGRect(x:textViewComment.frame.minX,y: textViewComment.frame.minY,width : widthTextView,height : 34 )
//            self.tableView.frame = CGRect(x: 0,y: 0,width : UIScreen.main.bounds.width,height :  UIScreen.main.bounds.height-(50+keyboardHeightValue+self.view.safeAreaInsets.bottom))
        
        }
    }

}
extension SDCommentsViewController:UITableViewDelegate,UITableViewDataSource,UITextViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? isFeed ? feed != nil ? 1 :0 :post != nil ? 1 : 0 : comments!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            if !isFeed
            {
                if post!.post_type == PostType.text
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! SDMy_Timeline_TableViewCell
                    cell.post = post
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "postMediaCell") as! SDMy_Timeline_TableViewCell
                    cell.post = post
                    return cell
                }
            }
            else
            {
                return Utilities.getCellFor(tableView: tableView,feed:feed!, indexPath: indexPath)!
            }
        }
        else
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChattViewCellId.commentTextCell) as! SDChatt_CommentTableViewCell
        cell.comment = self.comments![indexPath.row]
        return cell
        }
        
            
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let textView = (cell as? SDFeedTableViewCell)?.textViewDescription
        {
            textView.onSizeChange = { [unowned tableView, unowned self] r in

               tableView.beginUpdates()
            tableView.endUpdates()
                
            }
            
        }
        else if let textView = (cell as? SDMy_Timeline_TableViewCell)?.textViewDescription
        {
            textView.onSizeChange = { [unowned tableView, unowned self] r in

                tableView.beginUpdates()
                tableView.endUpdates()
                
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newLineHeight:CGFloat = 0.0
        if (text == "\n"){
            newLineHeight = HeightConstants.newLineHeight
            if(textView.text.isEmpty){
                return false
            }
        }
    
        let wid = ContainerTextView.frame.width
        let widthTextView = textView.frame.width
        textView.sizeToFit()
        let textViewHeight:CGFloat = textView.contentSize.height
        
        if textViewHeight > HeightConstants.textViewDefaultHeight {
            
            textView.frame = CGRect(x:textView.frame.minX,y: textView.frame.minY,width : widthTextView,height :  min(textViewHeight, HeightConstants.textViewMaximumHeight)+newLineHeight )
            ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,
                                             y:  self.view.bounds.height - (keyBoardSize.height + HeightConstants.textViewContainerDefaultHeight + newLineHeight  + (min(textViewHeight, HeightConstants.textViewMaximumHeight) - HeightConstants.textViewDefaultHeight)),
                                             width : wid,
                                             height : HeightConstants.textViewContainerDefaultHeight + ( min(textViewHeight, HeightConstants.textViewMaximumHeight) - HeightConstants.textViewDefaultHeight) + newLineHeight )
            
        }else{
            
            ContainerTextView.frame = CGRect(x:ContainerTextView.frame.minX,
                                             y:  self.view.bounds.height - (keyBoardSize.height + HeightConstants.textViewContainerDefaultHeight + newLineHeight) ,
                                             width : wid,
                                             height : HeightConstants.textViewContainerDefaultHeight + newLineHeight )
            
            textView.frame = CGRect(x:textView.frame.minX,y: textView.frame.minY,width : widthTextView,height : HeightConstants.textViewDefaultHeight+newLineHeight )
            
        }
        
      
    
        return true
    }
    
    
}
