//
//  SDMessagesTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 14/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDMessagesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var tableViewMessages: UITableView!
    var chatList : [ChatList]!
    var filteredList : [ChatList]!
    override func viewDidLoad() {
        super.viewDidLoad()
        chatList = [ChatList]()
       
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.getChatList()
    }
    func getChatList()
    {
        ApiService.getChatList { (status, list, error) in
            self.chatList.removeAll()
            if list != nil
            {
                self.chatList.append(contentsOf: list!)
                self.filteredList = self.chatList
                self.tableViewMessages.reloadData()
            }
            else if error != nil
            {
                KRProgressHUD.showError(withMessage: error?.localizedDescription)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        filteredList = self.chatList.filter({($0.first_name?.contains(str))! || ($0.surname?.contains(str))! })
        self.tableViewMessages.reloadData()
        
        return true
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMessage") as! SDSearchUserTableViewCell
        cell.chat = filteredList[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chat = chatList[indexPath.row]
        let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDChattViewController") as! SDChattViewController
        self.hideTabBar()
        chatVC.chatDetail = chat
        chatVC.showBackButton = true
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
}
