//
//  SDNotificationsTableViewController.swift
//  SportDare
//
//  Created by Binoy T on 20/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDNotificationsTableViewController: UITableViewController {
    var notifications :[Notifications]?
    override func viewDidLoad() {
        super.viewDidLoad()
         getNotifications()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
func getNotifications()
{
    ApiService.getNotifications { (status, notifications, error) in
        if status
        {
            if let notificationList = notifications as? [Notifications]
            {
                self.notifications = notificationList
                self.tableView.reloadData()
            }
            
        }
    }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notifications?.count ?? 0
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNotify") as! SDNoticicationTableViewCell

        cell.notification = notifications![indexPath.row]

        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
