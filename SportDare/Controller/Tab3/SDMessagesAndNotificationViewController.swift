//
//  SDMEssagesAndNotificationViewController.swift
//  SportDare
//
//  Created by Binoy T on 20/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit

class SDMessagesAndNotificationViewController: BaseViewController {
    @IBOutlet weak var btnMessages: SDMenuButtonView!
    @IBOutlet weak var btnNotifications: SDMenuButtonView!
    
    @IBOutlet weak var containerView: UIView!
    var messagesVC : SDMessagesViewController?
    var notificationVc : SDNotificationsTableViewController?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadViewControllers()
         btnNotifications.isSelected = true
        self.displayContentController(content: notificationVc!)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    @IBAction func didSelectMessages(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnNotifications.isSelected = false
        self.removeChildVC()
        displayContentController(content: messagesVC!)
       
    }
    @IBAction func didSelectNotifications(_ sender: SDMenuButtonView) {
        sender.isSelected = true
        btnMessages.isSelected = false
        self.removeChildVC()
        displayContentController(content: notificationVc!)
      
    }
    func loadViewControllers()
    {
        messagesVC = self.storyboard?.instantiateViewController(withIdentifier: "SDMessagesViewController") as? SDMessagesViewController
        notificationVc = self.storyboard?.instantiateViewController(withIdentifier: "SDNotificationsTableViewController") as? SDNotificationsTableViewController
    }
    func displayContentController(content: UIViewController) {
        addChild(content)
        content.view.frame = CGRect(x: 0, y: 0, width: containerView.frame.width, height: containerView.frame.height)
        self.containerView.addSubview(content.view)
        containerView.layoutIfNeeded()
        content.didMove(toParent: self)
    }
    
    func removeChildVC() {
        if let content = self.children.first
        {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
