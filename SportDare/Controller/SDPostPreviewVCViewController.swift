//
//  SDPostPreviewVCViewController.swift
//  Sportdare
//
//  Created by Binoy T on 26/12/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import AVKit
import BonsaiController

protocol PreviewVCProtocol {
    func didDeletedMedia_at(index:Int)
    func didUpdatePost(post:Posts)
    func didUpdateFeed(feed:Feed)
}

extension PreviewVCProtocol{
    func didUpdatePost(post:Posts)
    {
        
    }
    func didUpdateFeed(feed:Feed){
        
        
    }
    
}
class SDPostPreviewVCViewController: BaseViewController, PreviewProtocol,PopupViewControllerDelegate {
    
    @IBOutlet weak var buttonComments: UIButton!
    @IBOutlet weak var accessoryView: UIView!
    @IBOutlet weak var labelHighFive: UILabel!
    @IBOutlet weak var labelComments: UILabel!
    //    var accessoryView: UIView!
    @IBOutlet weak var btnHighFive: UIButton!
    
    var mediaArray :[Any]?
    var previewDelegate : PreviewVCProtocol?
    var passedContentOffset = IndexPath()
    var playerViewController: AVPlayerViewController!
    var btnMore : UIButton?
    var isPlayerFeed = Bool()
    var isRolesMedia = Bool()
    var myCollectionView: UICollectionView!

    var post :Posts?
    var feed : Feed?
    
    override func viewDidLoad() {
        self.showSearch = false
        self.showBackButton = true
         super.viewDidLoad()
        self.view.backgroundColor=UIColor.black
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0 , left: 0, bottom: accessoryView.frame.height, right: 0)
        layout.minimumInteritemSpacing=3
        layout.minimumLineSpacing=0
        layout.scrollDirection = .horizontal
        myCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        myCollectionView.delegate=self
        myCollectionView.dataSource=self
        myCollectionView.register(ImagePreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        myCollectionView.isPagingEnabled = true
        self.view.addSubview(myCollectionView)
        myCollectionView.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
       self.navigationController?.navigationBar.isTranslucent = false
        if !isPlayerFeed
        {
            btnMore = UIButton()
            btnMore?.setImage(UIImage(named: "more"), for: .normal)
            btnMore?.addTarget(self, action: #selector(self.moreOptions(_:)), for: .touchUpInside)
            let moreBtn = UIBarButtonItem(customView: btnMore!)
            self.navigationItem.setRightBarButton(moreBtn, animated: true)
            if isRolesMedia
            {
              
                accessoryView.isHidden = true
            }
            else
            {
                  btnHighFive.isSelected = (post?.is_clapped)!
            }
            
        }
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.view.setNeedsLayout() // force update layout
        navigationController?.view.layoutIfNeeded() // to
        view.layoutIfNeeded()
        myCollectionView.scrollToItem(at: passedContentOffset, at: .right, animated: false)
        self.view.bringSubviewToFront(accessoryView)
        
        if post != nil
        {
            self.labelHighFive.text = "\(post!.post_claps_count!)"
            self.labelComments.text = "\(post!.post_comments_count!) comments"
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayout = myCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        if isRolesMedia
        {
            flowLayout.sectionInset = UIEdgeInsets(top: 0 , left: 0, bottom:  0, right: 0)
            flowLayout.itemSize = CGSize(width: myCollectionView.frame.size.width, height: myCollectionView.frame.size.height )
//            flowLayout.invalidateLayout()
            
        }
        else
        {
        flowLayout.sectionInset = UIEdgeInsets(top: 0 , left: 0, bottom:  accessoryView.frame.height, right: 0)
        flowLayout.itemSize = CGSize(width: myCollectionView.frame.size.width, height: myCollectionView.frame.size.height - accessoryView.frame.height)
//        flowLayout.invalidateLayout()
        }
        myCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        
    }
    override func viewDidAppear(_ animated: Bool) {
      
    }
    @objc func moreOptions(_ sender:UIButton)
    {
       let alertOptions = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete Photo", style: .destructive) { (action) in
            self.removeMedia()
           
        }
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertOptions.addAction(deleteAction)
         alertOptions.addAction(cancelAction)
        self.present(alertOptions, animated: true) {
            
        }
        
    }
    
    @IBAction func commentAction(_ sender: UIButton) {
       let commentsVc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDCommentsViewController") as! SDCommentsViewController
        commentsVc.commentProtocol = self
        commentsVc.postId = self.post?.post_id
        commentsVc.modalTransitionStyle = .coverVertical
        commentsVc.modalPresentationStyle = .custom
        commentsVc.transitioningDelegate = self
        self.present(commentsVc, animated: true) {
            
        }
    }
    
    @IBAction func highFiveAction(_ sender: UIButton) {
        post!.is_clapped = !post!.is_clapped!
        btnHighFive.isSelected = post!.is_clapped!
        highFive()
        if  post!.is_clapped!
        {
            post!.post_claps_count = (post!.post_claps_count)! + 1
        }
        else
        {
            
            
            post!.post_claps_count = (post!.post_claps_count)! - 1
        }
        self.viewWillAppear(true)
   
    }
    
    func highFive()
    {
        ApiService.postHighFiveFeed(param: ["type":post!.post_type!,"id":post?.post_id! as Any]) { (status, result, error) in
            if result != nil
            {
               
                self.previewDelegate?.didUpdatePost(post: self.post!)
            }
        }
    }
    
    func getPostDetail()
    {
        ApiService.viewMyPost(postId: (post?.post_id!)!) { (status, post, error) in
            if post != nil
            {
                self.post = post
                self.previewDelegate?.didUpdatePost(post: post!)
            }
        }
    }
    func removeMedia()
    {
        let mediaIndex = self.passedContentOffset.row
        if let media = self.mediaArray![mediaIndex] as? Media
        {
            ApiService.removeMedia(mediaId: (media.am_id)!, completion: { (status, result) in
                if status
                {
                    self.mediaArray?.remove(at: mediaIndex)
                    if !self.isRolesMedia
                    {
                    self.post?.post_media?.remove(at: mediaIndex)
                    self.previewDelegate?.didUpdatePost(post: self.post!)
                    }
                    self.myCollectionView.reloadData()
                    self.previewDelegate?.didDeletedMedia_at(index: mediaIndex)
                }
            })
        }
    }
     }
extension SDPostPreviewVCViewController:BonsaiControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,CommentsProtocol{
        
    func didAddedComment() {
        self.post!.post_comments_count =  self.post!.post_comments_count! + 1
         self.getPostDetail()
        self.viewWillAppear(true)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (mediaArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImagePreviewFullViewCell
        cell.imgView.image =  nil
        cell.delegate = self
          if let post = mediaArray?[indexPath.row] as? Post_media
          {
          cell.post = post
          }
        else if let media = mediaArray?[indexPath.row] as? Media
          {
            cell.media = media
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let mediaCell =  cell as? ImagePreviewFullViewCell
        {
            mediaCell.pauseVideo()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        passedContentOffset =  self.myCollectionView.indexPath(for:self.myCollectionView.visibleCells.first!)!
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let offset = myCollectionView.contentOffset
        let width  = myCollectionView.bounds.size.width
        
        let index = round(offset.x / width)
        let newOffset = CGPoint(x: index * size.width, y: offset.y)
        
        myCollectionView.setContentOffset(newOffset, animated: false)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.myCollectionView.reloadData()
            
            self.myCollectionView.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    func didBeginZooming() {
     
        accessoryView.hideWithAnimation()
        self.viewDidLayoutSubviews()
    }
    func didEndZooming() {
        if !isRolesMedia
        {
            accessoryView.showWithAnimation()
            self.viewDidLayoutSubviews()
        }
        
    }
    
    //    MARK:- Popup List Controller
    func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: 0, y: 70), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height-70))
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return BonsaiController(fromDirection: .bottom, blurEffectStyle: .light, presentedViewController: presented, delegate: self)
    }
}
