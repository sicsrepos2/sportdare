//
//  SDSettingsTableViewController.swift
//  Sportdare
//
//  Created by Binoy T on 26/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import MessageUI
class Section{
    var isOpen = Bool()
    var tag : Int?
    init(isOpen:Bool, tag:Int) {
        self.isOpen = isOpen
        self.tag = tag
    }
}
class SDSettingsTableViewController: BaseTableViewController,MFMailComposeViewControllerDelegate {
var selectedSection = 0
    var sections : [Section]!
    @IBOutlet weak var labelVersion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "settingsBG"))
        sections = [Section(isOpen: false, tag: 1),Section(isOpen: false, tag: 2)]
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print(text)
            labelVersion.text = "Vesrion:\(text)"
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func goToSportDare(_ sender: UIButton) {

            if UIApplication.shared.canOpenURL(Constants.sportDareUrl)
            {
                UIApplication.shared.open(Constants.sportDareUrl, options: [:], completionHandler: nil)
            }
        
    }
    @IBAction func expandAction(_ sender: UIButton) {
        let section = sections[sender.tag - 1]
        section.isOpen = !section.isOpen
        self.tableView.reloadSections(IndexSet(integer: sender.tag), with: .none)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section > 0 && section < 3
        {
            let sect = sections[section - 1]
            if sect.isOpen
            {
                return super.tableView(tableView, numberOfRowsInSection: section)
            }
            return 1
        }
        return 1
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 && indexPath.row  == 0
        {
            self.showAlertWith(title: "Log Out", message: "Are you sure you want to logout?", CancelBtnTitle: "No", OtherBtnTitle: "Yes") { (index, title) in
                if index == 1
                {
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.logOut_User()
                }
            }
            
        }
        if indexPath.section == 1
        {
            if indexPath.row == 1
            {
            let tutVC = self.storyboard?.instantiateViewController(withIdentifier: "SDTutorialViewController") as! SDTutorialViewController
            tutVC.isFromSetting = true
            self.present(tutVC, animated: true, completion: {
                
            })
            }
            else if indexPath.row == 2
            {
            
                let faqVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_FAQ_ViewController") as! SD_FAQ_ViewController
                faqVC.fileUrl = Bundle.main.url(forResource: "FAQ", withExtension: "pdf")
                self.present(faqVC, animated: true, completion: {
                    
                })
            }
            else if indexPath.row == 3
            {
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["info@sportdare.com"])
//                    mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                    
                    present(mail, animated: true)
                } else {
                    // show failure alert
                }
            }
        }
        else if indexPath.section == 2
        {
            if indexPath.row == 3
            {
                let faqVC = self.storyboard?.instantiateViewController(withIdentifier: "SD_FAQ_ViewController") as! SD_FAQ_ViewController
                faqVC.fileUrl = Bundle.main.url(forResource: "SPORTDARE_TC", withExtension: "pdf")
                self.present(faqVC, animated: true, completion: {
                    
                })
            }
        }
    }
}
