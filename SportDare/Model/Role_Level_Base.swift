//  Sportdare
//
//  Created by Binoy T on 24/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct Role_Level_Base : Codable {
	let status : Bool?
	let message : String?
	let atheletLevel : [AthleteLevel]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case atheletLevel = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		atheletLevel = try values.decodeIfPresent([AthleteLevel].self, forKey: .atheletLevel)
	}

}

struct AthleteLevel : Codable {
    let id : String?
    let sub_cat_id : String?
    let level_id : String?
    let stars : String?
    let verified_count : String?
    let is_verified : Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case sub_cat_id = "sub_cat_id"
        case level_id = "level_id"
        case stars = "stars"
        case verified_count = "verified_count"
        case is_verified = "is_verified"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        sub_cat_id = try values.decodeIfPresent(String.self, forKey: .sub_cat_id)
        level_id = try values.decodeIfPresent(String.self, forKey: .level_id)
        stars = try values.decodeIfPresent(String.self, forKey: .stars)
        verified_count = try values.decodeIfPresent(String.self, forKey: .verified_count)
        is_verified = try values.decodeIfPresent(Bool.self, forKey: .is_verified)
        
    }
    
}
