

import Foundation
struct Cards_Base : Codable {
	let status : Bool?
	let message : String?
	let card_details : [Card_details]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case card_details = "card_details"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		card_details = try values.decodeIfPresent([Card_details].self, forKey: .card_details)
	}

}
struct Card_details : Codable {
    let card_id : String?
    let card_name : String?
    let card_image : String?
    let card_amount : String?
    let cards_count : String?
    
    enum CodingKeys: String, CodingKey {
        
        case card_id = "card_id"
        case card_name = "card_name"
        case card_image = "card_image"
        case card_amount = "card_amount"
        case cards_count = "cards_count"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        card_id = try values.decodeIfPresent(String.self, forKey: .card_id)
        card_name = try values.decodeIfPresent(String.self, forKey: .card_name)
        card_image = try values.decodeIfPresent(String.self, forKey: .card_image)
        card_amount = try values.decodeIfPresent(String.self, forKey: .card_amount)
        cards_count = try values.decodeIfPresent(String.self, forKey: .cards_count)
    }
    
}
