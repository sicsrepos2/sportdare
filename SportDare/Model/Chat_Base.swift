

import Foundation
struct Chat_Base : Codable {
	let status : Bool?
	let message : String?
	let chats : [Chat]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case chats = "posts"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		chats = try values.decodeIfPresent([Chat].self, forKey: .chats)
	}

}

struct Chat : Codable {
    let id : String?
    let is_iam_sender : Bool?
    let sender : Sender?
    let reciver : Reciver?
    let message : String?
    let timestamp : String?
    let type : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case is_iam_sender = "is_iam_sender"
        case sender = "sender"
        case reciver = "reciver"
        case message = "message"
        case timestamp = "timestamp"
        case type = "type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        is_iam_sender = try values.decodeIfPresent(Bool.self, forKey: .is_iam_sender)
        sender = try values.decodeIfPresent(Sender.self, forKey: .sender)
        reciver = try values.decodeIfPresent(Reciver.self, forKey: .reciver)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
    
}
