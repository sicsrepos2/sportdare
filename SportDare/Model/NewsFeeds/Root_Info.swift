

import Foundation
struct Root_Info : Codable {
	let message : String?
	let rooted_who : Rooted_who?
	let rooted_whom : Rooted_whom?

	enum CodingKeys: String, CodingKey {

		case message = "message"
		case rooted_who = "rooted_who"
		case rooted_whom = "rooted_whom"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		rooted_who = try values.decodeIfPresent(Rooted_who.self, forKey: .rooted_who)
		rooted_whom = try values.decodeIfPresent(Rooted_whom.self, forKey: .rooted_whom)
	}

}
