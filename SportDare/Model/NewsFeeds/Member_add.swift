

import Foundation
struct Member_add : Codable {
	let id : String?
	let name : String?
	let image : String?
	let type : String?
	let added_on : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case image = "image"
		case type = "type"
		case added_on = "added_on"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		added_on = try values.decodeIfPresent(String.self, forKey: .added_on)
	}

}
