

import Foundation
struct User : Codable {
	let userData : UserData?
//    let profile_update : [String]?
//    let profile_pic_update : [String]?
//    let gambits : [String]?

	enum CodingKeys: String, CodingKey {

		case userData = "data"
//        case profile_update = "profile_update"
//        case profile_pic_update = "profile_pic_update"
//        case gambits = "gambits"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userData = try values.decodeIfPresent(UserData.self, forKey: .userData)
//        profile_update = try values.decodeIfPresent([String].self, forKey: .profile_update)
//        profile_pic_update = try values.decodeIfPresent([String].self, forKey: .profile_pic_update)
//        gambits = try values.decodeIfPresent([String].self, forKey: .gambits)
	}

}
