

import Foundation
struct Feeds_Base : Codable {
	let status : Bool?
	let message : String?
	let feed : [Feed]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case feed = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		feed = try values.decodeIfPresent([Feed].self, forKey: .feed)
	}

}
