

import Foundation
class Feed : Codable {
	let activity_id : String?
	let activity_type : String?
	let referance_id : String?
	let referance_text : String?
	let post : Posts?
	let root : Root_Info?
	let user : User?
	let dare : Dare?
	let team : UserTeam?
	let role : Role?
    var claps_count : Int?
    var comments_count : Int?
    var is_clapped : Bool?
    var time : String?

	enum CodingKeys: String, CodingKey {

		case activity_id = "activity_id"
		case activity_type = "activity_type"
		case referance_id = "referance_id"
		case referance_text = "referance_text"
		case post = "post"
		case root = "root"
		case user = "user"
		case dare = "dare"
		case team = "team"
		case role = "role"
        case claps_count = "claps_count"
        case comments_count = "comments_count"
        case is_clapped = "is_clapped"
        case time = "time"
	}

    required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		activity_id = try values.decodeIfPresent(String.self, forKey: .activity_id)
		activity_type = try values.decodeIfPresent(String.self, forKey: .activity_type)
		referance_id = try values.decodeIfPresent(String.self, forKey: .referance_id)
		referance_text = try values.decodeIfPresent(String.self, forKey: .referance_text)
		post = try values.decodeIfPresent(Posts.self, forKey: .post)
		root = try values.decodeIfPresent(Root_Info.self, forKey: .root)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		dare = try values.decodeIfPresent(Dare.self, forKey: .dare)
		team = try values.decodeIfPresent(UserTeam.self, forKey: .team)
		role = try values.decodeIfPresent(Role.self, forKey: .role)
        claps_count = try values.decodeIfPresent(Int.self, forKey: .claps_count)
        comments_count = try values.decodeIfPresent(Int.self, forKey: .comments_count)
        is_clapped = try values.decodeIfPresent(Bool.self, forKey: .is_clapped)
        time = try values.decodeIfPresent(String.self, forKey: .time)
	}

}
