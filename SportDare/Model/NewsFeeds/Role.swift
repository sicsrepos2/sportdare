

import Foundation
struct Role : Codable {
	let awards : Award?
	let level : Level?
	let media : [Media]?
	let personal_best : PersonalBest?

	enum CodingKeys: String, CodingKey {

		case awards = "awards"
		case level = "level"
		case media = "media"
		case personal_best = "personal_best"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		awards = try values.decodeIfPresent(Award.self, forKey: .awards)
		level = try values.decodeIfPresent(Level.self, forKey: .level)
		media = try values.decodeIfPresent([Media].self, forKey: .media)
		personal_best = try values.decodeIfPresent(PersonalBest.self, forKey: .personal_best)
	}

}
