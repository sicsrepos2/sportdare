 

import Foundation
struct Event : Codable {
	let title : String?
	let created_by : Created_by?
	let created_for : Created_for?
	let type : String?
	let location : String?
	let gambits : String?
	let date : String?
	let time : String?
	let privacy : String?
	let satus : String?
	let created_at : String?

	enum CodingKeys: String, CodingKey {

		case title = "title"
		case created_by = "created_by"
		case created_for = "created_for"
		case type = "type"
		case location = "location"
		case gambits = "gambits"
		case date = "date"
		case time = "time"
		case privacy = "privacy"
		case satus = "satus"
		case created_at = "created_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		created_by = try values.decodeIfPresent(Created_by.self, forKey: .created_by)
		created_for = try values.decodeIfPresent(Created_for.self, forKey: .created_for)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		location = try values.decodeIfPresent(String.self, forKey: .location)
		gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		time = try values.decodeIfPresent(String.self, forKey: .time)
		privacy = try values.decodeIfPresent(String.self, forKey: .privacy)
		satus = try values.decodeIfPresent(String.self, forKey: .satus)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
	}

}
