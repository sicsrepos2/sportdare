
import Foundation
struct Level : Codable {
	let id : String?
	let stars : String?
	let name : String?
	let sub_name : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case stars = "stars"
		case name = "name"
		case sub_name = "sub_name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		stars = try values.decodeIfPresent(String.self, forKey: .stars)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		sub_name = try values.decodeIfPresent(String.self, forKey: .sub_name)
	}

}
