

import Foundation
struct Dare : Codable {
	let challenge : Challenge?
	let training : Training?
    let event : Event?

	enum CodingKeys: String, CodingKey {

		case challenge = "dare"
		case training = "training"
		case event = "event"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		challenge = try values.decodeIfPresent(Challenge.self, forKey: .challenge)
		training = try values.decodeIfPresent(Training.self, forKey: .training)
		event = try values.decodeIfPresent(Event.self, forKey: .event)
	}

}
