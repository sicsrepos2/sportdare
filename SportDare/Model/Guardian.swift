
//  Sportdare
//
//  Created by Binoy T on 09/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//
import Foundation
struct Guardian : Codable {
	let status : Bool?
	let message : String?
	let details : [GuardianDetails]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case details = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		details = try values.decodeIfPresent([GuardianDetails].self, forKey: .details)
	}

}
struct GuardianDetails : Codable {
    let userid : String?
    let name : String?
    let surName  : String?
    enum CodingKeys: String, CodingKey {
        
        case userid = "userid"
        case name = "name"
        case surName = "surName"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userid = try values.decodeIfPresent(String.self, forKey: .userid)
        name = try values.decodeIfPresent(String.self, forKey: .name)
         surName = try values.decodeIfPresent(String.self, forKey: .surName)
    }
    
}
