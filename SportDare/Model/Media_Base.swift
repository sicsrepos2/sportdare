

import Foundation
struct Media_Base : Codable {
	let status : Bool?
	let message : String?
	let medias : [Media]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case medias = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		medias = try values.decodeIfPresent([Media].self, forKey: .medias)
	}

}
struct Media : Codable {
    let am_id : String?
    let am_category_ac_id : String?
    let am_user_id : String?
    let am_file : String?
    let am_added_on : String?
    let am_title : String?
    let am_file_type : String?
    
    enum CodingKeys: String, CodingKey {
        
        case am_id = "am_id"
        case am_category_ac_id = "am_category_ac_id"
        case am_user_id = "am_user_id"
        case am_file = "am_file"
        case am_added_on = "am_added_on"
        case am_title = "am_title"
        case am_file_type = "am_file_type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        am_id = try values.decodeIfPresent(String.self, forKey: .am_id)
        am_category_ac_id = try values.decodeIfPresent(String.self, forKey: .am_category_ac_id)
        am_user_id = try values.decodeIfPresent(String.self, forKey: .am_user_id)
        am_file = try values.decodeIfPresent(String.self, forKey: .am_file)
        am_added_on = try values.decodeIfPresent(String.self, forKey: .am_added_on)
        am_title = try values.decodeIfPresent(String.self, forKey: .am_title)
        am_file_type = try values.decodeIfPresent(String.self, forKey: .am_file_type)
    }
    
}
