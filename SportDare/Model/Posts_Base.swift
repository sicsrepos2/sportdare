


import Foundation

struct Posts_Base : Codable {
	let status : Bool?
	let message : String?
	let posts : [Posts]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case posts = "posts"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		posts = try values.decodeIfPresent([Posts].self, forKey: .posts)
	}

}

class Posts : Codable {
    let post_id : String?
    let post_type : String?
    let post_message : String?
    var is_clapped : Bool?
    let added_on : String?
    var post_media : [Post_media]?
    var post_comments_count : Int?
    var post_claps_count : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case post_id = "post_id"
        case post_type = "post_type"
        case post_message = "post_message"
        case is_clapped = "is_clapped"
        case added_on = "added_on"
        case post_media = "post_media"
        case post_comments_count = "post_comments_count"
        case post_claps_count = "post_claps_count"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        post_id = try values.decodeIfPresent(String.self, forKey: .post_id)
        post_type = try values.decodeIfPresent(String.self, forKey: .post_type)
        is_clapped = try values.decodeIfPresent(Bool.self, forKey: .is_clapped)
        post_message = try values.decodeIfPresent(String.self, forKey: .post_message)
        added_on = try values.decodeIfPresent(String.self, forKey: .added_on)
        post_media = try values.decodeIfPresent([Post_media].self, forKey: .post_media)
        post_comments_count = try values.decodeIfPresent(Int.self, forKey: .post_comments_count)
        post_claps_count = try values.decodeIfPresent(Int.self, forKey: .post_claps_count)
    }
    
}

struct Post_media : Codable {
    let media_id : String?
    let media_url : String?
    let media_message : String?
    let media_type : String?
    
    enum CodingKeys: String, CodingKey {
        
        case media_id = "media_id"
        case media_url = "media_url"
        case media_message = "media_message"
        case media_type = "media_type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        media_id = try values.decodeIfPresent(String.self, forKey: .media_id)
        media_url = try values.decodeIfPresent(String.self, forKey: .media_url)
        media_message = try values.decodeIfPresent(String.self, forKey: .media_message)
        media_type = try values.decodeIfPresent(String.self, forKey: .media_type)
    }
    
}
