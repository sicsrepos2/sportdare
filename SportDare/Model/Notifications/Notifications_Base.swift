

import Foundation
struct Notifications_Base : Codable {
	let status : Bool?
	let message : String?
	let notifications : [Notifications]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case notifications = "notifications"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		notifications = try values.decodeIfPresent([Notifications].self, forKey: .notifications)
	}

}
