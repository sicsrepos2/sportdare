

import Foundation
struct Notifications : Codable {
	let id : String?
	let message : String?
	let relation_id : String?
	let relation_user_data : Relation_user_data?
	let time : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case message = "message"
		case relation_id = "relation_id"
		case relation_user_data = "relation_user_data"
		case time = "time"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		relation_id = try values.decodeIfPresent(String.self, forKey: .relation_id)
		relation_user_data = try values.decodeIfPresent(Relation_user_data.self, forKey: .relation_user_data)
		time = try values.decodeIfPresent(String.self, forKey: .time)
	}

}
