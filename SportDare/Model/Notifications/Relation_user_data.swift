

import Foundation
struct Relation_user_data : Codable {
	let user_id : String?
	let first_name : String?
	let surname : String?
	let image : String?
	let gambits : String?
	let sheild : Sheild?

	enum CodingKeys: String, CodingKey {

		case user_id = "user_id"
		case first_name = "first_name"
		case surname = "surname"
		case image = "image"
		case gambits = "gambits"
		case sheild = "sheild"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		surname = try values.decodeIfPresent(String.self, forKey: .surname)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
		sheild = try values.decodeIfPresent(Sheild.self, forKey: .sheild)
	}

}
