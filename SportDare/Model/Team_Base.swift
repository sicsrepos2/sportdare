//  Sportdare
//
//  Created by Binoy T 
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct Team_Base : Codable {
	let status : Bool?
	let message : String?
	let teams : [Team]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case teams = "team_details"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		teams = try values.decodeIfPresent([Team].self, forKey: .teams)
	}

}
struct Team : Codable {
    let id : String?
    let user_id : String?
    let team_name : String?
    var team_members : [Team_members]?
    let team_image : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case team_name = "team_name"
        case team_members = "team_members"
        case team_image = "team_image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        team_name = try values.decodeIfPresent(String.self, forKey: .team_name)
        team_members = try values.decodeIfPresent([Team_members].self, forKey: .team_members)
        team_image = try values.decodeIfPresent(String.self, forKey: .team_image)
    }
    
}
struct Team_members : Codable {
    let tm_id : String?
    let tm_team_id : String?
    let tm_user_id : String?
    let tm_added_on : String?
    let u_name : String?
    let u_lastname : String?
    let u_profile_image : String?
    
    enum CodingKeys: String, CodingKey {
        
        case tm_id = "tm_id"
        case tm_team_id = "tm_team_id"
        case tm_user_id = "tm_user_id"
        case tm_added_on = "tm_added_on"
        case u_name = "u_name"
        case u_lastname = "u_lastname"
        case u_profile_image = "u_profile_image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        tm_id = try values.decodeIfPresent(String.self, forKey: .tm_id)
        tm_team_id = try values.decodeIfPresent(String.self, forKey: .tm_team_id)
        tm_user_id = try values.decodeIfPresent(String.self, forKey: .tm_user_id)
        tm_added_on = try values.decodeIfPresent(String.self, forKey: .tm_added_on)
        u_name = try values.decodeIfPresent(String.self, forKey: .u_name)
        u_lastname = try values.decodeIfPresent(String.self, forKey: .u_lastname)
        u_profile_image = try values.decodeIfPresent(String.self, forKey: .u_profile_image)
    }
    
}
