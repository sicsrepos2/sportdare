//  Sportdare
//
//  Created by Binoy T on 24/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct Roles_Athlete_Base : Codable {
	let status : Bool?
	let message : String?
	let sportCategories : [SportCategory]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case sportCategories = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		sportCategories = try values.decodeIfPresent([SportCategory].self, forKey: .sportCategories)
	}

}

struct SportCategory : Codable {
    let id : String?
    let name : String?
    let sub_category : [SubSport_category]?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case sub_category = "sub_category"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        sub_category = try values.decodeIfPresent([SubSport_category].self, forKey: .sub_category)
    }
    
}


struct SubSport_category : Codable {
    let id : String?
    let name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
}
