 

import Foundation
struct Comment_Base : Codable {
	let status : Bool?
	let message : Bool?
	let comments : [Comment]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case comments = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(Bool.self, forKey: .message)
		comments = try values.decodeIfPresent([Comment].self, forKey: .comments)
	}

}


struct Comment : Codable {
    let comment_id : String?
    let type : String?
    let comment : String?
    let comment_time : String?
    let commented_by : Commented_by?
    
    enum CodingKeys: String, CodingKey {
        
        case comment_id = "comment_id"
        case type = "type"
        case comment = "comment"
        case comment_time = "comment_time"
        case commented_by = "commented_by"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        comment_id = try values.decodeIfPresent(String.self, forKey: .comment_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        comment = try values.decodeIfPresent(String.self, forKey: .comment)
        comment_time = try values.decodeIfPresent(String.self, forKey: .comment_time)
        commented_by = try values.decodeIfPresent(Commented_by.self, forKey: .commented_by)
    }
    
}
struct Commented_by : Codable {
    let user_id : String?
    let first_name : String?
    let surname : String?
    let image : String?
    let gambits : String?
    let sheild : Sheild?
    
    enum CodingKeys: String, CodingKey {
        
        case user_id = "user_id"
        case first_name = "first_name"
        case surname = "surname"
        case image = "image"
        case gambits = "gambits"
        case sheild = "sheild"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        surname = try values.decodeIfPresent(String.self, forKey: .surname)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
        sheild = try values.decodeIfPresent(Sheild.self, forKey: .sheild)
    }
    
}

