//  Sportdare
//
//  Created by Binoy T on 24/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct Awards_Base : Codable {
	let status : Bool?
	let message : String?
	let awards : [Award]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case awards = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		awards = try values.decodeIfPresent([Award].self, forKey: .awards)
	}

}
struct Award : Codable {
    let id : String?
    let name : String?
    let date : String?
    let added_on : String?
    let description : String?
    let image : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case date = "date"
        case added_on = "added_on"
        case description = "description"
        case image = "image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        added_on = try values.decodeIfPresent(String.self, forKey: .added_on)
        description = try values.decodeIfPresent(String.self, forKey: .description)
         image = try values.decodeIfPresent(String.self, forKey: .image)
    }
    
}
