//
//  SearchUserBase.swift
//  Sportdare
//
//  Created by Binoy T on 09/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//
import Foundation
struct SearchUserBase : Codable {
	let status : Bool?
	let message : String?
	let list : [SearchUserDetail]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case list = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		list = try values.decodeIfPresent([SearchUserDetail].self, forKey: .list)
	}

}
class SearchUserDetail : Codable {
    let user_id : String?
    let first_name : String?
    let surname : String?
    let image : String?
    let gambits : String?
    var rooting : String?
    var sheild : Sheild?
    
    enum CodingKeys: String, CodingKey {
        
        case user_id = "user_id"
        case first_name = "first_name"
        case surname = "surname"
        case image = "image"
        case gambits = "gambits"
        case rooting = "rooting"
        case sheild = "sheild"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        surname = try values.decodeIfPresent(String.self, forKey: .surname)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
        rooting = try values.decodeIfPresent(String.self, forKey: .rooting)
        sheild = try values.decodeIfPresent(Sheild.self, forKey: .sheild)
    }
    
}
