


import Foundation
struct DareData : Codable {
	let d_id : String?
	let d_title : String?
	let d_created_by : UserData?
	let d_created_for : UserData?
	let d_type : String?
	let d_other : String?
	let d_location : String?
	let d_description : String?
	let d_gambits : String?
	let d_is_gambits : String?
	let d_date : String?
	let d_time : String?
	let d_privacy : String?
	var d_satus : String?
	let sc_name : String?
	let ct_name : String?
    let dare_files : [Dare_files]?

	enum CodingKeys: String, CodingKey {

		case d_id = "d_id"
		case d_title = "d_title"
		case d_created_by = "d_created_by"
		case d_created_for = "d_created_for"
		case d_type = "d_type"
		case d_other = "d_other"
		case d_location = "d_location"
		case d_description = "d_description"
		case d_gambits = "d_gambits"
		case d_is_gambits = "d_is_gambits"
		case d_date = "d_date"
		case d_time = "d_time"
		case d_privacy = "d_privacy"
		case d_satus = "d_satus"
		case sc_name = "sc_name"
		case ct_name = "ct_name"
        case dare_files = "dare_files"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		d_id = try values.decodeIfPresent(String.self, forKey: .d_id)
		d_title = try values.decodeIfPresent(String.self, forKey: .d_title)
		d_created_by = try values.decodeIfPresent(UserData.self, forKey: .d_created_by)
		d_created_for = try values.decodeIfPresent(UserData.self, forKey: .d_created_for)
		d_type = try values.decodeIfPresent(String.self, forKey: .d_type)
		d_other = try values.decodeIfPresent(String.self, forKey: .d_other)
		d_location = try values.decodeIfPresent(String.self, forKey: .d_location)
		d_description = try values.decodeIfPresent(String.self, forKey: .d_description)
		d_gambits = try values.decodeIfPresent(String.self, forKey: .d_gambits)
		d_is_gambits = try values.decodeIfPresent(String.self, forKey: .d_is_gambits)
		d_date = try values.decodeIfPresent(String.self, forKey: .d_date)
		d_time = try values.decodeIfPresent(String.self, forKey: .d_time)
		d_privacy = try values.decodeIfPresent(String.self, forKey: .d_privacy)
		d_satus = try values.decodeIfPresent(String.self, forKey: .d_satus)
		sc_name = try values.decodeIfPresent(String.self, forKey: .sc_name)
		ct_name = try values.decodeIfPresent(String.self, forKey: .ct_name)
        dare_files = try values.decodeIfPresent([Dare_files].self, forKey: .dare_files)
	}

}
