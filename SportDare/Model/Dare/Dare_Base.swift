

import Foundation
struct Dare_Base : Codable {
	let status : Bool?
	let message : String?
	let dare : [UserDare]?

	enum CodingKeys: String,CodingKey {

		case status = "status"
		case message = "message"
		case dare = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
        dare = try values.decodeIfPresent([UserDare].self, forKey: .dare)
	}

}
struct UserDare : Codable {
    let dareDate : String?
    let dareData : [DareData]?
    
    enum CodingKeys: String, CodingKey {
        
        case dareDate = "dareDate"
        case dareData = "dareData"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dareDate = try values.decodeIfPresent(String.self, forKey: .dareDate)
        dareData = try values.decodeIfPresent([DareData].self, forKey: .dareData)
    }
    
}
