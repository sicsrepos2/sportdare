 

import Foundation
struct Dare_files : Codable {
	let df_dare_id : String?
	let df_file_name : String?
	let df_file_type : String?

	enum CodingKeys: String, CodingKey {

		case df_dare_id = "df_dare_id"
		case df_file_name = "df_file_name"
		case df_file_type = "df_file_type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		df_dare_id = try values.decodeIfPresent(String.self, forKey: .df_dare_id)
		df_file_name = try values.decodeIfPresent(String.self, forKey: .df_file_name)
		df_file_type = try values.decodeIfPresent(String.self, forKey: .df_file_type)
	}

}
