

import Foundation
struct ChatList_Base : Codable {
	let status : Bool?
	let message : String?
	let chatList : [ChatList]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case chatList = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		chatList = try values.decodeIfPresent([ChatList].self, forKey: .chatList)
	}

}
struct ChatList : Codable {
    let user_id : String?
    let first_name : String?
    let surname : String?
    let image : String?
    let gambits : String?
    let sheild : Sheild?
    
    enum CodingKeys: String, CodingKey {
        
        case user_id = "user_id"
        case first_name = "first_name"
        case surname = "surname"
        case image = "image"
        case gambits = "gambits"
        case sheild = "sheild"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        surname = try values.decodeIfPresent(String.self, forKey: .surname)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
        sheild = try values.decodeIfPresent(Sheild.self, forKey: .sheild)
    }
    
}
