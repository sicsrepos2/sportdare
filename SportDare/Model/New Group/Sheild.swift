

import Foundation
struct Sheild : Codable {
	let shield_id : String?
	let name : String?
	let fans : Int?
    let levels : Int?
	let gambits : String?
    let shield_stars : String?

	enum CodingKeys: String, CodingKey {

		case shield_id = "shield_id"
		case name = "name"
		case fans = "fans"
        case levels = "levels"
		case gambits = "gambits"
        case shield_stars = "shield_stars"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		shield_id = try values.decodeIfPresent(String.self, forKey: .shield_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		fans = try values.decodeIfPresent(Int.self, forKey: .fans)
        levels = try values.decodeIfPresent(Int.self, forKey: .levels)
		gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
        shield_stars = try values.decodeIfPresent(String.self, forKey: .shield_stars)
	}

}
