//  Sportdare
//
//  Created by Binoy T on 29/01/19.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct Athlete_levels : Codable {
	
    let als_id : String?
    let als_name : String?
    let als_subname : String?
    let als_status : String?

    
    enum CodingKeys: String, CodingKey {
        
        case als_id = "als_id"
        case als_name = "als_name"
        case als_subname = "als_subname"
        case als_status = "als_status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        als_id = try values.decodeIfPresent(String.self, forKey: .als_id)
        als_name = try values.decodeIfPresent(String.self, forKey: .als_name)
        als_subname = try values.decodeIfPresent(String.self, forKey: .als_subname)
        als_status = try values.decodeIfPresent(String.self, forKey: .als_status)
    }


}
