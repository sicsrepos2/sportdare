
//  Sportdare
//
//  Created by Binoy T on 29/01/19.
//  Copyright © 2018 Binoy T. All rights reserved.
//
import Foundation
struct Privacy_levels : Codable {
	let id : String?
	let name : String?
	let description : String?
	let icon : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case description = "description"
		case icon = "icon"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		icon = try values.decodeIfPresent(String.self, forKey: .icon)
	}

}
