
//  Sportdare
//
//  Created by Binoy T on 29/01/19.
//  Copyright © 2018 Binoy T. All rights reserved.
//
import Foundation
struct Other_dare_types : Codable {
	let id : String?
	let name : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}

}
