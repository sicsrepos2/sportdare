
//
//  Misscellaneous_Base.swift
//  Sportdare
//
//  Created by Binoy T on 29/01/19.
//  Copyright © 2018 Binoy T. All rights reserved.
//
import Foundation
struct Misscellaneous_Base : Codable {
	let status : Bool?
	let message : String?
	let sports_category : [Sports_category]?
	let event_levels : [Event_levels]?
	let privacy_levels : [Privacy_levels]?
	let challenge_types : [Challenge_types]?
	let other_dare_types : [Other_dare_types]?
    let athlete_levels : [Athlete_levels]?
        let business_types : [Business_types]?
    
	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case sports_category = "sports_category"
		case event_levels = "event_levels"
		case privacy_levels = "privacy_levels"
		case challenge_types = "challenge_types"
		case other_dare_types = "other_dare_types"
         case athlete_levels = "athlete_levels"
        case business_types = "business_types"
      
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		sports_category = try values.decodeIfPresent([Sports_category].self, forKey: .sports_category)
		event_levels = try values.decodeIfPresent([Event_levels].self, forKey: .event_levels)
		privacy_levels = try values.decodeIfPresent([Privacy_levels].self, forKey: .privacy_levels)
		challenge_types = try values.decodeIfPresent([Challenge_types].self, forKey: .challenge_types)
		other_dare_types = try values.decodeIfPresent([Other_dare_types].self, forKey: .other_dare_types)
        athlete_levels = try values.decodeIfPresent([Athlete_levels].self, forKey: .athlete_levels)
        business_types = try values.decodeIfPresent([Business_types].self, forKey: .business_types)
	}

}
