//  Sportdare
//
//  Created by Binoy T on 29/01/19.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct Event_levels : Codable {
	let el_id : String?
	let el_leven_name : String?

	enum CodingKeys: String, CodingKey {

		case el_id = "el_id"
		case el_leven_name = "el_leven_name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		el_id = try values.decodeIfPresent(String.self, forKey: .el_id)
		el_leven_name = try values.decodeIfPresent(String.self, forKey: .el_leven_name)
	}

}
