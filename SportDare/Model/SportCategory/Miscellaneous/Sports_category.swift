//  Sportdare
//
//  Created by Binoy T on 29/01/19.
//  Copyright © 2018 Binoy T. All rights reserved.
//
import Foundation
struct Sports_category : Codable {
	let id : String?
	let name : String?
	let sub_category : [Sub_category]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case sub_category = "sub_category"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		sub_category = try values.decodeIfPresent([Sub_category].self, forKey: .sub_category)
	}

}
