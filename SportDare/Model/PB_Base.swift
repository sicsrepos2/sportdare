//  Sportdare
//
//  Created by Binoy T on 24/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct PB_Base : Codable {
	let status : Bool?
	let message : String?
	let personalBest : [PersonalBest]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case personalBest = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		personalBest = try values.decodeIfPresent([PersonalBest].self, forKey: .personalBest)
	}

}
struct PersonalBest : Codable {
    let id : String?
    let name : String?
    let description : String?
    let added_on : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case description = "description"
        case added_on = "added_on"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        added_on = try values.decodeIfPresent(String.self, forKey: .added_on)
    }
    
}
