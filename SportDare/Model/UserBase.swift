//
//  ModalBase.swift
//  Sportdare
//
//  Created by Binoy T on 24/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
struct UserBase :Codable {
    static var currentUser:UserBase? = nil
    let status : Bool?
    let message : String?
    var details : UserDetails?
    var social_data : [Social_data]?
    var auth_token : String?
    var selectedPlayer : PlayerDetails?
    var invitationUrl : URL?
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case details = "data"
        case social_data = "social_data"
        case auth_token = "auth_token"
    }
    
     init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        details = try values.decodeIfPresent(UserDetails.self, forKey: .details)
        social_data = try values.decodeIfPresent([Social_data].self, forKey: .social_data)
        auth_token = try values.decodeIfPresent(String.self, forKey: .auth_token)
        
    }
   
    // MARK: NSCoding
  
    func encode(with aCoder: NSCoder) {
        aCoder.encode(status, forKey: CodingKeys.status.rawValue)
        aCoder.encode(message, forKey: CodingKeys.message.rawValue)
        aCoder.encode(details, forKey: CodingKeys.details.rawValue)
        aCoder.encode(social_data, forKey: CodingKeys.social_data.rawValue)
        aCoder.encode(auth_token, forKey: CodingKeys.auth_token.rawValue)
    }
    
}

struct UserDetails : Codable {
    let userid : String?
    let name : String?
    let surName : String?
    let gender : String?
    let dOB : String?
    let email : String?
    let guardian : String?
    let location : String?
    let role : String?
    let role_name : String?
    let gambits : String?
    let level : String?
    let moto : String?
    let goal : String?
    let goal_description : String?
    let account : String?
    let account_id : String?
    var image : String?
    let sheild : Sheild?
    let rank : String?
    
    enum CodingKeys: String, CodingKey {
        
        case userid = "userid"
        case name = "name"
        case surName = "surName"
        case gender = "gender"
        case dOB = "DOB"
        case email = "email"
        case guardian = "guardian"
        case location = "location"
        case role = "role"
        case role_name = "role_name"
        case gambits = "gambits"
        case level = "level"
        case moto = "moto"
        case goal = "goal"
        case goal_description = "goal_description"
        case account = "account"
        case account_id = "account_id"
        case image = "image"
        case sheild = "sheild"
        case rank = "rank"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userid = try values.decodeIfPresent(String.self, forKey: .userid)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        surName = try values.decodeIfPresent(String.self, forKey: .surName)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        dOB = try values.decodeIfPresent(String.self, forKey: .dOB)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        guardian = try values.decodeIfPresent(String.self, forKey: .guardian)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        role = try values.decodeIfPresent(String.self, forKey: .role)
        role_name = try values.decodeIfPresent(String.self, forKey: .role_name)
        gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
        level = try values.decodeIfPresent(String.self, forKey: .level)
        moto = try values.decodeIfPresent(String.self, forKey: .moto)
        goal = try values.decodeIfPresent(String.self, forKey: .goal)
        goal_description = try values.decodeIfPresent(String.self, forKey: .goal_description)
        account = try values.decodeIfPresent(String.self, forKey: .account)
        account_id = try values.decodeIfPresent(String.self, forKey: .account_id)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        sheild = try values.decodeIfPresent(Sheild.self, forKey: .sheild)
        rank = try values.decodeIfPresent(String.self, forKey: .rank)
    }

    
}
struct Social_data : Codable {
    let provider : String?
    let image_url : String?
    let profile_url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case provider = "provider"
        case image_url = "image_url"
        case profile_url = "profile_url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        profile_url = try values.decodeIfPresent(String.self, forKey: .profile_url)
    }
    
}

