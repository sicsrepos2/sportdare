//  Sportdare
//
//  Created by Binoy T on 24/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//
import Foundation
struct PlayerBase : Codable {
	let status : Bool?
	let message : String?
	let data : PlayerDetails?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(PlayerDetails.self, forKey: .data)
	}

}
struct PlayerDetails : Codable {
    let userid : String?
    let name : String?
    let surName : String?
    let gender : String?
    let dOB : String?
    let email : String?
    let guardian : String?
    let location : String?
    let role : String?
    let role_name : String?
    let gambits : String?
    let level : String?
    let moto : String?
    let account : String?
    let image : String?
    let sheild : Sheild?
    
    enum CodingKeys: String, CodingKey {
        
        case userid = "userid"
        case name = "name"
        case surName = "surName"
        case gender = "gender"
        case dOB = "DOB"
        case email = "email"
        case guardian = "guardian"
        case location = "location"
        case role = "role"
        case role_name = "role_name"
        case gambits = "gambits"
        case level = "level"
        case moto = "moto"
        case account = "account"
        case image = "image"
        case sheild = "sheild"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userid = try values.decodeIfPresent(String.self, forKey: .userid)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        surName = try values.decodeIfPresent(String.self, forKey: .surName)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        dOB = try values.decodeIfPresent(String.self, forKey: .dOB)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        guardian = try values.decodeIfPresent(String.self, forKey: .guardian)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        role = try values.decodeIfPresent(String.self, forKey: .role)
        role_name = try values.decodeIfPresent(String.self, forKey: .role_name)
        gambits = try values.decodeIfPresent(String.self, forKey: .gambits)
        level = try values.decodeIfPresent(String.self, forKey: .level)
        moto = try values.decodeIfPresent(String.self, forKey: .moto)
        account = try values.decodeIfPresent(String.self, forKey: .account)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        sheild = try values.decodeIfPresent(Sheild.self, forKey: .sheild)
    }
    
}
