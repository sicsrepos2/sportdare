

import Foundation
struct Business_Base : Codable {
	let status : Bool?
	let message : String?
	let businesses : [Business]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case businesses = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		businesses = try values.decodeIfPresent([Business].self, forKey: .businesses)
	}

}


struct Business : Codable {
    let name : String?
    let details : String?
    let cat_id : String?
    let image : String?
    let cat_name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case name = "name"
        case details = "details"
        case cat_id = "cat_id"
        case image = "image"
        case cat_name = "cat_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        details = try values.decodeIfPresent(String.self, forKey: .details)
        cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        cat_name = try values.decodeIfPresent(String.self, forKey: .cat_name)
    }
    
}
