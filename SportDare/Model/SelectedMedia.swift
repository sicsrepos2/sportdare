//
//  MediaPreview.swift
//  Sportdare
//
//  Created by Binoy T on 09/11/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import Photos
import AVKit
import TLPhotoPicker
enum MediaType : String{
    case Image = "Image"
    case Video = "Video"
}
class SDMedia{
    var type : MediaType!
    var asset : PHAsset!
    var tlAsset:TLPHAsset!
    var thumbImage : UIImage!
    var description : String!
    var url : URL!
    init() {
        
    }
    init(media_type:MediaType,media_data:UIImage) {
        
        self.type = media_type
        self.description = ""
    }
    
    init(asset:TLPHAsset) {
        self.tlAsset = asset
        if asset.type == .photo
        {
            self.type =  MediaType.Image
            let options = PHImageRequestOptions()
            options.deliveryMode = .fastFormat
            options.resizeMode = .fast
            asset.tempCopyMediaFile(videoRequestOptions: nil, imageRequestOptions: options, exportPreset:AVAssetExportPresetMediumQuality , convertLivePhotosToJPG: true, progressBlock: { (progress) in
                
            }) { (imgUrl, error) in
                self.url = imgUrl
            }
        }
        else{
            self.type =  MediaType.Video
            asset.exportVideoFile { (exportUrl, option) in
                self.url = exportUrl
            }
        }
        self.asset = asset.phAsset
        self.description = ""
        PHCachingImageManager.default().requestImage(for: asset.phAsset!, targetSize: CGSize(width: 50, height: 50), contentMode: .aspectFill, options: nil) { (image, info) in
            self.thumbImage = image
        }
    }
    init(asset:PHAsset) {
        if asset.mediaType == PHAssetMediaType.image
        {
            self.type =  MediaType.Image
        }
        else
        {
            self.type =  MediaType.Video
        }
        self.asset = asset
        self.description = ""
        PHCachingImageManager.default().requestImage(for: asset, targetSize: CGSize(width: 50, height: 50), contentMode: .aspectFill, options: nil) { (image, info) in
            self.thumbImage = image
        }
       
            if asset.mediaType == PHAssetMediaType.image
            {
                self.asset.getData { (data, filename) in
                    if data!.count > 524288
                    {
                        let compressRatio = 524288.0/Double(data!.count)
                        let reducedFile = UIImage(data: data!)?.jpegData(compressionQuality: CGFloat(compressRatio))
                        self.writeData_To_Temporarypath(data: reducedFile, filename: filename, completion: { (url, error) in
                            self.url = url
                        })
                    }
                    else
                    {
                        self.writeData_To_Temporarypath(data: data, filename: filename, completion: { (url, error) in
                            self.url = url
                        })
                    }
                }
            }
           else
            {
                let options = PHVideoRequestOptions()
                options.deliveryMode = .automatic
                options.isNetworkAccessAllowed = true
                KRProgressHUD.show(withMessage: "Prepairing Your Post", completion: nil)
                PHCachingImageManager.default().requestAVAsset(forVideo: asset, options: options) { (avasset, audio, info) in
                    let asset = avasset as? AVURLAsset
                    self.exportToNewPath(urlAsset: asset, completion: { (url, error) in
                        KRProgressHUD.dismiss()
                        self.url = url
                    })
                  
                }
            
            }
        
        
    }
    func writeData_To_Temporarypath(data:Data?,filename:String,completion:(URL?,Error?)->Void)
    {
        
        let tempDirectoryURL =  URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
        
        // Create a destination URL.
        let targetURL = tempDirectoryURL.appendingPathComponent("\(filename)")
        
        // Copy the file.
        do {
            try data?.write(to: targetURL)
            completion(targetURL,nil)
        } catch let error {
            NSLog("Unable to copy file: \(error)")
            completion(nil,error)
        }
    }
    
    func exportToNewPath(urlAsset:AVURLAsset?,completion:@escaping (URL?,Error?)->Void)
    {
        
        let tempDirectoryURL =  URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
        
        // Create a destination URL.
        let targetURL = tempDirectoryURL.appendingPathComponent("\(Utilities.makeFileName()).mp4")
        guard let exportSession = AVAssetExportSession(asset: urlAsset!, presetName: AVAssetExportPresetMediumQuality) else {
         
            
            return
        }
        exportSession.outputURL = targetURL
        exportSession.outputFileType = AVFileType.mov
//        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            print(exportSession.status)
            switch exportSession.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: targetURL) else {
                    return
                }
                completion(targetURL,nil)
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                break
            }
           
        }
      
    }
  
}
    

