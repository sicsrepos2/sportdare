

import Foundation
struct Badges_Base : Codable {
	let status : Bool?
	let message : String?
	let challenge : [ChallengeBadge]?
	let gambits : [GambitsBadge]?
	let surprise : [SurpriseBadge]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case challenge = "challenge"
		case gambits = "gambits"
		case surprise = "surprise"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		challenge = try values.decodeIfPresent([ChallengeBadge].self, forKey: .challenge)
		gambits = try values.decodeIfPresent([GambitsBadge].self, forKey: .gambits)
		surprise = try values.decodeIfPresent([SurpriseBadge].self, forKey: .surprise)
	}

}

struct GambitsBadge : Codable {
    let b_id : String?
    let badge_image : String?
    let b_name : String?
    let b_type : String?
    let b_price : String?
    let is_earned : String?
    let locked_image : String?
    
    enum CodingKeys: String, CodingKey {
        
        case b_id = "b_id"
        case badge_image = "badge_image"
        case b_name = "b_name"
        case b_type = "b_type"
        case b_price = "b_price"
        case is_earned = "is_earned"
        case locked_image = "locked_image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        b_id = try values.decodeIfPresent(String.self, forKey: .b_id)
        badge_image = try values.decodeIfPresent(String.self, forKey: .badge_image)
        b_name = try values.decodeIfPresent(String.self, forKey: .b_name)
        b_type = try values.decodeIfPresent(String.self, forKey: .b_type)
        b_price = try values.decodeIfPresent(String.self, forKey: .b_price)
        is_earned = try values.decodeIfPresent(String.self, forKey: .is_earned)
        locked_image = try values.decodeIfPresent(String.self, forKey: .locked_image)
    }
    
}
struct SurpriseBadge : Codable {
    let b_id : String?
    let badge_image : String?
    let b_name : String?
    let b_type : String?
    let b_price : String?
    
    enum CodingKeys: String, CodingKey {
        
        case b_id = "b_id"
        case badge_image = "badge_image"
        case b_name = "b_name"
        case b_type = "b_type"
        case b_price = "b_price"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        b_id = try values.decodeIfPresent(String.self, forKey: .b_id)
        badge_image = try values.decodeIfPresent(String.self, forKey: .badge_image)
        b_name = try values.decodeIfPresent(String.self, forKey: .b_name)
        b_type = try values.decodeIfPresent(String.self, forKey: .b_type)
        b_price = try values.decodeIfPresent(String.self, forKey: .b_price)
    }
    
}
struct ChallengeBadge : Codable {
    let b_id : String?
    let badge_image : String?
    let b_name : String?
    let b_type : String?
    let b_price : String?
    let is_earned : String?
    let locked_image : String?
    
    enum CodingKeys: String, CodingKey {
        
        case b_id = "b_id"
        case badge_image = "badge_image"
        case b_name = "b_name"
        case b_type = "b_type"
        case b_price = "b_price"
        case is_earned = "is_earned"
        case locked_image = "locked_image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        b_id = try values.decodeIfPresent(String.self, forKey: .b_id)
        badge_image = try values.decodeIfPresent(String.self, forKey: .badge_image)
        b_name = try values.decodeIfPresent(String.self, forKey: .b_name)
        b_type = try values.decodeIfPresent(String.self, forKey: .b_type)
        b_price = try values.decodeIfPresent(String.self, forKey: .b_price)
        is_earned = try values.decodeIfPresent(String.self, forKey: .is_earned)
        locked_image = try values.decodeIfPresent(String.self, forKey: .locked_image)
    }
    
}
