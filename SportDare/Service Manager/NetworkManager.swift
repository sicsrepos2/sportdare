//
//  NetworkManager.swift
//  SportfyParent
//
//  Created by Srishti on 20/09/18.
//  Copyright © 2018 sics. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    var completionHandler : (Bool, AnyObject?, NSError?)->() = {_,_,_ in }
    static let sharedInstance = NetworkManager()
    func postMethodAlamofire(_ serviceName : String, dictionary : NSDictionary, completion : @escaping (Bool, AnyObject?, NSError?)->Void)
    {
        completionHandler = completion
        if let url = URL(string: kBaseUrl + serviceName) {
            let header = ["Content-Type":"application/x-www-form-urlencoded"]
            request(url, method: .post, parameters: dictionary as? Parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response:DataResponse<Any>) in
                switch response.result {
                case .success(let jsonData):
                    print("Success with JSON: \(jsonData)")
                    let dictionary = jsonData as! NSDictionary
                    let status:Bool = dictionary.object(forKey: "status") as! Bool
                    if(status){
                        self.getModalObject(serviceUrl: serviceName, response: response)
                    }else{
                        let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : dictionary.value(forKey: "message")! as! String])
                        let response = dictionary.value(forKey: "message")! as? String
                        
                        self.completionHandler(true,response as AnyObject,error)
                    }
                case .failure(let error): completion(false,nil,error as NSError)
                    break
                    
                }
            }
        }
    }
    
    
    func getModalObject(serviceUrl:String,response:DataResponse<Any>){
        let data = getSerializedData(response: response)
        let decoder = JSONDecoder()
        let resp = response.result.value!
        do{
            switch serviceUrl {
            case Service.kRegister:
                let responseData = try decoder.decode(UserBase.self, from: data)
                completionHandler(true,responseData as AnyObject,nil)
            case Service.kLogin :
                completionHandler(true,resp as AnyObject,nil)
        
            default:
                break
            }
        }catch let error {
                print(error)
        }
    }
    
    func getSerializedData(response:DataResponse<Any>)->Data{
        var dataNew = Data()
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: response.result.value!, options: .prettyPrinted)
            let reqJSONStr = String(data: jsonData, encoding: .utf8)
            dataNew = (reqJSONStr?.data(using: .utf8))!
        }catch let error {
            print(error)
        }
        return dataNew
    }
    
    func getMethodAlamofire(_ serviceName : String, param : String, completion : @escaping (Bool, AnyObject?, NSError?)->Void)
    {
        completionHandler = completion
        if let url = URL(string:kBaseUrl + serviceName) {
            let header:NSMutableDictionary = ["Content-Type":"application/x-www-form-urlencoded"]
//            if let currentUser = UserBase.currentUser{
////                header.setValue(currentUser.data?.authToken, forKey: "authtoken")
//            }
            request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: header as? HTTPHeaders).responseJSON { (response:DataResponse<Any>) in
                switch response.result {
                case .success(let jsonData):
                    print("Success with JSON: \(jsonData)")
                    let dictionary = jsonData as! NSDictionary
                    let status:Bool = dictionary.object(forKey: "status") as! Bool
                    if(status){
                        self.getResponse(serviceUrl: serviceName, parameter: param, response: response)
                    }else{
                        let err = dictionary.value(forKey: "error")!
                        let response = dictionary.value(forKey: "message")! as? String
                        self.completionHandler(false,response as AnyObject,err as? NSError )
                    }
                case .failure(let error): completion(false,nil,error as NSError)
                    break
                    
                }
            }
        }
    }
    
    func getResponse(serviceUrl:String,parameter: String, response:DataResponse<Any>){
        let data = getSerializedData(response: response)
        let decoder = JSONDecoder()
        let resp = (response.result.value! as AnyObject)
        do{
            switch serviceUrl {
          
            case Service.kLogin + parameter :
                let responseData = try decoder.decode(UserBase.self, from: data)
                completionHandler(true,responseData as AnyObject,nil)
         
            default:
                break
            }
        }catch{
            
        }
    }
    
    func putMethodAlamofire(_ serviceName : String, dictionary : NSDictionary,imgdata : Data,filename: String,methodType:HTTPMethod, completion : @escaping (Bool, AnyObject?, NSError?)->Void)
    {
        completionHandler = completion
        let url = URL(string: kBaseUrl + serviceName)
        let header: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded",
            "authtoken" : ""]
        upload(multipartFormData: { (multipartFormData) in
                if filename != ""
                {
                    multipartFormData.append( imgdata, withName: filename, fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
                }
                for (key, value) in dictionary {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                }
        }, to: url!, method: methodType, headers: header) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { (response:DataResponse<Any>) in
                    switch response.result {
                    case .success( _):
                       self.getModalObject(serviceUrl: serviceName, response: response)
                        break
                    case .failure( _):
                        self.completionHandler(false,response as AnyObject,nil)
                        break
                    }
                }
                
            case .failure(let error):
                debugPrint(error)
                break
            }
        }
    }
    
    func putMethodWithoutImageAlamofire(_ serviceName : String, dictionary : NSDictionary,completion : @escaping (Bool, AnyObject?, NSError?)->Void){
        completionHandler = completion
        if let url = URL(string: kBaseUrl + serviceName) {
            let header:NSMutableDictionary = ["Content-Type":"application/x-www-form-urlencoded"]
            if let currentUser = UserBase.currentUser{
//                header.setValue(currentUser.data?.authToken, forKey: "authtoken")
            }
            request(url, method: .put, parameters: dictionary as? Parameters, encoding: URLEncoding.httpBody, headers: header as? HTTPHeaders).responseJSON { (response:DataResponse<Any>) in
                switch response.result {
                case .success(let jsonData):
                    print("Success with JSON: \(jsonData)")
                    self.getModalObject(serviceUrl: serviceName, response: response)
                case .failure(let error): completion(false,nil,error as NSError)
                    self.completionHandler(false,response as AnyObject,nil)
                    break
                }
            }
        }
        
    }
}
