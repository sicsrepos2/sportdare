//
//  ServiceManager.swift
//  sparX
//
//  Created by Srishti Innovative on 11/12/17.
//  Copyright © 2017 Srishti Innovative. All rights reserved.//

import UIKit
import Alamofire

class ServiceManager: NSObject {
   static let sharedInstance = ServiceManager()
    typealias completionHandler = (_ status:Bool, _ result:DataResponse<Any>?,_ error:NSError?)->Void

    public var sessionManager: Alamofire.SessionManager // most of your web service clients will call through sessionManager
    public var backgroundSessionManager: Alamofire.SessionManager // your web services you intend to keep running when the system backgrounds your app will use this
    private override init() {
        self.sessionManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
        self.backgroundSessionManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.background(withIdentifier: "com.sics.sportdare.backgroundtransfer"))
    }
    
    //MARK: POST
    
    
    func postMethod_UploadData(_ serviceName : String,path:String,parameter:[String:Any]?,imagedata:Data?,filename:String, completion : @escaping completionHandler)
    {

        upload(multipartFormData: { (multipartFormData) in
            if imagedata != nil
            {
                multipartFormData.append(imagedata!, withName: path, fileName:filename, mimeType: "image/jpg")
            }
            for (key, value) in parameter! {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64.init(), to: "\(kBaseUrl)"+"\(serviceName)", method: .post, headers: ["Auth":(UserBase.currentUser?.auth_token)!]) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print(progress)
                })
                upload.responseJSON { response in
                    if response.error == nil{
                        completion(true, response , nil)
                    }else
                    {
                        completion(false, response , response.error! as NSError)
                    }
                }
            case .failure(let encodingError):
                completion(false, nil, encodingError as NSError)
            }
        }
    }
    
    func postMethodAlamofire_With_Multiple_Data(_ serviceName : String, with dictionary : NSDictionary,imagedatas:[String:Data]?, completion : @escaping (Bool, AnyObject?, NSError?) -> Void)
    {
        upload(multipartFormData: { (multipartFormData) in
            for (key,data) in imagedatas!
            {
                multipartFormData.append(data, withName: key , fileName:"\( Utilities.makeFileName()).png", mimeType: "image/jpeg")
               
            }
           
            for (key, value) in dictionary {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        }, to:"\(kBaseUrl)"+"\(serviceName)")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print(progress)
                })
                upload.responseJSON { response in
                    if response.error == nil{
                        completion(true, response.result.value as AnyObject , nil)
                    }else
                    {
                        completion(false, response.result.value as AnyObject, response.error! as NSError)
                    }
                }
            case .failure(let encodingError):
                completion(false, nil, encodingError as NSError)
            }
        }
        
    }
    
    
    func postMethodAlamofire2(_ serviceName : String, with dictionary : NSDictionary,imagedata:Data?,filename:String, completion : @escaping (Bool, AnyObject?, NSError?) -> Void)
    {
        upload(multipartFormData: { (multipartFormData) in
            if imagedata != nil
            {
                multipartFormData.append(imagedata!, withName: "avatar", fileName:filename, mimeType: "image/jpeg")
            }
            for (key, value) in dictionary {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        }, to:"\(kBaseUrl)"+"\(serviceName)")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print(progress)
                })
                upload.responseJSON { response in
                    if response.error == nil{
                        completion(true, response.result.value as AnyObject , nil)
                    }else
                    {
                        completion(false, response.result.value as AnyObject, response.error! as NSError)
                    }
                }
            case .failure(let encodingError):
                completion(false, nil, encodingError as NSError)
            }
        }
        
    }
    
    func postMethodAlamofire1(_ serviceName : String, with dictionary : NSDictionary,imagedata:Data?,filename:String?, completion : @escaping (Bool, AnyObject?, NSError?) -> Void)
    {
        upload(multipartFormData: { (multipartFormData) in
            if imagedata != nil
            {
                multipartFormData.append(imagedata!, withName: "profile_pic", fileName:filename!, mimeType: "image/jpeg")
            }
            for (key, value) in dictionary {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        }, to:"\(kBaseUrl)"+"\(serviceName)")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print(progress)
                })
                upload.responseString { response in
                    if response.error == nil{
                        completion(true, response.result.value as AnyObject , nil)
                    }else
                    {
                        completion(false, response.result.value as AnyObject, response.error! as NSError)
                    }
                }
            case .failure(let encodingError):
                completion(false, nil, encodingError as NSError)
            }
        }
        
    }
    
    func postMethod(_ service : String, parameter:[String:Any]? ,completion :@escaping completionHandler)
    {
        let headers = ["Content-Type":"application/x-www-form-urlencoded"]
        request("\(kBaseUrl)"+"\(service)", method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let jsonData):
                print("Success with JSON: \(jsonData)")
                let dictionary = jsonData as! NSDictionary
                let status:Bool = dictionary.object(forKey: "status") as! Bool
                if(status){
                      completion(true,response,nil)
                }else{
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : dictionary.value(forKey: "message")! as! String])
                    let response = dictionary.value(forKey: "message")! as? String
                    
                   completion(false,nil,error)
                }
                
            case .failure(let error):
                completion(false, nil, error as NSError)
            }
        }
    }
    
    func postMethod_With_Auth(_ service : String, parameter:[String:Any]? ,completion :@escaping completionHandler)
    {
                let headers = ["Content-Type":"application/x-www-form-urlencoded","auth":(UserBase.currentUser?.auth_token)!]
        request("\(kBaseUrl)"+"\(service)", method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let jsonData):
                print("Success with JSON: \(jsonData)")
                let dictionary = jsonData as! NSDictionary
                let status:Bool = dictionary.object(forKey: "status") as! Bool
                if(status){
                    completion(true,response,nil)
                }else{
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : dictionary.value(forKey: "message")! as! String])
                    let response = dictionary.value(forKey: "message")! as? String
                    
                    completion(false,nil,error)
                }
                
            case .failure(let error):
                completion(false, nil, error as NSError)
            }
        }
    }
    
    
    
    //MARK: GET
    func getMethodAlamofire(_ url : String, completion :@escaping completionHandler)
    {

        request(url).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success( _):
                completion(true, response , nil)
                
            case .failure(let error):
                completion(false, nil, error as NSError)
            }
        })
        
    }
    func getMethodAlamofire_With_Header(_ url : String,parameters:[String:Any]? ,completion :@escaping completionHandler)
    {
    
        request(url, method: .get, parameters: parameters, encoding: URLEncoding.httpBody, headers: ["auth":UserBase.currentUser!.auth_token!]).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success( _):
                completion(true, response , nil)
                
            case .failure(let error):
                completion(false, nil, error as NSError)
            }
        })
     
    }
    func deleteMethodAlamofire_With_Header(_ url : String,parameters:[String:Any]? ,completion :@escaping completionHandler)
    {
        
        request(url, method: .delete, parameters: parameters, encoding: URLEncoding.httpBody, headers: ["auth":UserBase.currentUser!.auth_token!]).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success( _):
                completion(true, response , nil)
                
            case .failure(let error):
                completion(false, nil, error as NSError)
            }
        })
        
    }
    
    func getModalObject(serviceUrl:String,response:DataResponse<Any>,completion:completionHandler){
        let data = response.data
        let decoder = JSONDecoder()
        let resp = response.result.value!
        do{
            switch serviceUrl {
            case Service.kRegister:
                let responseData = try decoder.decode(UserBase.self, from: data!)
                
            case Service.kLogin :
                completionHandler.self
            default:
                break
            }
        }catch let error {
            print(error)
        }
    }
}
