//
//  AppDelegate.swift
//  SportDare
//
//  Created by Binoy T on 13/09/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import UIKit
import CoreData
import FBSDKLoginKit
import SDWebImage
import SwiftyStoreKit
import UserNotifications
import FirebaseCore
import FirebaseDynamicLinks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
       application.applicationIconBadgeNumber = 0
       self.registerForPushNotifications(application: application)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        KRProgressHUD.set(activityIndicatorViewColors: [UIColor.appBase, UIColor.appBaseLight])
        getInitialDatas()
        checkAutoLogin()
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingUrl = userActivity.webpageURL
        {
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl) { (dynamicLink, error) in
                guard error == nil else{
                    print("error:\(error!.localizedDescription)")
                    return
                }
                self.handleIncomingDynamicUrl(dynamicLink!)
            }
            if linkHandled {
                return true
            }
            else
            {
                return false
            }
        }
        return false
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
        {
            self.handleIncomingDynamicUrl(dynamicLink)
            return true
        }
        else{
        let handled =  ApplicationDelegate.shared.application(app, open: url, options: options)
        return handled
        }
    }
    
    func handleIncomingDynamicUrl(_ dynamicUrl:DynamicLink)
    {
        guard let url = dynamicUrl.url else{
            return
        }
        print("Incoming Link Parameter :\(url.absoluteString)")
        if let referral =  URLComponents(url: url, resolvingAgainstBaseURL: true)?.queryItems?.first?.value
        {
            Constants.referralId = referral
        }
    }
    
   
    func verifySubscrtiptions()
    {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            KRProgressHUD.dismiss()
            switch result {
            case .success(let receipt):
                let productIds = Set([ InAppServices.platinum.rawValue,
                                       InAppServices.diamond.rawValue])
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                switch purchaseResult {
                case .purchased(let expiryDate, let items):
                    
                    print("\(productIds) are valid until \(expiryDate)\n\(items)\n")
                    items.last?.productId
                
                case .expired(let expiryDate, let items):
                    print("\(productIds) are expired since \(expiryDate)\n\(items)\n")
                   
                case .notPurchased:
                    print("The user has never purchased \(productIds)")
                }
            case .error(let error):
                print("Receipt verification failed: \(error)")
                KRProgressHUD.showError(withMessage: error.localizedDescription)
               
            }
        }
    }
    
    func registerForPushNotifications(application:UIApplication) {
        UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
                granted, error in
                guard granted else{
                    return
                }
                DispatchQueue.main.async(execute: {
                 application.registerForRemoteNotifications()
                })
                // 3
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        Constants.deviceTokenString = token
        print("APNs device token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "SportDare")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
    
    
    //    MARK:- User Handlers
    func checkAutoLogin()
    {
        if let userData = UserDefaults.standard.value(forKey: AppKeys.UserDetail.rawValue) as? Data
        {
            let decoder = JSONDecoder()
            let decoded = try? decoder.decode(UserBase.self, from: userData)
            UserBase.currentUser = decoded
            self.setRootView()
        }
        else if let isFirstlogin = UserDefaults.standard.value(forKey: AppKeys.IsFirstLogin.rawValue) as? Bool
        {
            
        }
        else
        {
            UserDefaults.standard.set(true, forKey: AppKeys.IsFirstLogin.rawValue)
            return
        }
        
    }
    
    func setRootView()
    {
        let baseVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDTabBarViewController") as! SDTabBarViewController
        window?.rootViewController?.dismiss(animated: false, completion: nil)
        UIView.transition(with: window!, duration: 0.3, options: .transitionFlipFromLeft, animations: {
            
            self.window?.rootViewController = baseVC
        }, completion: { completed in
            // maybe do something here
        })
       
    }
    func logOut_User()
    {
        let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDBaseNavigationController") as! SDBaseNavigationController
        window?.rootViewController?.dismiss(animated: false, completion: nil)
        UIView.transition(with: window!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = mainVC
        }, completion: { completed in
            UserDefaults.standard.removeObject(forKey: AppKeys.UserDetail.rawValue)
             UserDefaults.standard.removeObject(forKey: AppKeys.DefaultLists.rawValue)
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk()
            
        })
        
    }
    //    MARK:- Initial Datas
    
    func getInitialDatas()
    {
        if let defaults = UserDefaults.standard.value(forKey: AppKeys.DefaultLists.rawValue) as? Data
        {
            let decoder = JSONDecoder()
            let decoded = try? decoder.decode(Misscellaneous_Base.self, from: defaults)
            Constants.setDefaults(response: decoded!)
            ApiService.getSportCategories { (status, result) in
                if !status
                {
                    self.getInitialDatas()
                }
                
            }
        }
        else
        {
            ApiService.getSportCategories { (status, result) in
                if !status
                {
                    self.getInitialDatas()
                }
            }
        }
        
    }
}

