//
//  UserApiService.swift
//  Sportdare
//
//  Created by Binoy T on 24/10/18.
//  Copyright © 2018 Binoy T. All rights reserved.
//

import Foundation
import Alamofire
import Photos

class ApiService{
    static let sharedInstance = ApiService()
    var isTaskRunning = Bool()
    var progressHandler: ((_ isRunning:Bool,_ progress:Progress?,_ error:Error?) -> Void)?
     typealias completionHandler = (_ status:Bool, _ result:AnyObject?,_ error:NSError?)->Void
     typealias progressClosure = (_ status:Progress)->Void
    private  init ()
    {
        
    }
    
//    Register
    
    static func socialLogin_withDetails(param:[String:Any]?,completion:@escaping completionHandler)
    {
        
        let decoder = JSONDecoder()
        ServiceManager.sharedInstance.postMethod(Service.kSocialLogin, parameter: param) { (status, result, error) in
            if status
            {
                let data = result?.data
                do{
                    let responseData = try decoder.decode(UserBase.self, from: data!)
                    UserBase.currentUser = responseData
                    let encoder = JSONEncoder()
                    let jsonData = try encoder.encode( UserBase.currentUser)
                    UserDefaults.standard.set(jsonData, forKey: AppKeys.UserDetail.rawValue)
                    completion(true,responseData as AnyObject,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error as NSError)
                }
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func registerUser_withDetails(param:[String:Any]?,completion:@escaping completionHandler)
    {

         let decoder = JSONDecoder()
        ServiceManager.sharedInstance.postMethod(Service.kRegister, parameter: param) { (status, result, error) in
              if status
              {
                let data = result?.data
                do{
                    let responseData = try decoder.decode(UserBase.self, from: data!)
                    UserBase.currentUser = responseData
                  
                    completion(true,responseData as AnyObject,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error as NSError)
                }
            }
            else
              {
                completion(status,nil,error)
            }
            
        }
    }
    
    static func login_User_With(param:[String:Any]?,completion:@escaping completionHandler)
    {
        let decoder = JSONDecoder()
        ServiceManager.sharedInstance.postMethod(Service.kLogin, parameter: param) { (status, result, error) in
            if status
            {
                let data = result?.data
                do{
                    let responseData = try decoder.decode(UserBase.self, from: data!)
                    UserBase.currentUser = responseData
                    let encoder = JSONEncoder()
                    let jsonData = try encoder.encode( UserBase.currentUser)
                    UserDefaults.standard.set(jsonData, forKey: AppKeys.UserDetail.rawValue)
                    completion(true,responseData as AnyObject,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error as NSError)
                }
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func forgot_Password(param:[String:Any]?,completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod(Service.kForgotPassword, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
                
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    
    
    static func upload_ProfileImage(image:Data?,completion:@escaping completionHandler)
    {
        let decoder = JSONDecoder()
        ServiceManager.sharedInstance.postMethod_UploadData(Service.kProfileImage, path: Service.kPathProfile, parameter: ["userid":"\(UserBase.currentUser!.details!.userid!)"], imagedata: image, filename: "\(Utilities.makeFileName()).jpg") { (status, result, error) in
            if status
            {
                
                let data = result?.data
                do{
                    let responseData = try decoder.decode(UserBase.self, from: data!)
                    UserBase.currentUser?.details?.image = responseData.details?.image ?? ""
                    let encoder = JSONEncoder()
                    let jsonData = try encoder.encode( UserBase.currentUser)
                    UserDefaults.standard.set(jsonData, forKey: AppKeys.UserDetail.rawValue)
                    completion(true,responseData as AnyObject,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error as NSError)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func update_Profile(image:Data?,parameter:[String:Any]?,completion:@escaping completionHandler)
    {
        let decoder = JSONDecoder()
        
        ServiceManager.sharedInstance.postMethod_UploadData(Service.kEditProfile, path: Service.kPathProfile, parameter: parameter, imagedata: image, filename: "\(Utilities.makeFileName()).jpg") { (status, result, error) in
            if status
            {
                
                let data = result?.data
                do{
                    
                    let responseData = try decoder.decode(UserBase.self, from: data!)
                    UserBase.currentUser = responseData
                    let encoder = JSONEncoder()
                    let jsonData = try encoder.encode( UserBase.currentUser)
                    UserDefaults.standard.set(jsonData, forKey: AppKeys.UserDetail.rawValue)
                    completion(true,responseData as AnyObject,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error as NSError)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func upgradePlan(parameter:[String:Any]?,completion:@escaping completionHandler)
    {
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kUpgradeSubscription, parameter: parameter) { (status, response, error) in
            if status{
                completion(status,response as AnyObject?,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
        
    }
    static func getCurrentPlan(completion:@escaping completionHandler)
    {
        let url = kBaseUrl + "/subscription/check/\(UserBase.currentUser!.details!.userid!)"
        ServiceManager.sharedInstance.getMethodAlamofire(url) { (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Guardian.self, from: data!)
                    
                    completion(true,responseData.details as AnyObject?,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error as NSError)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
        
    }
    //    MARK:- Search
//
    static func searchGuardian(key:String,completion:@escaping (_ status:Bool,_ result:[GuardianDetails]?)->Void)
    {
        
        let url = kBaseUrl + "user/guardian_search/\(key)"
        ServiceManager.sharedInstance.getMethodAlamofire(url) { (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Guardian.self, from: data!)
                    
                    completion(true,responseData.details)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    
    static func searchAll(key:String,completion:@escaping (_ status:Bool,_ result:[SearchUserDetail]?)->Void)
    {
        
        let url = kBaseUrl + "roots/find/\(key)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(SearchUserBase.self, from: data!)
                    
                    completion(true,responseData.list)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
        
        
        
    }
    static func viewProfile(id:String,completion:@escaping (_ status:Bool,_ result:PlayerDetails?,_ error:Error?)->Void)
    {
       
        let url = kBaseUrl + "user/view/\(id)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(PlayerBase.self, from: data!)
                    
                    completion(true,responseData.data,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
   
    }
    static func viewFullProfile(completion:@escaping (_ status:Bool,_ result:UserBase?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "user/view_full/\(UserBase.currentUser!.details!.userid!)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(UserBase.self, from: data!)
                    UserBase.currentUser = responseData
                    let encoder = JSONEncoder()
                    let jsonData = try encoder.encode( UserBase.currentUser)
                    UserDefaults.standard.set(jsonData, forKey: AppKeys.UserDetail.rawValue)
                    completion(true,responseData,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
        
    }
    //    MARK:- ROOT
    static func root_User(param:[String:Any]?,completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kRootUser, parameter: param) { (status, result, error) in
            if status
            {
                
                
                completion(true,result?.value as AnyObject,nil)
                
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func unRoot_User(id:String,completion:@escaping completionHandler)
    {
        
        let url = kBaseUrl + "roots/unroot/\(id)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                
                
                completion(true,response?.value as AnyObject,nil)
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func getRoots(completion:@escaping (_ status:Bool,_ result:[Root]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "roots"
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Roots_Base.self, from: data!)
                    if responseData.status!
                    {
                    completion(true,responseData.roots,nil)
                    }
                    else
                    {
                      completion(false,nil,error)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func getTeams(completion:@escaping (_ status:Bool,_ result:[Team]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "team"
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url, parameters: nil) { (status, response, error) in
            if status
            {
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Team_Base.self, from: data!)
                    if responseData.status == true
                    {
                    completion(true,responseData.teams,nil)
                    }
                    else
                    {
                     completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    

    static func add_Team(image:Data?,parameter:[String:Any]?,completion:@escaping completionHandler)
    {
     
   
        ServiceManager.sharedInstance.postMethod_UploadData(Service.kAddTeam, path: Service.kFile, parameter: parameter, imagedata: image, filename: "\(Utilities.makeFileName()).jpg") { (status, result, error) in
            if status
            {
                
                let data = result?.data
                    completion(true,data as AnyObject,nil)
               
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func add_Business(image:Data?,parameter:[String:Any]?,completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod_UploadData(Service.kAddBusiness, path: "files", parameter: parameter, imagedata: image, filename: "\(Utilities.makeFileName()).jpg") { (status, result, error) in
            if status
            {
                
                let data = result?.data
                completion(true,data as AnyObject,nil)
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
//    MARK:- Post Comment
    
    static func post_Comment(comment:String,privacy:String,completion:@escaping completionHandler)
    {
        let param = ["type":"text","message":comment,"privacy":privacy]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kPostFeed, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func removePost(postId:String,completion:@escaping completionHandler)
    {
        let url = kBaseUrl + "\(Service.kRemovePost)/\(postId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in

            if status
            {
                completion(status,response as AnyObject?,error)
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    //    MARK:- Post Multiple Media
    func post_Media(files:[SDMedia],privacy:String,completion:@escaping completionHandler,startedBlock:@escaping(_ started:Bool)->Void,progressBlock:@escaping progressClosure)
     {
        let param = ["type":"media","message":"","privacy":privacy]
        
      ServiceManager.sharedInstance.backgroundSessionManager.upload(multipartFormData: { (multipartFormData) in
            for media in files{
                let url = media.url
        
                let description = media.description
                let fileName = media.type == MediaType.Image ? "\(Utilities.makeFileName()).jpg" : "\(Utilities.makeFileName()).mp4"
                let mimeType = media.type == MediaType.Image ? "image/jpg" : "video/mp4"
                multipartFormData.append(url!, withName: "files[]", fileName: fileName, mimeType: mimeType)
                multipartFormData.append((description?.data(using: String.Encoding.utf8))!, withName: "file_message[]")
               
            }
            for (key, value) in param {
                
                multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key )
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: "\(kBaseUrl)"+"\(Service.kPostFeed)", method: .post, headers: ["auth":(UserBase.currentUser?.auth_token)!,"Content-Type": "application/x-www-form-urlencoded"]) { (encodingResult) in
            switch (encodingResult) {

            case .success(let request, _,  _):
                self.isTaskRunning = true
                startedBlock(true)
    
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress: \(progress.fractionCompleted)")
                    self.progressHandler?(self.isTaskRunning, progress,nil)
                    progressBlock(progress)
                    
                })
                
                request.responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success(let jsonData):
            
                        print(jsonData)
                    
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,nil)
                    case .failure(let error):
                        print(error)
                        self.isTaskRunning = false
                          self.progressHandler?(self.isTaskRunning, nil,error)
                    }
                })
                
 
            case .failure(let error):
                print(error)
                startedBlock(false)
                self.isTaskRunning = false
                self.progressHandler?(self.isTaskRunning, nil,error)
                
            } // end encodingresul
        }
    }
    
    //    MARK:- Challenge Dare
    func make_challenge(parameters:[String:Any]?,files:[SDMedia]?,completion:@escaping completionHandler,startedBlock:@escaping(_ started:Bool)->Void,progressBlock:@escaping progressClosure)
    {
        
        
        ServiceManager.sharedInstance.backgroundSessionManager.upload(multipartFormData: { (multipartFormData) in
            if files != nil
            {
                for media in files!{
                  
                    let fileName = media.type == MediaType.Image ? "\(Utilities.makeFileName()).jpg" : "\(Utilities.makeFileName()).mp4"
                    let mimeType = media.type == MediaType.Image ? "image/jpg" : "video/mp4"
                      if let url = media.url
                      {
                        multipartFormData.append(url, withName: "files[]", fileName: fileName, mimeType: mimeType)
                    }
                
                    
                }
            }
            for (key, value) in parameters! {
                
                 multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: "\(kBaseUrl)"+"\(Service.kDareChallenge)", method: .post, headers: ["auth":(UserBase.currentUser?.auth_token)!,"Content-Type": "application/x-www-form-urlencoded"]) { (encodingResult) in
            switch (encodingResult) {
                
            case .success(let request, _,  _):
                self.isTaskRunning = true
                startedBlock(true)
                
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress: \(progress.fractionCompleted)")
                    self.progressHandler?(self.isTaskRunning, progress,nil)
                    progressBlock(progress)
                    
                })
                
                request.responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success(let jsonData):
                        
                        print(jsonData)
                        
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,nil)
                    case .failure(let error):
                        print(error)
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,error)
                    }
                })
                
                
            case .failure(let error):
                print(error)
                startedBlock(false)
                self.isTaskRunning = false
                self.progressHandler?(self.isTaskRunning, nil,error)
                
            } // end encodingresul
        }
    }
    
    
    func create_Training(parameters:[String:Any]?,files:[SDMedia]?,completion:@escaping completionHandler,startedBlock:@escaping(_ started:Bool)->Void,progressBlock:@escaping progressClosure)
    {
        
        
        ServiceManager.sharedInstance.backgroundSessionManager.upload(multipartFormData: { (multipartFormData) in
            if files != nil
            {
                for media in files!{
                   
                    let fileName = media.type == MediaType.Image ? "\(Utilities.makeFileName()).jpg" : "\(Utilities.makeFileName()).mp4"
                    let mimeType = media.type == MediaType.Image ? "image/jpg" : "video/mp4"
                    if let url = media.url
                    {
                        multipartFormData.append(url, withName: "files[]", fileName: fileName, mimeType: mimeType)
                    }
                    
                    
                }
            }
            for (key, value) in parameters! {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: "\(kBaseUrl)"+"\(Service.kDareTraining)", method: .post, headers: ["auth":(UserBase.currentUser?.auth_token)!,"Content-Type": "application/x-www-form-urlencoded"]) { (encodingResult) in
            switch (encodingResult) {
                
            case .success(let request, _,  _):
                self.isTaskRunning = true
                startedBlock(true)
                
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress: \(progress.fractionCompleted)")
                    self.progressHandler?(self.isTaskRunning, progress,nil)
                    progressBlock(progress)
                    
                })
                
                request.responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success(let jsonData):
                        
                        print(jsonData)
                        
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,nil)
                    case .failure(let error):
                        print(error)
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,error)
                    }
                })
                
                
            case .failure(let error):
                print(error)
                startedBlock(false)
                self.isTaskRunning = false
                self.progressHandler?(self.isTaskRunning, nil,error)
                
            } // end encodingresul
        }
    }
    func create_Event(parameters:[String:Any]?,files:[SDMedia]?,completion:@escaping completionHandler,startedBlock:@escaping(_ started:Bool)->Void,progressBlock:@escaping progressClosure)
    {
        
        
        ServiceManager.sharedInstance.backgroundSessionManager.upload(multipartFormData: { (multipartFormData) in
            if files != nil
            {
                for media in files!{
                    
                    let fileName = media.type == MediaType.Image ? "\(Utilities.makeFileName()).jpg" : "\(Utilities.makeFileName()).mp4"
                    let mimeType = media.type == MediaType.Image ? "image/jpg" : "video/mp4"
                    if let url = media.url
                    {
                        multipartFormData.append(url, withName: "files[]", fileName: fileName, mimeType: mimeType)
                    }
                    
                    
                }
            }
            for (key, value) in parameters! {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: "\(kBaseUrl)"+"\(Service.kDareEvent)", method: .post, headers: ["auth":(UserBase.currentUser?.auth_token)!,"Content-Type": "application/x-www-form-urlencoded"]) { (encodingResult) in
            switch (encodingResult) {
                
            case .success(let request, _,  _):
                self.isTaskRunning = true
                startedBlock(true)
                
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress: \(progress.fractionCompleted)")
                    self.progressHandler?(self.isTaskRunning, progress,nil)
                    progressBlock(progress)
                    
                })
                
                request.responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success(let jsonData):
                        
                        print(jsonData)
                        
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,nil)
                    case .failure(let error):
                        print(error)
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,error)
                    }
                })
                
                
            case .failure(let error):
                print(error)
                startedBlock(false)
                self.isTaskRunning = false
                self.progressHandler?(self.isTaskRunning, nil,error)
                
            } // end encodingresul
        }
    }
    static func acceptDare(dareId:String,completion:@escaping completionHandler)
    {
        let param = ["dare_id":dareId]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kAcceptDare, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func declineDare(dareId:String,completion:@escaping completionHandler)
    {
        let param = ["dare_id":dareId]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kDeclineDare, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func failedToComplete(dareId:String,completion:@escaping completionHandler)
    {
        let param = ["dare_id":dareId]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kFailedDare, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func approveResult(dareId:String,completion:@escaping completionHandler)
    {
        let param = ["dare_id":dareId,"message":""]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kApproveDareresult, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    func complete_Dare(files:[SDMedia],dare_id:String,message:String,completion:@escaping completionHandler,startedBlock:@escaping(_ started:Bool)->Void,progressBlock:@escaping progressClosure)
    {
        let param = ["dare_id":dare_id,"message":message]
        
        ServiceManager.sharedInstance.backgroundSessionManager.upload(multipartFormData: { (multipartFormData) in
            for media in files{
                let url = media.url
                let fileName = media.type == MediaType.Image ? "\(Utilities.makeFileName()).jpg" : "\(Utilities.makeFileName()).mp4"
                let mimeType = media.type == MediaType.Image ? "image/jpg" : "video/mp4"
                multipartFormData.append(url!, withName: "files", fileName: fileName, mimeType: mimeType)
            }
            for (key, value) in param {
                
                multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key )
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: "\(kBaseUrl)"+"\(Service.kDareComplete)", method: .post, headers: ["auth":(UserBase.currentUser?.auth_token)!,"Content-Type": "application/x-www-form-urlencoded"]) { (encodingResult) in
            switch (encodingResult) {
                
            case .success(let request, _,  _):
                self.isTaskRunning = true
                startedBlock(true)
                
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress: \(progress.fractionCompleted)")
                    self.progressHandler?(self.isTaskRunning, progress,nil)
                    progressBlock(progress)
                    
                })
                
                request.responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success(let jsonData):
                        
                        print(jsonData)
                        completion(true,jsonData as AnyObject,nil)
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,nil)
                    case .failure(let error):
                        print(error)
                        completion(false,nil,error as NSError)
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,error)
                    }
                })
                
                
            case .failure(let error):
                print(error)
                startedBlock(false)
                self.isTaskRunning = false
                self.progressHandler?(self.isTaskRunning, nil,error)
                
            } // end encodingresul
        }
    }
    
    
    static func getReply(dareId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewReply)/\(dareId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Reply_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    
    //    MARK:- Cards & Badges
    
    static func buyGambits(param:[String:Any],completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kBuyGambits, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func getMyBadges(completion:@escaping (_ status:Bool,_ result:Badges_Base?,_ error:Error?)->Void)
    {
        let url = kBaseUrl + "\(Service.kUserBadges)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Badges_Base.self, from: data!)
                    
                    
                    completion(true,responseData,nil)
                }catch let errorParse {
                    print(errorParse)
                    completion(false,nil,errorParse)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    static func getMyCards(completion:@escaping (_ status:Bool,_ result:Cards_Base?)->Void)
    {
        let url = kBaseUrl + "\(Service.kGetMyCards)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Cards_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getAvailableCards(completion:@escaping (_ status:Bool,_ result:Cards_Base?)->Void)
    {
        let url = kBaseUrl + "\(Service.kGetAvailableCards)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Cards_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func buyCard(cardId:String,completion:@escaping completionHandler)
    {
        let param = ["card_id":cardId]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kBuyNewCard, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    
    static func useCard(param:Parameters,completion:@escaping completionHandler)
    {
       
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kBuyNewCard, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    
    
       //    MARK:- ADD Sport Roles
    static func addSportCategory(categoryId:String,completion:@escaping completionHandler)
    {
        let param = ["category_id":categoryId]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kAddSportCategory, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    
    static func addSubSportCategory(subCategoryId:String,categoryId:String,completion:@escaping completionHandler)
    {
     
        let param = ["category_id":subCategoryId,"parent_category":categoryId]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kAddSubSport, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func addLevels(level:Athlete_levels,categoryId:String,completion:@escaping completionHandler)
    {
        let param = ["level":level.als_id!,"stars":level.als_id!,"role_id":categoryId]
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kAddLevel, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func addAward(param:[String:Any],image:Data?,completion:@escaping completionHandler)
    {
     
        ServiceManager.sharedInstance.postMethod_UploadData(Service.kAddAward, path: "files", parameter: param, imagedata: image, filename: "\(Utilities.makeFileName()).jpg") { (status, result, error) in
            if status
            {
                
                let data = result?.data
                completion(true,data as AnyObject,nil)
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    static func addPB(param:[String:Any],completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kAddPB, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    
    func add_Media(files:[SDMedia],role_id:String,completion:@escaping completionHandler,startedBlock:@escaping(_ started:Bool)->Void,progressBlock:@escaping progressClosure)
    {
        let param = ["role_id":role_id]
        
        ServiceManager.sharedInstance.backgroundSessionManager.upload(multipartFormData: { (multipartFormData) in
            for media in files{
                let url = media.url
                
//                let description = media.description
                let fileName = media.type == MediaType.Image ? "\(Utilities.makeFileName()).jpg" : "\(Utilities.makeFileName()).mp4"
                let mimeType = media.type == MediaType.Image ? "image/jpg" : "video/mp4"
                multipartFormData.append(url!, withName: "files[]", fileName: fileName, mimeType: mimeType)
//                multipartFormData.append((description?.data(using: String.Encoding.utf8))!, withName: "file_message[]")
                
            }
            for (key, value) in param {
                
                multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key )
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: "\(kBaseUrl)"+"\(Service.kAddMedia)", method: .post, headers: ["auth":(UserBase.currentUser?.auth_token)!,"Content-Type": "application/x-www-form-urlencoded"]) { (encodingResult) in
            switch (encodingResult) {
                
            case .success(let request, _,  _):
                self.isTaskRunning = true
                startedBlock(true)
                
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress: \(progress.fractionCompleted)")
                    self.progressHandler?(self.isTaskRunning, progress,nil)
                    progressBlock(progress)
                    
                })
                
                request.responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success(let jsonData):
                        
                        print(jsonData)
                        completion(true,jsonData as AnyObject,nil)
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,nil)
                    case .failure(let error):
                        print(error)
                        completion(false,nil,error as NSError)
                        self.isTaskRunning = false
                        self.progressHandler?(self.isTaskRunning, nil,error)
                    }
                })
                
                
            case .failure(let error):
                print(error)
                startedBlock(false)
                self.isTaskRunning = false
                self.progressHandler?(self.isTaskRunning, nil,error)
                
            } // end encodingresul
        }
    }
    
  
//    GetRoles
    static func getRolesCategories(completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        
        let url = kBaseUrl + Service.kViewSportCategory
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Roles_Athlete_Base.self, from: data!)
          
            
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    
    
    static func getLevelRatingForRole(roleId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + Service.kViewLevels
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Role_Level_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getPersonalBests(roleId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewPB)/\(roleId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(PB_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getAwards(roleId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewAwards)/\(roleId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Awards_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getMedia(roleId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kViewMedia)/\(roleId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Media_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getBusiness(id:String?,completion:@escaping (_ status:Bool,_ result:Any?,_ error:Error?)->Void)
    {
        let url = kBaseUrl + Service.kViewBusiness + (id != nil ? "/\(id!)":"")
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Business_Base.self, from: data!)
                    
                    
                    completion(true,responseData.businesses,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func removeMedia(mediaId:String,completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        let url = kBaseUrl + "\(Service.kRemoveMedia)/\(mediaId)"
        
        ServiceManager.sharedInstance.deleteMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Media_Base.self, from: data!)
                    
                    
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    
    //    MARK:- Get News Feed
    static func getNotifications(completion:@escaping (_ status:Bool,_ result:Any?,_ error:Error?)->Void)
    {
        let url = kBaseUrl + Service.kGetNotifications
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil){ (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Notifications_Base.self, from: data!)
                   
                    if  responseData.notifications != nil
                    {
                        completion(true, responseData.notifications,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
               
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    
    static func getNewsFeed(offset:Int,completion:@escaping (_ status:Bool,_ result:[Feed]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "/feed/view/10/\(offset)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Feeds_Base.self, from: data!)
                    if responseData.feed != nil
                    {
                        completion(true,responseData.feed,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    static func getCommentsFor(Id:String,completion:@escaping (_ status:Bool,_ result:[Comment]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl +  "feed/view_comment/post/\(Id)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Comment_Base.self, from: data!)
                    if responseData.comments != nil
                    {
                        completion(true,responseData.comments,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func postComment(param:[String:Any],completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kPostComment, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func postHighFiveFeed(param:[String:Any],completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kHighFiveFeed, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    static func postHighFive_Wall(param:[String:Any],completion:@escaping completionHandler)
    {
        
        ServiceManager.sharedInstance.postMethod_With_Auth(Service.kHighFive_Wall, parameter: param) { (status, result, error) in
            if status
            {
                completion(true,result?.value as AnyObject,nil)
            }
            else
            {
                completion(status,nil,error)
            }
            
        }
    }
    //   MARK:- Get Chat
    
    
    static func getChatList(completion:@escaping (_ status:Bool,_ result:[ChatList]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl +  "misc/my_chats"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(ChatList_Base.self, from: data!)
                    if responseData.chatList != nil
                    {
                        completion(true,responseData.chatList,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func getChat(recieverId:String,offset:Int,completion:@escaping (_ status:Bool,_ result:[Chat]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl +  "misc/chat/\(recieverId)/20/\(offset)/asc"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Chat_Base.self, from: data!)
                    if responseData.chats != nil
                    {
                        completion(true,responseData.chats?.reversed(),nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    
    static func post_Chat(message:String,recieverId:String,image:Data?,completion:@escaping completionHandler)
    {
        let param = ["reciver_id":recieverId,"message":message]
 
        ServiceManager.sharedInstance.postMethod_UploadData(Service.kAddChat, path: "files", parameter: param, imagedata: image, filename: "\(Utilities.makeFileName()).jpg") { (status, result, error) in
            if status
            {
                
                let data = result?.value
                completion(true,data as AnyObject,nil)
                
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
   
   
    //    MARK:- Get Miscellaneous Datas
    static func getSportCategories(completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        
        let url = kBaseUrl + "misc/all_list"
        
        ServiceManager.sharedInstance.getMethodAlamofire(url) { (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Misscellaneous_Base.self, from: data!)
                    Constants.setDefaults(response: responseData)
                    UserDefaults.standard.setValue(data, forKey: AppKeys.DefaultLists.rawValue)
                    completion(true,responseData)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    static func getPrivacies(completion:@escaping (_ status:Bool,_ result:Any?)->Void)
    {
        
        let url = kBaseUrl + "misc/privacy_levels"
        
        ServiceManager.sharedInstance.getMethodAlamofire(url) { (status, response, error) in
            if status
            {
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Guardian.self, from: data!)
                    
                    completion(true,responseData.details)
                }catch let error {
                    print(error)
                    completion(false,nil)
                }
            }
            else
            {
                completion(status,nil)
            }
        }
    }
    
    static func getMyPosts(offset:Int,completion:@escaping (_ status:Bool,_ result:[Posts]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "posts/all/10/\(offset)/DESC"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Posts_Base.self, from: data!)
                    if responseData.posts != nil
                    {
                        completion(true,responseData.posts,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    static func getDares(month:String,year:String,completion:@escaping (_ status:Bool,_ result:[UserDare]?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "\(Service.kGetDares)/\(month)/\(year)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Dare_Base.self, from: data!)
                    if responseData.dare != nil
                    {
                        completion(true,responseData.dare,nil)
                    }
                    else
                    {
                        completion(false,nil,nil)
                    }
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
    static func viewMyPost(postId:String,completion:@escaping (_ status:Bool,_ result:Posts?,_ error:Error?)->Void)
    {
        
        let url = kBaseUrl + "/posts/view/\(postId)"
        
        ServiceManager.sharedInstance.getMethodAlamofire_With_Header(url,parameters: nil) { (status, response, error) in
            if status
            {
                
                
                let data = response?.data
                do{
                    let decoder = JSONDecoder()
                    let responseData = try decoder.decode(Posts.self, from: data!)
                  completion(true,responseData,nil)
                }catch let error {
                    print(error)
                    completion(false,nil,error)
                }
            }
            else
            {
                completion(status,nil,error)
            }
        }
    }
  
    
    
}
